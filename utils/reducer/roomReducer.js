/* eslint-disable no-param-reassign */
import { createSlice } from "@reduxjs/toolkit";

// const a = {
//   adults: 1,
// };

const roomSlice = createSlice({
  name: "roomDetail",
  initialState: { value: [] },
  reducers: {
    roomSelect: (state, { payload }) => {
      state.value = [...payload];
    },
  },
});

export default roomSlice.reducer;
export const { roomSelect } = roomSlice.actions;
