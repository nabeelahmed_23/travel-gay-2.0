/* eslint-disable no-param-reassign */
import { createSlice } from "@reduxjs/toolkit";

// const a = {
//   adults: 1,
// };

const hotelSlice = createSlice({
  name: "hotelSearch",
  initialState: { value: {} },
  reducers: {
    hotelSearch: (state, { payload }) => {
      const { name, value } = payload;
      state.value[name] = value;
    },
  },
});

export default hotelSlice.reducer;
export const { hotelSearch } = hotelSlice.actions;
