/* eslint-disable no-param-reassign */
import axios from "axios";

export function getOrdinal(n) {
  const ordinals = ["th", "st", "nd", "rd"];
  const v = n % 100;
  return n + (ordinals[(v - 20) % 10] || ordinals[v] || ordinals[0]);
}

export const monthArray = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
export const monthArrayCompleteName = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
export const dayArray = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
export const dayArrayFullName = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

export function getDate(date, format) {
  const tempD = new Date(date);
  const newDate = tempD.getDate();
  const month = tempD.getMonth();
  const year = tempD.getFullYear();
  const monthName = monthArray[month];
  const day = tempD.getDay();
  const dayName = dayArray[day - 1];
  const dayFullName = dayArrayFullName[day - 1];
  switch (format) {
    case "eee, mm dd": {
      return `${dayName}, ${monthName} ${newDate}`;
    }
    case "dd-mm-yyyy": {
      return `${newDate}-${month + 1}-${year}`;
    }
    case "mmm, dd yyyy": {
      return `${monthName}, ${newDate} ${year}`;
    }
    case "yyyy-mm-dd": {
      return `${year}-${month < 9 ? `0${month + 1}` : month + 1}-${
        newDate < 10 ? `0${newDate}` : newDate
      }`;
    }
    case "mmmm dd,yyyy": {
      return `${monthArrayCompleteName[month]} ${
        newDate < 10 ? `0${newDate}` : newDate
      }, ${year}`;
    }
    case "ddd mmmm, yyyy": {
      return `${getOrdinal(newDate)} ${monthArrayCompleteName[month]} ${year}`;
    }
    case "dd mmmm yyyy eee": {
      return `${getOrdinal(newDate)} ${
        monthArrayCompleteName[month]
      } ${year} ${dayFullName}`;
    }
    case "dd-mmm-yyyy": {
      return `${newDate}-${monthName}-${year}`;
    }
    case "dd mmm yyyy": {
      return `${newDate} ${monthName} ${year}`;
    }
    case "dd mmmm yyyy": {
      return `${newDate} ${monthArrayCompleteName[month]} ${year}`;
    }
    default: {
      return "Nothing matches, stupid";
    }
  }
}

export function debounce(fn, delay) {
  let timer;
  return (() => {
    clearTimeout(timer);
    timer = setTimeout(() => fn(), delay);
  })();
}

export function getDifferenceInDays(date1, date2) {
  const diffInMs = Math.abs(date2 - date1);
  return diffInMs / (1000 * 60 * 60 * 24);
}

export function getArrayFromURLParams(arr) {
  const filters = [];

  arr.forEach((f) => {
    const id = f.match(/\[([\d]+)\]/)?.[1];
    const prop = f.match(/\[([a-z]+)\]/)?.[1];
    const idx = f
      .match(/\[([\d]+)\]/g)[1]
      ?.replace("[", "")
      ?.replace("]", "");
    if (filters[id] && !idx) {
      filters[id][prop] = Number(f.match(/=([\w]+)/)[1]);
    } else if (idx) {
      const value = Number(f.match(/=([\w]+)/)[1]);
      if (!filters[id][prop]) {
        filters[id][prop] = [value];
      } else {
        const b = [...filters[id][prop]];
        b[idx] = value;
        filters[id] = {
          ...filters[id],
          children: [...b],
        };
      }
    } else {
      filters[id] = {};
      filters[id][prop] = Number(f.match(/=([\w]+)/)[1]);
    }
  });

  const x = filters?.map((item) =>
    !item?.children ? { ...item, children: [] } : item,
  );

  return x;
}

export function getQueryFromArrayObject(arr) {
  const roomsUrl = arr?.reduce((acc, curr, idx) => {
    acc += `&rooms[${idx}][adults]=${curr.adults}${
      curr.children?.length > 0
        ? curr.children?.reduce((acc, curr, i) => {
            acc += `&rooms[${idx}][children][${i}]=${curr}`;
            return acc;
          }, "")
        : ""
    }`;
    return acc;
  }, "");

  return roomsUrl;
}

export function getArrayWithNumberOfElements(n) {
  return [...Array(n).keys()];
}
export const env = process.env.NODE_ENV;

export function getAPIUrl() {
  const url =
    env === "development"
      ? process.env.NEXT_PUBLIC_LOCAL
      : process.env.NEXT_PUBLIC_UAT;

  return url;
}

export function getBaseURL() {
  const url =
    env === "development"
      ? process.env.NEXT_PUBLIC_WEBBASE_LOCAL
      : process.env.NEXT_PUBLIC_WEBBASE_UAT;

  return url;
}

export function scrollIntoView(id, offset) {
  const yOffset = offset;
  const element = document.getElementById(id);
  const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

  window.scrollTo({ top: y, behavior: "smooth" });
}

export async function captchaTokenVerification(verifyToken) {
  const res = await axios.post("/api/captcha-token-verification", {
    response: verifyToken,
  });

  return res.data.success;
}

export function extractContent(s) {
  const span = document.createElement("span");
  span.innerHTML = s;
  return span.textContent || span.innerText;
}
