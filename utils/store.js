import { configureStore } from "@reduxjs/toolkit";
import hotelReducer from "./reducer/hotelReducer";
import roomReducer from "./reducer/roomReducer";

const store = configureStore({
  reducer: {
    hotelSearch: hotelReducer,
    roomSelect: roomReducer,
  },
});

export default store;
