import axios from "axios";
import calls from "./PromiseHandler/PromiseHandler";

let editorialCancelToken;
let menuCancelToken;
let hotelCancelToken;
let eventCancelToken;
let tripCancelToken;
let suggestionsCancelToken;
let globalSearchCancelToken;
let groupTripsCancelToken;
let prideCancelToken;
export async function getMegaMenu(url) {
  if (typeof menuCancelToken !== typeof undefined) {
    menuCancelToken.cancel();
  }
  menuCancelToken = axios.CancelToken.source();

  const res = await calls(url, "get", null, null, null, menuCancelToken.token);
  return res;
}

export async function socialLogin(data) {
  try {
    const res = await calls("login/social", "post", data, null);
    return res;
  } catch (err) {
    return err;
  }
}

export async function deleteUser(id, token) {
  try {
    const res = await calls(`user/${id}`, "delete", null, {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    });
    return res;
  } catch (err) {
    return err;
  }
}

export async function homepage(url) {
  const res = await calls(url, "get", null);
  return res;
}

export async function getFeaturedHotels(url, params) {
  if (typeof hotelCancelToken !== typeof undefined) {
    hotelCancelToken.cancel();
  }
  hotelCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    hotelCancelToken.token,
  );
  return res;
}

export async function getFeaturedEditorials(url, params) {
  if (typeof editorialCancelToken !== typeof undefined) {
    editorialCancelToken.cancel();
  }
  editorialCancelToken = axios.CancelToken.source();

  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    editorialCancelToken.token,
  );
  return res;
}
export async function getEvents(url, params) {
  if (typeof eventCancelToken !== typeof undefined) {
    eventCancelToken.cancel();
  }
  eventCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    eventCancelToken.token,
  );
  return res;
}
export async function getEventsDetails(url) {
  const res = await calls(
    url,
    "get",
    null,
    {
      Accept: "application/json",
    },
    null,
  );
  return res;
}
export async function getFeaturedEvents(url, params) {
  if (typeof eventCancelToken !== typeof undefined) {
    eventCancelToken.cancel();
  }
  eventCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    eventCancelToken.token,
  );
  return res;
}
export async function getFeaturedTrips(url, params) {
  if (typeof tripCancelToken !== typeof undefined) {
    tripCancelToken.cancel();
  }
  tripCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    tripCancelToken.token,
  );
  return res;
}

export async function getHotelListingBySearch(url, params) {
  const res = await calls(
    url,
    "get",
    null,
    {
      Accept: "application/json",
    },
    params,
  );
  return res;
}
export async function getGlobalSearch(url, params) {
  const res = await calls(url, "get", null, null, params);
  return res;
}

export async function registration(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    {
      "Content-Type": "application/json",
    },
    null,
    null,
  );
  return res;
}
export async function login(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    {
      "Content-Type": "application/json",
    },
    null,
    null,
  );
  return res;
}

export async function tokenSignin(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    {
      "Content-Type": "application/json",
    },
    null,
    null,
  );
  return res;
}

export async function getHotelDetails(url) {
  const res = await calls(
    url,
    "get",
    null,
    {
      Accept: "application/json",
    },
    null,
  );
  return res;
}
export async function getCountries(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}
export async function getCities(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}

export async function getSingleCity(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}
export async function getLocationSuggestions(url, params) {
  if (typeof suggestionsCancelToken !== typeof undefined) {
    suggestionsCancelToken.cancel();
  }
  suggestionsCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    suggestionsCancelToken.token,
  );
  return res;
}
export async function getGlobalSerachSuggestions(url) {
  if (typeof globalSearchCancelToken !== typeof undefined) {
    globalSearchCancelToken.cancel();
  }
  globalSearchCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    null,
    globalSearchCancelToken.token,
  );
  return res;
}

export async function sendPaymentToken(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    { Accept: "application/json" },
    null,
  );
  return res;
}

export async function preBooking(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    {
      Accept: "application/json",
    },
    null,
  );
  return res;
}

export async function getPodcasts(url, params) {
  const res = await calls(url, "get", null, null, params);
  return res;
}
export async function abandonedCartCall(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    { Accept: "application/json" },
    null,
  );
  return res;
}
export async function getGroupTrips(url, data, params) {
  if (typeof groupTripsCancelToken !== typeof undefined) {
    groupTripsCancelToken.cancel();
  }
  groupTripsCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    data,
    null,
    params,
    groupTripsCancelToken.token,
  );
  return res;
}

export async function getSingleGroupTrip(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}

export async function getContinents(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}
export async function getVenueDetail(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}
export async function getVenues(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}
export async function sendReviewComment(url, data) {
  const res = await calls(
    url,
    "post",
    data,
    {
      "Content-Type": "multipart/form-data",
      Accept: "application/json",
    },
    null,
  );
  return res;
}

export async function getPrides(url, params) {
  if (typeof prideCancelToken !== typeof undefined) {
    prideCancelToken.cancel();
  }
  prideCancelToken = axios.CancelToken.source();
  const res = await calls(
    url,
    "get",
    null,
    null,
    params,
    prideCancelToken.token,
  );
  return res;
}

export async function getFilters(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}

export async function getStaticPages(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}

export async function getSingleStaticPage(url) {
  const res = await calls(url, "get", null, null, null);
  return res;
}

// path,
// method,
// data,
// headers,
// params,
// cancelTokenProduct,
