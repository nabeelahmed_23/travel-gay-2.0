import axios from "axios";
import { getAPIUrl } from "../../Helper";

const calls = async (
  path,
  method,
  data,
  headers,
  params,
  cancelTokenProduct,
) => {
  const URL = getAPIUrl();
  const response = await axios({
    method,
    url: `${URL}${path}`,
    data,
    headers,
    params,
    cancelToken: cancelTokenProduct,
  });

  return response;
};

export default calls;
