import Link from "next/link";
import { useState } from "react";
import ImageFallback from "../ImageWithFallBack/ImageWithFallBack";

const overleyStyle = {
  background:
    "linear-gradient(0.68deg, #D64D78 0.59%, rgba(213, 77, 120, 0.6) 56.19%, rgba(0, 0, 0, 0) 99.43%)",
  position: "absolute",
  left: 0,
  right: 0,
  height: "50%",
  bottom: 0,
};

const EventCard = function ({ slug, image, title, description }) {
  const [showMoreDescription, setShowMoreDescription] = useState(false);
  const handleShowMore = (e) => {
    e.stopPropagation();
    setShowMoreDescription(!showMoreDescription);
    console.log("image--->>", image);
    console.log("show more here");
  };

  return (
    <div>
      <div className="shadow-[0px_0px_13.3165px_rgba(0,0,0,0.2)] h-full flex flex-col justify-between ">
        <Link legacyBehavior href={`/events/${slug}`}>
          <a>
            <div className="relative">
              <div className="w-full h-40">
                <ImageFallback src={image} alt={title} layout="fill" />
              </div>
              <div style={overleyStyle} className="flex justify-end">
                <button
                  className="bottom-2 right-2 absolute text-[13px] md:text-md text-dark py-2 px-4 font-semibold capitalize bg-white rounded-md"
                  type="button"
                >
                  view Event
                </button>
              </div>
            </div>
            <div className="p-2">
              <h4 className="text-sm md:text-[15px] mt-1 font-semibold text-primary">
                {title}
              </h4>
              <p className="mt-1 text-[11px]">
                {Array.isArray(description)
                  ? description?.map((item) => item.start_date)
                  : description}
              </p>
              {/* ? description?.map((item) => item.start_date) */}
            </div>
          </a>
        </Link>
        <button
          type="button"
          onClick={handleShowMore}
          className="p-3 ml-auto block text-[11px] text-primary font-semibold"
        >
          show {!showMoreDescription ? "more" : "less"}
        </button>
      </div>
    </div>
  );
};

export default EventCard;
