import Link from "next/link";
import PropTypes from "prop-types";
import { getDate } from "../../utils/Helper";
import ImageFallback from "../ImageWithFallBack/ImageWithFallBack";

export default function PrideCard({ pride }) {
  return (
    <div className="rounded-md overflow-hidden shadow-[0_0_18px_rgba(0,0,0,0.2)]">
      <Link href={`/events/${pride?.slug}`}>
        <div className="relative h-40">
          <ImageFallback
            src={pride?.image?.original_url}
            alt={pride?.name}
            layout="fill"
            objectFit="cover"
          />
          <div className="bg-linear-gradient h-4/5 absolute inset-x-0 bottom-0 flex items-end p-3">
            <h4 className="text-sm font-semibold text-white h-10">
              {pride?.name}
            </h4>
          </div>
        </div>
        <div className="p-3 text-xs">
          <div className="">
            {pride?.sub_title && pride?.sub_title.length > 70
              ? `${pride.sub_title.substring(0, 70)}...`
              : pride?.sub_title}
            {pride?.sub_title?.length > 70 && (
              <span className="text-primary font-semibold">Read More</span>
            )}
          </div>
          <p className="text-primary font-medium mt-[10px]">
            {getDate(pride?.recuringDates?.[0]?.start_date, "dd-mmm-yyyy")} to{" "}
            {getDate(pride?.recuringDates?.[0]?.end_date, "dd-mmm-yyyy")}
          </p>
        </div>
      </Link>
    </div>
  );
}

PrideCard.propTypes = {
  pride: PropTypes.shape().isRequired,
};
