import Skeleton from "react-loading-skeleton";
import PropTypes from "prop-types";
import PrideCard from "./PrideCard";

export default function PrideListingWrapper({ isLoading, prides, error }) {
  if (isLoading) {
    return (
      <div className="grid grid-cols-3 gap-4">
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
      </div>
    );
  }

  if (error) {
    return (
      <div className="my-6">
        <p className="text-sm text-red-500">{error}</p>
      </div>
    );
  }

  return (
    <div className="grid md:grid-cols-3 gap-4">
      {prides?.length > 0 ? (
        prides?.map((item) => (
          <PrideCard pride={item} key={`prideListingResult${item.id}`} />
        ))
      ) : (
        <p className="text-red-500 text-sm m-4">No data found</p>
      )}
    </div>
  );
}

PrideListingWrapper.defaultProps = {
  error: null,
};

PrideListingWrapper.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  prides: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  error: PropTypes.string,
};
