/* eslint-disable react/no-unescaped-entities */
export default function HaveWeGotSomething() {
  return (
    <div>
      <section className=" lg:-mt-4  mx-auto">
        <div className="h-full gap-6  w-11/12 max-w-[1311px] mx-auto px-2 py-4 md:py-2 relative grid grid-cols-1 lg:grid-cols-3 lg:py-10 rounded-l lg:min-h-[500px]">
          <div className="flex justify-center relative mt-5">
            <img
              src="images/havewegot.png"
              alt=""
              className="md:mx-auto lg:h-full object-cover rounded lg:rounded-r"
            />
          </div>
          <div className="lg:mt-10">
            <div>
              <h2 className="text-primary font-bold text-[20px] md:whitespace-nowrap md:text-[28px] capitalize">
                Have we got something wrong?
              </h2>
              <p className="text-sm mt-3">
                Are we missing a new venue or has a business closed? Or has
                something changed and we've not yet updated our pages?
              </p>
            </div>
            <h2 className="text-primary font-bold text-md capitalize mt-2">
              Let us know
              <a className="inline-flex">
                <img
                  src="/icons/Arrow7.svg"
                  className="ml-2 w-2 md:w-3"
                  alt="arrow"
                />
              </a>
            </h2>
            <div className="mt-5">
              <h2 className="text-dark font-semibold text-md capitalize">
                Best for nightlife
              </h2>
              <p className="text-sm md:text-[13px] mt-3">
                <span className="font-bold">
                  London, Paris, Berlin, Madrid, Toronto
                </span>{" "}
                and <span className="font-bold">New York</span> offer world
                class gay scenes with countless bars, saunas and clubs to choose
                from.
              </p>
            </div>
            <div className="mt-5">
              <h2 className="text-dark font-semibold text-md capitalize">
                Best for the beach
              </h2>
              <p className="text-sm md:text-[13px] mt-3">
                Sun, sea and men. Hot favourites include
                <span className="font-bold">
                  {" "}
                  Ibiza, Mykonos, Sitges, Gran Canaria, Miami,
                </span>{" "}
                but don't forget <span className="font-bold">Tenerife</span>,
                Key West, Fort Lauderdale or Nice. All offer fabulous beaches,
                great nightlife and fabulous hotels.
              </p>
            </div>
            <div className="flex my-4 gap-6 ">
              <button
                type="button"
                className="bg-[#743D7D] 2xl:px-4 py-3 rounded text-white"
              >
                <span className="px-2 font-semibold text-sm 2xl:text-base">
                  Add a Business
                </span>
              </button>
              <button
                type="button"
                className="bg-[#743D7D] 2xl:px-4 py-3 rounded text-white  "
              >
                <span className="px-2 font-semibold text-sm 2xl:text-base">
                  Add an Event
                </span>
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
