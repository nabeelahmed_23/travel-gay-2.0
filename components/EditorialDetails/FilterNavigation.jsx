import { useEffect, useState } from "react";
import NavButtons from "../Shared/internalNavigation/NavButtons";

export default function InternalNavigation() {
  const [length, setLength] = useState();
  const nav = [
    { id: 1, name: "Luxury Hotels" },
    { id: 2, name: "Mid-Range Hotels" },
    { id: 3, name: "Articles " },
    { id: 4, name: "Blogs" },
    { id: 5, name: "News" },
    { id: 6, name: "Entertainment " },
    { id: 7, name: "Interviews" },
    { id: 8, name: "Features        " },
    { id: 9, name: "Entertainment" },
    { id: 10, name: "Bars" },
    { id: 11, name: "Dance Clubs" },
  ];

  useEffect(() => {
    setLength(window.innerWidth <= 1023 ? 7 : nav.length);
  }, []);

  return (
    <div className="flex flex-wrap gap-2 items-center p-4 md:px-0">
      {nav.slice(0, length).map((item, i) => (
        <NavButtons navItem={item} key={i} />
      ))}
      <button
        onClick={() => (length <= 8 ? setLength(nav.length) : setLength(8))}
        type="button"
        className="text-primary underline text-xs lg:text-sm"
      >
        Show {length <= 10 ? "More" : "Less"}
      </button>
    </div>
  );
}
