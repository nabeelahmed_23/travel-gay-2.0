import { IconContext } from "react-icons";
import { BsArrowRight } from "react-icons/bs";

const arrowIcon = { className: "text-white w-6 h-6" };
export default function PopularPlace() {
  return (
    <div className="w-full md:max-w-[720px] lg:max-w-[428px]">
      <div className="bg-[#D0436E] grid grid-cols-[1fr_136px] rounded-tl-md rounded-tr-md overflow-hidden">
        <div className=" py-3 pl-2  pr-8 xs:p-4">
          <h3 className="text-white font-semibold text-lg sm:text-xl mb-1">
            Gay Thailand
          </h3>
          <p className="text-xs sm:text-sm text-white">
            Thailand - One of the world most Welcoming LGBTQ Destinations.
          </p>
        </div>
        <div className="relative">
          <button
            type="button"
            className="shadow-[0_0_15px_rgba(0,0,0,0.25)] p-2 rounded absolute bg-[#D0436E] top-[50%] -translate-y-[50%] -translate-x-[50%]"
          >
            <IconContext.Provider value={arrowIcon}>
              <BsArrowRight />
            </IconContext.Provider>
          </button>
          <img
            src="/homepage/popular-place.jpg"
            alt=""
            className="p-[2px] h-full object-cover"
          />
        </div>
      </div>
    </div>
  );
}
