import { useState, useEffect, useRef } from "react";
import Link from "next/link";
import axios from "axios";
import PartySlider from "../Shared/partySlider/PartySlider";
import { getFeaturedEvents } from "../../utils/services/ApiCalls";

export default function PartyComponent() {
  const [events, setEvents] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setErrors] = useState("");
  const reff = useRef();
  // const parties = [
  //   {
  //     src: "/homepage/party1.jpg",
  //     label: "Firebird tell us love story. Story of bar See more about next...",
  //     date: "Friday, May 27",
  //   },
  //   {
  //     src: "/homepage/party2.jpg",
  //     label: "Firebird tell us love story. Story of bar See more about next...",
  //     date: "Friday, May 27",
  //   },
  // ];

  async function getEvents() {
    setIsLoading(true);
    try {
      const res = await getFeaturedEvents("events", {
        feature: "is_home_feature",
      });
      setEvents(res.data.data);
      setIsLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setErrors("");
        return;
      }
      setErrors(error.message);
      setIsLoading(false);
    }
  }
  useEffect(() => {
    getEvents();
  }, []);
  return (
    <div className="md:px-0">
      <div className="flex items-center justify-between">
        <h1 className="font-bold text-primary text-xl xl:text-[28px] pl-4 md:pl-0">
          Parties & Events
        </h1>
        <Link legacyBehavior href="/events">
          <a className="text-[#666666] pr-4 md:pr-0 lg:pr-4 underline ">
            View All
          </a>
        </Link>
      </div>
      <div>
        <div className="mt-6 md:mt-2 xl:mt-5 lg:-ml-4 trip-slider">
          <PartySlider
            data={events}
            error={error}
            isLoading={isLoading}
            loc="home"
            reff={reff}
          />
        </div>
      </div>
    </div>
  );
}
