import axios from "axios";
import Link from "next/link";
import { useEffect, useState } from "react";
import { getFeaturedTrips } from "../../utils/services/ApiCalls";
import TripSlider from "../Shared/tripSlider/TripSlider";

export default function GroupTripsComponent() {
  const [trips, setTrips] = useState([]);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  // const trips = [
  //   {
  //     label: "Sydney to Dubai",
  //     src: "/homepage/trip1.jpg",
  //     date: "3-Jul-2022",
  //     price: "4500",
  //   },
  //   {
  //     label: "Sydney to Dubai",
  //     src: "/homepage/trip2.jpg",
  //     date: "3-Jul-2022",
  //     price: "4500",
  //   },
  //   {
  //     label: "Sydney to Dubai",
  //     src: "/homepage/trip3.jpg",
  //     date: "3-Jul-2022",
  //     price: "4500",
  //   },
  //   {
  //     label: "Sydney to Dubai",
  //     src: "/homepage/trip4.jpg",
  //     date: "3-Jul-2022",
  //     price: "4500",
  //   },
  // ];

  async function getTrips() {
    setIsLoading(true);
    try {
      const res = await getFeaturedTrips("grouptrips", {
        feature: "is_home_feature",
      });
      setTrips(res.data.data);
      setIsLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setError(error.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getTrips();
  }, []);
  return (
    <div className="my-8 lg:my-0">
      <div className="flex items-center justify-between">
        <h1 className="font-bold text-primary text-xl xl:text-[28px] pl-4 md:pl-0">
          Gay Group Trips
        </h1>
        <Link legacyBehavior href="/gay-group">
          <a className="text-[#666666] underline pr-4 md:pr-0 "> View All </a>
        </Link>
      </div>
      <div className="mt-2 xl:mt-5 lg:-mx-4 lg:-mr-4 trip-slider">
        <TripSlider
          data={trips}
          error={error}
          isLoading={isLoading}
          loc="home"
        />
      </div>
    </div>
  );
}
