import PartyComponent from "./PartyComponent";
import GroupTripsComponent from "./GroupTripsComponent";

export default function PartiesEvents() {
  return (
    <section className="bg-secondary py-6 lg:py-12 md:mt-8 2xl:mt-20 overflow-x-hidden sm:overflow-x-visible">
      <div className="theme-container">
        <div className="grid lg:grid-cols-2 gap-y-6 gap-x-4">
          <PartyComponent />
          <GroupTripsComponent />
        </div>
      </div>
    </section>
  );
}
