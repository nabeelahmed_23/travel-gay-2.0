import { useRef, useState, useMemo } from "react";
import { IconContext } from "react-icons";
import { AiOutlinePlus, AiOutlineMinus } from "react-icons/ai";
import PropTypes from "prop-types";

export default function FaqsComponent({ faqs }) {
  return (
    <div className="border-t border-[#d64d78] border-opacity-50 mt-4">
      {faqs?.map((faq) => (
        <FaqSingle faq={faq} key={faq.q} />
      ))}
    </div>
  );
}

FaqsComponent.propTypes = {
  faqs: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.string)).isRequired,
};

export function FaqSingle({ faq }) {
  const [active, setActive] = useState(false);
  const ref = useRef(null);
  const iconClass = useMemo(() => ({ className: "text-primary" }), []);
  const { q, a } = faq;
  return (
    <div className="border-b border-[#d64d78] border-opacity-50 py-3">
      <div className="flex justify-between">
        <h1 className="font-semibold w-max-[300px] text-sm capitalize">{q}</h1>
        <button type="button" onClick={() => setActive(!active)}>
          {active ? (
            <IconContext.Provider value={iconClass}>
              <AiOutlineMinus />
            </IconContext.Provider>
          ) : (
            <IconContext.Provider value={iconClass}>
              <AiOutlinePlus />
            </IconContext.Provider>
          )}
        </button>
      </div>
      <p
        className={
          active
            ? "mt-2 h-0 overflow-hidden text-xs transition-all"
            : "h-0 overflow-hidden text-xs transition-all"
        }
        style={active ? { height: `${ref.current.scrollHeight}px` } : {}}
        ref={ref}
      >
        {a}
      </p>
    </div>
  );
}

FaqSingle.propTypes = {
  faq: PropTypes.objectOf(PropTypes.string).isRequired,
};
