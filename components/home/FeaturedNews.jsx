import { useEffect, useState } from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import axios from "axios";
import { getFeaturedEditorials } from "../../utils/services/ApiCalls";
import NewsSlider from "../Shared/newsSlider/NewsSlider";

export default function FeaturedNews({ data }) {
  const [editorials, setEditorials] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");

  async function getEditorials() {
    setIsLoading(true);
    try {
      const res = await getFeaturedEditorials("editorials", {
        feature: "is_home_feature",
      });
      setEditorials(res.data.data);
      setIsLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setError(error.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getEditorials();
  }, []);
  return (
    <section className=" theme-container py-10 lg:py-16 xl:py-20">
      <div className="grid grid-cols-1 lg:grid-cols-[20rem_1fr] gap-4">
        <div className="px-4 md:px-0">
          <h1 className="capitalize text-xl font-bold text-primary max-w-[19.5rem] xl:text-[1.75rem] xl:leading-10 sm:max-w-none">
            {data?.editorial_title}
          </h1>
          <hr className="bg-[#D64D78] w-[7.5rem] mt-2 h-[2px] lg:h-1 xl:mt-4" />
          <p className="text-sm mt-2 xl:mt-4">{data?.editorial_content}</p>
          <Link legacyBehavior href="/editorials">
            <a className="text-primary mt-4 hidden md:inline-block underline font-semibold">
              View All Update
            </a>
          </Link>
        </div>
        <NewsSlider
          editorials={editorials}
          isLoading={isLoading}
          error={error}
          slidesPerPage={3}
        />
      </div>
    </section>
  );
}

FeaturedNews.propTypes = {
  data: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.arrayOf(
        PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.number,
          PropTypes.shape([]),
        ]),
      ),
    ]),
  ).isRequired,
};
