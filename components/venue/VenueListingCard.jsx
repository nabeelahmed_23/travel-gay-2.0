import { Splide, SplideSlide } from "@splidejs/react-splide";
import Link from "next/link";
import PropTypes from "prop-types";
import Rate from "rc-rate";

export default function VenueListingCard({ item }) {
  return (
    <Link href={`venues/${item?.slug}`}>
      <div className="p-4 border transition shadow-lg duration-300 hover:shadow-[0_0_15px_rgba(116,61,125,0.8)] rounded">
        <section className="mt-3 md:mt-6">
          <div className="hidden md:grid grid-cols-1 md:grid-cols-4 md:grid-row-2 gap-4 md:max-h-[340px] mt-6">
            {item?.media?.slice(0, 5).map((item) => (
              <div
                key={item.uuid}
                className="first:md:col-span-2 first:md:row-span-2 first:md:max-h-[330px] [&:not(:nth-child(1))]:hidden md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]"
              >
                <div className="relative h-full">
                  <img
                    src={item?.original_url}
                    alt=""
                    className=" rounded-lg w-full h-full object-cover"
                  />
                </div>
              </div>
            ))}
          </div>
          <div className="md:hidden">
            {item?.media && (
              <Splide
                options={{
                  type: "loop",
                  perPage: 1,
                  autoplay: true,
                  rewind: true,
                  arrows: false,
                  gap: "0.5rem",
                  pagination: false,
                }}
              >
                {item?.media?.map((item) => (
                  <SplideSlide key={item.uuid} className="max-h-[216px]">
                    <img
                      src={item?.original_url}
                      alt={item?.name}
                      className="rounded-lg w-full h-full object-cover"
                    />
                  </SplideSlide>
                ))}
              </Splide>
            )}
          </div>
        </section>

        <div className="mt-4 grid md:grid-cols-[1fr_166px] gap-4">
          <div>
            <div className="flex items-center gap-2">
              <h5 className="text-primary font-semibold">{item?.name}</h5>
              <Rate />
            </div>
            <div className="bg-primary text-[13px] text-primary font-medium px-3 py-1 rounded mt-2">
              Tomorrow: UK Drag every Weds, doors open 7pm- show 9pm. Happy Hour
              5-9pm, 50% off drinks
            </div>

            <div className="flex items-center gap-2 mt-3">
              <img src="/icons/location.svg" alt="Location Icon" />
              <p className="text-xs">{item?.address}</p>
              <button
                type="button"
                className="text-primary text-xs font-medium underline w-[90px]"
              >
                Show on map
              </button>
            </div>

            <div className="flex md:flex-col items-center md:items-start gap-1 md:hidden">
              {item?.tg_approved && (
                <img src="/icons/tgApproved.svg" alt="" className="w-36" />
              )}

              <div className="flex items-center gap-1 ">
                <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
                  {item?.averageRating?.averageRating ?? 0}
                </div>
                <div>
                  <h6 className="text-[10px] font-semibold ">Exceptional</h6>
                  <p className="text-[8px] ">
                    Based on {item?.averageRating?.totalNumberOfRating ?? 0}{" "}
                    votes
                  </p>
                </div>
              </div>

              {item?.latest_audience_award && (
                <div className="flex items-center gap-1">
                  {item?.latest_audience_award?.image?.original_url && (
                    <div className="w-7 h-7 flex items-center justify-center">
                      <img
                        src={item?.latest_audience_award?.image?.original_url}
                        alt=""
                      />
                    </div>
                  )}
                  <div>
                    <h6 className="text-[10px] font-semibold text-white">
                      {item?.latest_audience_award?.name}
                    </h6>
                    <p className="text-[8px] text-white">Top 100</p>
                  </div>
                </div>
              )}
            </div>

            <p className="text-xs mt-[10px]">{item?.sub_title}</p>

            <div className="mt-2 flex items-center gap-2 flex-wrap">
              <span className="font-semibold text-[13px] ">Features:</span>
              <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
                <img src="" alt="" className="h-3 w-3" />
                Garden
              </div>
              <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
                <img src="" alt="" className="h-3 w-3" />
                Pool
              </div>
              <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
                <img src="" alt="" className="h-3 w-3" />
                Massage
              </div>
              <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
                +3
              </div>
            </div>
            <div className="items-center gap-2 flex-wrap mt-8 hidden md:flex">
              <button
                type="button"
                className="text-white rounded-md py-[6px] px-3 text-[13px] font-medium btn-primary"
              >
                Post a review
              </button>
              <button
                type="button"
                className="btn-primary rounded text-white py-[6px] px-3 text-[13px] font-medium flex items-center gap-1"
              >
                Contact
                <img src="/icons/fb-white.svg" alt="" />
                <img src="/icons/instawhite.svg" alt="" />
                <img src="/icons/globe.svg" alt="" />
                <img src="/icons/phone-white.svg" alt="" />
              </button>
              <button
                type="button"
                className="text-white rounded-md py-[6px] px-3 text-[13px] font-medium btn-primary"
              >
                More Info
              </button>
            </div>
          </div>

          <div>
            <div className="md:flex-col items-center md:items-start gap-1 hidden md:flex">
              {item?.tg_approved && (
                <img src="/icons/tgApproved.svg" alt="" className="w-36" />
              )}

              <div className="flex items-center gap-1 ">
                <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
                  {item?.averageRating?.averageRating ?? 0}
                </div>
                <div>
                  <h6 className="text-[10px] font-semibold ">Exceptional</h6>
                  <p className="text-[8px] ">
                    Based on {item?.averageRating?.totalNumberOfRating ?? 0}{" "}
                    votes
                  </p>
                </div>
              </div>

              {item?.latest_audience_award && (
                <div className="flex items-center gap-1">
                  {item?.latest_audience_award?.image?.original_url && (
                    <div className="w-7 h-7 flex items-center justify-center">
                      <img
                        src={item?.latest_audience_award?.image?.original_url}
                        alt=""
                      />
                    </div>
                  )}
                  <div>
                    <h6 className="text-[10px] font-semibold text-white">
                      {item?.latest_audience_award?.name}
                    </h6>
                    <p className="text-[8px] text-white">Top 100</p>
                  </div>
                </div>
              )}
            </div>

            <div className="mt-4 ">
              <div className="flex items-center md:block gap-2">
                {item?.venue_scheduler?.map(
                  (item, i) =>
                    item?.open_time && (
                      <p
                        className={`${
                          i === 0 ? "mt-4" : "mt-2"
                        } font-medium text-sm`}
                      >
                        <span className="text-primary">
                          {item?.name?.substring(0, 3)}:
                        </span>{" "}
                        {item?.open_time}-{item?.close_time}
                      </p>
                    ),
                )}
              </div>
              <button
                type="button"
                className="text-[13px] underline font-medium"
              >
                View More
              </button>
              <p className="hidden md:flex text-end text-xs mt-4">
                Updated at: 01-Nov-2022
              </p>
            </div>

            <div className="items-center gap-2 flex-wrap mt-8 flex md:hidden">
              <button
                type="button"
                className="text-white rounded-md py-[6px] px-3 text-[13px] font-medium btn-primary"
              >
                Post a review
              </button>
              <button
                type="button"
                className="btn-primary rounded text-white py-[6px] px-3 text-[13px] font-medium flex items-center gap-1"
              >
                Contact
                <img src="/icons/fb-white.svg" alt="" />
                <img src="/icons/instawhite.svg" alt="" />
                <img src="/icons/globe.svg" alt="" />
                <img src="/icons/phone-white.svg" alt="" />
              </button>
              <button
                type="button"
                className="text-white rounded-md py-[6px] px-3 text-[13px] font-medium btn-primary"
              >
                More Info
              </button>
            </div>
            <p className="md:hidden text-end text-xs mt-4">
              Updated at: 01-Nov-2022
            </p>
          </div>
        </div>
      </div>
    </Link>
  );
}

VenueListingCard.propTypes = {
  item: PropTypes.shape().isRequired,
};
