import PropTypes from "prop-types";
import Loader from "../Shared/Loader";
import VenueListingCard from "./VenueListingCard";

export default function VenueListingWrapper({ isLoading, error, venues }) {
  if (isLoading) {
    return <Loader text="Please wait while we fetch the results." />;
  }
  if (error) {
    return <p className="text-red-500 text-sm my-4">{error}</p>;
  }
  return (
    <div className="flex flex-col gap-8">
      {venues?.length > 0 ? (
        venues?.map((item, i) => (
          <VenueListingCard item={item} key={`VenueListingCard${i}`} />
        ))
      ) : (
        <p className="text-red-500 text-sm my-4">No data found.</p>
      )}
    </div>
  );
}

VenueListingWrapper.defaultProps = {
  error: null,
};

VenueListingWrapper.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  venues: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};
