import HotelSlider from "../Shared/hotelSlider/HotelSlider";
import TripSlider from "../Shared/tripSlider/TripSlider";

export default function AsideSearchHotel({ heading, data, trips, hotels }) {
  return (
    <div className="my-8">
      <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
        {heading}
      </h2>
      {trips && <TripSlider data={data} sidebar />}
      {hotels && (
        <HotelSlider noDataMessage="No Data Found" data={[]} loc="sidebar" />
      )}
    </div>
  );
}
