import Link from "next/link";

export default function AsideLinks() {
  return (
    <div className="px-4 md:px-0 mb-2 mt-6">
      <h2 className="text-primary text-lg xl:text-xl font-bold">
        More About Us
      </h2>
      <div className="">
        <ul>
          <li className="flex items-center justify-between cursor-pointer py-3 border-b border-[#D64D7880] text-primary">
            <p className="text-primary text-sm font-semibold">- Contact Us</p>
            <Link legacyBehavior href="#">
              <a className="text-primary text-sm lg:text-base flex items-center gap-1 mt-4 font-medium">
                <img
                  src="/icons/Arrow7.svg"
                  className="ml-2 w-2 md:w-3"
                  alt="arrow"
                />
              </a>
            </Link>
          </li>
          <li className="flex items-center justify-between cursor-pointer py-3 border-b border-[#D64D7880] text-primary">
            <p className="text-primary text-sm font-semibold">
              - Privacy & Moderation
            </p>
            <Link legacyBehavior href="#">
              <a className="text-primary text-sm lg:text-base flex items-center gap-1 mt-4 font-medium">
                <img
                  src="/icons/Arrow7.svg"
                  className="ml-2 w-2 md:w-3"
                  alt="arrow"
                />
              </a>
            </Link>
          </li>
          <li className="flex items-center justify-between cursor-pointer py-3 border-b border-[#D64D7880] text-primary">
            <p className="text-primary text-sm font-semibold">
              - Friends & Partners
            </p>
            <Link legacyBehavior href="#">
              <a className="text-primary text-sm lg:text-base flex items-center gap-1 mt-4 font-medium">
                <img
                  src="/icons/Arrow7.svg"
                  className="ml-2 w-2 md:w-3"
                  alt="arrow"
                />
              </a>
            </Link>
          </li>
          <li className="flex items-center justify-between cursor-pointer py-3 border-b border-[#D64D7880] text-primary">
            <p className="text-primary text-sm font-semibold">- Who We Are</p>
            <Link legacyBehavior href="#">
              <a className="text-primary text-sm lg:text-base flex items-center gap-1 mt-4 font-medium">
                <img
                  src="/icons/Arrow7.svg"
                  className="ml-2 w-2 md:w-3"
                  alt="arrow"
                />
              </a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}
