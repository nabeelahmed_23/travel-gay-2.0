/* eslint-disable react/button-has-type */
import { useState } from "react";

export default function FrequentlyAsk() {
  const [messageForm, setMessageForm] = useState(false);
  const [frequentlyAsk, setFrequentlyAsk] = useState(true);
  const [successBox, setSuccessBox] = useState(false);
  const askQuestionHandler = () => {
    if (frequentlyAsk) {
      setFrequentlyAsk(false);
      setSuccessBox(false);
      setMessageForm(true);
    }
  };

  const successBoxHandler = () => {
    if (messageForm) {
      setMessageForm(false);
      setFrequentlyAsk(false);
      setSuccessBox(true);
    }
  };
  const afterSuccessHandler = () => {
    if (successBox) {
      setSuccessBox(false);
      setFrequentlyAsk(true);
    }
  };
  return (
    <div>
      {frequentlyAsk && (
        <div className="mb-[40px] mt-4 md:mt-0 bg-[#FCF4F6] px-[18px] py-[22px]">
          <h2 className="mb-[10px]  font-[600] text-[24px] text-primary">
            Most Recent Questions
          </h2>
          <ul>
            <li className="text-[14px] font-[600] cursor-pointer py-3 border-b border-[#D64D7880] text-primary">
              Gay Travel – freedom to be yourself ?
            </li>
            <li className="text-[14px] font-[600] cursor-pointer py-3 border-b border-[#D64D7880]">
              Gay Travel – freedom to be yourself
            </li>
            <li className="text-[14px] font-[600] cursor-pointer py-3 border-b border-[#D64D7880]">
              Best for nightlife
            </li>
            <li className="text-[14px] font-[600] cursor-pointer py-3 border-b border-[#D64D7880]">
              Best for the beach
            </li>
            <li className="text-[14px] font-[600] cursor-pointer py-3 border-b border-[#D64D7880]">
              Gay Group Travel
            </li>
            <li className="text-[14px] font-[600] cursor-pointer py-3 border-b border-[#D64D7880]">
              Best for nightlife
            </li>
          </ul>

          <p className="text-lg text-center font-semibold py-3 text-primary">
            Still have questions ?
          </p>
          <div className="flex justify-center">
            <button
              type="button"
              className="w-[106px] xs:w-32 sm:w-[142px] 2xl:w-[162px] bg-[#743D7D]	y 2xl:px-4 py-3 rounded text-white 2xl:gap-1 "
              onClick={askQuestionHandler}
            >
              <span className="text-sm 2xl:text-base">Ask a Question</span>
            </button>
          </div>
        </div>
      )}
      {messageForm && (
        <div>
          <div className="bg-[rgba(215,_72,_116,_0.8)] rounded-t	m-0 py-5">
            <h2 className=" md:text-[20px] font-[600] text-[18px] text-white text-center">
              Add your Answer?
            </h2>
          </div>
          <div className="mb-[40px] bg-[#FCF4F6] px-[18px] py-[22px]">
            <label
              htmlFor="name"
              className="flex flex-col mt-2 lg:mt-4 text-sm"
            >
              <span>Name</span>
              <input
                type="text"
                name="name"
                className="border border-slate-200 border-solid mt-1 rounded-lg bg-transparent"
                placeholder="james"
              />
            </label>
            <label
              htmlFor="email"
              className="flex flex-col mt-2 lg:mt-4 text-sm"
            >
              <span>Email</span>
              <input
                type="email"
                name="email"
                className="border border-slate-200 border-solid mt-1 rounded-lg bg-transparent"
                placeholder="james@gmail.com"
              />
            </label>
            <label
              htmlFor="name"
              className="flex flex-col mt-2 lg:mt-4 text-sm"
            >
              <span>Your Answer</span>
              <textarea
                type="email"
                name="email"
                className="border border-slate-200 border-solid mt-1 rounded-lg bg-transparent resize-none"
                placeholder="Enter Your Answer"
              />
            </label>
            <div className="flex justify-center my-4">
              <button
                type="button"
                className="w-full bg-[#743D7D]	y 2xl:px-4 py-3 rounded text-white 2xl:gap-1 "
                onClick={successBoxHandler}
              >
                <span className="text-sm 2xl:text-base">SEND</span>
              </button>
            </div>
          </div>
        </div>
      )}
      {successBox && (
        <div>
          <div className="bg-[rgba(215,_72,_116,_0.8)] rounded-t	m-0 py-5">
            <h2 className=" md:text-[20px] font-[600] text-[18px] text-white text-center">
              Message Sent
            </h2>
          </div>
          <div className="mb-[40px] bg-[#FCF4F6] px-[18px] py-[22px]">
            <div>
              <div className="flex justify-center gap-2">
                <img src="/icons/messagesent.svg" alt="message sent" />
              </div>
              <p className="text-lg text-center font-semibold py-3 text-primary">
                Thanks for reaching out
              </p>
              <p className="text-sm text-center font-semibold  text-[#666666]">
                Someone will get back to you soon
              </p>
              <div className="flex justify-center">
                <button
                  onClick={afterSuccessHandler}
                  type="button "
                  className="text-sm text-center font-bold py-4 text-primary cursor-pointer"
                >
                  Go back
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
