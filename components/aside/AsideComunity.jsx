import ApplePodcastLogo from "../Shared/ApplePodcastLogo";
import GooglePodcastLogo from "../Shared/GooglePodcastLogo";
import SpotifyPodcastLogo from "../Shared/SpotifyPodcastLogo";

export default function AsideComunity() {
  return (
    <div>
      <div className="flex items-center justify-between">
        <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
          Travelgay Podcast
        </h2>
        <div className="flex items-center gap-1">
          <GooglePodcastLogo />
          <SpotifyPodcastLogo />
          <ApplePodcastLogo />
        </div>
      </div>
      <iframe
        title="castBoxPlayer"
        src="https://castbox.fm/app/castbox/player/id3813372/id358919215?v=8.22.11&autoplay=0"
        frameBorder="0"
        width="100%"
        height="300"
        className="mt-4"
      />
    </div>
  );
}

// eslint-disable-next-line no-lone-blocks
{
  /* <div className="mt-2 lg:-mt-1">
              <div className="px-4 md:px-2">
                <h2 className="text-primary text-xl xl:text-2xl font-bold xl:font-semibold mb-4 xl:mb-2 ">
                  What&lsquo;s On Today
                </h2>
              </div>
              <FeaturedSlider reff={featuredRefSide} data={venues} sidebar />
            </div>
            <div className="mt-12 xl:mt-[3.35rem]">
              <div className="px-4 md:px-2">
                <h2 className="text-primary text-xl xl:text-2xl font-bold xl:font-semibold mb-4 xl:mb-3 ">
                  What&lsquo;s On Tomorrow
                </h2>
              </div>
              <FeaturedSlider reff={featuredRefSideTwo} data={venues} sidebar />
            </div> */
}
// eslint-disable-next-line no-lone-blocks
{
  /* <div className="mt-16 lg:mt-[32rem] px-4">
              <h2 className="text-primary font-semibold text-xl lg:text-2xl">
                LGBT Rights In USA
              </h2>
              <div className="pl-2 lg:pl-4">
                <div className="mt-4 border-b border-[#F8E3E8] pb-4">
                  <div className="flex justify-between items-center">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Homosexual
                    </h4>
                    <p className="text-sm">Homosexual</p>
                  </div>
                  <div className="flex justify-between items-center mt-2">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Activity
                    </h4>
                    <p className="text-sm">Since 2000</p>
                  </div>
                </div>
                <div className="mt-4 border-b border-[#F8E3E8] pb-4">
                  <div className="flex justify-between items-center">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Homosexual
                    </h4>
                    <p className="text-sm">Homosexual</p>
                  </div>
                  <div className="flex justify-between items-center mt-2">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Activity
                    </h4>
                    <p className="text-sm">Since 2000</p>
                  </div>
                </div>
                <div className="mt-4 border-b border-[#F8E3E8] pb-4">
                  <div className="flex justify-between items-center">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Homosexual
                    </h4>
                    <p className="text-sm">Homosexual</p>
                  </div>
                  <div className="flex justify-between items-center mt-2">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Activity
                    </h4>
                    <p className="text-sm">Since 2000</p>
                  </div>
                </div>
                <div className="mt-4 border-b border-[#F8E3E8] pb-4">
                  <div className="flex justify-between items-center">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Homosexual
                    </h4>
                    <p className="text-sm">Homosexual</p>
                  </div>
                  <div className="flex justify-between items-center mt-2">
                    <h4 className="font-semibold text-[#666] text-sm">
                      Activity
                    </h4>
                    <p className="text-sm">Since 2000</p>
                  </div>
                </div>
                <Link legacyBehavior href="#">
                  <a className="text-primary text-sm lg:text-base flex items-center gap-1 mt-4 font-medium">
                    Learn More
                    <IconContext.Provider value={iconClass}>
                      <BsArrowUpRight />
                    </IconContext.Provider>
                  </a>
                </Link>
              </div>
              <div className="mt-12">
                <h2 className="text-primary font-semibold text-xl lg:text-2xl">
                  Search Hotels By Area
                </h2>
                <div className="mt-8 pl-2 lg:pl-4">
                  <div className="flex items-center justify-between mt-4 border-b border-[#F8E3E8] pb-4">
                    <div>
                      <h5 className="text-primary font-semibold">
                        - All Hotels in Belfast
                      </h5>
                      <p className="text-sm">Top trending hotels in Belfast</p>
                    </div>
                    <IconContext.Provider value={iconClass}>
                      <BsArrowUpRight />
                    </IconContext.Provider>
                  </div>
                  <div className="flex items-center justify-between mt-4 border-b border-[#F8E3E8] pb-4">
                    <div>
                      <h5 className="text-primary font-semibold">
                        - All Hotels in Belfast
                      </h5>
                      <p className="text-sm">
                        Top trending hotels in Birmingham
                      </p>
                    </div>
                    <IconContext.Provider value={iconClass}>
                      <BsArrowUpRight />
                    </IconContext.Provider>
                  </div>
                  <div className="flex items-center justify-between mt-4 border-b border-[#F8E3E8] pb-4">
                    <div>
                      <h5 className="text-primary font-semibold">
                        - All Hotels in Belfast
                      </h5>
                      <p className="text-sm">Top trending hotels in Leeds</p>
                    </div>
                    <IconContext.Provider value={iconClass}>
                      <BsArrowUpRight />
                    </IconContext.Provider>
                  </div>
                </div>
              </div>
            </div> */
}
