// import Image from "next/image";
import { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Rate from "rc-rate";
import { IconContext } from "react-icons";
import { ImLocation } from "react-icons/im";
import PropTypes from "prop-types";
import queryString from "query-string";
import ImageFallback from "../ImageWithFallBack/ImageWithFallBack";

const locationIcon = { className: "fill-[#D74874]" };
export default function HotelListResult({ data, handleShowOnMap, adults }) {
  const [mounted, setMounted] = useState(false);
  const router = useRouter();

  const {
    "check-in": checkIn,
    "check-out": checkOut,
    name,
    q,
    slug,
    type,
    ...rest
  } = router.query;

  const query = queryString.stringify({
    "check-in": checkIn,
    "check-out": checkOut,
  });

  const urlQuery = Object.keys(rest).reduce((acc, curr) => {
    // eslint-disable-next-line no-param-reassign
    acc += `&${curr}=${rest[curr]}`;
    return acc;
  }, "");

  function getDifferenceInDays(date1, date2) {
    const diffInMs = Math.abs(date2 - date1);
    return diffInMs / (1000 * 60 * 60 * 24);
  }

  useEffect(() => {
    setMounted(true);
  }, []);
  return (
    <div className="grid grid-cols-1 lg:grid-cols-3 shadow-[0_0_16px_rgba(0,0,0,0.25)] relative rounded-md ">
      <div>
        <div className="relative h-[290px] -top-2/4 lg:h-[calc(100%_+_16px)] lg:-top-4 mx-4 lg:mx-0">
          {/* <Image
            src={
              data?.hotel?.image?.original_url
                ? data?.hotel?.image?.original_url
                : "/scb/listResult.jpg"
            }
            alt="Hotel Image"
            layout="fill"
            className="object-cover relative rounded-md md:rounded-md md:rounded-br-none"
            priority
          /> */}
          <ImageFallback
            src={data?.hotel?.image?.original_url}
            layout="fill"
            alt={data?.hotel?.name}
            className="object-cover"
            priority
            loading="eager"
          />
          <div className="lg:hidden absolute inset-x-0 smHotelImageStatsBg top-0 h-2/6 py-4 px-2 rounded-t-md">
            <div className="lg:hidden flex gap-1 flex-wrap xs:justify-center">
              {data?.hotel?.tg_approved && (
                <img src="/tg-approved.jpg" alt="" className="max-w-[114px]" />
              )}
              {/* <img src="/scb/stats.jpg" alt="" className="lg:-mt-2" /> */}
              <div className="flex items-center gap-1 ">
                <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
                  {data?.hotel?.averageRating?.averageRating}
                </div>
                <div>
                  <h6 className="text-[10px] font-semibold text-white">
                    Exceptional
                  </h6>
                  <p className="text-[8px] text-white">
                    Based on {data?.hotel?.averageRating?.totalNumberOfRating}{" "}
                    votes
                  </p>
                </div>
              </div>
              {data?.hotel?.latest_audience_award && (
                <div className="flex items-center gap-1">
                  {data?.hotel?.latest_audience_award?.image?.original_url && (
                    <div className="w-7 h-7 flex items-center justify-center">
                      <img
                        src={
                          data?.hotel?.latest_audience_award?.image
                            ?.original_url
                        }
                        alt=""
                      />
                    </div>
                  )}
                  <div>
                    <h6 className="text-[10px] font-semibold text-white">
                      {data?.hotel?.latest_audience_award?.name}
                    </h6>
                    <p className="text-[8px] text-white">Top 100</p>
                  </div>
                </div>
              )}
              <img
                src="/white-tg-approved.svg"
                alt=""
                className="max-w-[114px]"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="p-4 lg:pl-8 lg:col-span-2 lg:grid grid-cols-1 lg:grid-cols-[1fr_150px] -mt-36 lg:mt-0 gap-4">
        <div>
          <div className="flex flex-col md:flex-row md:items-start gap-0 lg:gap-2 justify-between">
            <h2 className="text-primary font-semibold text-xl max-w-[calc(100%_-_85px)]">
              {data?.hotel?.name}
            </h2>
            <div className="w-full md:w-[75px]">
              <Rate
                count={5}
                value={Number(data?.hotel?.star_rating) ?? 0}
                allowHalf
                disabled
              />
            </div>
          </div>
          <div className="mb-3 lg:mt-1 lg:mb-0">
            <div className="flex items-center flex-wrap gap-1">
              <p className="text-black text-xs flex item-center">
                <IconContext.Provider value={locationIcon}>
                  <ImLocation />
                </IconContext.Provider>{" "}
                {data?.hotel?.address}
              </p>
              <button
                type="button"
                className="text-primary underline text-sm"
                onClick={() =>
                  handleShowOnMap([
                    data?.hotel?.longitude,
                    data?.hotel?.latitude,
                  ])
                }
              >
                Show on Map
              </button>
            </div>
          </div>
          <div className="mt-6 hidden lg:block">
            <p className="font-bold text-black text-sm">
              {data?.rooms?.[0]?.type_name}
            </p>
            <Link legacyBehavior href="#">
              <a className="text-primary text-xs"> Free Cancelation </a>
            </Link>
          </div>
        </div>
        {/* <div className="grid grid-cols-2 items-center lg:grid-cols-1 gap-y-2"> */}
        <div className="hidden lg:flex lg:flex-col lg:items-end gap-4 flex-wrap justify-between">
          {data?.hotel?.tg_approved ? (
            <img src="/tg-approved.jpg" alt="" className="max-w-[114px]" />
          ) : (
            ""
          )}
          {/* <img src="/scb/stats.jpg" alt="" className="lg:-mt-2" /> */}
          <div className="flex items-center gap-1">
            <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
              {data?.hotel?.averageRating?.averageRating}
            </div>
            <div>
              <h6 className="text-xs font-semibold text-primary">
                Exceptional
              </h6>
              <p className="text-[10px]">
                Based on {data?.hotel?.averageRating?.totalNumberOfRating} votes
              </p>
            </div>
          </div>
          {data?.hotel?.latest_audience_award && (
            <div className="flex items-center gap-1">
              <div className="w-7 h-7 flex items-center justify-center">
                <img
                  src={data?.hotel?.latest_audience_award?.image?.original_url}
                  alt=""
                />
              </div>
              <div>
                <h6 className="text-[10px] font-semibold text-black">
                  {data?.hotel?.latest_audience_award?.name}
                </h6>
                <p className="text-[10px]">Top 100</p>
              </div>
            </div>
          )}
          <div className=" hidden lg:block">
            <p className="font-medium text-xs text-right">
              {getDifferenceInDays(new Date(checkIn), new Date(checkOut))}{" "}
              nights, {adults} adults
            </p>
            {/* <span className="font-semibold text-sm line-through">$1000</span> */}
            {data?.rooms[0]?.total && (
              <p className="font-semibold text-lg text-primary text-right">
                {data?.rooms[0]?.currency}
                {data?.rooms[0]?.total}
              </p>
            )}
          </div>
        </div>

        {/* Mobile view starts */}
        <div className="lg:hidden my-4">
          <p className="text-black text-sm my-1 font-medium">
            Resort Two Queen Room
          </p>
          <p className="font-medium text-sm text-black my-1">
            {getDifferenceInDays(new Date(checkIn), new Date(checkOut))} nights,{" "}
            {adults} adults
          </p>
          <p className="text-primary text-sm my-1">Free cancellation</p>
        </div>
        {/* Mobile view ends */}

        <div className="flex flex-col md:flex-row md:items-center md:justify-between col-span-2 mt-4 lg:mt-0 gap-y-4">
          <div className="text-xs">
            <Link legacyBehavior href="#">
              <a className="text-primary text-xs underline"> Login </a>
            </Link>{" "}
            to see cheaper rates
          </div>
          <div className="flex flex-col md:flex-row md:items-center gap-2 justify-start">
            {/* Mobile view price starts */}

            <div className="flex items-center justify-between gap-4 ">
              <p className="text-white bg-[#E39318] px-4 md:px-3 rounded-full text-xs text-center w-max">
                <span className="inline-block relative top-[2px]">
                  Get $789
                </span>
                <br />
                <span className="text-[9px] relative -top-[2px] ">
                  points Credit
                </span>
              </p>
              <div className="lg:mt-8 lg:hidden">
                {data?.rooms[0]?.total && (
                  <p className="font-semibold text-lg text-primary text-right">
                    ${data?.rooms[0]?.total}
                  </p>
                )}
              </div>
            </div>
            {/* Mobile view price ends */}
            {mounted && (
              <Link
                legacyBehavior
                href={`hotels/${data?.hotel?.slug}${encodeURI("?")}${
                  query + urlQuery
                }`}
              >
                <a className="bg-[#743D7D] rounded py-4 md:py-2 px-4 2xl:px-8 text-sm text-white text-center">
                  Check Availability
                </a>
              </Link>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

HotelListResult.propTypes = {
  data: PropTypes.shape().isRequired,
  handleShowOnMap: PropTypes.func.isRequired,
  adults: PropTypes.number.isRequired,
};
