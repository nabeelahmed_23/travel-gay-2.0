import { GrClose } from "react-icons/gr";
import { useRouter } from "next/router";
import PropTypes from "prop-types";

export default function BookingConfirmPopup({ setBookingConfirmationPopup }) {
  const router = useRouter();
  function localStorageDeleteHandler() {
    router.push("/");
    setTimeout(() => {
      if (typeof window !== "undefined") {
        localStorage.removeItem("hotel");
        localStorage.removeItem("room");
        localStorage.removeItem("routerQuery");
        localStorage.removeItem("roomInfo");
        localStorage.removeItem("prebookingRequest");
      }
    }, 1000);
  }
  return (
    <div className="fixed inset-0 backdrop-blur-sm z-30 bg-[#BCBCBCCC] flex items-center justify-center">
      <div className="m-4 lg:m-auto w-full md:max-w-[500px] xl:max-w-[1023px] relative bg-white rounded-lg">
        <button
          type="button"
          className="absolute right-4 top-4"
          onClick={() => setBookingConfirmationPopup(false)}
        >
          <GrClose />
        </button>
        <div className="p-6 md:py-12 xl:py-[90px]">
          <h1 className="font-semibold text-2xl md:text-3xl xl:text-[55px] text-center">
            Thank You!
          </h1>
          <h2 className="text-center text-primary font-semibold text-lg md:text-xl xl:text-4xl mt-6 xl:mt-16">
            Your Room has been booked with Travelgay{" "}
          </h2>
          <p className="text-center text-black mt-1 text-sm md:text-sm xl:text-xl">
            You will receive a confirmation email shortly
          </p>
          <div className="flex justify-center mt-8 xl:mt-12">
            <img src="/logo.svg" alt="" className=" w-40 xl:w-[300px]" />
          </div>
          <div className="text-center mt-6">
            <button
              className="text-xs md:text-sm xl:text-lg underline"
              onClick={localStorageDeleteHandler}
              type="button"
            >
              Go back to homepage
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

BookingConfirmPopup.propTypes = {
  setBookingConfirmationPopup: PropTypes.func.isRequired,
};
