/* eslint-disable react/no-array-index-key */
import PropTypes from "prop-types";
// import { useSelector } from "react-redux";

const temp = {
  adults: 1,
  children: [],
};
export default function GuestRoomPopup({ members, setMembers }) {
  // const hotelForm = useSelector((state) => state.hotelSearch.value);
  return (
    <>
      <div className="flex items-center justify-between ">
        <span className="rooms-title">Rooms</span>
        <div className="flex items-center gap-2">
          <button
            type="button"
            className="bg-primary h-6 w-6 rounded-full text-black rooms-minus"
            onClick={() => {
              if (members.length > 1) {
                const a = [...members];
                a.pop();
                setMembers(a);
              }
            }}
          >
            -
          </button>
          <span className="inline-block w-3 rooms-length">
            {members.length}
          </span>
          <button
            className="rounded-full bg-[#D74874] text-white h-6 w-6 rooms-add"
            onClick={() => {
              const a = [...members];
              a.push(temp);
              setMembers(a);
            }}
            type="button"
          >
            +
          </button>
        </div>
      </div>
      {members?.map((item, index) => (
        <AdultChildrenComponent
          item={item}
          index={index}
          key={`Rooms${index}`}
          setMembers={setMembers}
          members={members}
        />
      ))}
    </>
  );
}
GuestRoomPopup.propTypes = {
  members: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  setMembers: PropTypes.func.isRequired,
};

export function AdultChildrenComponent({ item, index, setMembers, members }) {
  function handleAdultsChange(p) {
    const a = [...members];
    if (a[index].adults + Number(p) > 0)
      a[index] = { ...a[index], adults: a[index].adults + Number(p) };
    setMembers(a);
  }

  function handleChildrenAge(e, idx) {
    const { value } = e.target;
    const a = [...members];
    const b = [...a[index].children];
    b[idx] = Number(value);
    a[index] = {
      ...a[index],
      children: [...b],
    };
    setMembers(a);
  }
  return (
    <div className="rounded mt-2 guestpopupwrapper">
      <div className="font-semibold roomNo">Room {index + 1}</div>
      <div className="flex items-center justify-between mt-3 adults-wrapper">
        <span className="adult-title">Adults</span>
        <div className="flex items-center gap-2">
          <button
            type="button"
            className="bg-primary h-6 w-6 rounded-full text-black rooms-minus"
            onClick={() => handleAdultsChange("-1")}
          >
            -
          </button>
          <span className="inline-block w-3 adult-length">{item?.adults}</span>
          <button
            className="rounded-full bg-[#D74874] text-white h-6 w-6 rooms-add"
            type="button"
            onClick={() => handleAdultsChange("+1")}
          >
            +
          </button>
        </div>
      </div>
      <div className="flex items-start justify-between mt-3 child-wrapper">
        <div className="child-title-wrapper">
          <span className="child-title">Children</span>
          <p className="text-[10px] child-age-text">Ages 2 to 17</p>
        </div>
        <div className="flex items-center gap-2">
          <button
            type="button"
            className="bg-primary h-6 w-6 rounded-full text-black rooms-minus"
            onClick={() => {
              if (members[index].children?.length > 0) {
                const a = [...members];
                a[index] = {
                  ...a[index],
                  children: a[index].children.slice(0, -1),
                };
                setMembers(a);
              }
            }}
          >
            -
          </button>
          <span className="inline-block w-3 child-length">
            {item.children?.length}
          </span>
          <button
            className="rounded-full bg-[#D74874] text-white h-6 w-6 rooms-add"
            type="button"
            onClick={() => {
              if (members[index].children?.length < 10) {
                const a = [...members];
                a[index] = {
                  ...a[index],
                  children: [...a[index].children, 2],
                };
                setMembers(a);
              }
            }}
          >
            +
          </button>
        </div>
      </div>
      <div className="mt-2 flex gap-2 flex-wrap">
        {item.children?.map((item, index) => (
          <select
            className="bg-[#FCF4F6] text-xs w-full 2xl:w-[calc(50%_-_8px)] rounded child-age-select"
            onChange={(e) => handleChildrenAge(e, index)}
            value={item}
            key={`childSelect${index}`}
          >
            <option hidden>Child {index} age</option>
            {[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17].map(
              (item, index) => (
                <option value={item} key={`option${index}`}>
                  {item} yrs old
                </option>
              ),
            )}
          </select>
        ))}
      </div>
    </div>
  );
}

AdultChildrenComponent.propTypes = {
  item: PropTypes.shape().isRequired,
  index: PropTypes.number.isRequired,
  setMembers: PropTypes.func.isRequired,
  members: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};
