import { useState, useEffect } from "react";
import { GrClose } from "react-icons/gr";
import { BsTrash, BsChevronRight } from "react-icons/bs";
import { useSession } from "next-auth/react";
import axios from "axios";
import { IconContext } from "react-icons";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { roomSelect } from "../../utils/reducer/roomReducer";
import { getAPIUrl, getBaseURL } from "../../utils/Helper";
import { abandonedCartCall } from "../../utils/services/ApiCalls";

const icon = { className: " w-5 h-5 fill-red-500" };

export default function CartComponent({ data }) {
  const [isOpen, setIsOpen] = useState(false);
  const [mounted, setMounted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [, setRooms] = useState(0);
  const roomsArray = useSelector((state) => state.roomSelect.value);
  const hotelForm = useSelector((state) => state.hotelSearch.value);
  const dispatch = useDispatch();
  const router = useRouter();
  const session = useSession();

  useEffect(() => {
    setMounted(true);
    const a =
      typeof window !== "undefined" &&
      JSON.parse(localStorage.getItem("roomsArray"));
    if (a) {
      dispatch(
        roomSelect(
          typeof window !== "undefined" &&
            JSON.parse(localStorage.getItem("roomsArray")),
        ),
      );
    }
  }, []);

  function handleRoomChange(item, e) {
    setRooms(Number(e.target.value));
    const a =
      typeof window !== "undefined" &&
      JSON.parse(localStorage.getItem("roomsArray"));

    const index = a.findIndex(
      (x) => x.roomDetails.booking_token === item.roomDetails.booking_token,
    );

    a[index] = { ...a[index], noOfRooms: Number(e.target.value) };
    if (typeof window !== "undefined") {
      localStorage.setItem("roomsArray", JSON.stringify(a));
      dispatch(roomSelect(a));
    }
  }

  function handleRoomDelete(item) {
    const a =
      typeof window !== "undefined" &&
      JSON.parse(localStorage.getItem("roomsArray"));

    const index = a.filter(
      (x) => x.roomDetails.booking_token !== item.roomDetails.booking_token,
    );

    if (typeof window !== "undefined") {
      localStorage.setItem("roomsArray", JSON.stringify(index));
      dispatch(roomSelect(index));
    }
  }

  async function callAbandonCart() {
    try {
      const res = await abandonedCartCall("bookings/pending", {
        hotel_id: data?.hotel?.id,
        external_id: data?.hotel?.external_id,
        booking_token: data?.booking_token,
        email: session?.data?.user?.email,
        user_id: session?.data?.user?.user_id,
        check_in: router.query["check-in"],
        check_out: router.query["check-out"],
        params: hotelForm?.rooms,
        first_name: "John",
        last_name: "Doe",
      });

      if (typeof window !== "undefined") {
        localStorage.setItem("abandonCartId", res.data.data.id);
      }
    } catch (error) {
      console.log(error);
    }
  }
  async function handleCartPrebooking() {
    const tempArr = [];
    setIsLoading(true);
    roomsArray.map((item1) => {
      for (let i = 0; i < item1?.noOfRooms; i += 1) {
        tempArr.push({
          roomDetails: item1.roomDetails,
        });
      }
      return null;
    });

    const obj = tempArr?.map((item) => {
      const obj = item?.roomDetails?.supplier?.references?.reduce(
        (a, c, i) => ({
          ...a,
          [`supplier_reference${i + 1}`]: c,
        }),
        {},
      );
      return {
        booking_token: item?.roomDetails?.booking_token,
        ...obj,
      };
    });

    axios({
      url: `${getBaseURL()}sanctum/csrf-cookie`,
    }).then(() => {
      if (session?.status === "authenticated") {
        callAbandonCart();
        setIsLoading(false);
      }
      axios({
        method: "post",
        url: `${getAPIUrl()}prebookings`,
        headers: {
          Accept: "application/json",
        },
        data: {
          booking_token: data?.booking_token,
          room_bookings: obj,
        },
        withCredentials: true,
      })
        .then((res) => {
          if (typeof window !== "undefined") {
            localStorage.setItem("roomInfo", JSON.stringify(res.data.data));
            localStorage.setItem(
              "prebookingRequest",
              JSON.stringify({
                booking_token: data?.booking_token,
                room_bookings: obj,
              }),
            );
            localStorage.setItem("hotel", JSON.stringify({ ...data?.hotel }));
            localStorage.setItem("routerQuery", JSON.stringify(router.query));
            localStorage.setItem("room", JSON.stringify(tempArr));
            localStorage.setItem("guests", JSON.stringify(hotelForm?.rooms));
          }
          setIsLoading(false);
          router.push({
            pathname: `/hotels/${router.query.slug}/booking`,
            query: {
              ...res.data.data,
              hotel_booking_token: data?.booking_token,
              room_bookings: obj,
              hotel_name: data?.hotel?.name,
              hotel_address: data?.hotel?.address,
              ...router.query,
            },
          });
        })
        .catch((err) => {
          console.log(err);
          setIsLoading(false);
        });
    });
  }

  useEffect(() => {
    const handleBrowseAway = (e) => {
      if (typeof window !== "undefined" && !e.includes("booking")) {
        console.log("Cleaning localstorage ... ");
        setTimeout(() => {
          if (typeof window !== "undefined") {
            localStorage.removeItem("roomsArray");
            dispatch(roomSelect([]));
          }
        }, 2000);
        localStorage.removeItem("abandonCartId");
      }
    };

    router.events.on("routeChangeStart", handleBrowseAway);
    return () => {
      router.events.off("routeChangeStart", handleBrowseAway);
    };
  }, []);

  if (!isOpen) {
    return (
      <div className="fixed bottom-4 right-8 z-10">
        <button
          type="button"
          className=" w-24 h-24 flex items-center justify-center bg-white shadow-[0_3px_17px_6px_rgba(215,72,116,0.15)] rounded-full relative"
          onClick={() => setIsOpen(true)}
        >
          <img src="/icons/cart.svg" alt="" className="max-w-[55px]" />
          {mounted && (
            <div className="w-7 h-7 rounded-full bg-[#D64D78] flex items-center justify-center text-white absolute top-0 right-0">
              {roomsArray?.reduce((acc, curr) => {
                // eslint-disable-next-line no-param-reassign
                acc += curr.noOfRooms;
                return acc;
              }, 0)}
            </div>
          )}
        </button>
      </div>
    );
  }
  return (
    <div className="fixed bottom-4 right-8 z-10">
      <div className="p-4 bg-white shadow-[0_3px_17px_rgba(0,0,0,0.08)] w-[455px] rounded-lg">
        <button type="button" onClick={() => setIsOpen(false)}>
          <GrClose />
        </button>
        <h1 className="text-primary text-2xl font-semibold mt-2">
          Booking Details
        </h1>

        <div className="">
          {roomsArray?.length > 0 ? (
            roomsArray?.map((item, i) => (
              <div
                className="flex items-start justify-between gap-4 border-b py-5"
                key={item?.roomDetails?.booking_token}
              >
                <div>
                  <h4 className="font-semibold">
                    {item?.roomDetails?.type_name}
                  </h4>
                  <h4 className="font-semibold text-primary mt-3">
                    {item?.roomDetails?.currency}
                    {item?.roomDetails?.total}
                  </h4>
                </div>
                <div className="flex items-center gap-2">
                  <select
                    name="rooms"
                    id="rooms"
                    className="shadow-[0_0_5px_rgba(0,0,0,0.15)] rounded-md"
                    value={roomsArray?.[i]?.noOfRooms}
                    onChange={(e) => handleRoomChange(item, e)}
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select>
                  <button type="button" onClick={() => handleRoomDelete(item)}>
                    <IconContext.Provider value={icon}>
                      <BsTrash />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>
            ))
          ) : (
            <p className="mt-2">No rooms selected.</p>
          )}

          <div className="flex items-center justify-between my-5">
            <p className="text-black text-sm font-medium">Subtotal</p>
            <p className="text-black text-sm font-medium">
              {roomsArray[0]?.roomDetails?.currency}
              {roomsArray
                ?.reduce((acc, curr) => {
                  // eslint-disable-next-line no-param-reassign
                  acc += curr.roomDetails.total * curr.noOfRooms;
                  return acc;
                }, 0)
                .toFixed(2)}
            </p>
          </div>

          <button
            type="button"
            className={`p-4 font-semibold text-white w-full rounded-lg bg-[#743D7D] flex items-center justify-between ${
              isLoading ? "opacity-70" : ""
            }`}
            onClick={handleCartPrebooking}
          >
            <span>
              {" "}
              {roomsArray[0]?.roomDetails?.currency}
              {roomsArray
                ?.reduce((acc, curr) => {
                  // eslint-disable-next-line no-param-reassign
                  acc += curr.roomDetails.total * curr.noOfRooms;
                  return acc;
                }, 0)
                .toFixed(2)}
            </span>

            <div className="flex items-center gap-4">
              {isLoading ? "Loading..." : "Confirm Booking"}
              <span>
                <BsChevronRight />
              </span>
            </div>
          </button>
        </div>
      </div>
    </div>
  );
}

CartComponent.propTypes = {
  data: PropTypes.shape().isRequired,
};
