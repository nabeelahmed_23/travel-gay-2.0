import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";
import HotelRoomCard from "./HotelRoomCard";

export default function HotelRooms({ data, rooms, isLoading, error }) {
  if (isLoading) {
    return (
      <div>
        <Skeleton className="w-full h-[361px] md:h-[373px] lg:h-[223px]" />
        <Skeleton className="w-full h-[361px] md:h-[373px] lg:h-[223px] mt-6" />
        <Skeleton className="w-full h-[361px] md:h-[373px] lg:h-[223px] mt-6" />
        <Skeleton className="w-full h-[361px] md:h-[373px] lg:h-[223px] mt-6" />
      </div>
    );
  }

  if (error) {
    return <div className="text-sm  text-red-500 m-4">{error}</div>;
  }

  return (
    <div>
      {rooms?.length > 0 ? (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-1 gap-6">
          {rooms?.map((item, idx) => (
            <HotelRoomCard
              item={item}
              key={item.type_code + item.booking_token}
              data={data}
              index={idx}
            />
          ))}
        </div>
      ) : (
        <div className="m-4 text-red-500 text-sm">No Rooms found.</div>
      )}
    </div>
  );
}

HotelRooms.defaultProps = {
  error: "",
};

HotelRooms.propTypes = {
  data: PropTypes.shape().isRequired,
  rooms: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};
