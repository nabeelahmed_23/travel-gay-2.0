import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import HotelListResult from "./HotelListResult";
import Loader from "../Shared/Loader";

export default function HotelListing({
  error,
  isLoading,
  hotelListing,
  handleShowOnMap,
}) {
  const hotelForm = useSelector((state) => state.hotelSearch.value);

  if (isLoading) {
    return <Loader text=" Please wait while we fetch the results" />;
  }

  if (error) {
    return <div className="text-sm text-red-500 my-4">{error}</div>;
  }
  return (
    <div>
      {hotelListing?.length > 0 ? (
        <div className="flex flex-col gap-y-48 lg:gap-y-12 mt-44 lg:mt-12">
          {hotelListing.map((a) => (
            <HotelListResult
              key={a.booking_token}
              data={a}
              handleShowOnMap={handleShowOnMap}
              adults={hotelForm?.rooms?.reduce((acc, curr) => {
                // eslint-disable-next-line no-param-reassign
                acc += curr.adults;
                return acc;
              }, 0)}
            />
          ))}
        </div>
      ) : (
        <div className="text-red-500 my-4">No data found</div>
      )}
    </div>
  );
}

HotelListing.defaultProps = {
  error: null,
};

HotelListing.propTypes = {
  error: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  hotelListing: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  handleShowOnMap: PropTypes.func.isRequired,
};
