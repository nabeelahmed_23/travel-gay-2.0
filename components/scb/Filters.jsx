import { startCase } from "lodash";
import PropTypes from "prop-types";
import { useState } from "react";
import CheckBox from "../Shared/CheckBox";

export default function Filters({
  featured,
  setFeatured,
  sliderValue,
  setSliderValue,
  handleRating,
  rating,
  handleInputChange,
  filterData,
  amenities,
  category,
}) {
  const [min] = useState(0);
  const [max] = useState(100);

  return (
    <div className="py-4 px-3 border mt-6 rounded-md shadow-[0_0_4px_rgba(0,0,0,0.25)] mb-4">
      <div className="py-6 px-4 border-b-2">
        <div className="flex items-center justify-between">
          <img src="/tg-approved.jpg" alt="" />
          <label htmlFor="tg_approved" className="cursor-pointer">
            <input
              type="checkbox"
              name="tg_approved"
              id="tg_approved"
              className="hidden tgApprovedInput"
              defaultChecked={featured === "tg_approved"}
              onChange={(e) => {
                const { checked } = e.target;
                if (checked) setFeatured(1);
                else setFeatured(0);
              }}
            />
            <div className="relative h-3 w-8 bg-primary rounded-full tgApprovedLabel">
              <div className="w-4 h-4 rounded-full bg-[#743D7D] shadow-md absolute -top-[2px]  transition-all tgApprovedKnob" />
            </div>
          </label>
        </div>
      </div>
      <div className="py-6 border-b-2 px-6">
        <label htmlFor="price" className="cursor-pointer flex flex-col">
          <span className="filter-label mb-4 ">Price</span>
          <div className="relative py-6">
            <div className="absolute top-0 left-1">{min}</div>
            <div className="absolute top-0 right-0">{max}</div>
            <input
              type="range"
              name="price"
              id="price"
              max={max}
              min={min}
              value={sliderValue}
              className="priceRangeInput pb-1"
              onChange={(e) => setSliderValue(e.target.value)}
              style={{
                backgroundSize: `${(sliderValue / max) * 100}% 100%`,
              }}
            />
            <div
              className={`absolute bottom-0 ${
                sliderValue === "0" ? "ml-1" : ""
              }`}
              style={{
                left: `${(sliderValue / max) * 100}%`,
                transform: `translate(-${(sliderValue / max) * 100}%)`,
              }}
            >
              {sliderValue}
            </div>
          </div>
        </label>
      </div>
      <div className="py-6 border-b-2 px-6 ">
        <span className="filter-label mb-4">Ratings</span>
        <div className="flex items-center gap-1 flex-wrap">
          <RadioBoxRating
            value={1}
            name="rating"
            onChange={(e) => handleRating(e)}
            defaultChecked={rating.includes("1")}
          />
          <RadioBoxRating
            value={2}
            name="rating"
            onChange={(e) => handleRating(e)}
            defaultChecked={rating.includes("2")}
          />
          <RadioBoxRating
            value={3}
            name="rating"
            onChange={(e) => handleRating(e)}
            defaultChecked={rating.includes("3")}
          />
          <RadioBoxRating
            value={4}
            name="rating"
            onChange={(e) => handleRating(e)}
            defaultChecked={rating.includes("4")}
          />
          <RadioBoxRating
            value={5}
            name="rating"
            onChange={(e) => handleRating(e)}
            defaultChecked={rating.includes("5")}
          />
        </div>
      </div>
      {/* <div className="py-6 border-b-2 px-6">
        <span className="filter-label mb-4 ">Meals</span>
        <CheckBox
          label="Room Only"
          name="roomOnly"
          checked={filterData?.includes("roomOnly")}
          id="roomOnly"
          onChange={(e) => handleInputChange(e)}
        />
        <CheckBox
          label="Breakfast"
          name="breakfast"
          id="breakfast"
          onChange={(e) => handleInputChange(e)}
          checked={filterData?.includes("breakfast")}
        />
        <CheckBox
          label="Halfboard"
          name="halfBoard"
          id="halfBoard"
          onChange={(e) => handleInputChange(e)}
          checked={filterData?.includes("halfBoard")}
        />
        <CheckBox
          label="FullBoard"
          name="fullBoard"
          id="fullBoard"
          onChange={(e) => handleInputChange(e)}
          checked={filterData?.includes("fullBoard")}
        />
        <CheckBox
          label="All Inclusive"
          name="allInclusive"
          id="allInclusive"
          onChange={(e) => handleInputChange(e)}
          checked={filterData?.includes("allInclusive")}
        />
      </div> */}
      {category?.length > 0 && (
        <div className="py-6 border-b-2 px-6">
          <span className="filter-label mb-4 ">Property Type</span>
          {category?.map((item, idx) => (
            <CheckBox
              key={`categoryList${idx}`}
              label={startCase(item?.name)}
              name="category"
              id={item?.value}
              onChange={handleInputChange}
              checked={filterData?.category?.includes(item?.value)}
            />
          ))}
        </div>
      )}
      {amenities?.length > 0 && (
        <div className="py-6 border-b-2 px-6">
          <span className="filter-label mb-4 ">Hotel Amenitites</span>
          {amenities?.map((item, idx) => (
            <CheckBox
              key={`amenitiesList${idx}`}
              label={startCase(item?.name)}
              name="amenities"
              id={item?.value}
              onChange={handleInputChange}
              checked={filterData?.amenities?.includes(item?.value)}
            />
          ))}
        </div>
      )}
    </div>
  );
}

Filters.propTypes = {
  featured: PropTypes.string.isRequired,
  sliderValue: PropTypes.string.isRequired,
  setSliderValue: PropTypes.func.isRequired,
  handleRating: PropTypes.func.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  setFeatured: PropTypes.func.isRequired,
  rating: PropTypes.arrayOf(PropTypes.string).isRequired,
  filterData: PropTypes.shape().isRequired,
  category: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  amenities: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export function RadioBoxRating({ value, name, defaultChecked, onChange }) {
  return (
    <label htmlFor={name + value} className="cursor-pointer">
      <input
        type="checkbox"
        name="rating"
        id={name + value}
        className="hidden ratingInput"
        value={value}
        onChange={onChange}
        checked={defaultChecked}
      />
      <div className="flex items-center gap-[2px] bg-primary p-2 ratingLabel">
        <span className="font-semibold h-5">{value}</span>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="13"
          height="13"
          viewBox="0 0 13 13"
          fill="none"
        >
          <path
            d="M6.35446 0.756767L7.79592 5.1931L12.4606 5.1931L8.68678 7.93491L10.1282 12.3712L6.35446 9.62943L2.58069 12.3712L4.02215 7.93491L0.248376 5.1931L4.91301 5.1931L6.35446 0.756767Z"
            fill="#DDC700"
          />
        </svg>
      </div>
    </label>
  );
}

RadioBoxRating.propTypes = {
  value: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  defaultChecked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};
