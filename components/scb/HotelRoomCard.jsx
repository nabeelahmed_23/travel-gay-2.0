import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import { getArrayFromURLParams, getDifferenceInDays } from "../../utils/Helper";
import { roomSelect } from "../../utils/reducer/roomReducer";
// import { preBooking } from "../../utils/services/ApiCalls";

export default function HotelRoomCard({ item, data }) {
  const [isLoading, setIsLoading] = useState(false);
  const [noOfRooms, setNoOfRooms] = useState("");
  const [refundable, setRefundable] = useState(false);
  const [cancellationDate, setCancellationDate] = useState("");
  const [adults, setAdults] = useState(0);
  const router = useRouter();
  const dispatch = useDispatch();
  const roomsArray = useSelector((state) => state.roomSelect.value);
  const { "check-in": checkIn, "check-out": checkOut } = router.query;

  // function getUptoFreeCancellationDate(hrs) {
  //   const days = Math.floor(hrs / 24);
  //   const date = new Date(checkIn);
  //   date.setDate(date.getDate() - days);

  //   return getDate(date, "mmm, dd yyyy");
  // }
  // async function handleRoomSelect() {
  //   setIsLoading(true);

  //   const obj = item?.supplier?.references.reduce(
  //     (a, c, i) => ({
  //       ...a,
  //       [`supplier_reference${i + 1}`]: c,
  //     }),
  //     {},
  //   );
  //   const a = {
  //     booking_token: data?.booking_token,
  //     room_bookings: [
  //       {
  //         booking_token: item?.booking_token,
  //         ...obj,
  //       },
  //     ],
  //   };
  //   try {
  //     const res = await preBooking("prebookings", a);

  //     if (typeof window !== "undefined") {
  //       localStorage.setItem("room", JSON.stringify(item));
  //       localStorage.setItem("routerQuery", JSON.stringify(router.query));
  //       localStorage.setItem("hotel", JSON.stringify({ ...data?.hotel }));
  //       localStorage.setItem("roomInfo", JSON.stringify(res.data.data));
  //       localStorage.setItem("prebookingRequest", JSON.stringify(a));
  //     }
  //     setIsLoading(false);
  //     router.push(`/hotels/${router.query.slug}/booking-confirmation`);
  //   } catch (error) {
  //     console.log(error);
  //     setIsLoading(false);
  //   }
  // }

  useEffect(() => {
    if (roomsArray.length > 0) {
      const b = roomsArray?.filter(
        (x) => x.roomDetails.booking_token === item.booking_token,
      )[0];
      if (b) {
        setNoOfRooms(b.noOfRooms);
      } else {
        setNoOfRooms(1);
      }
    } else {
      setNoOfRooms(1);
    }
  }, [roomsArray]);

  useEffect(() => {
    if (item) {
      const endsAt = item?.cancellations?.filter(
        (item) => item?.amount === 0,
      )?.[0]?.ends_at;
      if (endsAt) {
        const date = new Date(endsAt);
        date.setDate(date.getDate() - 2);
        setCancellationDate(new Date(date.setDate(date.getDate() - 1)));
        setRefundable(true);
      } else {
        const checkIn = new Date(router.query?.["check-in"]);
        checkIn.setDate(checkIn.getDate() - 1);
        setRefundable(false);
      }
    }
  }, [item]);

  useEffect(() => {
    const arr = window.location.search
      .slice(1)
      .split("&")
      .filter((item) => item.includes("rooms["));
    const a = getArrayFromURLParams(arr);
    setAdults(
      a?.reduce((acc, curr) => {
        // eslint-disable-next-line no-param-reassign
        acc += curr.adults;
        return acc;
      }, 0),
    );
  }, []);

  function handleRoomSelect() {
    setIsLoading(true);
    let a = [];
    setTimeout(() => {
      if (typeof window !== "undefined") {
        const b = JSON.parse(localStorage.getItem("roomsArray"));
        if (!b) {
          a = [
            {
              noOfRooms,
              roomDetails: {
                ...item,
                hotel_booking_token: data?.booking_token,
              },
            },
          ];
          localStorage.setItem("roomsArray", JSON.stringify(a));
          dispatch(roomSelect(a));
          setIsLoading(false);
        } else {
          const exist = b.find(
            (x) => x.roomDetails.booking_token === item.booking_token,
          );
          if (exist) {
            const index = b.indexOf(exist);
            b[index] = {
              noOfRooms,
              roomDetails: {
                ...item,
                hotel_booking_token: data?.booking_token,
              },
            };
            a = [...b];
            localStorage.setItem("roomsArray", JSON.stringify(a));
            dispatch(roomSelect(a));
            setIsLoading(false);
          } else {
            a = [
              ...b,
              {
                noOfRooms,
                roomDetails: {
                  ...item,
                  hotel_booking_token: data?.booking_token,
                },
              },
            ];
            localStorage.setItem("roomsArray", JSON.stringify(a));
            dispatch(roomSelect(a));
            setIsLoading(false);
          }
        }
      }
    }, 300);
  }
  return (
    <div className="flex flex-col lg:flex-row gap-2 shadow-[0_5px_20px_rgba(0,0,0,0.1)] rounded-lg">
      {item?.image && (
        <img
          src={item?.image ?? "/scb/hotel-room.png"}
          alt=""
          className="w-full lg:w-[300px] h-full object-cover"
        />
      )}
      <div className="px-6 pt-6 pb-0 lg:py-6 lg:flex-1">
        <h3 className="font-semibold text-xl">{item?.type_name ?? ""}</h3>

        <div className="flex items-center flex-wrap max-w-[440px]">
          <div className="flex items-center gap-1 text-sm">
            <img src="/scb/beds.svg" alt="" />2 King Beds
          </div>
          <div className="flex items-center gap-1 text-sm">
            <img src="/scb/wifi.svg" alt="" />
            Free Wifi
          </div>
          <div className="flex items-center gap-1 text-sm">
            <img src="/scb/people.svg" alt="" />
            {item?.occupancies?.length} People
          </div>
          <div className="flex items-center gap-1 text-sm">
            <img src="/scb/beds.svg" alt="" />2 King Beds
          </div>
          <div className="flex items-center gap-1 text-sm">
            <img src="/scb/wifi.svg" alt="" />
            Free Wifi
          </div>
        </div>

        <div className="mt-3 lg:mt-10">
          <p className="text-red-500 text-xs xl:text-sm">
            {refundable && `Free cancellation upto ${cancellationDate}.`}
          </p>
          <div className="mt-3">
            <img src="/scb/cards.png" alt="" />
          </div>
        </div>
      </div>
      <div className="px-6 lg:px-0 lg:pr-4 pt-1 pb-6 lg:py-6 lg:w-[220px] flex flex-col justify-between">
        {/* <div className="rounded-full bg-[#E39318] flex flex-col w-[105px] h-[38px] lg:ml-auto">
          <span className="text-center text-white ">Get $789</span>
          <span className="text-center text-white -mt-1 text-xs">
            points Credit
          </span>
        </div> */}
        <div className="h-12" />
        <div>
          <div className="mt-4 lg:mt-12 text-xs hidden lg:block lg:text-right">
            {getDifferenceInDays(new Date(checkIn), new Date(checkOut))} nights,{" "}
            {adults} adults
          </div>
          <div className="flex items-center justify-between lg:justify-end mt-4 lg:mt-0">
            <p className="text-xs lg:hidden">Terms and Conditions apply</p>
            <div>
              {/* <span className="line-through font-semibold">$1000</span> */}
              <span className="font-semibold text-lg xl:text-2xl text-primary">
                {item?.currency}
                {item?.total}
              </span>
            </div>
          </div>
        </div>

        <div className="flex items-end gap-2 mt-4 lg:mt-0">
          <label htmlFor="rooms">
            <span className="text-sm">Rooms</span>
            <select
              name="rooms"
              id="rooms"
              className="shadow-[0_0_5px_rgba(0,0,0,0.15)] rounded-md"
              onChange={(e) => setNoOfRooms(Number(e.target.value))}
              value={noOfRooms}
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
          </label>
          <button
            className={`w-full inline-flex justify-center py-2  bg-[#743D7D] text-white rounded-lg ${
              isLoading ? "opacity-70" : ""
            }`}
            onClick={handleRoomSelect}
            onKeyUp={handleRoomSelect}
            type="button"
            disabled={isLoading}
          >
            {isLoading ? "Loading..." : "Book Now"}
          </button>
        </div>
      </div>
    </div>
  );
}

HotelRoomCard.propTypes = {
  item: PropTypes.shape().isRequired,
  data: PropTypes.shape().isRequired,
};
