import { useState, useRef } from "react";
import HotelForm from "../../features/hotelBookingForm/HotelForm";

export default function NavmenuHotelForm() {
  const scrolled = true;
  const [smFormOpen, setSmFormOpen] = useState(false);
  const reff = useRef();

  return (
    <>
      {smFormOpen && (
        <div
          role="button"
          tabIndex={0}
          aria-label="Overlay button"
          className="fixed z-20 inset-0 bg-[rgba(0,0,0,0.5)] lg:hidden"
          onClick={() => setSmFormOpen(false)}
          onKeyDown={() => {}}
        />
      )}
      <div
        ref={reff}
        className="relative -bottom-1 lg:bottom-auto transition-all z-10 inset-x-0 lg:h-auto"
      >
        <div
          className={`${
            smFormOpen ? "h-auto md:h-[448px]" : "h-auto"
          } lg:h-auto ${
            scrolled
              ? "sticky lg:top-[80px] xl:top-[64px] 2xl:top-[80px] w-full"
              : "lg:relative lg:h-24 md:mt-0"
          }`}
        >
          <div
            className={`transition-all duration-700 h-full lg:h-auto ${
              scrolled
                ? "px-3 2xl:px-12 3xl:px-[102px] pb-3 pt-10 lg:pt-12 xl:pb-4 xl:pt-0 2xl:pb-[25px]"
                : "relative md:absolute top-auto left-auto right-auto lg:-top-[5.75rem] md:left-0 md:right-0 w-full max-w-[100%] lg:max-w-[960px] xl:max-w-[1140px] 2xl:max-w-[1312px] mx-auto md:shadow-lg lg:rounded-md lg:rounded-tr-none  py-6 md:py-6 xl:py-6 px-4 md:px-10 lg:px-6 xl:px-8 2xl:px-10"
            } bg-primary`}
          >
            <div className="flex items-center flex-col lg:flex-row gap-3 xl:gap-6">
              <h2
                className={`hidden lg:block font-semibold ${
                  scrolled
                    ? "max-w-[185px] 2xl:text-xl text-primary"
                    : "text-primary text-center lg:text-left text-xl sm:text-2xl md:text-3xl lg:text-2xl xl:text-[2rem]  lg:max-w-[230px] xl:max-w-[297px] xl:leading-[48px] "
                }`}
              >
                Book A Travel Gay Approved Hotel
              </h2>
              <HotelForm scrolled={scrolled} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
