import axios from "axios";
import Link from "next/link";
import { useState } from "react";
import { IconContext } from "react-icons";
import { GrClose } from "react-icons/gr";
import { MdOutlineKeyboardBackspace } from "react-icons/md";
import { getAPIUrl, getBaseURL } from "../../utils/Helper";

const backIcon = { className: "h-6 w-6 fill-primary" };
export default function ResetPassword({
  setEmailPopup,
  setResetPasswordPopup,
  setAuthPopup,
}) {
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [successMessage, setSuccessMessage] = useState(null);
  const [emailSent, setEmailSent] = useState(false);
  const [emailProvider, setEmailProvider] = useState("");

  function resetPasswordEmailSend() {
    setIsLoading(true);
    setError("");
    setSuccessMessage("");

    if (!email || email.length <= 0) {
      setError("The email should not be empty");
      setIsLoading(false);
      return;
    }

    if (!email.includes("@") || !email.includes(".")) {
      setError("The email must be a valid email address");
      setIsLoading(false);
      return;
    }
    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      axios({
        url: `${getAPIUrl()}forgot-password`,
        method: "post",
        data: { email },
        headers: {
          Accept: "application/json",
        },
        withCredentials: true,
      })
        .then((res) => {
          setSuccessMessage(res.data.data);
          const a = email.split("@")[1].split(".")[0];
          setEmailSent(true);
          setEmailProvider(a);
          setIsLoading(false);
        })
        .catch((err) => {
          setError(err.response.data.errors.email[0]);
          setIsLoading(false);
        });
    });
  }

  function getEmailProviderLink() {
    const providers = {
      "gmail.com": "https://mail.google.com",
      "yahoo.com": "https://mail.yahoo.com",
      "hotmail.com": "https://outlook.live.com",
      "aol.com": "https://mail.aol.com",
    };

    const domain = email.split("@")[1];
    if (providers[domain]) {
      return providers[domain];
    }
    return null;
  }

  if (emailSent) {
    return (
      <div className="fixed inset-0 max-w-[568px] h-max max-h-[90vh] m-auto z-50 overflow-auto">
        <div className="bg-white mx-4 md:mx-0 rounded">
          <div className="px-4 py-10 md:px-6 md:py-12 lg:px-[100px] relative">
            <button
              type="button"
              className="absolute top-2 left-4 md:top-4 lg:top-8 lg:left-8"
              onClick={() => {
                setEmailPopup(true);
                setResetPasswordPopup(false);
              }}
            >
              <IconContext.Provider value={backIcon}>
                <MdOutlineKeyboardBackspace />
              </IconContext.Provider>
            </button>
            <button
              type="button"
              className="absolute top-2 right-4 md:top-4 lg:top-8 lg:right-8"
              onClick={() => {
                setAuthPopup(false);
                setResetPasswordPopup(false);
              }}
            >
              <IconContext.Provider value={backIcon}>
                <GrClose />
              </IconContext.Provider>
            </button>
            <h1 className="font-semibold text-xl lg:text-2xl xl:text-[32px]">
              Change Password
            </h1>
            <p className="text-xs lg:text-sm mt-4">
              Password link is sent to <strong>{email}</strong>
            </p>

            <a
              href={
                getEmailProviderLink(email) !== null &&
                getEmailProviderLink(email)
              }
              target="_blank"
              rel="noreferrer"
              className={`inline-block mt-12 rounded text-white bg-[#743D7D] w-full py-2 text-center capitalize ${
                getEmailProviderLink(email) === null ? "hidden" : ""
              }`}
            >
              Open {email && emailProvider}
            </a>

            <p className="mt-4 lg:mt-12 pt-4 border-t text-xs md:text-sm text-center ">
              By signing in or creating an account, you agree with our{" "}
              <Link legacyBehavior href="#">
                <a className="text-blue-500"> Terms & conditions </a>
              </Link>
              and{" "}
              <Link legacyBehavior href="#">
                <a className="text-blue-500"> Privacy statement</a>
              </Link>
            </p>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="fixed inset-0 max-w-[568px] h-max max-h-[90vh] m-auto z-50 overflow-auto">
      <div className="bg-white mx-4 md:mx-0 rounded">
        <div className="px-4 py-10 md:px-6 md:py-12 lg:px-[100px] relative">
          <button
            type="button"
            className="absolute top-2 left-4 md:top-4 lg:top-8 lg:left-8"
            onClick={() => {
              setEmailPopup(true);
              setResetPasswordPopup(false);
            }}
          >
            <IconContext.Provider value={backIcon}>
              <MdOutlineKeyboardBackspace />
            </IconContext.Provider>
          </button>
          <h1 className="font-semibold text-xl lg:text-2xl xl:text-[32px]">
            Forgot password
          </h1>
          <p className="text-xs lg:text-base mt-4">
            Use a minimum of 10 characters, including uppercase letters,
            lowercase letters and numbers.
          </p>

          <label htmlFor="email" className="flex flex-col mt-4 lg:mt-10">
            <span>Email</span>
            <input
              type="email"
              name="email"
              id="email"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="border border-slate-200 border-solid mt-1 rounded-lg w-full"
            />
            {error && <p className="text-red-500 text-sm">{error}</p>}
          </label>
          <button
            type="button"
            className={`py-2 text-center w-full rounded bg-[#743D7D] text-white mt-4 ${
              isLoading ? "opacity-70" : ""
            }`}
            onClick={resetPasswordEmailSend}
          >
            {isLoading ? "Loading..." : "Reset Password"}
          </button>
          {successMessage && (
            <div className="text-sm text-green-500 mt-2">{successMessage}</div>
          )}
          <p className="mt-4 lg:mt-12 pt-4 border-t text-xs md:text-sm text-center ">
            By signing in or creating an account, you agree with our{" "}
            <Link legacyBehavior href="#">
              <a className="text-blue-500"> Terms & conditions </a>
            </Link>
            and{" "}
            <Link legacyBehavior href="#">
              <a className="text-blue-500"> Privacy statement</a>
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}
