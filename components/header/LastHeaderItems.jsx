import { useSession } from "next-auth/react";
import { IconContext } from "react-icons";
import { AiOutlineUser } from "react-icons/ai";
import PropTypes from "prop-types";
import { useState, useRef, useEffect } from "react";
import AccountDropdown from "./AccountDropdown";

const iconClassWhite = { className: "text-white" };
export default function LastHeaderItems({ setAuthPopup }) {
  const [userDropdown, setUserDropdown] = useState(false);
  const session = useSession();
  const ref = useRef(null);

  function checkClickOutside(e) {
    if (ref.current && !ref.current.contains(e.target)) {
      setUserDropdown(false);
    }
  }
  useEffect(() => {
    document.addEventListener("click", checkClickOutside);

    return () => {
      document.removeEventListener("click", checkClickOutside);
    };
  }, []);

  if (session.status === "unauthenticated") {
    return (
      <button
        type="button"
        className="w-[106px] xs:w-32 sm:w-[142px] 2xl:w-[162px] btn-primary transition-colors hover:bg-[rgba(215,_72,_116,_0.8)]  2xl:px-4 py-3 rounded text-white 2xl:gap-1 my-account-btn"
        onClick={() => setAuthPopup(true)}
      >
        <span className="text-sm 2xl:text-base">Login/Register</span>
      </button>
    );
  }

  if (session.status === "authenticated") {
    return (
      <div className="relative" ref={ref}>
        <button
          type="button"
          className="w-[106px] xs:w-32 sm:w-[142px] 2xl:w-[162px] btn-primary transition-colors hover:bg-[rgba(215,_72,_116,_0.8)]
  px-2 2xl:px-4 py-3 rounded text-white flex items-center justify-between 2xl:gap-1 my-account-btn"
          onClick={() => setUserDropdown(!userDropdown)}
        >
          <span>
            <IconContext.Provider value={iconClassWhite}>
              <AiOutlineUser />
            </IconContext.Provider>
          </span>
          <span className="text-[10px] xs:text-sm 2xl:text-base">
            My Account
          </span>
          <span>
            <svg
              className="w-3 transition fill-white"
              viewBox="0 0 11 7"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M6.10208 5.93414L10.4883 1.56289C10.7278 1.30843 10.7278 0.91919 10.4883 0.679634C10.2488 0.440122 9.84464 0.440122 9.60508 0.679634L5.66797 4.61675L1.73085 0.679634C1.4764 0.440122 1.08716 0.440122 0.847603 0.679634C0.608091 0.919147 0.608091 1.30837 0.847603 1.56289L5.21886 5.93414C5.47331 6.17365 5.86255 6.17365 6.10211 5.93414H6.10208Z"
              />
            </svg>
          </span>
        </button>
        {userDropdown && <AccountDropdown />}
      </div>
    );
  }
  return (
    <button
      type="button"
      className="w-[106px] xs:w-32 sm:w-[142px] 2xl:w-[162px] btn-primary transition-colors hover:bg-[rgba(215,_72,_116,_0.8)] 2xl:px-4 rounded text-white 2xl:gap-1 my-account-btn opacity-70"
      disabled
    >
      <div className="lds-ring">
        <div />
        <div />
        <div />
        <div />
      </div>
    </button>
  );
}

LastHeaderItems.propTypes = {
  setAuthPopup: PropTypes.func.isRequired,
};
