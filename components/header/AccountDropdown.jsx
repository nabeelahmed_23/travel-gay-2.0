import Link from "next/link";
import { signOut } from "next-auth/react";
import { FaUser } from "react-icons/fa";
import { GoSignOut } from "react-icons/go";

export default function AccountDropdown() {
  return (
    <div className="absolute bg-white shadow-[0_0_15px_rgba(0,0,0,0.15)] p-4 rounded right-0 min-w-[180px] account-dropdown top-16">
      <ul>
        <li className="py-1">
          <Link href="#" className="text-sm flex items-center gap-2">
            <span className="text-primary">
              <FaUser />
            </span>{" "}
            My Account
          </Link>
        </li>
        <li className="py-1">
          <button
            className="text-sm flex items-center gap-2"
            type="button"
            onClick={() => signOut()}
          >
            <span className="text-primary inline-block mt-1">
              <GoSignOut />
            </span>
            Log out
          </button>
        </li>
      </ul>
    </div>
  );
}
