import PropTypes from "prop-types";
import { useState } from "react";
import axios from "axios";
import { signIn } from "next-auth/react";
import { getAPIUrl, getBaseURL } from "../../utils/Helper";
import AddPasswordPopup from "./auth/AddPasswordPopup";
import AuthPopups from "./auth/AuthPopups";
import PasswordPopup from "./auth/PasswordPopup";
import ResetPassword from "./ResetPassword";

export default function EmailPopup({ setAuthPopup }) {
  const [emailPopup, setEmailPopup] = useState(true);
  const [passwordPopup, setPasswordPopup] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState({
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [addPasswordPopup, setAddPasswordPopup] = useState(false);
  const [backendErrors, setBackendErrors] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [resetPasswordPopup, setResetPasswordPopup] = useState(false);

  async function checkEmailHandler() {
    if (email.length <= 0) {
      setError((p) => ({ ...p, email: "Email cannot be empty." }));
      return;
    }
    setError({
      email: "",
      password: "",
      confirmPassword: "",
    });

    setBackendErrors({});
    setPassword("");
    setConfirmPassword("");
    setIsLoading(true);
    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      axios({
        method: "GET",
        url: `${getAPIUrl()}users/${email}/verify`,
        withCredentials: true,
      })
        .then((res) => {
          if (res.status === 200) {
            setAddPasswordPopup(true);
            setEmailPopup(false);
            setIsLoading(false);
          }
        })
        .catch((error) => {
          if (error.response.status === 404) {
            setEmailPopup(false);
            setPasswordPopup(true);
            setIsLoading(false);
          }
        });
    });
  }

  function handleLogin() {
    if (!password.length) {
      setError((p) => ({
        ...p,
        password: "Password can not be empty.",
      }));
      return;
    }
    setBackendErrors({});
    setIsLoading(true);
    setError({
      email: "",
      password: "",
      confirmPassword: "",
    });

    signIn("credentials", {
      redirect: false,
      email,
      password,
      origin: "login",
    }).then((res) => {
      if (res?.error) {
        setBackendErrors(JSON.parse(res?.error));
      } else {
        setTimeout(() => {
          setAuthPopup(false);
        }, 1000);
      }

      setIsLoading(false);
    });
  }

  async function handleRegistration() {
    setBackendErrors({});
    if (!password.length) {
      setError((p) => ({
        ...p,
        password: "Password can not be empty.",
      }));
    }
    if (!confirmPassword.length) {
      setError((p) => ({
        ...p,
        confirmPassword: "Confirm password can not be empty.",
      }));
      return;
    }
    if (password !== confirmPassword) {
      setError((p) => ({
        ...p,
        password: "Password and confirm password do not match.",
        confirmPassword: "Password and confirm password do not match.",
      }));
      return;
    }

    setIsLoading(true);

    setError({
      email: "",
      password: "",
      confirmPassword: "",
    });

    signIn("credentials", {
      redirect: false,
      email,
      password,
      password_confirmation: confirmPassword,
      origin: "registration",
    }).then((res) => {
      if (res?.error) {
        const errors = JSON.parse(res?.error);
        setBackendErrors(errors);
      } else {
        setSuccessMessage("Congratulations! You have successfully registered.");
        setTimeout(() => {
          setAuthPopup(false);
        }, 3000);
      }
      setIsLoading(false);
    });
  }

  return (
    <>
      {emailPopup && (
        <AuthPopups
          checkEmailHandler={() => checkEmailHandler()}
          setEmail={setEmail}
          email={email}
          error={error}
          backendErrors={backendErrors?.email}
          isLoading={isLoading}
          setResetPasswordPopup={setResetPasswordPopup}
          setEmailPopup={setEmailPopup}
        />
      )}
      {passwordPopup && (
        <PasswordPopup
          setPassword={setPassword}
          setConfirmPassword={setConfirmPassword}
          setEmailPopup={setEmailPopup}
          setPasswordPopup={setPasswordPopup}
          password={password}
          confirmPassword={confirmPassword}
          error={error}
          handleRegistration={() => handleRegistration()}
          backendErrors={backendErrors}
          isLoading={isLoading}
          successMessage={successMessage}
        />
      )}
      {addPasswordPopup && (
        <AddPasswordPopup
          setAddPasswordPopup={setAddPasswordPopup}
          error={error}
          password={password}
          handleLogin={() => handleLogin()}
          setPassword={setPassword}
          backendErrors={backendErrors}
          isLoading={isLoading}
          setEmailPopup={setEmailPopup}
        />
      )}

      {resetPasswordPopup && (
        <ResetPassword
          setResetPasswordPopup={setResetPasswordPopup}
          setEmailPopup={setEmailPopup}
          isLoading={isLoading}
          setAuthPopup={setAuthPopup}
        />
      )}

      <div
        className="fixed inset-0 backdrop-blur-md bg-[rgba(0,0,0,0.3)] z-40 cursor-default"
        onClick={() => setAuthPopup(false)}
        onKeyDown={() => setAuthPopup(false)}
        role="button"
        tabIndex="0"
      >
        <span className="hidden">Button to close Auth popup</span>
      </div>
    </>
  );
}

EmailPopup.propTypes = {
  setAuthPopup: PropTypes.func.isRequired,
};
