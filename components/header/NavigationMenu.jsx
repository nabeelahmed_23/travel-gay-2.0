import { useState, useEffect, useRef } from "react";
import { PropTypes } from "prop-types";
import { IconContext } from "react-icons";
import { BiArrowBack } from "react-icons/bi";
import axios from "axios";
import NavigationMenuItems from "./NavigationMenuItems";
import DropdownWrapper from "./megamenu/DropdownWrapper";
import { getMegaMenu } from "../../utils/services/ApiCalls";
import NavMenuMore from "./megamenu/NavMenuMore";

const backIcon = { className: "text-primary h-6 w-6" };
export default function NavigationMenu({ setIsActive, isActive, reff }) {
  const [active, setActive] = useState("");
  const [menu, setMenu] = useState("");
  const [error, setError] = useState("");
  const dropdownRef = useRef();

  async function getMenu() {
    try {
      const res = await getMegaMenu("menu");
      setMenu(res.data.data);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setError(error.message);
    }
  }

  const clickOutsideCheck = (e) => {
    if (reff.current && !reff.current.contains(e.target) && active === "") {
      setActive("");
    }
  };
  useEffect(() => {
    getMenu();
    window.addEventListener("click", clickOutsideCheck);
    if (menu.length > 0) setError("");

    return () => {
      window.addEventListener("click", clickOutsideCheck);
    };
  }, []);

  const navItems = [
    {
      label: "Destinations",
      children: (
        <DropdownWrapper menu={menu} reff={dropdownRef} error={error} />
      ),
    },
    { label: "Hotels", link: "/hotels" },
    { label: "Group Trips", link: "/gay-group" },
    { label: "Pride", link: "/gay-pride-calendar" },
    { label: "Editorials", link: "/editorials" },
    { label: "More", children: <NavMenuMore /> },
  ];
  return (
    <>
      <div
        role="switch"
        aria-checked="true"
        tabIndex={0}
        aria-label="Background Overlay"
        className={
          isActive
            ? "fixed inset-0 bg-black opacity-70 z-20 lg:relative lg:z-[-1] lg:bg-transparent"
            : ""
        }
        onClick={() => setIsActive(false)}
        onKeyUp={() => setIsActive(false)}
      />
      <ul
        className={`fixed left-0 right-0 bg-primary top-0 bottom-0 pt-4 pb-8 lg:py-0 ${
          isActive
            ? "overflow-auto lg:h-auto w-[368px] max-w-[368px] z-30"
            : "p-0 overflow-hidden lg:overflow-visible lg:h-auto w-0 z-30 lg:w-auto"
        } lg:relative lg:inset-0 lg:flex lg:gap-4 2xl:gap-[23px] transition-all`}
        ref={reff}
      >
        {isActive && (
          <div className="px-3 pb-4 pt-1 flex items-center gap-4">
            <button type="button" onClick={() => setIsActive(false)}>
              <IconContext.Provider value={backIcon}>
                <BiArrowBack />
              </IconContext.Provider>
            </button>
            <img src="/logo.svg" className="w-[135px]" alt="TG Logo" />
          </div>
        )}
        {navItems.map((item) => (
          <li
            key={item.label}
            className="relative first-of-type:border-t lg:first-of-type:border-t-0 border-[#EEB6C7]"
          >
            <NavigationMenuItems
              label={item.label}
              link={item.link}
              className="flex justify-between w-full items-center lg:inline-block lg:w-max font-medium text-lg lg:text-sm 2xl:text-base p-3 lg:px-0 hover:text-[#D74874] group"
              active={active}
              setActive={setActive}
            >
              {item.children}
            </NavigationMenuItems>
          </li>
        ))}
      </ul>
    </>
  );
}

NavigationMenu.defaultProps = {
  isActive: false,
};

NavigationMenu.propTypes = {
  isActive: PropTypes.bool,
  reff: PropTypes.objectOf(PropTypes.shape()).isRequired,
  setIsActive: PropTypes.func.isRequired,
};
