import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import Link from "next/link";
import { IconContext } from "react-icons";
import { FaRegMoneyBillAlt } from "react-icons/fa";
import { MdLocationPin } from "react-icons/md";
import { useSession } from "next-auth/react";
import { TbWorld } from "react-icons/tb";
import Searchbar from "../../features/searchbar/Searchbar";
import HamburgerButton from "./HamburgerButton";
import NavigationMenu from "./NavigationMenu";
import LastHeaderItems from "./LastHeaderItems";
import LastItemButton from "./LastItemButton";
import EmailPopup from "./EmailPopup";
import NavmenuHotelForm from "./NavmenuHotelForm";
import HotelBookingProgressBar from "./megamenu/HotelBookingProgressBar";

const iconClassRed = { className: "text-primary h-6 w-6 sm:w-4 sm:h-4" };
export default function HeaderWrapper() {
  const [isActive, setIsActive] = useState(false);
  const [authPopup, setAuthPopup] = useState(false);
  const router = useRouter();
  const session = useSession();
  const reff = useRef();
  const items = [
    {
      label: "loc",
      icon: (
        <IconContext.Provider value={iconClassRed}>
          <MdLocationPin />
        </IconContext.Provider>
      ),
      className: "block uppercase text-sm 2xl:text-base",
    },
    {
      label: "eng",
      icon: (
        <IconContext.Provider value={iconClassRed}>
          <TbWorld />
        </IconContext.Provider>
      ),
      className: "block uppercase text-sm 2xl:text-base",
    },
    {
      label: "usd",
      icon: (
        <IconContext.Provider value={iconClassRed}>
          <FaRegMoneyBillAlt />
        </IconContext.Provider>
      ),
      className: "block uppercase text-sm 2xl:text-base",
    },
  ];

  const urls = ["search-by-hotel", "booking"];

  useEffect(() => {
    window.addEventListener("resize", () => {
      if (window.innerWidth >= 1024) setIsActive(false);
    });

    return () => {
      window.removeEventListener("resize", () => {
        if (window.innerWidth >= 1024) setIsActive(false);
      });
    };
  }, []);

  useEffect(() => {
    if (session.status === "authenticated") {
      setAuthPopup(false);
    }
  });

  useEffect(() => {
    if (session.status === "unauthenticated") {
      if (
        urls.findIndex((item) => router.pathname.includes(item)) !== -1 ||
        router.asPath.includes(`/hotels/${router.query?.slug}`)
      ) {
        setTimeout(() => {
          setAuthPopup(true);
        }, 5000);
      }
    }
  }, [session.status]);

  function handleHamburgerMenu() {
    setIsActive(!isActive);
  }

  return (
    <div className="sticky top-0 z-20">
      <header
        className="relative top-0 bg-primary px-2 2xl:px-12 3xl:px-[102px] py-3 pb-6 xl:py-4 2xl:py-[25px]
     flex items-center justify-between z-20 sm:gap-4 lg:gap-0"
      >
        <div className="flex items-center lg:gap-4 xl:gap-8 flex-1">
          <div className="flex items-center gap-2 sm:gap-4 ">
            <HamburgerButton
              handleHamburgerMenu={() => handleHamburgerMenu()}
              className=" w-5 h-5 xs:w-6 xs:h-6 lg:hidden"
            />
            <Link legacyBehavior href="/">
              <a className="lg:mt-1 xl:mt-0 inline-block">
                <img
                  src="/logo.svg"
                  className="w-28 xs:w-32 md:w-36 2xl:w-40 3xl:w-[195px]"
                  alt="TG Logo"
                />
              </a>
            </Link>
          </div>
          <div className="flex items-center mt-2 xl:mt-0 flex-1">
            <NavigationMenu
              isActive={isActive}
              reff={reff}
              setIsActive={setIsActive}
            />
            <div className="flex items-center gap-0 sm:gap-3 lg:ml-3 justify-around sm:justify-start w-full">
              {items.map((item) => (
                <LastItemButton
                  key={item.label}
                  label={item.label}
                  icon={item.icon}
                  className={item.className}
                />
              ))}
            </div>
          </div>
        </div>
        <div className="flex items-center">
          <div className="absolute -bottom-7 left-4 right-4 xl:relative xl:inset-auto xl:w-[280px] 2xl:w-[316px] 3xl:w-[376px]">
            <Searchbar
              className={`flex items-center bg-white rounded mt-[2px] ${
                isActive ? "" : "shadow-[0_0_10px_rgba(0,0,0,0.25)]"
              } xl:shadow-none`}
            />
          </div>
          <div className="ml-2">
            <LastHeaderItems setAuthPopup={setAuthPopup} />
          </div>
        </div>

        {authPopup && <EmailPopup setAuthPopup={setAuthPopup} />}
      </header>
      <div id="menu-hotel-form" className="-mt-1 lg:mt-0">
        <NavmenuHotelForm />
      </div>
      {(router.pathname.includes("booking") ||
        router.pathname.includes("confirmation")) && (
        <div>
          <HotelBookingProgressBar />
        </div>
      )}
    </div>
  );
}
