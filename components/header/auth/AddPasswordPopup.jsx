import Link from "next/link";
import { IconContext } from "react-icons";
import { MdOutlineKeyboardBackspace } from "react-icons/md";
import PropTypes from "prop-types";

const backIcon = { className: "h-6 w-6 fill-primary" };

export default function AddPasswordPopup({
  password,
  setPassword,
  error,
  handleLogin,
  backendErrors,
  isLoading,
  setEmailPopup,
  setAddPasswordPopup,
}) {
  return (
    <div className="fixed inset-0 max-w-[568px] h-max max-h-[90vh] m-auto z-50">
      <div className="bg-white mx-4 md:mx-0">
        <div className="px-4 py-8 md:px-6 md:py-12 lg:px-[100px] relative">
          <button
            type="button"
            className="absolute top-2 left-4 md:top-4 lg:top-8 lg:left-8"
            onClick={() => {
              setEmailPopup(true);
              setAddPasswordPopup(false);
            }}
          >
            <IconContext.Provider value={backIcon}>
              <MdOutlineKeyboardBackspace />
            </IconContext.Provider>
          </button>
          <h1 className="font-semibold text-xl lg:text-2xl xl:text-[32px]">
            Add password
          </h1>
          <p className="text-xs lg:text-base mt-1">
            Use a minimum of 10 characters, including uppercase letters,
            lowercase letters and numbers.
          </p>
          <label htmlFor="password" className="flex flex-col mt-4 lg:mt-10">
            <span>Password</span>
            <input
              type="password"
              name="password"
              id="authEmail"
              className="border border-slate-200 border-solid mt-1 rounded-lg"
              placeholder="********"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {error && (
              <p className="text-red-500 text-xs my-1">{error.password}</p>
            )}
            {backendErrors &&
              backendErrors?.email?.map((item) => (
                <p className="text-red-500 text-xs" key={item}>
                  {item}
                </p>
              ))}
          </label>

          <button
            onClick={handleLogin}
            disabled={isLoading}
            type="button"
            className={`w-full text-center py-3 bg-[#743D7D] rounded-lg text-white xl:text-xl mt-4 ${
              isLoading ? "opacity-70" : ""
            }`}
          >
            {isLoading ? "Loading..." : "Login"}
          </button>
          <p className="mt-4 lg:mt-12 pt-4 border-t text-xs md:text-sm text-center ">
            By signing in or creating an account, you agree with our{" "}
            <Link legacyBehavior href="#">
              <a className="text-blue-500"> Terms & conditions </a>
            </Link>
            and{" "}
            <Link legacyBehavior href="#">
              <a className="text-blue-500"> Privacy statement</a>
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
}

AddPasswordPopup.propTypes = {
  password: PropTypes.string.isRequired,
  setPassword: PropTypes.func.isRequired,
  handleLogin: PropTypes.func.isRequired,
  setEmailPopup: PropTypes.func.isRequired,
  setAddPasswordPopup: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  backendErrors: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
};
