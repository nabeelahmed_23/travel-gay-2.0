import Link from "next/link";
import { FcGoogle } from "react-icons/fc";
import { BsFacebook } from "react-icons/bs";
import { IconContext } from "react-icons";
import PropTypes from "prop-types";

const fbIcon = { className: "fill-[#3B5998]" };
export default function AuthPopups({
  checkEmailHandler,
  setEmail,
  email,
  error,
  backendErrors,
  isLoading,
  setEmailPopup,
  setResetPasswordPopup,
}) {
  const popupCenter = (url, title) => {
    const dualScreenLeft = window.screenLeft ?? window.screenX;
    const dualScreenTop = window.screenTop ?? window.screenY;

    const width =
      window.innerWidth ??
      document.documentElement.clientWidth ??
      window.screen.width;

    const height =
      window.innerHeight ??
      document.documentElement.clientHeight ??
      window.screen.height;

    const systemZoom = width / window.screen.availWidth;

    const left = (width - 500) / 2 / systemZoom + dualScreenLeft;
    const top = (height - 550) / 2 / systemZoom + dualScreenTop;

    const newWindow = window.open(
      url,
      title,
      `width=${500 / systemZoom},height=${
        550 / systemZoom
      },top=${top},left=${left}`,
    );

    newWindow?.focus();
  };
  return (
    <div className="fixed inset-0 max-w-[1165px] h-max m-auto z-50">
      <div className="grid grid-cols-1 xl:grid-cols-2 bg-white mx-4 md:mx-12 xl:mx-0">
        <div className="px-4 py-6 md:px-6 md:py-12 xl:px-12 xl:py-20">
          <h1 className="font-semibold text-xl lg:text-2xl xl:text-[32px]">
            Sign In or create an account
          </h1>
          <label htmlFor="email" className="flex flex-col mt-4 lg:mt-10">
            <span>E-mail Address</span>
            <input
              type="email"
              name="email"
              id="authEmail"
              value={email}
              className="border border-slate-200 border-solid mt-1 rounded-lg"
              placeholder="Enter your email..."
              onChange={(e) => setEmail(e.target.value)}
            />
            {error && (
              <p className="text-red-500 text-xs mt-2">{error?.email}</p>
            )}
            {backendErrors &&
              backendErrors.map((item) => (
                <p className="text-red-500 text-xs mt-2" key={item}>
                  {item}
                </p>
              ))}
          </label>
          <button
            onClick={checkEmailHandler}
            type="button"
            disabled={isLoading}
            className={`w-full text-center py-3 bg-[#743D7D] rounded-lg text-white xl:text-xl mt-4 xl:mt-7 ${
              isLoading ? "opacity-70" : ""
            }`}
          >
            {isLoading ? "Loading..." : "Continue with email"}
          </button>

          <div className="mt-2 text-center">
            <button
              className="text-blue-500 text-sm"
              type="button"
              onClick={() => {
                setResetPasswordPopup(true);
                setEmailPopup(false);
              }}
            >
              Forgot Password ?
            </button>
          </div>

          <div className="relative mt-6 lg:mt-8 xl:mt-12 mb-6 xl:mb-8">
            <hr className="bg-slate-300 " />
            <div className="text-xs lg:text-sm px-4 py-1 bg-white absolute top-2/4 left-2/4 -translate-x-2/4 -translate-y-2/4 w-max">
              Or use of of these Options
            </div>
          </div>

          <div className="flex flex-col sm:flex-row items-center justify-center xl:justify-start gap-4 pb-6 lg:pb-8 xl:pb-12 border-b">
            <button
              // onClick={() => signIn("google")}
              onClick={() => popupCenter("/google-signin", "Google Signin")}
              type="button"
              className="border px-4 py-2 rounded-md flex items-center gap-2 text-xs md:text-sm lg:text-base"
            >
              <FcGoogle />
              Sign in with Google
            </button>
            <button
              type="button"
              className="border px-4 py-2 rounded-md flex items-center gap-2 text-xs md:text-sm lg:text-base"
              onClick={() => popupCenter("/facebook-signin", "Facebook Signin")}
            >
              <IconContext.Provider value={fbIcon}>
                <BsFacebook />
              </IconContext.Provider>
              Sign in with Facebook
            </button>
          </div>
          <p className="mt-4 lg:mt-6 text-xs md:text-sm text-center">
            By signing in or creating an account, you agree with our{" "}
            <Link legacyBehavior href="#">
              <a className="text-blue-500"> Terms & conditions </a>
            </Link>
            and{" "}
            <Link legacyBehavior href="#">
              <a className="text-blue-500"> Privacy statement</a>
            </Link>
          </p>
        </div>
        <div className="signinAuthImage relative hidden xl:block">
          <blockquote className="absolute bottom-10 inset-x-0 max-w-[436px] mx-auto px-4 py-10 bg-gradient-to-r from-[#ffffff59] to-[#ffffff1f] backdrop-blur-xl rounded-xl">
            <p className="text-white font-normal text-sm">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type and scrambled it to make.
            </p>
          </blockquote>
        </div>
      </div>
    </div>
  );
}
AuthPopups.defaultProps = {
  backendErrors: [],
};
AuthPopups.propTypes = {
  checkEmailHandler: PropTypes.func.isRequired,
  setEmail: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  error: PropTypes.shape().isRequired,
  backendErrors: PropTypes.arrayOf(PropTypes.shape()),
  isLoading: PropTypes.bool.isRequired,
};
