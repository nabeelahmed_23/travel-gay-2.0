/* eslint-disable react/prop-types */
import React, { useState } from "react";
import DropdownContinentItem from "./DropdownContinentItem";
import DropdownContent from "./DropdownContent";

export default function DropdownWrapper({ menu, reff, error }) {
  const [active, setActive] = useState(menu[0]?.name);
  const [continent, setContinent] = useState(menu[0]);

  const handleActive = (continent) => {
    setContinent(continent);
    // if (active === continent?.name && window.innerWidth <= 1024) {
    //   setActive("");
    // } else
    setActive(continent?.name);
  };

  return (
    <div
      className="lg:absolute z-30 lg:top-14 2xl:top-16 lg:-left-[8rem] xl:-left-[0.5rem] lg:h-[34rem]"
      ref={reff}
    >
      <div className="h-full lg:grid lg:grid-cols-[208px_1fr] lg:shadow-[0_0_25px_rgba(0,0,0,0.2)]">
        <ul className="lg:py-4 bg-primary w-full lg:w-52 h-full">
          {menu.length > 0 &&
            menu?.map((continent) => (
              <React.Fragment key={continent?.id}>
                <DropdownContinentItem
                  active={active && active}
                  label={continent.name}
                  handleActive={handleActive}
                  continent={continent}
                />
                {active === continent?.name && (
                  <div className="lg:hidden">
                    <DropdownContent countriesData={continent?.countries} />
                  </div>
                )}
              </React.Fragment>
            ))}
        </ul>
        {active === continent?.name && (
          <div className="hidden lg:block w-[748px] bg-white">
            {error && menu.length <= 0 ? (
              <div className="p-8">
                <p className="text-sm text-red-500">{error}</p>
              </div>
            ) : (
              <DropdownContent countriesData={continent?.countries} />
            )}
          </div>
        )}
      </div>
    </div>
  );
}
