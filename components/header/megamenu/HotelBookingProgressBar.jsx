import { useRouter } from "next/router";

export default function HotelBookingProgressBar() {
  const router = useRouter();
  return (
    <div className="bg-[#FCF4F6] pt-11 pb-5 xl:py-5">
      <div className="px-4 w-full md:max-w-[688px] lg:max-w-[923px] mx-auto">
        <div className="flex items-center justify-between gap-2 md:gap-4">
          <div className="flex items-center gap-1 md:gap-2">
            <img
              src="/icons/tick-red-bg.svg"
              alt=""
              className="w-4 h-4 md:h-auto md:w-auto"
            />
            <span className="font-semibold text-[10px] md:text-base w-[76px] md:w-[120px] inline-block">
              Hotel Detail
            </span>
          </div>
          <hr className="w-full" />
          <div className="flex items-center gap-1 md:gap-2">
            <img
              src="/icons/tick-red-bg.svg"
              alt=""
              className="w-4 h-4 md:h-auto md:w-auto"
            />
            <span className="font-semibold text-[10px] md:text-base w-[90px] md:w-[145px] inline-block">
              Booking Detail
            </span>
          </div>
          <hr className="w-full" />
          <div className="flex items-center gap-1 md:gap-2">
            {router.pathname.includes("/confirmation") ? (
              <img
                src="/icons/tick-red-bg.svg"
                alt=""
                className="w-4 h-4 md:h-auto md:w-auto"
              />
            ) : (
              <div>
                <div className="w-4 h-4 md:w-6 md:h-6 rounded-full bg-[#CCCCCC]" />
              </div>
            )}
            <span className="font-semibold text-[10px] md:text-base w-[60px] md:w-[110px] inline-block">
              Confirmation
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
