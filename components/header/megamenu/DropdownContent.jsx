import PropTypes from "prop-types";
import Link from "next/link";

export default function DropdownContent({ countriesData }) {
  const chunk = Math.ceil(
    countriesData.length / (window.innerWidth < 1280 ? 2 : 4) + 1,
  );

  const arr = countriesData
    ?.map((e, i) =>
      i % chunk === 0 ? countriesData.slice(i, i + chunk) : null,
    )
    .filter((e) => e);
  function checkState(country) {
    if (country?.country_id) {
      return `/destination/gay-usa/${country?.slug}`;
    }
    return `/destination/${country?.slug}`;
  }

  function checkCityUrl(country, city) {
    if (country?.country_id) {
      return `/destination/gay-usa/${country?.slug}/${city.slug}`;
    }
    return `/destination/${country.slug}/${city.slug}`;
  }
  return (
    <div className="dropdownContent p-4 overflow-auto bg-white lg:relative lg:w-[748px] h-[50vh] lg:max-h-[544px] lg:h-full">
      <div className="columns-2 lg:columns-4">
        {arr.map((countries) =>
          countries.map((country) => (
            <div className="mb-4" key={country.slug + country.id}>
              <Link legacyBehavior href={checkState(country)}>
                <a className="text-primary font-bold text-sm block w-full">
                  {country.name}
                </a>
              </Link>
              {country?.cities.length > 0 && (
                <div className="mt-2">
                  {country.cities.map((city) => (
                    <Link
                      legacyBehavior
                      href={checkCityUrl(country, city)}
                      key={city.id}
                    >
                      <a className="text-sm mt-1 block">{city.name}</a>
                    </Link>
                  ))}
                </div>
              )}
            </div>
          )),
        )}
      </div>
    </div>
  );
}
DropdownContent.defaultProps = {
  countriesData: [],
};

DropdownContent.propTypes = {
  countriesData: PropTypes.arrayOf(PropTypes.shape()),
};
