import Link from "next/link";

export default function NavMenuMore() {
  return (
    <div className="absolute bg-white inset-x-0 lg:shadow-[0_2px_8px_rgba(0,0,0,0.25)] lg:w-[210px] py-3 px-4 lg:px-5">
      <div>
        <Link
          href="/events"
          className=" py-2 lg:py-1 block font-medium hover:text-[#D74874] group text-sm 2xl:text-base"
        >
          Events
        </Link>
        <Link
          href="#"
          className=" py-2 lg:py-1 block font-medium hover:text-[#D74874] group text-sm 2xl:text-base"
        >
          Travel Gay Women
        </Link>
        <Link
          href="#"
          className=" py-2 lg:py-1 block font-medium hover:text-[#D74874] group text-sm 2xl:text-base"
        >
          Travel Gay Podcast
        </Link>
        <Link
          href="#"
          className=" py-2 lg:py-1 block font-medium hover:text-[#D74874] group text-sm 2xl:text-base"
        >
          Gay Saunas
        </Link>
        <Link
          href="#"
          className=" py-2 lg:py-1 block font-medium hover:text-[#D74874] group text-sm 2xl:text-base"
        >
          Gay Maps
        </Link>
      </div>
    </div>
  );
}
