import Link from "next/link";

export default function FooterLinks() {
  return (
    <div className="">
      <h3 className="font-semibold hidden md:block">Business</h3>

      <ul className="md:flex flex-wrap gap-x-1">
        <li className=" md:hidden">
          <h3 className="font-semibold ">Business</h3>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Media Kit <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Advertise <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Add Business <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Add Event <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Contact Us <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Add Offer <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Careers <span className="hidden lg:inline">|</span>
            </a>
          </Link>
        </li>
        <li className="">
          <Link legacyBehavior href="#">
            <a className="text-[13px] text-[#222]">
              Become GayTravel Approved{" "}
            </a>
          </Link>
        </li>
      </ul>
    </div>
  );
}
