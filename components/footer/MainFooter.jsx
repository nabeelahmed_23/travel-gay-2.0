import Link from "next/link";
import FooterLinks from "./FooterLinks";
import Logo from "../Shared/Logo";
import InstaIcon from "../Shared/InstaIcon";
import TwitterIcon from "../Shared/TwitterIcon";
import FacebookIcon from "../Shared/FacebookIcon";
import PintrestIcon from "../Shared/PintrestIcon";

export default function MainFooter() {
  return (
    <footer className="bg-secondary pt-5 pb-12 lg:py-10 mt-12 lg:mt-24 border-t border-[rgba(0,0,0,0.3)]">
      <div className="theme-container px-4 md:px-0">
        <div className="grid grid-cols-2 lg:grid-cols-3 mt-4 gap-7 lg:mt-9">
          <div className="grid grid-cols-2 lg:grid-cols-1 gap-2 col-span-2 lg:col-span-1 row-start-2 lg:row-start-1">
            <Logo className="w-full max-w-[180px]" />
            <p className="text-[13px] ">Travelgay.com © copyright 2022.</p>
            <p className="text-[13px]">All rights reserved.</p>
            <div className="flex items-center gap-2 col-start-2 row-start-1 lg:row-start-4 lg:col-start-1">
              <InstaIcon className="w-8" />
              <TwitterIcon className="w-8" />
              <FacebookIcon className="w-8" />
              <PintrestIcon className="w-8" />
            </div>
          </div>
          <div>
            <h3 className="font-semibold hidden md:block">Quick Links</h3>
            <ul className="md:flex flex-wrap gap-x-1">
              <li className=" md:hidden">
                <h3 className="font-semibold ">Quick Links</h3>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    About Us <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Contact Us <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Outofoffice.com <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Gay Weddings <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Gay Honeymoons <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Terms Of Use <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="/cookie-policy">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Cookie Policy <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    {" "}
                    Luxury Gay Travel{" "}
                    <span className="hidden lg:inline">|</span>
                  </a>
                </Link>
              </li>
              <li className="">
                <Link legacyBehavior href="#">
                  <a className="text-[13px] text-[#222]">
                    Picture acknowledgement
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <FooterLinks />
        </div>
      </div>
    </footer>
  );
}
