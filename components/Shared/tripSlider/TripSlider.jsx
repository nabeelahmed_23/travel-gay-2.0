import { Splide, SplideSlide } from "@splidejs/react-splide";
import { Grid } from "@splidejs/splide-extension-grid";
import Link from "next/link";
import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function TripSlider({ data, isLoading, error, loc }) {
  // console.log("data----->>", data);
  if (error) {
    return (
      <div>
        <p className="text-red-500 text-sm">{error}</p>
      </div>
    );
  }

  if (isLoading) {
    return (
      <div className="mt-0 overflow-hidden">
        <Splide
          // ref={reff}
          extensions={{ Grid }}
          options={{
            autoplay: true,
            type: "loop",
            // perPage: loc === "home" ? 2 : 3,
            grid: {
              cols: loc === "home" ? 2 : 1,
              rows: loc === "home" ? 2 : 1,
              gap: {
                col: "-1rem",
                row: "-0.5rem",
              },
            },
            arrows: false,
            gap: "1rem",
            // gap: isLoading ? "1rem" : "-0.5rem",
            pagination: true,
            classes: {
              pagination: "splide__pagination mid-bar-pagination",
            },
            breakpoints: {
              1023: {
                padding: { right: "2rem" },
              },
              768: {
                padding: { right: "4rem" },
              },
              640: {
                perPage: 1,
              },
            },
          }}
        >
          {isLoading && (
            <>
              <SplideSlide>
                <div className="w-full relative aspect-[39_/_26] lg:aspect-[39_/_58] 2xl:aspect-[39_/_60] my-4 lg:m-2 rounded-md overflow-hidden pl-4 pr-2 lg:-mb-4 2xl:-mb-8">
                  <Skeleton className="w-full h-full" />
                </div>
              </SplideSlide>
              <SplideSlide>
                <div className="w-full relative aspect-[39_/_26] lg:aspect-[39_/_58] 2xl:aspect-[39_/_60] my-4 lg:m-2 rounded-md overflow-hidden pl-4 pr-2 lg:-mb-4 2xl:-mb-8">
                  <Skeleton className="w-full h-full" />
                </div>
              </SplideSlide>
            </>
          )}
        </Splide>
      </div>
    );
  }
  return (
    <Splide
      extensions={{ Grid }}
      options={{
        autoplay: true,
        type: "loop",
        rewind: true,
        direction: "ltr",
        grid: {
          cols: loc === "home" ? 2 : "",
          rows: loc === "home" ? 2 : "",
          gap: {
            col: "-1rem",
            row: "-0.5rem",
          },
        },
        arrows: false,
        classes: {
          pagination:
            "splide__pagination mid-bar-pagination max-w-[100vw] sm:max-w-auto",
        },
        breakpoints: {
          1023: {
            gap: "-1rem",
            grid: {
              cols: 2,
              rows: 1,
              gap: {
                row: "-1rem",
              },
            },
            padding: { right: "4rem" },
          },
          640: {
            grid: false,
            perPage: 1,
            gap: "-1rem",
            padding: {
              right: "0rem",
            },
          },
        },
      }}
    >
      {data.length > 0 &&
        !isLoading &&
        data?.map((trip) => (
          <SplideSlide key={trip.id}>
            <TripSlide
              src={trip?.image?.original_url ?? "/images/no-image.jpg"}
              label={trip?.name}
              price={trip?.price}
              date={trip?.upcoming_trip?.start_date}
              link={trip?.slug}
            />
          </SplideSlide>
        ))}
    </Splide>
  );
}
TripSlider.defaultProps = {
  // sidebar: false,
  error: "",
  loc: "notHome",
};

TripSlider.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  // sidebar: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  loc: PropTypes.string,
};

export function TripSlide({ src, label, price, date, link }) {
  return (
    <Link legacyBehavior href={`/package/${link}`}>
      <div className=" relative group transition-transform duration-500 w-full scale-90 hover:scale-100 rounded-lg cursor-pointer overflow-hidden hover:z-10">
        <div className="w-full h-full relative aspect-[626_/_478] 2xl:aspect-[626_/_481]">
          {/* <img src={src} alt="" className="w-full h-full " /> */}
          <ImageFallback
            src={src}
            layout="fill"
            objectFit="contain"
            loading="eager"
            priority
          />
        </div>
        <button
          type="button"
          className="capitalize group-hover:hidden bg-white px-4 py-3 rounded absolute bottom-3 left-3 text-sm lg:text-xs xl:text-sm font-semibold"
        >
          {label}
        </button>
        <div className="slide-linear-gradient hidden group-hover:block absolute left-0 right-0 bottom-0 h-[50%] p-2 items-end">
          <div className="flex justify-between items-start h-max gap-4 absolute bottom-1 inset-x-2">
            <div>
              <p className="text-white font-semibold text-xs lg:text-xs xl:text-base capitalize">
                {label}
              </p>
              <p className="text-white font-semibold text-xs md:text-sm lg:text-xs xl:text-sm">
                {date}
              </p>
            </div>
            <div>
              <p className="text-white font-semibold text-xs md:text-sm lg:text-xs xl:text-sm">
                Starting From:
              </p>
              <p className="text-white font-semibold text-xs lg:text-base xl:text-2xl">
                ${price}
              </p>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
}
TripSlide.defaultProps = {
  date: "",
};

TripSlide.propTypes = {
  src: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  date: PropTypes.string,
  price: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};
