import PropTypes from "prop-types";

export default function CheckBox({ label, name, id, onChange, checked }) {
  return (
    <label htmlFor={id} className="flex items-center gap-2 mb-4">
      <input
        type="checkbox"
        name={name}
        id={id}
        onChange={onChange}
        checked={checked}
        className="bg-[#F8E3E8] border-0 checked:bg-[#743D7D] checked:hover:bg-[#743D7D] focus:ring-0 checked:focus:bg-[#743D7D]  w-5 h-5"
      />
      {label && <span className="">{label}</span>}
    </label>
  );
}

CheckBox.defaultProps = {
  checked: false,
};

CheckBox.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  checked: PropTypes.bool,
};
