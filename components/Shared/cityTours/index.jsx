import { Splide, SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";
import Rate from "rc-rate";

export default function TourSlider({ data }) {
  return (
    <div className="mt-3 pl-4 md:p-0 xl:mt-6">
      <Splide
        options={{
          type: "loop",
          autoplay: true,
          arrows: false,
          pagination: false,
          gap: "1.25rem",
          padding: "1px",
          perPage: 4,
          breakpoints: {
            650: {
              perPage: 1,
              padding: { right: "4rem" },
            },
          },
        }}
      >
        {data?.map((item) => (
          <SplideSlide key={item.img}>
            <SliderWithTextSlideTwo
              img={item.image?.original_url}
              heading={item.name}
              desc={item.sub_title}
              rating={item.rating}
              reviewNo={0}
            />
          </SplideSlide>
        ))}
      </Splide>
    </div>
  );
}
TourSlider.defaultProps = {
  data: [],
};
TourSlider.propTypes = {
  data: PropTypes.arrayOf,
};

export function SliderWithTextSlide({ img, heading, desc }) {
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden">
      <img src={img} alt="" className="w-full h-auto  md:h-28 object-fit " />
      <div className="bg-white py-4 px-3">
        <h4 className="text-sm font-semibold truncate">{heading}</h4>
        <p className="text-sm my-3 h-16	text-ellipsis overflow-hidden">{desc}</p>
      </div>
    </div>
  );
}

SliderWithTextSlide.propTypes = {
  img: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
};

export function SliderWithTextSlideTwo({ img, heading, rating, reviewNo }) {
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden">
      <img src={img} alt="" className="w-full h-[260px] md:h-28 object-fit" />
      <div className="bg-white py-5 px-3 h-[135px]">
        <h4 className="text-sm font-semibold truncate">{heading}</h4>
        <Rate count={5} value={rating} size={24} isHalf edit={false} />
        <p className="text-sm my-3 font-semibold">{reviewNo} Reviews</p>
      </div>
    </div>
  );
}

SliderWithTextSlideTwo.propTypes = {
  img: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  reviewNo: PropTypes.number.isRequired,
};
