import PropTypes from "prop-types";

export default function InputField({
  name,
  type,
  onChange,
  placeholder,
  className,
  autoComplete,
  value,
  min,
  max,
  ...props
}) {
  return (
    <input
      type={type}
      name={name}
      id={name}
      placeholder={placeholder}
      onChange={onChange}
      className={className}
      autoComplete={autoComplete}
      value={value}
      min={min}
      max={max}
      {...props}
    />
  );
}
InputField.defaultProps = {
  autoComplete: "off",
  value: "",
  min: "",
  max: "",
};

InputField.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  autoComplete: PropTypes.string,
  value: PropTypes.string,
  min: PropTypes.string,
  max: PropTypes.string,
};
