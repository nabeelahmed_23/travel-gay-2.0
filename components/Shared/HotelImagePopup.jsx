import { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { GrClose } from "react-icons/gr";
import ImageFallback from "../ImageWithFallBack/ImageWithFallBack";

export default function HotelImagePopup({ media, setImagePopup }) {
  const ref = useRef();

  function handleClickOutside(e) {
    if (ref.current && !ref.current.contains(e.target)) {
      setImagePopup(false);
    }
  }
  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);
  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center z-40">
        <div
          className="bg-white p-8 mx-4 md:mx-0 w-[95vw] md:w-[500px] lg:w-[800px] m-auto rounded-xl"
          ref={ref}
        >
          <div className="flex items-center justify-between mb-8">
            <h1 className="text-lg md:text-xl lg:text-2xl xl:text-3xl font-semibold">
              Hotel Images
            </h1>
            <button type="button" onClick={() => setImagePopup(false)}>
              <GrClose />
            </button>
          </div>
          <Splide
            options={{
              type: "loop",
              perPage: 1,
              rewind: true,
              arrows: false,
              gap: "0.5rem",
              pagination: true,
            }}
          >
            {media?.map((item) => (
              <SplideSlide
                key={item.uuid}
                className="h-[242px] w-[316px] lg:h-[484px] lg:w-[632px] relative"
              >
                <ImageFallback
                  src={item?.original_url}
                  alt={media?.name}
                  className="rounded-lg h-full object-cover mx-auto"
                  layout="fill"
                  priority
                />
                {/* <img
                    src={item?.original_url}
                    alt={media?.name}
                    className="rounded-lg h-full object-cover mx-auto"
                  /> */}
              </SplideSlide>
            ))}
          </Splide>
        </div>
      </div>

      <div className="fixed inset-0 backdrop-blur-md bg-[rgba(0,0,0,0.3)] z-30 cursor-default" />
    </>
  );
}

HotelImagePopup.propTypes = {
  media: PropTypes.shape().isRequired,
  setImagePopup: PropTypes.func.isRequired,
};
