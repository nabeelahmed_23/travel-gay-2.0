import parse from "html-react-parser";
import PropTypes from "prop-types";

export default function Pagination({ handlePagination, meta }) {
  return (
    <div className="flex items-center justify-between border-t border-gray-200 bg-white px-4 py-3">
      <div className="flex flex-1 justify-between lg:hidden">
        <button
          type="button"
          id={meta?.links && meta?.links[0]?.url?.split("=")[1]}
          disabled={meta?.links && !meta?.links[0]?.url}
          onClick={handlePagination}
          className={`relative inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50 ${
            meta?.links && !meta?.links[0]?.url ? "opacity-70" : ""
          }`}
        >
          Previous
        </button>
        <button
          type="button"
          id={
            meta?.links &&
            meta?.links[meta.links.length - 1]?.url?.split("=")[1]
          }
          disabled={meta?.links && !meta?.links[meta.links.length - 1]?.url}
          onClick={handlePagination}
          className={`relative ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-50 ${
            meta?.links && !meta?.links[meta.links.length - 1]?.url
              ? "opacity-70"
              : ""
          }`}
        >
          Next
        </button>
      </div>
      <div className="hidden lg:flex lg:flex-1 lg:items-center lg:justify-between">
        <div>
          <p className="text-sm text-gray-700">
            Showing
            <span className="font-medium"> {meta?.from ?? "0"} </span>
            to
            <span className="font-medium"> {meta?.to ?? meta?.length} </span>
            of
            <span className="font-medium"> {meta?.total ?? meta?.length} </span>
            results
          </p>
        </div>
        <div>
          <nav
            className="isolate inline-flex -space-x-px rounded-md shadow-sm"
            aria-label="Pagination"
          >
            {meta?.links?.map((link) => (
              <button
                key={link.label}
                type="button"
                id={link?.url?.split("=")[1]}
                disabled={!link?.url}
                onClick={handlePagination}
                aria-current="page"
                className={`relative first:rounded-tl first:rounded-bl last:rounded-tr last:rounded-br  items-center border px-4 py-2 text-sm focus:z-10 ${
                  link.active
                    ? "text-indigo-600 border-indigo-500 z-10"
                    : " border-gray-300 bg-white text-gray-700 z-[1]"
                } ${!link?.url ? "opacity-70" : ""}`}
              >
                {parse(link?.label)}
              </button>
            ))}
          </nav>
        </div>
      </div>
    </div>
  );
}

Pagination.propTypes = {
  handlePagination: PropTypes.func.isRequired,
  meta: PropTypes.shape().isRequired,
};
