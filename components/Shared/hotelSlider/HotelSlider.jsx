import { Splide, SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";
import Link from "next/link";
import TGApprovedLogoWhite from "../TGApprovedLogoWhite";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function HotelSlider({
  reff,
  data,
  loc,
  isLoading,
  error,
  noDataMessage,
}) {
  if (error) {
    return (
      <div className="mt-2">
        <p className="text-red-500 text-sm">{error}</p>
      </div>
    );
  }

  return (
    <Splide
      ref={reff}
      options={{
        type: "loop",
        // eslint-disable-next-line no-nested-ternary
        perPage: loc === "home" ? 3 : loc === "sidebar" ? 1 : 2,
        autoplay: true,
        rewind: true,
        arrows: false,
        gap: "0.5rem",
        pagination: false,
        breakpoints: {
          1280: {
            perPage: loc === "sidebar" ? 1 : 2,
          },
          1024: {
            perPage: loc === "sidebar" ? 1 : 1.5,
          },
          640: {
            perPage: 1,
            padding: loc === "notHome" && { right: "2rem" },
          },
        },
      }}
    >
      {isLoading && (
        <SplideSlide>
          <div className="w-full aspect-video m-2">
            <Skeleton className="h-full" />
          </div>
        </SplideSlide>
      )}

      {!isLoading &&
        (data?.length <= 0 ? (
          <div className="mt-2">
            <p className="text-red-500 text-sm">{noDataMessage}</p>
          </div>
        ) : (
          data.map((hotel) => (
            <SplideSlide key={hotel.id}>
              <CarouselSlide
                src={hotel?.image?.original_url}
                label={hotel?.name}
                slug={hotel?.slug}
              />
            </SplideSlide>
          ))
        ))}
    </Splide>
  );
}
HotelSlider.defaultProps = {
  loc: "home",
};
HotelSlider.propTypes = {
  reff: PropTypes.objectOf(PropTypes.shape()).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  loc: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
  noDataMessage: PropTypes.string.isRequired,
};

export function CarouselSlide({ src, label, slug }) {
  return (
    <div
      className="relative rounded-md m-2 overflow-hidden transition-shadow 
    shadow-[0_0_10px_rgba(0,_0,_0,_0.2)] max-h-[238px] w-auto h-[calc(100%_-_16px)]"
    >
      <Link legacyBehavior href={`/hotels/${slug}`}>
        <a>
          <div className="h-full">
            <div className="w-full aspect-video object-cover relative">
              <ImageFallback
                src={src}
                layout="fill"
                className="object-cover"
                priority
                loading="eager"
                alt={label}
              />
              {/* <img src={src} className="w-full h-full object-cover" alt="" /> */}
            </div>
          </div>
          <div
            className="flex justify-between items-end min-h-[5rem] slide-linear-gradient absolute
       bottom-0 left-0 right-0 px-4 py-3 gap-2"
          >
            <p className="font-semibold text-white text-xs sm:text-sm xl:text-base capitalize">
              {label}
            </p>
            <TGApprovedLogoWhite className="w-5 h-5" />
          </div>
        </a>
      </Link>
    </div>
  );
}

CarouselSlide.propTypes = {
  src: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
};
