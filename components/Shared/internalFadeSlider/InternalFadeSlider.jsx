import { useRef } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";

// export default function InternalFadeSlider({ data, city }) {
export default function InternalFadeSlider({ city }) {
  const ref = useRef();
  const info = [
    {
      original_url: "/images/internalhero.jpeg",
      image_desc: "A popular destination for gay travelers. ",
      image_title: "Time To Drink",
      city: "California",
    },
    {
      original_url: "/images/internalhero.jpeg",
      image_desc: "A popular destination for gay travelers. ",
      image_title: "Time To Drink",
      city: "California",
    },
    {
      original_url: "/images/internalhero.jpeg",
      image_desc: "A popular destination for gay travelers. ",
      image_title: "Time To Drink",
      city: "California",
    },
  ];

  return (
    <div className="px-4 md:px-0">
      <Splide
        hasTrack
        ref={ref}
        options={{
          type: "fade",
          rewind: true,
          autoplay: true,
          arrows: false,
          classes: {
            pagination: "splide__pagination internal-pagination",
          },
        }}
      >
        {info?.map((item) => (
          <SplideSlide key={item?.img}>
            <InternalFadeSliderSlide
              city={city}
              img={item?.original_url}
              label={
                item?.custom_properties?.image_desc ??
                "A popular destination for gay travelers."
              }
              heading={item?.custom_properties?.image_title ?? " Time to drink"}
            />
          </SplideSlide>
        ))}
      </Splide>
    </div>
  );
}

export function InternalFadeSliderSlide({ city, img, heading, label }) {
  return (
    <div className="max-h-[242px]  rounded-md border border-[#CCC] overflow-hidden relative">
      <div className="city-hero-img">
        <img src={img} alt="" className="h-full w-full object-cover" />
        <div className="city-img-overlay" />
      </div>
      <div className="flex items-center justify-between w-full absolute bottom-0 left-0">
        <div
          className="bg-white min-w-[282px] p-3 border-t 
      border-r border-[#CCC] rounded-tr-md"
        >
          {" "}
          <h4 className="text-sm font-semibold">{heading}</h4>
          <p className="text-xs mt-1">{label} </p>
        </div>

        <button
          type="button"
          className="hidden px-2 md:block mr-[12px] text-xs  h-[48px] bg-white p-0 rounded-[5px]"
        >
          Go to {city ?? ""}
        </button>
      </div>
    </div>
  );
}

InternalFadeSliderSlide.propTypes = {
  img: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};
