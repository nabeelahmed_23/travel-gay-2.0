import Link from "next/link";
import PropTypes from "prop-types";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function NewsSlide({ editorial, loc, alt }) {
  return (
    <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden">
      {/* <img
        src={editorial.image.original_url}
        alt=""
        className="w-full lg:max-h-[250px] lg:object-cover"
      /> */}
      <div
        className={` relative cursor-pointer ${
          loc === "home"
            ? " h-full aspect-[156_/_125]"
            : "w-full aspect-[834_/_670]"
        }`}
      >
        <Link legacyBehavior href={`/editorials/${editorial?.slug}`}>
          <ImageFallback
            src={editorial?.image?.original_url ?? "/images/no-image.jpg"}
            layout="fill"
            priority
            objectFit="cover"
            loading="eager"
            alt={alt ?? "no image"}
          />
        </Link>
      </div>
      <div className="p-4 min-h-[142px] lg:min-h-[123px] xl:min-h-[142px] cursor-pointer">
        {/* <p className="text-xs">14 March, 2022</p> */}
        <Link legacyBehavior href={`/editorials/${editorial?.slug}`}>
          <h2 className="mt-1 font-semibold text-primary text-sm capitalize truncate">
            {editorial?.name}
          </h2>
        </Link>
        <p className="text-[13px] mt-1 h-[90px] ">
          {`${editorial?.sub_title?.substring(0, 80)}...`}
          <Link legacyBehavior href={`/editorials/${editorial?.slug}`}>
            <a className="text-[13px] font-semibold text-primary">Read More</a>
          </Link>
        </p>
      </div>
    </div>
  );
}
NewsSlide.defaultProps = {
  loc: "home",
};

NewsSlide.propTypes = {
  editorial: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.objectOf(
        PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.arrayOf(PropTypes.string),
        ]),
      ),
    ]),
  ).isRequired,
  loc: PropTypes.string,
};
