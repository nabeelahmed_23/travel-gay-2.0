import { Splide, SplideSlide } from "@splidejs/react-splide";
import Skeleton from "react-loading-skeleton";
import PropTypes from "prop-types";
import NewsSlide from "./NewsSlide";

export default function NewsSlider({
  editorials,
  reff,
  error,
  isLoading,
  slidesPerPage,
}) {
  if (error) {
    return (
      <div className="mt-2">
        <p className="text-red-500 text-sm">{error}</p>
      </div>
    );
  }

  return (
    <div className="overflow-hidden md:-mx-4 lg:-mr-4 lg:-mt-4">
      <Splide
        ref={reff}
        options={{
          autoplay: true,
          type: "loop",
          direction: "ltr",
          rewind: true,
          perPage: slidesPerPage,
          arrows: false,
          gap: "-0.5rem",
          pagination: false,
          breakpoints: {
            1280: {
              perPage: 2,
            },
            1024: {
              perPage: 3,
            },
            640: {
              perPage: 1,
              padding: { right: "4rem" },
            },
          },
        }}
      >
        {isLoading && (
          <SplideSlide>
            <div className="relative p-2 box-border h-[363px] md:h-[321px] lg:h-[363px] xl:h-[344px] 2xl:h-[390px]">
              <Skeleton className="h-[70%]" />
              <Skeleton className="h-[30%]" />
            </div>
          </SplideSlide>
        )}

        {!isLoading &&
          editorials?.length > 0 &&
          editorials?.map((e) => (
            <SplideSlide key={e.id}>
              <NewsSlide editorial={e} />
            </SplideSlide>
          ))}
      </Splide>
    </div>
  );
}
NewsSlider.defaultProps = {
  // sidebar: false,
  // reff: {},
  error: "",
};

NewsSlider.propTypes = {
  // reff: PropTypes.shape(),
  // sidebar: PropTypes.bool,
  editorials: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.objectOf(
          PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.arrayOf(PropTypes.string),
          ]),
        ),
      ]),
    ),
  ).isRequired,
  error: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  slidesPerPage: PropTypes.number.isRequired,
};
