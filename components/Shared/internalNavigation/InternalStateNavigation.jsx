import { useEffect, useState } from "react";
import NavButtons from "./NavButtons";

export default function InternalStateNavigation() {
  const [length, setLength] = useState();
  const nav = [
    { id: 1, name: "All" },
    { id: 2, name: "Belfast" },
    { id: 3, name: "Birmingham" },
    { id: 4, name: "Dance Clubs" },
    { id: 5, name: "Blackpool" },
    { id: 6, name: "Hotels" },
    { id: 7, name: "Brighton" },
    { id: 8, name: "Cardiff" },
    { id: 9, name: "Execter" },
    { id: 10, name: "Oxford" },
    { id: 11, name: "Dance Clubs" },
    { id: 12, name: "Blackpool" },
    { id: 13, name: "Hotels" },
    { id: 14, name: "Brighton" },
    { id: 15, name: "Cardiff" },
    { id: 16, name: "Dance Clubs" },
    { id: 17, name: "Cruise Clubs" },
    { id: 18, name: "Tours" },
    { id: 19, name: "City Guide" },
    { id: 20, name: "Gay Map" },
    { id: 21, name: "Bars" },
    { id: 22, name: "Dance Bars" },
  ];
  useEffect(() => {
    setLength(window.innerWidth <= 1023 ? 7 : nav.length);
  }, []);

  return (
    <div className="flex flex-wrap gap-2 items-center p-4 md:px-0">
      {nav.slice(0, length).map((item, i) => (
        <NavButtons navItem={item} key={i} />
      ))}
      <button
        onClick={() => (length <= 8 ? setLength(nav.length) : setLength(8))}
        type="button"
        className="text-primary underline text-xs lg:text-sm"
      >
        Show {length <= 10 ? "More" : "Less"}
      </button>
    </div>
  );
}
