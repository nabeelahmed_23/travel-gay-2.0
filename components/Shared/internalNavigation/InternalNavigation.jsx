import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import calls from "../../../utils/services/PromiseHandler/PromiseHandler";
import NavButtons from "./NavButtons";

export default function InternalNavigation({ id, type }) {
  const [internalNavigation, setInternalNavigation] = useState([]);
  const fetchNavigationMenu = async () => {
    const {
      data: { data: navigationMenu },
    } = await calls(`custom-menu?id=${id}&type=${type}`);
    const a = navigationMenu?.items?.sort((a, b) => (a > b ? 1 : -1));
    setInternalNavigation(a);
  };

  useEffect(() => {
    fetchNavigationMenu();
  }, []);
  return (
    <div className="flex flex-wrap gap-2 items-center py-4">
      {internalNavigation?.map((item, i) => (
        <NavButtons navItem={item} key={i} />
      ))}
    </div>
  );
}

InternalNavigation.propTypes = {
  id: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
};
