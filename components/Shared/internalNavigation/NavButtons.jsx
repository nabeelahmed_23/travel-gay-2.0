import Link from "next/link";
import PropTypes from "prop-types";

export default function NavButtons({ navItem }) {
  if (navItem?.url?.includes("http") || navItem?.url?.includes("www")) {
    return (
      <a href={navItem?.url} target="_blank" rel="noreferrer">
        <a
          className={`text-xs lg:text-sm px-4 py-[10px] ${
            navItem.name.includes("Hotels")
              ? "bg-[#D74874] text-white"
              : "text-primary bg-primary"
          }  rounded-md inline-block`}
        >
          {navItem?.name}
        </a>
      </a>
    );
  }
  return (
    <Link
      href={`/${navItem?.url}`}
      className={`text-xs lg:text-sm px-4 py-[10px] ${
        navItem.name.includes("Hotels")
          ? "bg-[#D74874] text-white"
          : "text-primary bg-primary"
      }  rounded-md inline-block`}
    >
      {navItem?.name}
    </Link>
  );
}

NavButtons.propTypes = {
  navItem: PropTypes.shape().isRequired,
};
