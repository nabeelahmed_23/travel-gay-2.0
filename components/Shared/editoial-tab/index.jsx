import Link from "next/link";
import PropTypes from "prop-types";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function Editorials({ editorial, loc }) {
  console.log("editorial res---->>", editorial);
  return (
    <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-1 md:m-4 rounded-md overflow-hidden">
      <div
        className={` relative ${
          loc === "home"
            ? " h-full aspect-[156_/_125]"
            : "w-full aspect-[1500_/_670]"
        }`}
      >
        <ImageFallback
          src={editorial?.first_media ?? "/images/no-image.jpg"}
          layout="fill"
          priority
          objectFit="fit"
          loading="eager"
        />
      </div>
      <div className="p-4 min-h-[142px] lg:min-h-[123px] xl:min-h-[142px]">
        {/* <p className="text-xs">14 March, 2022</p> */}
        <h2 className="mt-1 font-semibold text-primary text-sm capitalize truncate">
          {editorial?.name}
        </h2>
        {editorial?.sub_title && (
          <p className="text-[13px] mt-1 h-[90px]">
            {`${editorial?.sub_title?.substring(0, 80)}...`}
            <Link legacyBehavior href="#">
              <a className="text-[13px] font-semibold text-primary">
                Read More
              </a>
            </Link>
          </p>
        )}
      </div>
    </div>
  );
}
Editorials.defaultProps = {
  loc: "home",
};

Editorials.propTypes = {
  editorial: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.objectOf(
        PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.arrayOf(PropTypes.string),
        ]),
      ),
    ]),
  ).isRequired,
  loc: PropTypes.string,
};
