import { Splide, SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";

export default function ImageGrid({ media, setImagePopup }) {
  function returnClass(idx) {
    if (idx === 0) {
      return "md:col-span-4 md:row-span-2 md:max-h-[330px]";
    }
    if (idx > 0 && idx < 5) {
      return "md:col-span-3 md:max-h-[160px]";
    }
    return "md:col-span-2";
  }
  return (
    <section className="mt-3 md:mt-6">
      <div className="hidden md:grid grid-cols-1 md:grid-cols-10 md:grid-row-3 gap-x-4 gap-y-2 md:max-h-[518px] mt-6">
        {media?.slice(0, 10).map((item, i) => (
          <div key={item.uuid} className={returnClass(i)}>
            {media?.length > 10 && i === 9 ? (
              <div className="relative h-full">
                <img
                  src={item?.original_url}
                  alt=""
                  className=" rounded-lg w-full h-full object-cover"
                />
                <button
                  type="button"
                  onClick={() => setImagePopup(true)}
                  className="absolute inset-0 bg-[rgba(0,0,0,0.4)] rounded-lg items-center justify-center font-semibold text-white text-4xl hidden md:flex"
                >
                  {(media?.length ?? 0) - 10}+
                </button>
              </div>
            ) : (
              <img
                src={item?.original_url}
                alt=""
                className=" rounded-lg w-full h-full object-cover"
              />
            )}
          </div>
        ))}
      </div>
      <div className="md:hidden">
        {media && (
          <Splide
            options={{
              type: "loop",
              perPage: 1,
              autoplay: true,
              rewind: true,
              arrows: false,
              gap: "0.5rem",
              pagination: false,
            }}
          >
            {media?.map((item) => (
              <SplideSlide key={item.uuid} className="max-h-[216px]">
                <img
                  src={item?.original_url}
                  alt=""
                  className="rounded-lg w-full h-full object-cover"
                />
              </SplideSlide>
            ))}
          </Splide>
        )}
      </div>
    </section>
  );
}

ImageGrid.propTypes = {
  media: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  setImagePopup: PropTypes.func.isRequired,
};
