import Link from "next/link";
import { SplideSlide, Splide } from "@splidejs/react-splide";
import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";
import { getDate } from "../../../utils/Helper";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function PartySlider({
  reff,
  isLoading,
  error,
  data,
  loc,
  pagination,
}) {
  if (error) {
    return (
      <div className="mt-4">
        <p className="text-sm text-red-500">{error}</p>
      </div>
    );
  }
  return (
    <div className="md:pl-0 lg:mt-[10px] xl:mt-[22px] 2xl:mt-[23px]">
      <Splide
        ref={reff}
        options={{
          autoplay: true,
          type: "loop",
          rewind: true,
          direction: "ltr",
          perPage: loc === "notHome" ? 3 : 2,
          arrows: false,
          gap: "-0.5rem",
          pagination,
          classes: {
            pagination:
              "splide__pagination mid-bar-pagination max-w-[100vw] sm:max-w-auto",
          },
          breakpoints: {
            1023: {
              padding: { right: "2rem" },
            },
            768: {
              padding: { right: "4rem" },
            },
            640: {
              perPage: 1,
            },
          },
        }}
      >
        {isLoading && (
          <SplideSlide>
            <div className="w-full relative aspect-[39_/_61] my-4 lg:m-2 rounded-md overflow-hidden pl-4 pr-2 lg:-mb-4 2xl:-mb-6">
              <Skeleton className="w-full h-full" />
            </div>
          </SplideSlide>
        )}

        {!isLoading &&
          data.length > 0 &&
          data.map((event) => (
            <SplideSlide key={event.label}>
              <PartiesSlide
                src={event?.image?.original_url ?? "/images/no-image.jpg"}
                label={event?.name}
                date={event?.upcoming_event?.start_date}
                link={event?.slug}
              />
            </SplideSlide>
          ))}
      </Splide>
    </div>
  );
}

PartySlider.defaultProps = {
  loc: "notHome",
  error: "",
};

PartySlider.propTypes = {
  reff: PropTypes.shape().isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  loc: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

export function PartiesSlide({ src, label, date, link }) {
  return (
    <div className="relative rounded-md overflow-hidden transition hover:shadow-[0_0_15px_rgba(0,_0,_0,_0.25)] m-4 lg:m-2 xl:m-[10px] 2xl:m-[11px]">
      <div className="relative h-full">
        {/* <img src={src} alt="" className="w-full h-full object-cover" /> */}
        {/* <div className="w-full relative h-[380px] md:h-[450px] lg:h-[371px] xl:h-[438px] 2xl:h-[500px]"> */}
        <div className="w-full relative aspect-[39_/_62]">
          <ImageFallback
            src={src}
            layout="fill"
            objectFit="cover"
            loading="eager"
          />
        </div>
        <div className="slide-linear-gradient h-[50%] absolute bottom-0 p-3 purple-overlay w-full rounded-b-md flex flex-col justify-end">
          <p className="font-medium text-white text-sm xl:text-base capitalize">
            {label}
          </p>
          <div className="flex justify-between items-center gap-2 mt-5">
            <Link legacyBehavior href={`/events/${link}`}>
              <a className="rounded px-3 py-2 2xl:px-4 2xl:py-3 font-semibold text-sm lg:text-xs xl:text-sm bg-white shadow-md">
                View Details
              </a>
            </Link>
            {date && (
              <p className="text-sm md:text-sm lg:text-xs 2xl:text-sm text-white">
                {getDate(date, "eee, mm dd")}
              </p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

PartiesSlide.defaultProps = {
  date: null,
};

PartiesSlide.propTypes = {
  src: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  date: PropTypes.string,
  link: PropTypes.string.isRequired,
};
