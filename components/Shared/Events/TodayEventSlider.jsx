import { SplideSlide, Splide } from "@splidejs/react-splide";
import { Grid } from "@splidejs/splide-extension-grid";
import Link from "next/link";
import PropTypes from "prop-types";
import Skeleton from "react-loading-skeleton";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function TodayEventSlider({ reff, data, isLoading }) {
  if (isLoading) {
    return (
      <div className="grid md:grid-cols-2 md:grid-rows-2 gap-4">
        <Skeleton className="w-full h-[120px]" />
        <Skeleton className="w-full h-[120px]" />
        <Skeleton className="w-full h-[120px]" />
        <Skeleton className="w-full h-[120px]" />
      </div>
    );
  }
  return (
    <div>
      <Splide
        ref={reff}
        extensions={{ Grid }}
        options={{
          autoplay: true,
          arrows: false,
          pagination: false,
          type: "loop",
          gap: "1rem",
          padding: { top: "0.5rem" },
          grid: {
            rows: 2,
            cols: 2,
            gap: {
              row: "1rem",
              col: "1rem",
            },
          },
          breakpoints: {
            768: {
              grid: {
                rows: 1,
                cols: 1,
                gap: {
                  row: "1rem",
                  col: "1rem",
                },
              },
              pagination: true,
              classes: {
                pagination: "splide__pagination bar-pagination",
              },
            },
          },
        }}
      >
        {data.map((party) => (
          <SplideSlide key={party.src}>
            <TodayEventSlide
              src={party?.image?.original_url}
              label={party.label}
              desc={party.sub_title}
              link={party?.slug}
            />
          </SplideSlide>
        ))}
      </Splide>
    </div>
  );
}

TodayEventSlider.propTypes = {
  reff: PropTypes.shape().isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export function TodayEventSlide({ src, label, desc, sidebar, link }) {
  return (
    <div
      className={`${
        sidebar ? "h-[130px] lg:h-[205px] xl:h-[137px]" : "h-[145px]"
      } flex items-end mx-1 w-full pr-2 pb-1`}
    >
      <Link href={`/events/${link}`} className="w-full ">
        <div
          className={`flex-1 bg-secondary grid ${
            sidebar
              ? "grid-cols-[115px_1fr] lg:grid-cols-1 xl:grid-cols-[142px_1fr] p-2 gap-2 "
              : "grid-cols-[115px_1fr] xl:grid-cols-[142px_1fr] gap-4 p-4 "
          } mx-4 md:mx-0 rounded shadow-[0_0_10px_rgba(0,0,0,0.15)]`}
        >
          <div className="relative">
            <div
              className={`h-[115px] lg:h-[123px] w-[146px] lg:w-[142px] shadow-md absolute bottom-0 rounded object-cover ${
                sidebar ? "lg:left-[20%] xl:left-0" : ""
              }`}
            >
              <ImageFallback src={src} layout="fill" alt={label} />
            </div>
            <img
              src="/images/featuredslidelogo.jpg"
              alt=""
              className={`absolute ${
                sidebar ? "-bottom-3" : "-bottom-4 "
              } left-1/2 -translate-x-1/2 rounded-full`}
            />
          </div>
          <div>
            <h4 className="h-5 text-sm md:text-base font-semibold text-ellipsis overflow-hidden">
              {label}
            </h4>
            <p className="h-12 text-xs mt-2 text-ellipsis overflow-hidden">
              {desc}
            </p>
          </div>
        </div>
      </Link>
    </div>
  );
}

TodayEventSlide.defaultProps = {
  sidebar: false,
};
TodayEventSlide.propTypes = {
  src: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  sidebar: PropTypes.bool,
};
