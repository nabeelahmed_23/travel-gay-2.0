import { SplideSlide } from "@splidejs/react-splide";
import parse from "html-react-parser";
import Link from "next/link";
import PropTypes from "prop-types";
import Rate from "rc-rate";

export default function HotelBooking({ data }) {
  console.log("Hotel data----->>", data);

  return (
    <div>
      {data.length > 0 ? (
        <>
          <h3 className="px-0 md:px-4 text-primary text-[20px] md:text-[28px] font-[700] mb-4">
            Hotels
          </h3>
          <div className="px-0 md:px-4 gap-6 overflow-auto justify-center flex flex-col md:grid md:grid-cols md:grid-cols-2 lg:grid-cols-3">
            {data?.map((item) => (
              <SplideSlide key={item.id}>
                <SliderWithTextSlide
                  img={item?.first_media}
                  heading={item.name}
                  rating={item.star_rating}
                  subTitle={item.sub_title}
                  desc={item.content}
                  address={item.address}
                  totalVotes={item.star_rating?.length}
                  audienceAwards={item.audience_awards}
                  tgApproved={item.tg_approved}
                />
              </SplideSlide>
            ))}
          </div>
        </>
      ) : (
        <div className="text-red-500 my-4">No data found</div>
      )}
    </div>
  );
}
HotelBooking.defaultProps = {
  data: [],
};
HotelBooking.propTypes = {
  data: PropTypes.arrayOf,
};

export function SliderWithTextSlide({
  img,
  heading,
  desc,
  subTitle,
  rating,
  address,
  totalVotes,
  audienceAwards,
  tgApproved,
}) {
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden h-full flex flex-col justify-between">
      {img && <img src={img} alt="" className="h-auto object-fit " />}
      <div className="bg-white py-4 px-3 h-full flex flex-col justify-between">
        <div className="items-center grid grid-cols-2 justify-between md:gap-3 mb-2">
          <h6 className="text-md font-semibold text-primary">{heading}</h6>
          <Rate count={5} value={rating} size={24} isHalf edit={false} />
        </div>
        {address ? (
          <div className="flex items-center py-3 ">
            <img src="/icons/location.svg" className="w-2 md:w-3" alt="arrow" />
            <p className=" ml-2 text-sm">{address}</p>
          </div>
        ) : (
          ""
        )}
        <div className="inline-flex gap-2 ">
          {/* <img src="/icons/mostBooked.svg" className="w-20" alt="booked" /> */}
          {audienceAwards?.length > 0 && (
            <div className="flex items-center gap-1">
              <div className="w-7 h-7 flex items-center justify-center">
                <img src="/scb/award-badge.png" alt="" />
              </div>
              <div>
                <h6 className="text-[10px] font-semibold text-slate-700">
                  2018 Most Booked
                </h6>
                <p className="text-[8px] ">Top 100</p>
              </div>
            </div>
          )}
          <div className="flex items-center gap-1 ">
            <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
              {rating}
            </div>
            <p className="ml-2 ">
              <h6 className="text-[10px] font-semibold ">Audience Rating</h6>
              <p className="text-xs">From {totalVotes} votes</p>
            </p>
          </div>
          {tgApproved ? (
            <img src="/tg-approved.jpg" alt="" className="max-w-[114px]" />
          ) : (
            ""
          )}
        </div>
        <h5 className="text-sm font-semibold text-primary my-3 flex">
          {subTitle}
        </h5>
        <div className="text-xs my-3 h-16">
          <span className="multiline-ellipsis-3">{parse(desc)}</span>
          <Link legacyBehavior href="#">
            <a className="text-xs font-semibold text-primary">Read More</a>
          </Link>
        </div>
        <div className="w-full">
          {/* <div className=" gap-2 grid grid-cols-1 lg:grid-cols-2 xl:grid-cols-3 "> */}
          {/* <button
            type="button"
            className="grid bg-[#E39318] w-full rounded-full text-white px-1 py-1"
          >
            <span className="text-sm xl:text-xs font-semibold ">Get $789</span>
            <span className="text-sm xl:text-xs"> points Credit</span>
          </button> */}
          <button
            type="button"
            className="w-full bg-[#743D7D]	y 2xl:px-4 py-3 rounded text-white xl:col-span-2"
          >
            <span className="text-sm 2xl:text-base">Book Direct</span>
          </button>
        </div>
      </div>
    </div>
  );
}

SliderWithTextSlide.propTypes = {
  img: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  address: PropTypes.string.isRequired,
};
