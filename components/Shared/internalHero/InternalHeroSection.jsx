import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import BreadCrumbs from "../../../features/breadcrumbs/BreadCrumbs";
import HotelFormWrapper from "../../../features/hotelBookingForm/HotelFormWrapper";

export default function InternalHeroSection({
  heading,
  description,
  hotelForm,
  breadCrumbsText,
  heroImage,
}) {
  const [breadcrumbs, setBreadcrumbs] = useState([]);
  const location = typeof window !== "undefined" && window?.location.pathname;
  useEffect(() => {
    if (typeof window !== "undefined") {
      setBreadcrumbs(window?.location.pathname.split("/"));
    }
  }, [location]);
  return (
    <>
      <section
        className="internalhero w-full min-h-[248px] lg:h-[300px] xl:h-[288px] pt-[75px] lg:pt-[52px] px-3 md:px-6 lg:px-12 xl:px-[103px] pb-3 md:pb-6 border"
        style={{
          background: `linear-gradient(0deg, rgba(116, 61, 125, 0.5), rgba(116, 61, 125, 0.5)), url(${heroImage})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <div className="text-white h-6">
          <BreadCrumbs breadCrumbs={breadCrumbsText || breadcrumbs} />
        </div>
        <div className="mt-6 lg:mt-[2.3rem]">
          <h1 className="text-xl md:text-2xl lg:text-4xl xl:text-[48px] font-bold text-white capitalize">
            {heading}
          </h1>
          <p className="text-xs lg:text-sm mt-4 lg:mt-6 xl:mt-8 xl:text-base leading-5 text-white capitalize">
            {description}
          </p>
        </div>
      </section>
      {hotelForm && (
        <div className="hidden lg:block">
          <HotelFormWrapper />
        </div>
      )}
    </>
  );
}

InternalHeroSection.defaultProps = {
  hotelForm: false,
  breadCrumbsText: "",
  heroImage: "/images/internalhero.jpeg",
};

InternalHeroSection.propTypes = {
  hotelForm: PropTypes.bool,
  heading: PropTypes.string.isRequired,
  heroImage: PropTypes.string,
  breadCrumbsText: PropTypes.string,
  description: PropTypes.string.isRequired,
};
