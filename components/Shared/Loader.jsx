import PropTypes from "prop-types";

export default function Loader({ text }) {
  return (
    <div className="fixed inset-0 z-50 bg-white">
      <div className="w-full h-full flex items-center justify-center">
        <div>
          <img
            src="/icons/TGProfileLogo.svg"
            alt=""
            className="w-[60px] md:w-[100px] mx-auto"
          />
          <div className="lds-ellipsis left-1/2 -translate-x-1/2">
            <div />
            <div />
            <div />
            <div />
          </div>
          <p className="text-base md:text-xl text-center px-4">{text}</p>
        </div>
      </div>
    </div>
  );
}

Loader.propTypes = {
  text: PropTypes.string.isRequired,
};
