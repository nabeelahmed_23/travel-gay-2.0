import { Splide, SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";
import Rate from "rc-rate";
import Skeleton from "react-loading-skeleton";
import ImageFallback from "../../ImageWithFallBack/ImageWithFallBack";

export default function SliderWithText({ data, withRating, isLoading }) {
  if (isLoading) {
    return (
      <div className="grid grid-cols-1 md:grid-cols-3 gap-5 mt-3 xl:mt-6">
        <Skeleton className="w-full h-[300px]" />
        <Skeleton className="w-full h-[300px]" />
        <Skeleton className="w-full h-[300px]" />
      </div>
    );
  }
  return (
    <div className="mt-3 pl-4 md:p-0 xl:mt-6">
      <Splide
        options={{
          type: "loop",
          autoplay: true,
          arrows: false,
          pagination: false,
          gap: "1.25rem",
          padding: "1px",
          perPage: 3,
          breakpoints: {
            650: {
              perPage: 1,
              padding: { right: "4rem" },
            },
          },
        }}
      >
        {!withRating
          ? data?.map((item, idx) => (
              <SplideSlide key={`sliderWithoutRating${idx}`}>
                <SliderWithTextSlide
                  img={item.image?.original_url}
                  heading={item.name}
                  desc={`${item.sub_title}...`}
                  rating={item.rating}
                  reviewNo={0}
                />
              </SplideSlide>
            ))
          : data?.map((item, idx) => (
              <SplideSlide key={`sliderWithRating${idx}`}>
                <SliderWithTextSlideTwo
                  img={item?.original_url}
                  rating={item?.rating ?? 0}
                  heading={item.name}
                  reviewNo={item?.reviews?.length ?? 0}
                />
              </SplideSlide>
            ))}
      </Splide>
    </div>
  );
}
SliderWithText.defaultProps = {
  data: [],
  withRating: false,
};
SliderWithText.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape()),
  withRating: PropTypes.bool,
  isLoading: PropTypes.bool.isRequired,
};

export function SliderWithTextSlide({ img, heading, desc }) {
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden">
      <div className="w-full h-full md:h-[160px] relative">
        <ImageFallback src={img ?? ""} alt="" objectFit="cover" layout="fill" />
      </div>
      <div className="bg-white py-4 px-3">
        <h4 className="text-sm font-semibold truncate">{heading}</h4>
        <p className="text-sm my-3 h-16	text-ellipsis overflow-hidden">{desc}</p>
      </div>
    </div>
  );
}

SliderWithTextSlide.propTypes = {
  img: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
};

export function SliderWithTextSlideTwo({ img, heading, rating, reviewNo }) {
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden">
      <div className="w-full h-full md:h-[160px] relative">
        <ImageFallback src={img} alt="" objectFit="cover" layout="fill" />
      </div>
      <div className="bg-white py-5 px-3 h-[135px]">
        <h4 className="text-sm font-semibold truncate">{heading}</h4>
        <Rate count={5} value={rating} size={24} isHalf edit={false} />
        <p className="text-sm my-3 font-semibold">{reviewNo} Reviews</p>
      </div>
    </div>
  );
}
SliderWithTextSlideTwo.defaultProps = {
  img: null,
};

SliderWithTextSlideTwo.propTypes = {
  img: PropTypes.string,
  heading: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  reviewNo: PropTypes.number.isRequired,
};
