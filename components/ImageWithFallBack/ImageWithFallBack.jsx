import Image from "next/image";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";

const fallbackSrc = "/images/no-image.jpg";
export default function ImageFallback({ src, className, alt, ...rest }) {
  const [imgSrc, setImgSrc] = useState(src);

  useEffect(() => {
    setImgSrc(src);
  }, [src]);
  return (
    <Image
      alt={alt ?? "no image"}
      {...rest}
      className={`bg-slate-100 ${className}`}
      src={imgSrc ?? fallbackSrc}
      onLoadingComplete={(result) => {
        if (result.naturalWidth === 0) {
          // Broken image
          setImgSrc(fallbackSrc);
        }
      }}
      onError={() => {
        setImgSrc(fallbackSrc);
      }}
    />
  );
}
ImageFallback.defaultProps = {
  src: "/images/no-image.jpg",
  className: "",
};
ImageFallback.propTypes = {
  src: PropTypes.string,
  className: PropTypes.string,
};
