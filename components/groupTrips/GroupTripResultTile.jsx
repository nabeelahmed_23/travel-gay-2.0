import PropTypes from "prop-types";
import Link from "next/link";
import { getDate } from "../../utils/Helper";
import ImageWithFallBack from "../ImageWithFallBack/ImageWithFallBack";

export default function GroupTripResultTile({ groupTrip }) {
  return (
    <div className="grid border lg:border-l-0 rounded-xl lg:grid-cols-[300px_1fr] xl:grid-cols-[405px_1fr] gap-y-4 gap-x-4">
      <div className="w-full h-[300px] lg:h-auto object-cover rounded-xl lg:scale-y-110 relative overflow-hidden">
        <ImageWithFallBack
          src={groupTrip?.image?.original_url}
          alt={groupTrip?.name}
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div className="pt-0 pb-4 lg:py-6 px-4 lg:pl-4 lg:pr-8 xl:px-8">
        <div className="flex items-center justify-between gap-4">
          <h1 className="text-primary font-semibold text-2xl">
            {groupTrip?.name}
          </h1>
          <img
            src="/icons/tgApproved.svg"
            alt=""
            className="hidden lg:block w-[150px]"
          />
        </div>
        <p className="my-3 text-sm ">{groupTrip?.sub_title}</p>
        <div className="flex items-end justify-between mt-4 lg:mt-0">
          <div className="max-w-[450px] font-medium">
            Departs on{" "}
            <span className="text-primary">
              {getDate(groupTrip?.upcoming_trip?.start_date, "ddd mmmm, yyyy")}
            </span>
          </div>
          <div className="hidden lg:block">
            <p className="font-medium text-black">
              {groupTrip?.total_days} days from
            </p>
            <p className="font-semibold text-[30px] text-primary -mt-2">
              ${groupTrip?.price}
            </p>
          </div>
        </div>
        <div className="flex lg:hidden items-center justify-between mt-3">
          <img
            src="/icons/tgApproved.svg"
            alt=""
            className="lg:block w-[120px]"
          />
          <div className="flex items-center gap-1">
            <p className="font-medium text-black text-xs">
              {groupTrip?.total_days} days from
            </p>
            <p className="font-semibold text-primary">${groupTrip?.price}</p>
          </div>
        </div>
        <div className="text-end">
          <Link
            href={`/package/${groupTrip.slug && groupTrip.slug}`}
            className="bg-[#743D7D] py-3 px-10 text-white rounded-md mt-2 inline-block"
          >
            View Trip
          </Link>
        </div>
      </div>
    </div>
  );
}

GroupTripResultTile.propTypes = {
  groupTrip: PropTypes.shape().isRequired,
};
