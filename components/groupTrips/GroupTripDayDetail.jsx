import PropTypes from "prop-types";
import ImageFallback from "../ImageWithFallBack/ImageWithFallBack";

export default function GroupTripDayDetail({ itinerary }) {
  return (
    <div className="grid grid-cols-1 md:grid-cols-[272px_1fr] rounded border md:border-l-0 mb-4 md:mb-8">
      <div className="relative top-0 md:h-[calc(100%_+_1rem)] overflow-hidden md:-translate-y-4 rounded-t-md min-h-[200px]">
        <ImageFallback
          src={itinerary?.first_media?.original_url}
          alt=""
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div className="p-4 md:pt-5 md:pb-8 md:pl-8 md:pr-12">
        <h5 className="text-primary font-semibold text-xl">
          {itinerary?.title}
        </h5>
        <p className="text-[13px] mt-3">{itinerary?.description}</p>
      </div>
    </div>
  );
}

GroupTripDayDetail.propTypes = {
  itinerary: PropTypes.shape().isRequired,
};
