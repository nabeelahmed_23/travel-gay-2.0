import Skeleton from "react-loading-skeleton";
import PropTypes from "prop-types";
import GroupTripResultSmallTile from "./GroupTripResultSmallTile";

export default function GroupTripFilterResultWrapper({
  isLoading,
  groupTrips,
  error,
}) {
  if (isLoading) {
    return (
      <div className="flex flex-col gap-4">
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
      </div>
    );
  }
  if (error) {
    return (
      <div className="my-6">
        <p className="text-sm text-red-500">{error}</p>
      </div>
    );
  }
  return (
    <div className="flex flex-col gap-y-8">
      {groupTrips?.length && !isLoading > 0 ? (
        groupTrips?.map((a, i) => (
          <GroupTripResultSmallTile key={`groupTripsList${i}`} groupTrip={a} />
        ))
      ) : (
        <p className="text-red-500 text-sm m-4">No data found</p>
      )}
    </div>
  );
}

GroupTripFilterResultWrapper.defaultProps = {
  error: null,
};

GroupTripFilterResultWrapper.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  groupTrips: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  error: PropTypes.string,
};
