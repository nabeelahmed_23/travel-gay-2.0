import PropTypes from "prop-types";

export default function CountryFilter({
  isActive,
  setIsActive,
  countries,
  handleCheckBoxSelect,
  filterData,
}) {
  return (
    <div className="my-6 pb-3">
      <button
        type="button"
        className="flex items-center justify-between w-full "
        onClick={() => {
          const { country } = isActive;
          setIsActive((p) => ({ ...p, country: !country }));
        }}
      >
        <p className="text-black font-semibold">Country</p>
        <span className={`transition ${isActive.country ? "" : "rotate-180"}`}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="12"
            viewBox="0 0 20 12"
            fill="none"
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M9.13178 1.29474L0.35927 9.80356C-0.119755 10.2989 -0.119755 11.0565 0.35927 11.5228C0.838295 11.9891 1.64666 11.9891 2.12577 11.5228L10 3.85909L17.8742 11.5228C18.3831 11.9891 19.1616 11.9891 19.6407 11.5228C20.1198 11.0566 20.1198 10.299 19.6407 9.80356L10.8982 1.29474C10.3893 0.828518 9.61083 0.828518 9.13172 1.29474H9.13178Z"
              fill="#999999"
            />
          </svg>
        </span>
      </button>
      <div
        className={`overflow-auto countryFilterScrollbar transition-[height_150ms_ease-in] ${
          isActive.country
            ? "h-[260px] my-6 shadow-[inset_-4px_-4px_4px_0_rgba(0,0,0,0.1)]"
            : "h-0"
        }`}
      >
        {countries?.length > 0 &&
          countries.map((item, idx) => (
            <label
              htmlFor={`_country${item?.id}`}
              className="flex items-center gap-4 first:mt-0 my-5"
              key={`countryListCheckbox${idx}`}
            >
              <input
                type="checkbox"
                name={item?.name}
                id={`_country${item?.id}`}
                data={item?.id}
                className="checkBoxFilter"
                onChange={handleCheckBoxSelect}
                checked={filterData.includes(item.id.toString())}
              />
              {item?.name}
            </label>
          ))}
      </div>
    </div>
  );
}

CountryFilter.defaultProps = {
  countries: [],
  filterData: [],
};

CountryFilter.propTypes = {
  isActive: PropTypes.objectOf(PropTypes.bool).isRequired,
  setIsActive: PropTypes.func.isRequired,
  countries: PropTypes.arrayOf(PropTypes.shape()),
  handleCheckBoxSelect: PropTypes.func.isRequired,
  filterData: PropTypes.arrayOf(PropTypes.string),
};
