import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Breadcrumb from "../../features/breadcrumbs/BreadCrumbs";

export default function GroupTripHeader({ text, showPrice, totalDays, price }) {
  const [breadcrumbs, setBreadcrumbs] = useState([]);
  const location = typeof window !== "undefined" && window?.location.pathname;
  useEffect(() => {
    if (typeof window !== "undefined") {
      setBreadcrumbs(window?.location.pathname.split("/"));
    }
  }, [location]);

  return (
    <div
      className="pt-11 pb-10 xl:py-10 px-4 sm:px-24 lg:px-28"
      style={{
        backgroundImage: `url(${"/grouptrips/internalHeader.jpg"})`,
      }}
    >
      <Breadcrumb
        breadCrumbs={
          showPrice ? ["", ...breadcrumbs.splice(1, 1)] : breadcrumbs
        }
      />
      <h1 className="font-bold text-white text-2xl md:text-4xl lg:text-5xl text-center leading-[36px] md:leading-[60px] lg:leading-[72px]">
        {text?.split(":")[0]}:
      </h1>
      <h1 className="font-bold text-white text-2xl md:text-4xl lg:text-5xl text-center leading-[36px] md:leading-[60px] lg:leading-[72px]">
        {text?.split(":")[1]}
      </h1>

      {showPrice && (
        <div className="text-center mt-4">
          <button
            type="button"
            className="bg-white py-4 px-6 font-bold text-primary rounded-md"
          >
            {totalDays} Days . From ${price}
          </button>
        </div>
      )}
      <img src="/icons/outofofficelogo.svg" alt="" className="mx-auto mt-5" />
    </div>
  );
}
GroupTripHeader.defaultProps = {
  showPrice: false,
  totalDays: null,
  price: null,
};

GroupTripHeader.propTypes = {
  text: PropTypes.string.isRequired,
  showPrice: PropTypes.bool,
  totalDays: PropTypes.number,
  price: PropTypes.string,
};
