export default function GroupTripTestimonial() {
  return (
    <div className="flex flex-col items-center justify-center py-10 px-5 rounded-xl shadow-[0_5px_20px_rgba(0,0,0,0.07)] m-4">
      <p className="font-medium text-[#2CA98B]">John Smith</p>
      <div>
        <img src="/grouptrips/testimonial-stars.svg" alt="" className="mt-2" />
      </div>
      <p className="text-center mt-4 text-sm md:text-base">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cursus nibh
        mauris, nec turpis orci lectus maecenas. Suspendisse sed magna eget nibh
        in turpis. Consequat duis diam lacus arcu.{" "}
      </p>
    </div>
  );
}
