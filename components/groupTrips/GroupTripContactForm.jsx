import axios from "axios";
import { startCase } from "lodash";
import { useRef, useState, useEffect } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import {
  getArrayWithNumberOfElements,
  monthArrayCompleteName,
  captchaTokenVerification,
} from "../../utils/Helper";

export default function GroupTripContactForm() {
  const [data, setData] = useState({
    LEADCF15: "Out Of Office",
    Lead_Source: "TravelGay.com",
    LEADCF14: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const [message, setMessage] = useState(null);
  const [error, setError] = useState({});
  const [verifyToken, setVerifyToken] = useState(null);
  const ref = useRef();

  const handleChange = (e) => {
    const { name, value } = e.target;
    if (name === "Mobile") {
      const regex = /^[0-9]*$/;
      if (value.match(regex)) setData((p) => ({ ...p, [name]: value }));
      return;
    }
    setData((p) => ({ ...p, [name]: value }));
  };

  function validateData() {
    const fields = ["First_Name", "Last_Name", "Mobile", "Email"];
    let err = {};
    for (let i = 0; i < fields.length; i += 1) {
      if (!data[fields[i]]) {
        err = {
          ...err,
          [fields[i]]: `${startCase(fields[i])} cannot be empty`,
        };
      }
    }
    setError(err);
    if (Object.keys(err).length > 0) return false;
    return true;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    setError({});
    setMessage(null);

    if (!validateData()) {
      setIsLoading(false);
      return;
    }

    const verified = await captchaTokenVerification(verifyToken);

    if (!verified) {
      if (verifyToken) ref.current.props?.grecaptcha?.reset();
      setError((p) => ({ ...p, captcha: "Captcha has failed. Try again." }));
      setVerifyToken(null);
      setIsLoading(false);
      return;
    }

    axios
      .get("/api/token")
      .then((res) => {
        const { access_token: accessToken } = res.data;
        axios
          .post("/api/send-lead", {
            data: {
              data: [
                {
                  ...data,
                },
              ],
              trigger: ["approval", "workflow", "blueprint"],
            },
            headers: {
              Authorization: `Zoho-oauthtoken ${accessToken}`,
              Accept: "application/json",
            },
          })
          .then(() => {
            setIsLoading(false);
            ref.current.props?.grecaptcha?.reset();
            setVerifyToken(null);
            setData({
              LEADCF15: "Out Of Office",
              Lead_Source: "TravelGay.com",
              LEADCF14: "",
            });
            setMessage(
              "Your message has been successfully sent! Thank you for contacting us.",
            );
          })
          .catch(() => {
            setIsLoading(false);
            console.log("Data not sent");
          });
      })
      .catch(() => setIsLoading(false));
  };

  useEffect(() => {
    const el = document.querySelector("#grecaptcha-wrapper div");
    if (el.children.length <= 0) {
      ref.current.props?.grecaptcha?.render(el, {
        sitekey: ref.current.props.sitekey,
        callback: ref.current.props.onChange,
      });
    }
  });

  return (
    <form
      className="mt-5 flex flex-wrap md:gap-x-7 lg:gap-x-16 gap-y-3 md:gap-y-7"
      id="groupTripContactForm"
      onSubmit={handleSubmit}
    >
      <label htmlFor="First_Name" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">First Name</div>
        <input
          type="text"
          name="First_Name"
          id="First_Name"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base"
          onChange={handleChange}
          value={data?.First_Name ?? ""}
        />
        {error?.First_Name && (
          <p className="text-red-500 text-sm">{error?.First_Name}</p>
        )}
      </label>
      <label htmlFor="Last_Name" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">Last Name</div>
        <input
          type="text"
          name="Last_Name"
          id="Last_Name"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base"
          onChange={handleChange}
          value={data?.Last_Name ?? ""}
        />
        {error?.Last_Name && (
          <p className="text-red-500 text-sm">{error?.Last_Name}</p>
        )}
      </label>
      <label htmlFor="Email" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">Email</div>
        <input
          type="email"
          name="Email"
          id="Email"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base"
          onChange={handleChange}
          value={data?.Email ?? ""}
        />
        {error?.Email && <p className="text-red-500 text-sm">{error?.Email}</p>}
      </label>
      <label htmlFor="Mobile" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">Contact Number</div>
        <input
          type="text"
          name="Mobile"
          id="Mobile"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base"
          value={data?.Mobile ?? ""}
          onChange={handleChange}
        />
        {error?.Mobile && (
          <p className="text-red-500 text-sm">{error?.Mobile}</p>
        )}
      </label>
      <label
        htmlFor="Departure_Month"
        className="w-full md:w-[calc(50%_-_2rem)]"
      >
        <div className="text-sm md:text-base">Departure Month</div>
        <select
          name="Departure_Month"
          id="Departure_Month"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base title-select"
          onChange={handleChange}
          value={data?.Departure_Month ?? "-None-"}
        >
          <option value="-None-">-None-</option>
          {monthArrayCompleteName?.map((a, i) => (
            <option key={`monthArrayTripContact${i}`} value={a}>
              {a}
            </option>
          ))}
        </select>
      </label>

      <label htmlFor="LEADCF3" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">Budget Per Person</div>
        <select
          name="LEADCF3"
          id="LEADCF3"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base title-select"
          value={data?.LEADCF3 ?? "-None-"}
          onChange={handleChange}
        >
          <option value="-None-">-None-</option>
          <option value="1,500-2,000">1,500-2,000</option>
          <option value="2,000-3,000">2,000-3,000</option>
          <option value="3,000-5,000">3,000-5,000</option>
          <option value="5,000-7,500">5,000-7,500</option>
          <option value="7,500-10,000">7,500-10,000</option>
          <option value="10,000+">10,000+</option>
        </select>
      </label>
      <label htmlFor="Travelers" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">Number of Travellers</div>
        <select
          name="Travelers"
          id="Travelers"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base title-select"
          onChange={handleChange}
          value={data?.Travelers ?? ""}
        >
          <option value="">-None-</option>
          {getArrayWithNumberOfElements(15)?.map((a, i) => (
            <option key={`monthArrayTripContact${i}`} value={a + 1}>
              {a + 1}
            </option>
          ))}
        </select>
      </label>
      <label
        htmlFor="Number_of_Nights"
        className="w-full md:w-[calc(50%_-_2rem)]"
      >
        <div className="text-sm md:text-base">Number of Nights</div>
        <select
          name="Number_of_Nights"
          id="Number_of_Nights"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base title-select"
          onChange={handleChange}
          value={data?.Number_of_Nights ?? ""}
        >
          <option value="">-None-</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5-9">5-9</option>
          <option value="10-15">10-15</option>
          <option value="More than 15">More than 15</option>
        </select>
      </label>
      <label htmlFor="LEADCF14" className="w-full md:w-[calc(50%_-_2rem)]">
        <div className="text-sm md:text-base">
          Sexual Orientation (optional)
        </div>
        <select
          id="LEADCF14"
          name="LEADCF14"
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base title-select"
          onChange={handleChange}
          value={data?.LEADCF14}
        >
          <option value="">-None-</option>
          <option value="Prefer not to Say">Prefer not to Say</option>
          <option value="Gay Man">Gay Man</option>
          <option value="Gay woman / Lesbian">Gay woman / Lesbian</option>
          <option value="Straight / Heterosexual">
            Straight / Heterosexual
          </option>
          <option value="Bisexual">Bisexual</option>
          <option value="Queer">Queer</option>
          <option value="Other">Other</option>
        </select>
      </label>
      <label htmlFor="Description" className="w-full">
        <div className="text-sm md:text-base">Comments</div>
        <textarea
          name="Description"
          id="Description"
          rows={7}
          className="border rounded-lg px-6 py-3 border-solid border-[#AAA9A9] mt-2 w-full text-sm md:text-base"
          onChange={handleChange}
          value={data?.Description ?? ""}
        />
      </label>

      <div id="grecaptcha-wrapper">
        <ReCAPTCHA
          sitekey={process.env.NEXT_PUBLIC_CAPTCHA_CLIENT_KEY}
          onChange={(token) => setVerifyToken(token)}
          ref={ref}
        />
        {error?.captcha && (
          <p className="text-sm text-red-500">{error.captcha}</p>
        )}
      </div>

      <button
        type="submit"
        className={`w-full text-center py-3 rounded-lg bg-[#743D7D] font-semibold text-base md:text-xl text-white ${
          isLoading ? "opacity-70" : ""
        }`}
        disabled={isLoading}
      >
        {isLoading ? "Loading..." : "Send Enquiry"}
      </button>
      {message && (
        <p className="text-green-600 text-sm lg:text-base">{message}</p>
      )}
    </form>
  );
}
