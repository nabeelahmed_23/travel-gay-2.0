import Link from "next/link";
import PropTypes from "prop-types";
import ImageFallback from "../ImageWithFallBack/ImageWithFallBack";

export default function ContinentGrid({ continents }) {
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-4">
      {continents?.map((item) => (
        <Link
          href={`gay-group/${item?.slug}`}
          key={`groupTripContinent${item.id}`}
        >
          <div className="relative w-full h-[225px] rounded-md">
            <ImageFallback
              layout="fill"
              src={item?.image?.original_url}
              alt={item?.label}
              objectFit="cover"
              className="rounded"
            />
            <div className="bg-linear-gradient rounded-md h-1/2 p-3 flex items-end text-white text-lg font-bold absolute bottom-0 inset-x-0">
              {item?.label}
            </div>
          </div>
        </Link>
      ))}
    </div>
  );
}

ContinentGrid.propTypes = {
  continents: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};
