export default function TGpodcast() {
  return (
    <div>
      <div className="px-4 md:px-0">
        <div className="my-2">
          <img
            className="mx-auto"
            src="/images/podcast-big-banner.png"
            alt=""
          />
        </div>
        <iframe
          title="castBoxPlayer"
          src="https://castbox.fm/app/castbox/player/id3813372/id358919215?v=8.22.11&autoplay=0"
          frameBorder="0"
          width="100%"
          height="400"
          className="mt-4"
        />
      </div>
      {/* <div className="mt-8">
        <h3 className="text-xl lg:text-2xl text-primary font-semibold pl-4 -mb-1">
          Gay Group Trips
        </h3>
        <TripSlider data={trips} sidebar />
      </div> */}
    </div>
  );
}
