import Rate from "rc-rate";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";
import { DateRange } from "react-date-range";
import { hotelSearch } from "../../utils/reducer/hotelReducer";
import GuestRoomPopup from "../scb/GuestRoomPopup";
import { getDate, getQueryFromArrayObject } from "../../utils/Helper";

export default function HotelCard({ item }) {
  const [ranges, setRanges] = useState({});
  const [members, setMembers] = useState([]);
  const dispatch = useDispatch();
  const hotelForm = useSelector((state) => state.hotelSearch.value);
  const handleSelect = (range) => {
    console.log(range.selection);
    setRanges(range.selection);
    dispatch(
      hotelSearch({
        name: "check_in",
        value: getDate(range.selection.startDate, "yyyy-mm-dd"),
      }),
    );
    dispatch(
      hotelSearch({
        name: "check_out",
        value: getDate(range.selection.endDate, "yyyy-mm-dd"),
      }),
    );
  };

  useEffect(() => {
    if (members?.length > 0) {
      dispatch(
        hotelSearch({
          name: "rooms",
          value: [...members],
        }),
      );
    }
  }, [members]);

  useEffect(() => {
    if (members.length <= 0) {
      setMembers(hotelForm?.rooms);
      setRanges({
        startDate: new Date(hotelForm?.check_in),
        endDate: new Date(hotelForm?.check_out),
        key: "selection",
      });
    }
  }, [hotelForm]);
  return (
    <div className="p-4 border transition shadow-lg duration-300 hover:shadow-[0_0_15px_rgba(116,61,125,0.8)] rounded mb-6">
      <section className="">
        <div className="grid grid-cols-4 grid-rows-3 md:grid-rows-2 gap-4 md:max-h-[340px] ">
          <div className="first:col-span-4 first:md:col-span-2 first:row-span-2 first:md:row-span-2 first:md:max-h-[330px] md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]">
            <img
              src="/homepage/faq1.jpg"
              alt=""
              className=" rounded-lg w-full h-full object-cover"
            />
          </div>
          <div className="first:col-span-4 first:md:col-span-2 first:row-span-2 first:md:row-span-2 first:md:max-h-[330px] md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]">
            <img
              src="/homepage/faq1.jpg"
              alt=""
              className=" rounded-lg w-full h-full object-cover"
            />
          </div>
          <div className="first:col-span-4 first:md:col-span-2 first:row-span-2 first:md:row-span-2 first:md:max-h-[330px] md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]">
            <img
              src="/homepage/faq1.jpg"
              alt=""
              className=" rounded-lg w-full h-full object-cover"
            />
          </div>
          <div className="first:col-span-4 first:md:col-span-2 first:row-span-2 first:md:row-span-2 first:md:max-h-[330px] md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]">
            <img
              src="/homepage/faq1.jpg"
              alt=""
              className=" rounded-lg w-full h-full object-cover"
            />
          </div>
          <div className="first:col-span-4 first:md:col-span-2 first:row-span-2 first:md:row-span-2 first:md:max-h-[330px] md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]">
            <img
              src="/homepage/faq1.jpg"
              alt=""
              className="rounded-lg w-full h-full object-cover"
            />
          </div>
        </div>
      </section>

      <div className="mt-4 ">
        <div className="flex flex-col md:flex-row md:items-center justify-between gap-2 md:gap-8">
          <div>
            <div className="flex items-center gap-2">
              <h5 className="text-primary font-semibold">Luxor Hotel</h5>
              <Rate />
            </div>
            <div className="flex items-center gap-2">
              <img src="/icons/location.svg" alt="Location Icon" />
              <p className="text-xs">5012 S Arville St #4, Las Vegas, USA</p>
              <button
                type="button"
                className="text-primary text-xs font-medium underline w-[90px]"
              >
                Show on map
              </button>
            </div>
          </div>

          <div className="flex items-center gap-1">
            <div className="flex items-center gap-1">
              <div className="w-7 h-7 flex items-center justify-center">
                <img src="/scb/award-badge.png" alt="" />
              </div>
              <div>
                <h6 className="text-[10px] font-semibold ">Top 10 2018</h6>
                <p className="text-[8px] ">Top 100</p>
              </div>
            </div>
            <div className="flex items-center gap-1 ">
              <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
                {item?.averageRating?.averageRating ?? 0}
              </div>
              <div>
                <h6 className="text-[10px] font-semibold ">Exceptional</h6>
                <p className="text-[8px] ">
                  Based on {item?.averageRating?.totalNumberOfRating ?? 0} votes
                </p>
              </div>
            </div>

            <img src="/icons/tgApproved.svg" alt="" className="w-[7rem]" />
          </div>
        </div>
        <h5 className="text-primary font-semibold text-[13px] mt-3">
          Why this hotel? Home of Zumanity. Close to The Fruit Loop gay scene.
        </h5>
        <p className="text-xs mt-2">
          Situated on the Las Vegas strip, a popular location for nightlife, New
          York-New York so you’ll have venues like Piranha and Freezone on your
          doorstep.Situated on the venues like Piranha. Situated on the Las
          Vegas strip, a popular location for nightlife, New York-New York so
          you’ll have venues like Piranha and Freezone on your doorstep.Situated
          on the venues like Piranha.
        </p>

        <div className="mt-2 flex items-center gap-2 flex-wrap">
          <span className="font-semibold text-[13px] ">Features:</span>
          <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
            <img src="" alt="" className="h-3 w-3" />
            Garden
          </div>
          <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
            <img src="" alt="" className="h-3 w-3" />
            Pool
          </div>
          <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
            <img src="" alt="" className="h-3 w-3" />
            Massage
          </div>
          <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
            +3
          </div>
        </div>

        <div className="flex flex-col md:flex-row md:justify-between gap-2 mt-4">
          <div className="gap-2 flex-wrap items-center flex order-1 md:-order-none">
            <button
              type="button"
              className="text-white rounded-md py-[6px] px-3 text-[13px] font-medium btn-primary"
            >
              Post a review
            </button>
            <button
              type="button"
              className="btn-primary rounded text-white py-[6px] px-3 text-[13px] font-medium flex items-center gap-1"
            >
              Contact
              <img src="/icons/fb-white.svg" alt="" />
              <img src="/icons/instawhite.svg" alt="" />
              <img src="/icons/globe.svg" alt="" />
              <img src="/icons/phone-white.svg" alt="" />
            </button>
            <button
              type="button"
              className="text-white rounded-md py-[6px] px-3 text-[13px] font-medium btn-primary"
            >
              More Info
            </button>
          </div>
          <div className="relative group">
            <Link
              href={`hotels/${item?.slug}?check-in=${
                hotelForm?.check_in
              }&check-out=${hotelForm?.check_out}&${getQueryFromArrayObject(
                hotelForm?.rooms,
              )}`}
              className={`px-5 py-3 bg-[#743D7D] rounded text-white font-semibold text-[13px] w-full ${
                members?.length > 0 ? "" : "opacity-70"
              }`}
            >
              Check Rates and Book Now
            </Link>
            <div className="hidden hotel-popup group-hover:block absolute bottom-10 right-0 bg-secondary shadow-[0_2px_8px_rgba(0,0,0,0.25)] p-4 rounded-md w-[467px]">
              <div className="grid grid-cols-[1fr_160px] gap-2">
                <div className="grid grid-cols-2 gap-2">
                  <div className="rounded-md bg-primary py-2 px-3">
                    <div>
                      <div className="text-xs font-medium flex items-center gap-1">
                        <img src="/icons/calendar.svg" alt="" /> Check In
                      </div>
                      <div className="ml-[22px] text-[10px]">
                        {getDate(hotelForm?.check_in, "dd mmm yyyy")}
                      </div>
                    </div>
                  </div>
                  <div className="rounded-md bg-primary py-2 px-3">
                    <div>
                      <div className="text-xs font-medium flex items-center gap-1">
                        <img src="/icons/calendar.svg" alt="" /> Check Out
                      </div>
                      <div className="ml-[22px] text-[10px]">
                        {getDate(hotelForm?.check_out, "dd mmm yyyy")}
                      </div>
                    </div>
                  </div>
                  <div className="col-span-2 rounded-lg overflow-hidden">
                    <DateRange
                      ranges={[ranges]}
                      onChange={handleSelect}
                      fixedHeight
                    />
                  </div>
                </div>

                <div>
                  <div className="rounded-md bg-primary py-2 px-3">
                    <div>
                      <div className="text-xs font-medium flex items-center gap-1">
                        <img src="/icons/calendar.svg" alt="" /> Rooms, Guests
                      </div>
                      <div className="ml-[22px] text-[10px]">
                        1 Room, 2 Guests
                      </div>
                    </div>
                  </div>
                  <div className="hotel-popup bg-white p-4 mt-2 rounded-lg h-[259.99px] overflow-auto hotel-date-popup-scroll">
                    <GuestRoomPopup members={members} setMembers={setMembers} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
