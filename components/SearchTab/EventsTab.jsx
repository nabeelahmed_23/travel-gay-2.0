import Eventscard from "../EventCard/EventCard";

export default function EventsTab({ data }) {
  // console.log("Events data----->>", data);
  return (
    <div className="">
      {data.length > 0 ? (
        <div className="overflow-auto">
          <div className="mb-12">
            <h3 className="px-0 md:px-4 text-primary text-[20px] md:text-[28px] font-[700] mb-4">
              Events
            </h3>
            {/* <div className="text-lg px-3 mb-2 flex items-center justify-between">
              <h3 className=" font-bold text-primary">
                Gay Events in London Today
              </h3>
              <span className="text-[#743D7D] text-sm font-semibold">
                Add Your Event {">"}
              </span>
            </div> */}
            <div className="gap-3 overflow-auto justify-center items-center grid grid-cols md:grid-cols-2 lg:grid-cols-3">
              {data?.map((event) => (
                <div className="max-w-auto min-w-full h-full p-2 ">
                  <Eventscard
                    title={event?.name}
                    slug={event?.slug ?? ""}
                    description={event?.recuring_dates ?? ""}
                    image={event?.first_media?.original_url}
                  />
                </div>
              ))}
            </div>
          </div>
          {/* <div className="mb-12">
            <div className="text-lg px-3 mb-2 flex items-center justify-between">
              <h3 className=" font-bold text-primary">
                Gay Events in London This Week
              </h3>
              <span className="text-[#743D7D] text-sm font-semibold">
                Add Your Event {">"}
              </span>
            </div>
            <div className="gap-3 overflow-auto justify-center items-center grid grid-cols md:grid-cols-2 lg:grid-cols-3">
              {data?.map((event) => (
                <div className="max-w-auto min-w-[276px] p-2">
                  <Eventscard
                    title={event?.title}
                    slug={event?.slug ?? ""}
                    description={event?.description}
                    image={event?.image}
                  />
                </div>
              ))}
            </div>
          </div>
          <div className="mb-12">
            <div className="text-lg px-3 mb-2 flex items-center justify-between">
              <h3 className=" font-bold text-primary">
                Further Ahead: Gay London Events
              </h3>
              <span className="text-[#743D7D] text-sm font-semibold">
                Add Your Event {">"}
              </span>
            </div>
            <div className="gap-3 overflow-auto justify-center items-center grid grid-cols md:grid-cols-2 lg:grid-cols-3">
              {data?.map((event) => (
                <div className="max-w-auto min-w-[276px] p-2">
                  <Eventscard
                    title={event?.title}
                    slug={event?.slug ?? ""}
                    description={event?.description}
                    image={event?.image}
                  />
                </div>
              ))}
            </div>
          </div> */}
        </div>
      ) : (
        <div className="text-red-500 my-4">No data found</div>
      )}
    </div>
  );
}
