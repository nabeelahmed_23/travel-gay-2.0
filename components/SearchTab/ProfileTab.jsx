import { SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";
import { useState } from "react";

export default function ProfileTab({ data }) {
  console.log("profile data----->>", data);
  return (
    <div>
      {data.length > 0 ? (
        <>
          <h3 className="px-0 md:px-4 text-primary text-[20px] md:text-[28px] font-[700] mb-4">
            Profiles
          </h3>
          <div className="px-0 md:px-4 gap-6 overflow-auto md:justify-between grid grid-cols md:grid-cols-2 lg:grid-cols-5 justify-center	">
            {data?.map((item) => (
              <SplideSlide key={item.img}>
                <SliderWithTextSlide
                  heading={item.name}
                  userName={item.user_name}
                  isFollowing={item.is_following}
                />
              </SplideSlide>
            ))}
          </div>
        </>
      ) : (
        <div className="text-red-500 my-4">No data found</div>
      )}
    </div>
  );
}
ProfileTab.defaultProps = {
  data: [],
};
ProfileTab.propTypes = {
  data: PropTypes.arrayOf,
};

export function SliderWithTextSlide({ userName, heading }) {
  const [isFollowing, setIsFowllowing] = useState(false);
  const FollowingHandler = () => {
    if (isFollowing) {
      setIsFowllowing(false);
    } else setIsFowllowing(true);
  };
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden">
      <div className="bg-white p-4 grid w-full  justify-center">
        <div className="bg-[#F8E3E8] p-5 rounded-full w-32 h-32 flex justify-center my-3">
          <img src="/icons/TGProfileLogo.svg" className=" w-10 " alt="arrow" />
        </div>
        <div className=" flex justify-center">
          <h6 className="text-lg font-semibold text-primary ">{heading}</h6>
        </div>
        <p className="flex justify-center text-xs text-gray-400 mb-5 ">
          @{userName}
        </p>

        {isFollowing === true ? (
          <button
            type="button"
            className="w-full	p-2 rounded border text-white border-[#ccc] bg-[#D74874] flex justify-center items-center gap-1"
            onClick={FollowingHandler}
          >
            <img
              src="/icons/following-white.svg"
              className="w-2 lg:w-3 font-semibold  "
              alt="arrow"
            />
            <span className="text-xs ">Following</span>
          </button>
        ) : (
          <button
            type="button"
            className="w-full	p-2 rounded border text-gray-500 border-[#ccc] flex justify-center items-center gap-1"
            onClick={FollowingHandler}
          >
            <img
              src="/icons/following-dark.svg"
              className="w-2 lg:w-3 "
              alt="arrow"
            />
            <span className="text-xs font-semibold">Follow</span>
          </button>
        )}
      </div>
    </div>
  );
}

SliderWithTextSlide.propTypes = {
  heading: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
  // isFollowing: PropTypes.bool.isRequired,
};
