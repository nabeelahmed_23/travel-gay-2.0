import VenueBooking from "../Shared/venue-booking-tab";

export default function VenueBookingTab({ data }) {
  return <VenueBooking data={data} />;
}
