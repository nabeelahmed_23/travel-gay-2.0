// import Link from "next/link";
import Editorials from "../Shared/editoial-tab";

export default function EditorialTab({ editorial }) {
  // console.log("Editorial data tab----->>", editorial);

  return (
    <div>
      {editorial.length > 0 ? (
        <div>
          <h3 className="px-0 md:px-4 text-primary text-[20px] md:text-[28px] font-[700] mb-4">
            Editorials
          </h3>
          {/* <div className="grid grid-cols-1 lg:grid-cols-2 grid-rows-1 md:grid-rows-2 ">
            <div className="row-span-2 overflow-hidden">
              <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden h-[calc(100%_-_32px)] flex flex-col">
                <div className="h-full relative">
                  <img
                    src="/homepage/fearureArticle.jpg"
                    alt=""
                    className="w-full h-full lg:object-cover"
                  />
                  <div className="featured-editorial-gradient absolute bottom-0 inset-x-0 top-[20%] flex items-end">
                    <p className="text-white font-bold text-xl p-4">
                      Featured Article
                    </p>
                  </div>
                </div>
                <div className="p-4">
                  <h2 className="mt-1 font-semibold text-primary">
                    Tom Prior On Gay Travel, Mindfulness
                  </h2>
                  <p className="text-[13px] mt-1">
                    Welcome to Washington, DC – the capital of the United States
                    of America. There’s plenty of culture, nightlife and LGBTQ+
                    of culture, nightlife check out....
                    <Link legacyBehavior href="#">
                      <a className="text-[13px] font-semibold text-primary">
                        Read More
                      </a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
            <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden h-[calc(100%_-_32px)] flex flex-col">
              <div className="h-full lg:max-h-[283px] relative">
                <img
                  src="/homepage/fearureArticle1.jpg"
                  alt=""
                  className="w-full h-full lg:object-cover"
                />
                <div className="featured-editorial-gradient absolute bottom-0 inset-x-0 top-[20%] flex items-end">
                  <p className="text-white font-bold text-xl p-4">
                    Featured Article
                  </p>
                </div>
              </div>
              <div className="p-4">
                <h2 className="mt-1 font-semibold text-primary">
                  Tom Prior On Gay Travel, Mindfulness
                </h2>
                <p className="text-[13px] mt-1">
                  Welcome to Washington, DC – the capital of the United States
                  of America. There’s plenty of culture, nightlife and LGBTQ+ of
                  culture, nightlife check out....
                  <Link legacyBehavior href="#">
                    <a className="text-[13px] font-semibold text-primary">
                      Read More
                    </a>
                  </Link>
                </p>
              </div>
            </div>
            <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden h-[calc(100%_-_32px)] flex flex-col">
              <div className="h-full lg:max-h-[283px] relative">
                <img
                  src="/homepage/fearureArticle2.jpg"
                  alt=""
                  className="w-full h-full lg:object-cover"
                />
                <div className="featured-editorial-gradient absolute bottom-0 inset-x-0 top-[20%] flex items-end">
                  <p className="text-white font-bold text-xl p-4">
                    Featured Article
                  </p>
                </div>
              </div>
              <div className="p-4">
                <h2 className="mt-1 font-semibold text-primary">
                  Tom Prior On Gay Travel, Mindfulness
                </h2>
                <p className="text-[13px] mt-1">
                  Welcome to Washington, DC – the capital of the United States
                  of America. There’s plenty of culture, nightlife and LGBTQ+ of
                  culture, nightlife check out....
                  <Link legacyBehavior href="#">
                    <a className="text-[13px] font-semibold text-primary">
                      Read More
                    </a>
                  </Link>
                </p>
              </div>
            </div>
          </div> */}
          <div className="gap-3 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
            {editorial?.map((e) => (
              <Editorials editorial={e} loc="editorial" key={e.id} />
            ))}
          </div>
        </div>
      ) : (
        <div className="text-red-500 my-4">No data found</div>
      )}
    </div>
  );
}
