import HotelBooking from "../Shared/hotel-booking-tab/index";

export default function HotelTab({ data }) {
  return <HotelBooking data={data} />;
}
