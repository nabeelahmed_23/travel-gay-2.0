import { SplideSlide } from "@splidejs/react-splide";
import PropTypes from "prop-types";

export default function BarTab({ data }) {
  console.log("Bars data----->>", data);

  return (
    <div className="">
      {data.length > 0 ? (
        <>
          <h3 className="px-0 md:px-4 text-primary text-[20px] md:text-[28px] font-[700] mb-4">
            Bars
          </h3>
          <div className="px-0 md:px-4 gap-6 overflow-auto md:justify-between grid grid-cols md:grid-cols-3 lg:grid-cols-4 ">
            {data?.map((item) => (
              <SplideSlide key={item.img}>
                <SliderWithTextSlideTwo
                  img={item.image?.original_url}
                  heading={item.name}
                  desc={item.sub_title}
                />
              </SplideSlide>
            ))}
          </div>
        </>
      ) : (
        <div className="text-red-500 my-4">No data found</div>
      )}
    </div>
  );
}
BarTab.defaultProps = {
  data: [],
};
BarTab.propTypes = {
  data: PropTypes.arrayOf,
};

export function SliderWithTextSlideTwo({ img, heading, desc }) {
  return (
    <div className="border border-[#ccc] rounded-md overflow-hidden">
      <img src={img} alt="" className="w-full h-full md:h-full object-fit " />
      <div className="bg-white py-4 px-3">
        <h4 className="text-sm font-semibold truncate">{heading}</h4>
        <p className="text-sm my-3 h-16	text-ellipsis overflow-hidden">{desc}</p>
      </div>
    </div>
  );
}

SliderWithTextSlideTwo.propTypes = {
  img: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
};
