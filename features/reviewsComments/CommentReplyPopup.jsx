import { useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { AiOutlineClose } from "react-icons/ai";
import { useSession } from "next-auth/react";
import { IconContext } from "react-icons";
import axios from "axios";
import { getAPIUrl, getBaseURL } from "../../utils/Helper";

const closeBtn = { className: "w-6 h-6" };
export default function CommentReplyPopup({ setIsActive, id, parentId }) {
  const [selectedImage, setSelectedImage] = useState([]);
  const [data, setData] = useState({
    is_private: 0,
    is_link_profile: 0,
    ...id,
    comment_id: parentId,
  });
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState({});
  const [successMessage, setSuccessMessage] = useState(null);
  const ref = useRef(null);
  const { data: session } = useSession();

  function handleChange(e) {
    const { name, value } = e.target;
    setData((p) => ({ ...p, [name]: value }));
  }

  function handleImageChange(e) {
    const { files } = e.target;
    if (files.length > 5) {
      setError((p) => ({ ...p, images: "Accept maximum of 5 images only" }));
      return;
    }
    setError((p) => ({ ...p, images: null }));
    setSelectedImage(Array.from(files));
  }
  function handleImageDelete(idx) {
    const a = Array.from(selectedImage);
    a.splice(idx, 1);
    setSelectedImage(a);
  }

  useEffect(() => {
    if (Array.from(selectedImage).length <= 0) {
      ref.current.value = null;
    }
  }, [selectedImage]);

  async function handleCommentSubmit(e) {
    e.preventDefault();
    setIsLoading(true);
    setError({});
    setSuccessMessage(null);
    if (!data?.email || !data?.content || data?.content?.length < 20) {
      if (!data?.email) setError((p) => ({ ...p, email: "Email is required" }));

      if (!data?.content)
        setError((p) => ({ ...p, content: "Review is required" }));
      else if (data?.content?.length < 20)
        setError((p) => ({
          ...p,
          review: "Review should have atleast 20 words",
        }));

      setIsLoading(false);
      return;
    }

    const fd = new FormData();
    Object.keys(data)?.forEach((item) => {
      fd.append([item], data[item]);
    });

    if (Array.from(selectedImage)?.length > 0) {
      Array.from(selectedImage).forEach((item) =>
        fd.append("media_files[]", item),
      );
    }

    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      axios({
        url: `${getAPIUrl()}comments`,
        method: "post",
        data: fd,
        headers: {
          "Content-Type": "multipart/form-data",
          Accept: "application/json",
          Authorization: `Bearer ${session?.user?.token}`,
        },
        withCredentials: true,
      })
        .then(() => {
          setSuccessMessage("Your comment has been sent for moderation.");
          setIsLoading(false);
          setData({});
          setSelectedImage([]);
          ref.current.value = null;
          setTimeout(() => {
            setIsActive(false);
          }, 2000);
        })
        .catch((error) => {
          console.log(error.response.data.message);
          setIsLoading(false);
        });
    });
  }
  return (
    <div className="fixed inset-0 z-20 flex items-center justify-center">
      <button
        type="button"
        onClick={() => setIsActive(false)}
        className="fixed inset-0 bg-[rgba(0,0,0,0.6)] z-20 pointer-events-auto cursor-default"
      >
        <span className="hidden">Button to close popup</span>
      </button>
      <div className="bg-white w-[914px] max-w-[95vw] max-h-[95vh] overflow-auto rounded py-4 md:pt-12 md:pb-10 px-6 md:px-20 z-30 relative">
        <div className="relative pt-8">
          <button
            type="button"
            className="absolute right-0 top-0"
            onClick={() => setIsActive(false)}
          >
            <IconContext.Provider value={closeBtn}>
              <AiOutlineClose />
            </IconContext.Provider>
          </button>
          <h1 className="text-primary font-semibold text-2xl text-center">
            Write a Reply
          </h1>
          <form onSubmit={handleCommentSubmit}>
            <div className="flex flex-wrap gap-x-6 gap-3 md:gap-y-5 mt-4">
              <label htmlFor="email" className="w-full">
                <span className="text-[13px]">Email*</span>
                <input
                  type="email"
                  name="email"
                  id="email"
                  className="review-comment-input"
                  placeholder="Enter your Email"
                  value={data?.email ?? ""}
                  onChange={handleChange}
                />
                {error?.email && (
                  <p className="text-xs text-red-500 mt-1">{error?.email}</p>
                )}
              </label>
              <label
                htmlFor="first_name"
                className="w-full md:w-[calc(50%_-_0.75rem)]"
              >
                <span className="text-[13px]">First Name</span>
                <input
                  type="text"
                  name="first_name"
                  id="first_name"
                  className="review-comment-input"
                  placeholder="Enter your First Name"
                  value={data?.first_name ?? ""}
                  onChange={handleChange}
                />
              </label>
              <label
                htmlFor="last_name"
                className="w-full md:w-[calc(50%_-_0.75rem)]"
              >
                <span className="text-[13px]">Last Name</span>
                <input
                  type="text"
                  name="last_name"
                  id="last_name"
                  className="review-comment-input"
                  placeholder="Enter your Last Name"
                  value={data?.last_name ?? ""}
                  onChange={handleChange}
                />
              </label>
              <label htmlFor="content" className="w-full">
                <span className="text-[13px]">Your Reply*</span>
                <textarea
                  name="content"
                  id="content"
                  className="review-comment-input resize-none"
                  rows="5"
                  placeholder="Your Reply should have atleast 20 words"
                  value={data?.content ?? ""}
                  onChange={handleChange}
                />
                {error?.content && (
                  <p className="text-xs text-red-500">{error?.content}</p>
                )}
              </label>
              <div className="flex items-center gap-2 w-full">
                <label htmlFor="images">
                  <div className="text-sm md:text-base flex items-center gap-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="40"
                      height="35"
                      viewBox="0 0 40 35"
                      fill="none"
                    >
                      <path
                        d="M39.0233 19.1046C38.8352 19.4716 38.8599 19.8841 38.753 20.27C37.7877 23.7299 35.5481 26.0378 31.9828 27.1348C31.2581 27.3576 30.4997 27.4581 29.7304 27.4553C27.8918 27.4486 26.0533 27.4524 24.2147 27.4534C23.7771 27.4534 23.4395 27.2837 23.2316 26.911C23.0395 26.5659 23.0573 26.216 23.2761 25.8889C23.5038 25.5466 23.8464 25.4167 24.2662 25.4186C25.9543 25.4243 27.6433 25.4157 29.3314 25.4224C31.8571 25.4319 33.858 24.404 35.3382 22.5001C38.2005 18.8202 36.9639 13.6858 32.7363 11.5239C32.1611 11.23 31.5452 11.0394 30.9066 10.9114C30.1641 10.7635 29.8967 10.4781 29.7987 9.75367C29.2819 5.93629 26.4196 3.02632 22.494 2.32846C18.6475 1.64482 14.709 3.49093 12.9071 6.82284C12.4427 7.68095 12.0952 7.82886 11.1338 7.58139C8.62893 6.93663 6.24087 8.58836 6.10523 11.0707C6.08246 11.4879 6.15375 11.9013 6.27553 12.3081C6.51413 13.1055 6.38938 13.4241 5.66267 13.883C4.03597 14.9108 3.02412 16.3473 2.80135 18.1878C2.50037 20.6748 3.46272 22.7153 5.51515 24.2599C6.58938 25.0687 7.83291 25.4394 9.21011 25.4281C11.2734 25.411 13.3377 25.4205 15.4011 25.4262C15.9753 25.4281 16.406 25.7713 16.4971 26.271C16.5832 26.7441 16.2981 27.2163 15.8268 27.3937C15.6456 27.4619 15.4595 27.4581 15.2723 27.4581C13.109 27.46 10.9447 27.461 8.78141 27.4572C8.53191 27.4572 8.28043 27.4401 8.0339 27.4079C5.60425 27.0826 3.72608 25.8841 2.31423 24.0077C0.96971 22.2204 0.386556 20.2112 0.672688 18.0123C0.951889 15.8666 1.98256 14.0935 3.73697 12.7262C4.00528 12.5167 4.08647 12.3223 4.03399 12.0037C3.51717 8.88798 5.99236 5.79975 9.26852 5.44608C9.81901 5.38635 10.3695 5.36549 10.912 5.4622C11.1734 5.50866 11.2804 5.41479 11.4071 5.22231C13.0823 2.68024 15.4486 1.04747 18.5158 0.40934C24.5711 -0.849846 30.475 2.83384 31.7482 8.6443C31.8116 8.93444 31.9393 9.07003 32.2403 9.16864C35.7699 10.3235 37.8936 12.7006 38.7965 16.1311C38.8926 16.4962 38.8708 16.8812 39.0223 17.2339C39.0223 17.689 39.0223 18.1441 39.0223 18.5993C38.9748 18.6476 38.9748 18.695 39.0223 18.7434C39.0233 18.8648 39.0233 18.9842 39.0233 19.1046Z"
                        fill="#D64D78"
                      />
                      <path
                        d="M18.753 16.3461C18.5857 16.3897 18.5223 16.4997 18.4382 16.5803C17.2867 17.6792 16.1372 18.7791 14.9907 19.8818C14.6877 20.1729 14.3412 20.3284 13.9105 20.2175C13.1353 20.0174 12.8561 19.1527 13.3709 18.5591C13.4343 18.4861 13.5076 18.4188 13.5779 18.3515C15.3551 16.6495 17.1313 14.9475 18.9084 13.2465C19.5421 12.6406 20.0846 12.6368 20.7133 13.2379C22.5173 14.9646 24.3232 16.6893 26.1221 18.4207C26.7865 19.0598 26.5805 19.9824 25.731 20.2118C25.2825 20.3322 24.9311 20.1625 24.6202 19.8638C23.4826 18.7696 22.341 17.6783 21.2005 16.586C21.1153 16.5044 21.0242 16.4267 20.8767 16.2949C20.8767 16.4855 20.8767 16.6106 20.8767 16.7358C20.8767 22.1357 20.8767 27.5356 20.8767 32.9355C20.8767 33.6561 20.5807 34.0543 19.9837 34.1482C19.3639 34.2459 18.8055 33.8268 18.7599 33.2247C18.75 33.0938 18.754 32.9611 18.754 32.8293C18.754 27.4654 18.754 22.1015 18.754 16.7377C18.753 16.6125 18.753 16.4864 18.753 16.3461Z"
                        fill="#D64D78"
                      />
                    </svg>
                    <span className="text-[13px] cursor-pointer">
                      {" "}
                      {selectedImage && Array.from(selectedImage).length > 0
                        ? ""
                        : "Choose File - Add a picture"}
                    </span>
                  </div>
                  <input
                    type="file"
                    name="images"
                    id="images"
                    className="hidden"
                    ref={ref}
                    accept="image/*"
                    max={5}
                    onChange={handleImageChange}
                  />
                  {error?.images && (
                    <p className="text-xs text-red-500 mt-1">{error?.images}</p>
                  )}
                </label>
                <span className="text-[13px] cursor-pointer">
                  {" "}
                  {selectedImage && Array.from(selectedImage).length > 0 && (
                    <div className="flex flex-wrap gap-3">
                      {Array.from(selectedImage).map((file, i) => (
                        <div key={`reviewImage${i}`} className="relative">
                          <img
                            src={URL.createObjectURL(file)}
                            alt={file.name}
                            className="w-16 h-16 rounded object-cover"
                          />
                          <button
                            type="button"
                            className="absolute top-1 right-1 bg-[#D74874] p-[2px] rounded text-white"
                            onClick={() => handleImageDelete(i)}
                          >
                            <AiOutlineClose />
                          </button>
                        </div>
                      ))}
                    </div>
                  )}
                </span>
              </div>
              <label
                htmlFor="is_private"
                className="w-full flex items-center gap-3"
              >
                <input
                  type="checkbox"
                  name="is_private"
                  id="is_private"
                  className="w-6 h-6 checked:bg-[#D74874] border border-solid rounded checked:hover:bg-[#D74874] checked:focus:bg-[#D74874]"
                  defaultChecked={data?.is_private === 1}
                  onChange={(e) => {
                    const { name, checked } = e.target;
                    if (checked) setData((p) => ({ ...p, [name]: 1 }));
                    else setData((p) => ({ ...p, [name]: 0 }));
                  }}
                />
                <span className="text-[13px]">Only Me</span>
              </label>
              <div className="mt-7 text-end w-full">
                <button
                  type="submit"
                  className={`bg-[#743D7D] rounded py-3 px-5 text-white font-semibold text-sm ${
                    isLoading ? "opacity-70" : ""
                  }`}
                >
                  {isLoading ? "Loading..." : "Reply Comment"}
                </button>
              </div>
              {successMessage && (
                <div className="bg-green-500 py-1 px-2 text-white font-medium rounded-md flex items-center justify-between gap-8 text-xs md:text-base w-full">
                  {successMessage}
                  <button
                    type="button"
                    onClick={() => setSuccessMessage(null)}
                    className="text-white"
                  >
                    <AiOutlineClose />
                  </button>
                </div>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

CommentReplyPopup.propTypes = {
  setIsActive: PropTypes.func.isRequired,
  id: PropTypes.shape().isRequired,
};
