/* eslint-disable no-nested-ternary */
/* eslint-disable react/prop-types */
import Link from "next/link";
import { startCase } from "lodash";
import { IconContext } from "react-icons";
import { AiFillHome } from "react-icons/ai";

const homeIcon = { className: "w-6 h-6 text-white" };
export default function Breadcrumb({ breadCrumbs }) {
  return (
    <nav aria-label="breadcrumb">
      <ol className="flex items-center flex-wrap">
        <li className="h-7">
          <Link legacyBehavior href="/">
            <a className="h-max">
              <i className="fa fa-home" aria-hidden="true" />{" "}
              <IconContext.Provider value={homeIcon}>
                <AiFillHome />
              </IconContext.Provider>
            </a>
          </Link>
        </li>
        {breadCrumbs?.map((bc, idx) => (
          <li
            key={bc}
            className={`text-sm md:text-base text-white ${
              idx === breadCrumbs.length - 1 && "active"
            }`}
            aria-current="page"
          >
            {idx !== breadCrumbs.length - 1 ? (
              <Link
                legacyBehavior
                href={`${breadCrumbs.slice(0, idx + 1).join("/")}`}
              >
                <a className="">
                  {startCase(bc)} &nbsp; {">"} &nbsp;{" "}
                </a>
              </Link>
            ) : bc ? (
              startCase(bc)
            ) : (
              ""
            )}
          </li>
        ))}
      </ol>
    </nav>
  );
}
