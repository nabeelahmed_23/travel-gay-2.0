import { BsChatRight } from "react-icons/bs";
import { IconContext } from "react-icons";
import { useSession } from "next-auth/react";
import PropTypes from "prop-types";
import { useState, useRef } from "react";
import ReviewReplyPopup from "../reviewsComments/ReviewReplyPopup";
import { getDate } from "../../utils/Helper";

const msgIcon = { className: "text-primary" };
export default function SingleReview({ item, id, isReply, border }) {
  const [replyActive, setReplyActive] = useState(false);
  const [showReply, setShowReply] = useState(false);

  const ref = useRef(null);
  const { status } = useSession();
  return (
    <>
      <div
        className={`flex flex-col md:flex-row md:items-center gap-6 pb-6 ${
          border ? "border-b " : ""
        }`}
      >
        <div className="min-w-[106px]">
          <div className="flex items-center md:block gap-2">
            <div className="flex items-center justify-center w-[80px] h-[80px] rounded-full bg-[#F8E3E8] text-primary text-[42px] font-semibold">
              G
            </div>
            <div className="">
              <div className="text-primary font-semibold max-w-[106px] mt-3">
                Stephan Fry Discoverer
              </div>
              <p className="mt-2 text-sm md:text-base">
                {getDate(item?.created_at, "dd-mmm-yyyy")}
              </p>
            </div>
          </div>
        </div>
        <div className="flex flex-col justify-between gap-6 flex-1">
          <div>
            <p className="text-primary font-semibold">{item?.name}</p>
            <p className="text-sm lg:text-base">{item?.content}</p>
            <div className="flex flex-wrap items-center gap-4 mt-4">
              {item?.media?.length > 0 &&
                item?.media?.map((a, idx) => (
                  <img
                    src={a.original_url}
                    alt={a.name}
                    className="bg-slate-200 w-16 h-16 rounded object-cover"
                    key={`__reviewImage${idx}`}
                  />
                ))}
            </div>
          </div>
          {!isReply && (
            <div className="flex items-center justify-end">
              <div className="flex items-center gap-6">
                <button
                  type="button"
                  className="flex items-center gap-4 text-[#666]"
                  onClick={() => setShowReply(!showReply)}
                >
                  <IconContext.Provider value={msgIcon}>
                    <BsChatRight />
                  </IconContext.Provider>{" "}
                  {item?.replies?.length ?? 0}{" "}
                  {item?.replies?.length > 1 ? "Replies" : "Reply"}
                </button>
                <button
                  className="bg-[#743D7D] py-3 px-6 text-white rounded-lg"
                  type="button"
                  onClick={() => {
                    if (status === "authenticated") {
                      setReplyActive(true);
                    }
                  }}
                >
                  Reply
                </button>
              </div>
            </div>
          )}
        </div>
        {replyActive && (
          <ReviewReplyPopup
            setIsActive={setReplyActive}
            id={id}
            parentId={item?.id}
          />
        )}
      </div>

      <div
        className="w-full transition-[height_200ms_ease-in] overflow-hidden"
        ref={ref}
        style={{
          height: !showReply ? "0px" : `${ref.current.scrollHeight}px`,
        }}
      >
        {item?.replies?.map((item, i) => (
          <div className="pl-16 border-b" key={`__reveiwReplies${i}`}>
            <SingleReview item={item} id={item?.id} isReply border={false} />
          </div>
        ))}
      </div>
    </>
  );
}

SingleReview.propTypes = {
  item: PropTypes.shape().isRequired,
};
