import { BsChatRight } from "react-icons/bs";
import { IconContext } from "react-icons";
import PropTypes from "prop-types";
import { useState, useRef } from "react";
import { getDate } from "../../utils/Helper";
import CommentReplyPopup from "../reviewsComments/CommentReplyPopup";

const msgIcon = { className: "text-primary" };
export default function SingleComment({ item, id, isReply, border }) {
  const [replyActive, setReplyActive] = useState(false);
  const [showReply, setShowReply] = useState(false);

  const ref = useRef(null);
  return (
    <>
      <div
        className={`flex flex-col md:flex-row md:items-center gap-8 pb-6 ${
          border ? "border-b " : ""
        }`}
      >
        <div className="min-w-[106px]">
          <div className="flex items-center md:block gap-2">
            <div className="flex items-center justify-center w-[80px] h-[80px] rounded-full bg-[#F8E3E8] text-primary text-[42px] font-semibold">
              G
            </div>
            <div className="">
              <div className="text-primary font-semibold max-w-[106px] mt-3">
                Stephan Fry Discoverer
              </div>
              <p className="mt-2 text-sm md:text-base">
                {getDate(item?.created_at, "dd-mmm-yyyy")}
              </p>
            </div>
          </div>
        </div>
        <div className="flex flex-col justify-between gap-6 flex-1">
          <div>
            <p className="text-sm lg:text-base">{item?.original_text}</p>
            <div className="flex flex-wrap items-center gap-4 mt-4">
              {item?.media?.length > 0 &&
                item?.media?.map((a, i) => (
                  <img
                    src={a.original_url}
                    alt={a.name}
                    className="bg-slate-200 w-16 h-16 rounded object-cover"
                    key={`__commentImage${i}`}
                  />
                ))}
            </div>
          </div>
          {!isReply && (
            <div className="flex items-center justify-end">
              <div className="flex items-center gap-6">
                <button
                  type="button"
                  className="flex items-center gap-4 text-[#666]"
                  onClick={() => setShowReply(!showReply)}
                >
                  <IconContext.Provider value={msgIcon}>
                    <BsChatRight />
                  </IconContext.Provider>{" "}
                  {item?.nested_comments?.length ?? 0}{" "}
                  {item?.nested_comments?.length > 1 ? "Replies" : "Reply"}
                </button>
                <button
                  className="bg-[#743D7D] py-3 px-6 text-white rounded-lg"
                  type="button"
                  onClick={() => setReplyActive(true)}
                >
                  Reply
                </button>
              </div>
            </div>
          )}
        </div>
        {replyActive && (
          <CommentReplyPopup
            setIsActive={setReplyActive}
            id={id}
            parentId={item?.id}
          />
        )}
      </div>
      <div
        className="w-full transition-[height_200ms_ease-in] overflow-hidden"
        ref={ref}
        style={{
          height: !showReply ? "0px" : `${ref.current.scrollHeight}px`,
        }}
      >
        {item?.nested_comments?.map((a, i) => (
          <div
            className="pl-8 md:pl-16 border-b mt-6"
            key={`__commentList${i}`}
          >
            <SingleComment item={a} id={item?.id} isReply border={false} />
          </div>
        ))}
      </div>
    </>
  );
}
SingleComment.defaultProps = {
  isReply: false,
};
SingleComment.propTypes = {
  item: PropTypes.shape().isRequired,
  id: PropTypes.shape().isRequired,
  isReply: PropTypes.bool,
  border: PropTypes.bool.isRequired,
};
