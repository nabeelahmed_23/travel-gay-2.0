import { useState } from "react";
import { IconContext } from "react-icons";
import Link from "next/link";
import { MdReportProblem } from "react-icons/md";
import PropTypes from "prop-types";
import { useSession } from "next-auth/react";
import ReviewsPopup from "../reviewsComments/ReviewsPopup";
import CommentsPopup from "../reviewsComments/CommentsPopup";
import ReviewWrapper from "./ReviewWrapper";
import CommentWrapper from "./CommentWrapper";

const reportIcon = { className: "text-primary w-6 h-6" };
export default function ReviewsCommentsWrapper({
  reviews,
  comments,
  id,
  reportItem,
}) {
  const [currentTab, setCurrentTab] = useState(1);
  const [isActive, setIsActive] = useState({
    reviews: false,
    comments: false,
  });
  const { status } = useSession();
  return (
    <>
      <div className="flex items-center justify-between">
        <ul className="flex gap-2 md:gap-4 items-start">
          <li>
            <button
              type="button"
              onClick={() => setCurrentTab(1)}
              className={`text-lg md:text-2xl font-semibold border-b-4 ${
                currentTab === 1
                  ? "text-primary border-primary border-[#D74874]"
                  : "border-white text-[#A3A3A3]"
              }`}
            >
              Reviews
            </button>
          </li>
          <li>
            <button
              type="button"
              onClick={() => setCurrentTab(2)}
              className={`text-lg md:text-2xl font-semibold border-b-4 ${
                currentTab === 2
                  ? "text-primary border-primary border-[#D74874]"
                  : "border-white text-[#A3A3A3]"
              }`}
            >
              Comments
            </button>
          </li>
        </ul>
        <div className="flex items-center gap-4">
          <Link
            href="/report-wrong-information"
            className="text-primary font-semibold flex items-center gap-1"
            onClick={() => {
              if (typeof window !== "undefined") {
                localStorage.setItem("reportItem", JSON.stringify(reportItem));
              }
            }}
          >
            <IconContext.Provider value={reportIcon}>
              <MdReportProblem />
            </IconContext.Provider>
            Report
          </Link>
          {currentTab === 1 && (
            <button
              type="button"
              className="py-2 px-2 text-sm md:text-base md:px-5 bg-[#743D7D] text-white rounded-md"
              onClick={() => {
                if (status === "authenticated") {
                  setIsActive((p) => ({ ...p, reviews: true }));
                }
              }}
            >
              Leave review
            </button>
          )}
          {currentTab === 2 && (
            <button
              type="button"
              className="py-2 px-2 text-sm md:text-base md:px-5 bg-[#743D7D] text-white rounded-md"
              onClick={() => {
                if (status === "authenticated") {
                  setIsActive((p) => ({ ...p, comments: true }));
                }
              }}
            >
              Leave comment
            </button>
          )}
        </div>
      </div>
      <div className="mt-6 px-4 py-6 md:px-6 md:py-4 lg:px-10 lg:py-7 2xl:py-9 2xl:px-14 border rounded-lg">
        {currentTab === 1 && <ReviewWrapper reviews={reviews} id={id} />}
        {currentTab === 2 && <CommentWrapper comments={comments} id={id} />}
      </div>
      {isActive.reviews && <ReviewsPopup setIsActive={setIsActive} id={id} />}
      {isActive.comments && <CommentsPopup setIsActive={setIsActive} id={id} />}
    </>
  );
}

ReviewsCommentsWrapper.propTypes = {
  id: PropTypes.shape().isRequired,
};
