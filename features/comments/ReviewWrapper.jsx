import PropTypes from "prop-types";
import SingleReview from "./SingleReview";

export default function ReviewWrapper({ reviews, id }) {
  return (
    <div className="flex flex-col gap-6">
      {reviews?.length > 0 ? (
        reviews?.map((item, i) => (
          <SingleReview item={item} id={id} border key={`_ReviewListing${i}`} />
        ))
      ) : (
        <p className="text-sm italic">No Reviews found.</p>
      )}
    </div>
  );
}

ReviewWrapper.propTypes = {
  reviews: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};
