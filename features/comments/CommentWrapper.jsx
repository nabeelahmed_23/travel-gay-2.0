import PropTypes from "prop-types";
import SingleComment from "./SingleComment";

export default function CommentWrapper({ comments, id }) {
  return (
    <div className="flex flex-col">
      {comments?.length > 0 ? (
        comments?.map((item, i) => (
          <SingleComment
            item={item}
            id={id}
            border
            key={`_CommentListing${i}`}
          />
        ))
      ) : (
        <p className="text-sm italic">No comments found.</p>
      )}
    </div>
  );
}

CommentWrapper.propTypes = {
  comments: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  id: PropTypes.shape().isRequired,
};
