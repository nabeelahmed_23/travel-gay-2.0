import { useState } from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { IconContext } from "react-icons/lib";
import PropTypes from "prop-types";
import InputField from "../../components/Shared/InputField";
import Suggestions from "./Suggestions";

const iconClass = { className: "text-white w-7 h-7" };
export default function SearchPopup({
  handleHotelForm,
  searchInput,
  handleSearch,
  setIsSearchPopupShowing,
  isLoading,
  error,
  suggestions,
  getUrlFromType,
  setSearchInput,
}) {
  const [showDropdown, setShowDropdown] = useState(false);
  return (
    <div className="fixed inset-0 flex justify-center z-[100]">
      <button
        type="button"
        className="fixed inset-0 bg-[rgba(0,0,0,0.7)] pointer-events-auto cursor-default"
        onClick={() => setIsSearchPopupShowing(false)}
      >
        <span className="hidden">Button to close popup</span>
      </button>
      <form
        onSubmit={handleHotelForm}
        className="absolute top-1/3 inset-x-4 lg:inset-x-0 lg:max-w-[800px] lg:mx-auto"
      >
        <div className="grid grid-cols-[1fr_64px] bg-white rounded-md">
          <InputField
            value={searchInput}
            type="text"
            name="search"
            id="search"
            autoComplete="off"
            onChange={handleSearch}
            className="rounded-l-md px-4 md:px-8 flex-1 w-full text-sm md:text-base"
            placeholder="What are you looking for?"
            onBlur={() => {
              setTimeout(() => {
                setShowDropdown(false);
              }, 150);
            }}
            onClick={() => {
              setShowDropdown(true);
            }}
          />
          <div>
            <button
              type="submit"
              className="btn-primary w-16 h-16 flex items-center justify-center rounded-r-md"
            >
              <IconContext.Provider value={iconClass}>
                <HiOutlineSearch />
              </IconContext.Provider>
            </button>
          </div>
        </div>
        {showDropdown && (
          <div className="absolute inset-x-0 top-[4.25rem] bg-white rounded-lg shadow-md max-h-[400px] overflow-auto suggestion-dropdown z-20">
            <Suggestions
              isLoading={isLoading}
              error={error}
              suggestions={suggestions}
              getUrlFromType={getUrlFromType}
              setIsSearchPopupShowing={setIsSearchPopupShowing}
              setSearchInput={setSearchInput}
            />
          </div>
        )}
      </form>
    </div>
  );
}
SearchPopup.defaultProps = {
  error: null,
};

SearchPopup.propTypes = {
  handleHotelForm: PropTypes.func.isRequired,
  searchInput: PropTypes.string.isRequired,
  handleSearch: PropTypes.func.isRequired,
  setIsSearchPopupShowing: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  suggestions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  getUrlFromType: PropTypes.func.isRequired,
  setSearchInput: PropTypes.func.isRequired,
};
