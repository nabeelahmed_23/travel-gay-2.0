import Link from "next/link";
import PropTypes from "prop-types";

export default function Suggestions({
  isLoading,
  error,
  suggestions,
  getUrlFromType,
  setIsSearchPopupShowing,
  setSearchInput,
}) {
  if (isLoading) {
    return <p className="p-6">Loading...</p>;
  }
  if (error) {
    return <p className="p-6">{error}</p>;
  }
  return (
    <div className="p-6">
      <div className="pb-4 border-b mb-4">
        <h2 className="text-primary font-semibold">Places</h2>
        <div className="flex items-center gap-4 flex-wrap mt-1">
          {suggestions?.filter((item) => item.type === "City")?.length > 0 ? (
            suggestions
              ?.filter((item) => item.type === "City")
              ?.slice(0, 5)
              ?.map((item) => (
                <Link
                  className="text-sm"
                  onClick={() => {
                    setIsSearchPopupShowing(false);
                    setSearchInput("");
                  }}
                  href={`${getUrlFromType(item.type)}/${item.slug}`}
                  key={`__citySuggestions${item.id}`}
                >
                  {item.name}
                </Link>
              ))
          ) : (
            <p className="text-sm italic">No places found.</p>
          )}
        </div>
      </div>
      <div className="pb-4 border-b mb-4">
        <h2 className="text-primary font-semibold">Hotels</h2>
        <div className="flex items-center gap-4 flex-wrap mt-1">
          {suggestions?.filter((item) => item.type === "Hotel")?.length > 0 ? (
            suggestions
              ?.filter((item) => item.type === "Hotel")
              ?.slice(0, 5)
              ?.map((item) => (
                <Link
                  className="text-sm"
                  onClick={() => {
                    setIsSearchPopupShowing(false);
                    setSearchInput("");
                  }}
                  href={`${getUrlFromType(item.type)}/${item.slug}`}
                  key={`__hotelSuggestions${item.id}`}
                >
                  {item.name}
                </Link>
              ))
          ) : (
            <p className="text-sm italic">No hotels found.</p>
          )}
        </div>
      </div>
      <div className="pb-4 border-b mb-4">
        <h2 className="text-primary font-semibold">Venues</h2>
        <div className="flex items-center gap-4 flex-wrap mt-1">
          {suggestions?.filter((item) => item.type === "Venue")?.length > 0 ? (
            suggestions
              ?.filter((item) => item.type === "Venue")
              ?.slice(0, 5)
              ?.map((item) => (
                <Link
                  className="text-sm"
                  onClick={() => {
                    setIsSearchPopupShowing(false);
                    setSearchInput("");
                  }}
                  href={`${getUrlFromType(item.type)}/${item.slug}`}
                  key={`__venueSuggestions${item.id}`}
                >
                  {item.name}
                </Link>
              ))
          ) : (
            <p className="text-sm italic">No venues found.</p>
          )}
        </div>
      </div>
      <div className="pb-4 border-b mb-4">
        <h2 className="text-primary font-semibold">Events</h2>
        <div className="flex items-center gap-4 flex-wrap mt-1">
          {suggestions?.filter((item) => item.type === "Event")?.length > 0 ? (
            suggestions
              ?.filter((item) => item.type === "Event")
              ?.slice(0, 5)
              ?.map((item) => (
                <Link
                  className="text-sm"
                  onClick={() => {
                    setIsSearchPopupShowing(false);
                    setSearchInput("");
                  }}
                  href={`${getUrlFromType(item.type)}/${item.slug}`}
                  key={`__eventsSuggestions${item.id}`}
                >
                  {item.name}
                </Link>
              ))
          ) : (
            <p className="text-sm italic">No events found.</p>
          )}
        </div>
      </div>
    </div>
  );
}

Suggestions.defaultProps = {
  error: null,
  suggestions: null,
};

Suggestions.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  suggestions: PropTypes.arrayOf(PropTypes.shape()),
  getUrlFromType: PropTypes.func.isRequired,
  setIsSearchPopupShowing: PropTypes.func.isRequired,
};
