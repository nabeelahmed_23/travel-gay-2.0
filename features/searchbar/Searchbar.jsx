/* eslint-disable no-nested-ternary */
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
// import axios from "axios";
import axios from "axios";
import { HiOutlineSearch } from "react-icons/hi";
import { getGlobalSerachSuggestions } from "../../utils/services/ApiCalls";
import SearchPopup from "./SearchPopup";

export default function Searchbar({ className }) {
  const [searchInput, setSearchInput] = useState("");
  const [isSearchPopupShowing, setIsSearchPopupShowing] = useState(false);
  const [suggestions, setSuggestions] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const router = useRouter();

  function handleHotelForm(e) {
    e.preventDefault();
    setIsSearchPopupShowing(false);
    router.push(`/search${encodeURI("?")}q=${searchInput}`);
    setSearchInput("");
  }

  async function getSuggestionClick(item) {
    setIsLoading(true);
    setError("");
    try {
      const res = await getGlobalSerachSuggestions(
        `global/search/suggestions?q=${item}`,
      );
      setSuggestions(res.data.data);
      setIsLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
      } else {
        setError("No data found regarding the given keyword.");
        setIsLoading(false);
      }
    }
  }
  const handleSearch = (e) => {
    setSearchInput(e.target.value);
  };

  const getUrlFromType = (item) => {
    switch (item) {
      case "Hotel": {
        return "/hotels";
      }
      case "State": {
        return "/destination";
      }
      case "Country": {
        return "/destination";
      }
      case "City": {
        return "/destination";
      }
      case "Venue": {
        return "/venues";
      }
      case "Event": {
        return "/events";
      }
      case "Editorial": {
        return "/editorials";
      }
      case "GroupTrip": {
        return "/gay-group";
      }

      default: {
        return "/hotels";
      }
    }
  };
  useEffect(() => {
    if (searchInput.length > 0) {
      getSuggestionClick(searchInput);
    } else {
      setSuggestions([]);
    }
  }, [searchInput]);

  return (
    <>
      <div className={className}>
        <button
          type="button"
          className="flex items-center rounded px-4 py-3 w-full "
          onClick={() => setIsSearchPopupShowing(true)}
        >
          <span className="flex-1 text-left text-sm lg:text-base text-[#777] inline-block w-[85%] overflow-hidden whitespace-nowrap mr-4 border-r">
            What are you looking for?
          </span>
          <span className="text-primary">
            <HiOutlineSearch />
          </span>
        </button>
      </div>

      {isSearchPopupShowing && (
        <SearchPopup
          handleHotelForm={(e) => handleHotelForm(e)}
          className={className}
          searchInput={searchInput}
          setIsSearchPopupShowing={setIsSearchPopupShowing}
          isLoading={isLoading}
          error={error}
          suggestions={suggestions}
          handleSearch={handleSearch}
          getUrlFromType={getUrlFromType}
          setSearchInput={setSearchInput}
        />
      )}
    </>
  );
}
Searchbar.propTypes = {
  className: PropTypes.string.isRequired,
};
