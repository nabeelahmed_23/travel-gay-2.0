import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import queryString from "query-string";
import PropTypes from "prop-types";
import axios from "axios";
import { hotelSearch } from "../../utils/reducer/hotelReducer";
import HotelInput from "./HotelInput";
import InputField from "../../components/Shared/InputField";
// import SelectInput from "../../components/Shared/SelectInput";
import TgApproveRedLogo from "../../components/Shared/TgApproveRedLogo";
import {
  getDate,
  getQueryFromArrayObject,
  getArrayFromURLParams,
} from "../../utils/Helper";
import { getLocationSuggestions } from "../../utils/services/ApiCalls";
import GuestRoomPopup from "../../components/scb/GuestRoomPopup";

const temp = {
  adults: 2,
  children: [],
};
export default function HotelForm({ scrolled }) {
  const [disabled, setDisabled] = useState(true);
  const [suggestions, setSuggestions] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");
  const [showDropdown, setShowDropdown] = useState(false);
  const [adultRoomDropdown, setAdultRoomDropdown] = useState(false);
  const [members, setMembers] = useState([]);
  const hotelForm = useSelector((state) => state.hotelSearch.value);
  const router = useRouter();
  const dispatch = useDispatch();
  const ref = useRef();

  function handleChange(e) {
    const { name, value } = e.target;
    dispatch(hotelSearch({ name, value }));
  }

  function handleHotelForm(e) {
    e.preventDefault();
    const {
      check_in: checkIn,
      check_out: checkOut,
      q,
      name,
      type,
      rooms,
    } = hotelForm;
    const stringified = queryString.stringify({
      q,
      name,
      type,
      "check-in": checkIn,
      "check-out": checkOut,
    });
    const roomsQuery = getQueryFromArrayObject(rooms);

    router.push(`/search-by-hotel${encodeURI("?") + stringified + roomsQuery}`);
    setIsLoading(false);
  }

  async function suggestionsCall() {
    setIsLoading(true);
    if (hotelForm?.name.length <= 0) {
      setSuggestions({});
      setIsLoading(false);
    }
    setError("");
    try {
      if (hotelForm?.name.length > 0) {
        const { data } = await getLocationSuggestions("search/suggestions", {
          q: hotelForm?.name,
        });

        setSuggestions(data.data.data);
        setIsLoading(false);
      }
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setIsLoading(false);
      setError(error.response.data.message);
    }
  }

  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      setAdultRoomDropdown(false);
    }
  }

  useEffect(() => {
    if (hotelForm?.q && hotelForm?.check_in && hotelForm?.check_out) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [hotelForm]);

  useEffect(() => {
    setIsLoading(false);
    if (!hotelForm?.check_in) {
      if (router.query["check-in"]) {
        dispatch(
          hotelSearch({
            name: "check_in",
            value: router.query["check-in"],
          }),
        );
      } else {
        dispatch(
          hotelSearch({
            name: "check_in",
            value: getDate(new Date(), "yyyy-mm-dd"),
          }),
        );
      }
    }
    if (!hotelForm?.check_out) {
      if (router.query["check-out"]) {
        dispatch(
          hotelSearch({
            name: "check_out",
            value: router.query["check-out"],
          }),
        );
      } else {
        dispatch(
          hotelSearch({
            name: "check_out",
            value: getDate(
              new Date().setDate(new Date().getDate() + 1),
              "yyyy-mm-dd",
            ),
          }),
        );
      }
    }

    const arr = window.location.search
      .slice(1)
      .split("&")
      .filter((item) => item.includes("rooms["));
    if (arr?.length <= 0) {
      setMembers([temp]);
      if (!hotelForm?.rooms) {
        dispatch(
          hotelSearch({
            name: "rooms",
            value: [temp],
          }),
        );
      }
    } else {
      const a = getArrayFromURLParams(arr);
      setMembers(a);
      dispatch(hotelSearch({ name: "rooms", value: a }));
    }

    window.addEventListener("click", handleClickOutside);
    return () => {
      window.removeEventListener("click", handleClickOutside);
    };
  }, []);

  useEffect(() => {
    if (hotelForm?.name === "") setSuggestions({});
    if (hotelForm?.name?.length) suggestionsCall();
  }, [hotelForm?.name, showDropdown]);

  function getSuggestionClick(item) {
    dispatch(hotelSearch({ name: "q", value: item.id }));
    dispatch(hotelSearch({ name: "name", value: item.name }));
    dispatch(hotelSearch({ name: "type", value: item.type }));
  }

  useEffect(() => {
    if (members?.length > 0) {
      dispatch(
        hotelSearch({
          name: "rooms",
          value: [...members],
        }),
      );
    }
  }, [members]);

  function getAdults() {
    const a = hotelForm?.rooms?.reduce((acc, curr) => {
      // eslint-disable-next-line no-param-reassign
      acc += curr.adults;
      return acc;
    }, 0);
    return a;
  }

  return (
    <form
      className="flex-1 grid grid-cols-1 lg:grid-cols-[auto_110px] lg:gap-6 xl:gap-10 w-full lg:w-auto"
      onSubmit={handleHotelForm}
    >
      <div
        className={`grid ${
          scrolled
            ? "lg:grid-cols-[200px_1fr] xl:grid-cols-[300px_1fr] gap-x-2 gap-y-2"
            : "grid-cols-1 gap-y-4"
        }  `}
      >
        <div className="relative">
          <HotelInput
            label="Location"
            control="country_id"
            icon={<img src="/icons/location.svg" alt="" />}
            className={scrolled ? " " : "col-span-3"}
          >
            <InputField
              type="text"
              name="country_id"
              value={hotelForm?.name}
              className={` py-0 px-0 focus:outline-none w-[calc(100%_-_25px)] ml-5 ${
                scrolled ? "text-xs sm:text-sm" : "text-sm"
              }`}
              onChange={(e) =>
                dispatch(hotelSearch({ name: "name", value: e.target.value }))
              }
              placeholder="Where are you going?"
              onBlur={() => {
                setTimeout(() => {
                  setShowDropdown(false);
                }, 150);
              }}
              onClick={() => {
                setShowDropdown(true);
              }}
              // onTouchEnd={() => {
              //   setShowDropdown(true);
              // }}
            />
          </HotelInput>
          {showDropdown ? (
            <div className="absolute inset-x-0 top-16 bg-white rounded-lg shadow-md max-h-[320px] overflow-auto suggestion-dropdown z-20">
              {isLoading && <p className="p-4">Loading...</p>}
              {!isLoading &&
                !error.length &&
                suggestions?.length > 0 &&
                suggestions?.map((item) => (
                  <button
                    type="button"
                    onClick={() => getSuggestionClick(item)}
                    onMouseDown={() => getSuggestionClick(item)}
                    key={item.type + item.id}
                    className="hover:bg-[#743D7D] hover:text-white cursor-pointer w-full"
                  >
                    <div className="flex items-start gap-2 mx-4 border-b py-4 lg:py-2 xl:py-4">
                      <div className="w-[13px]">
                        <img src="/icons/location.svg" alt="" />
                      </div>
                      <span className="w-max text-left text-xs md:text-sm font-semibold">
                        {item?.name}
                      </span>
                    </div>
                  </button>
                ))}
              {error && !isLoading && <p className="p-4 text-sm">{error}</p>}
            </div>
          ) : (
            ""
          )}
        </div>
        <div
          className={`grid relative ${
            scrolled ? "grid-cols-3 gap-2" : "grid-cols-2 lg:grid-cols-3 gap-4"
          }`}
        >
          <HotelInput
            label="Check In"
            control="check_in"
            icon={<img src="/icons/calendar.svg" alt="" />}
            className=""
          >
            <InputField
              type="date"
              name="check_in"
              value={hotelForm?.check_in}
              onChange={(e) => handleChange(e)}
              placeholder="Enter checkin date"
              className={`py-0 px-0 focus:outline-none justify-between  ${
                scrolled
                  ? "w-full xs:w-[calc(100%_-_25px)] xs:ml-6 text-xs sm:text-sm"
                  : "w-[calc(100%_-_25px)] ml-6 text-sm"
              }`}
              min={getDate(new Date(), "yyyy-mm-dd")}
            />
          </HotelInput>
          <HotelInput
            label="Check Out"
            icon={<img src="/icons/calendar.svg" alt="" />}
            control="check_out"
            className=""
          >
            <InputField
              type="date"
              name="check_out"
              value={hotelForm?.check_out}
              min={hotelForm?.check_in}
              onChange={(e) => handleChange(e)}
              placeholder="Enter checkin date"
              className={` py-0 px-0 focus:outline-none justify-between  ${
                scrolled
                  ? "w-full xs:w-[calc(100%_-_25px)] xs:ml-6 text-xs sm:text-sm"
                  : " w-[calc(100%_-_25px)] ml-6 text-sm"
              }`}
            />
          </HotelInput>
          <div
            className={
              !scrolled
                ? "relative col-span-2 lg:col-span-1"
                : "md:relative col-span-1"
            }
            ref={ref}
          >
            <HotelInput
              label="Guests"
              icon={<img src="/icons/users.svg" alt="" />}
              control="adults"
              className={`${
                scrolled ? "col-span-1" : "col-span-2"
              } lg:col-span-1`}
            >
              <InputField
                type="text"
                className={`py-0 px-0 focus:outline-none justify-between  ${
                  scrolled
                    ? "w-full xs:w-[calc(100%_-_25px)] xs:ml-6 text-xs sm:text-sm"
                    : "w-[calc(100%_-_25px)] ml-6 text-sm"
                }`}
                readOnly
                name="adultRoomField"
                placeholder=""
                onChange={() => {}}
                value={`${
                  hotelForm?.rooms?.length
                } Rooms ${getAdults()} Adults`}
                onClick={() => setAdultRoomDropdown(true)}
              />
            </HotelInput>
            {adultRoomDropdown && (
              <div className="absolute z-10 w-[calc(100vw_-_2.5rem)] right-0 md:w-auto inset-x-0 lg:inset-x-[-2rem] 2xl:inset-x-0 bg-white px-6 py-4 shadow-[0_0_8px_rgba(0,0,0,0.15)] rounded-md top-16">
                <GuestRoomPopup members={members} setMembers={setMembers} />
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="flex flex-col justify-between items-center gap-2 lg:gap-4">
        <TgApproveRedLogo
          className={`h-[60px] ${scrolled ? "hidden" : "hidden lg:block"}`}
        />
        <button
          disabled={disabled}
          type="submit"
          className={`bg-[#743D7D] text-white flex-1 rounded-md ${
            scrolled ? "lg:m-1" : "lg:m-[2px]"
          } w-full py-3 mt-4 lg:py-0 ${disabled ? "opacity-70" : ""}`}
        >
          Search
        </button>
      </div>
    </form>
  );
}

HotelForm.propTypes = {
  scrolled: PropTypes.bool.isRequired,
};
