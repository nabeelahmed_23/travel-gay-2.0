import PropTypes from "prop-types";

export default function HotelInput({
  className,
  icon,
  label,
  control,
  children,
}) {
  return (
    <label
      htmlFor={control}
      className={`bg-white py-[6px] px-2 xs:px-3 w-full inline-block rounded ${className}`}
    >
      <div className="flex items-center gap-1">
        {icon}
        <span className="font-semibold inline-block ml-1 text-xs xs:text-sm ">
          {label}
        </span>
      </div>
      {children}
    </label>
  );
}

HotelInput.propTypes = {
  className: PropTypes.string.isRequired,
  control: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  label: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};
