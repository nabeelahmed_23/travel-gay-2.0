// import { IconContext } from "react-icons";
import { useState, useEffect, useRef } from "react";
import TGApprovedLogoWhite from "../../components/Shared/TGApprovedLogoWhite";
import TgApproveRedLogo from "../../components/Shared/TgApproveRedLogo";
import HotelForm from "./HotelForm";

export default function HotelFormWrapper() {
  const [scrolled, setScrolled] = useState(false);
  const [smFormOpen, setSmFormOpen] = useState(false);
  const reff = useRef();

  function makeHotelFormSticky() {
    if (window.scrollY >= 620 && window.innerWidth >= 1024) setScrolled(true);
    else setScrolled(false);
  }

  function removeStickyForm() {
    if (window.innerWidth <= 1024) {
      setScrolled(false);
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", makeHotelFormSticky);
    window.addEventListener("resize", removeStickyForm);
    return () => {
      window.removeEventListener("scroll", makeHotelFormSticky);
      window.removeEventListener("resize", removeStickyForm);
    };
  }, []);
  return (
    <>
      {smFormOpen && (
        <div
          role="button"
          tabIndex={0}
          aria-label="Overlay button"
          className="fixed z-20 inset-0 bg-[rgba(0,0,0,0.5)] lg:hidden"
          onClick={() => setSmFormOpen(false)}
          onKeyDown={() => {}}
        />
      )}
      <div
        ref={reff}
        className={`relative -bottom-1 lg:bottom-auto transition-all ${
          smFormOpen
            ? " h-[430px] md:h-[448px] z-10"
            : "h-[430px] md:h-[448px] z-10"
        } inset-x-0 lg:h-auto`}
      >
        <div
          className={`${
            smFormOpen ? "h-auto md:h-[448px]" : "h-auto md:h-[448px]"
          } lg:h-auto ${
            scrolled
              ? "fixed lg:top-[80px] xl:top-[64px] 2xl:top-[80px] w-full"
              : "lg:relative lg:h-24 md:mt-0"
          }`}
        >
          <div
            className={`transition-all duration-700 h-full lg:h-auto ${
              scrolled
                ? "px-3 2xl:px-12 3xl:px-[102px] py-3 lg:pt-12 xl:py-4 2xl:py-[25px]"
                : "relative md:absolute top-auto left-auto right-auto lg:-top-[5.75rem] md:left-0 md:right-0 w-full max-w-[100%] lg:max-w-[960px] xl:max-w-[1140px] 2xl:max-w-[1312px] mx-auto md:shadow-lg lg:rounded-md lg:rounded-tr-none  py-6 md:py-6 xl:py-6 px-4 md:px-10 lg:px-6 xl:px-8 2xl:px-10"
            } bg-primary`}
          >
            <div className="flex items-center flex-col lg:flex-row gap-3 xl:gap-6">
              <TgApproveRedLogo className="h-16 lg:hidden" />
              <h2
                className={`font-semibold ${
                  scrolled
                    ? "max-w-[185px] 2xl:text-xl text-primary"
                    : "text-primary text-center lg:text-left text-xl sm:text-2xl md:text-3xl lg:text-2xl xl:text-[2rem]  lg:max-w-[230px] xl:max-w-[297px] xl:leading-[48px] "
                }`}
              >
                Book A Travel Gay Approved Hotel
              </h2>
              <HotelForm scrolled={scrolled} />
            </div>
          </div>
        </div>
      </div>

      <div className="fixed lg:hidden inset-x-0 bottom-0 h-[40px] bg-[#743D7D] rounded-t z-10 flex items-center justify-center">
        <button
          type="button"
          onClick={() => {
            const y =
              reff.current.getBoundingClientRect().top +
              window.pageYOffset -
              100;
            window.scrollTo({ top: y });
          }}
          className="flex items-center"
        >
          <TGApprovedLogoWhite className="w-6 " />
          <p className="font-semibold text-sm text-white ml-2">
            Book A TG Approved Hotel Now!
          </p>
        </button>
      </div>
    </>
  );
}
