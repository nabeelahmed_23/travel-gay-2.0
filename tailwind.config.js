/** @type {import('tailwindcss').Config} */

const { screens } = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  content: [
    "./pages/**/*.{jsx,js}",
    "./components/**/*.{jsx,js}",
    "./features/**/*.{js,jsx}",
  ],
  theme: {
    screens: {
      xs: "425px",
      ...screens,
      "3xl": "1920px",
    },
  },
  // eslint-disable-next-line global-require
  plugins: [require("@tailwindcss/forms")],
};
