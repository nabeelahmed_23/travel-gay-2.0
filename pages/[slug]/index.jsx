import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import HTMLReactParser from "html-react-parser";
import Head from "next/head";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import {
  getSingleStaticPage,
  getStaticPages,
} from "../../utils/services/ApiCalls";
import InternalNavigation from "../../components/Shared/internalNavigation/InternalNavigation";
import AsideComunity from "../../components/aside/AsideComunity";
import { extractContent } from "../../utils/Helper";
import HaveWeGotSomething from "../../components/haveWeGotSomething/HaveWeGotSomething";

export default function Index({ data }) {
  const [name, setName] = useState("");

  useEffect(() => {
    setName(extractContent(data?.name));
  }, []);
  return (
    <>
      <Head>
        <title>{name}</title>
      </Head>
      <InternalHeroSection heading={name} description={data?.sub_title} />
      <div className="theme-container px-4 md:px-0 mt-6 md:mt-12">
        <InternalNavigation type="static_data" id={data?.id} />
        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_16px)_calc(30%_-_16px)] gap-4 xl:gap-8 xl:mt-8">
          <div>
            <div className="flex items-center justify-center text-2xl text-primary font-semibold bg-primary h-[175px] w-full">
              ADS
            </div>
            <div className="mt-8 page-content">
              {HTMLReactParser(data?.content)}
            </div>
          </div>
          <div>
            <div className="flex items-center justify-center text-2xl text-primary font-semibold bg-primary h-[300px] w-full">
              ADS
            </div>
            <div className="mt-8 md:mt-16">
              <AsideComunity />
            </div>
          </div>
        </div>
      </div>
      <div className="bg-[#F8F8F9] mt-16">
        <HaveWeGotSomething />
      </div>
    </>
  );
}

Index.propTypes = {
  data: PropTypes.shape().isRequired,
};

export async function getStaticPaths() {
  const res = await getStaticPages("static-pages");
  const params = [];
  res.data.data.forEach((element) =>
    params.push({ params: { slug: element.slug } }),
  );
  return {
    paths: params,
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const { params } = context;
  const res = await getSingleStaticPage(`static-pages/${params.slug}`);

  return {
    props: { data: res.data.data },
    revalidate: 10,
  };
}
