import axios from "axios";
import { useEffect, useState } from "react";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { getAPIUrl, getBaseURL } from "../../../utils/Helper";

export default function Token() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);
  const router = useRouter();

  function handleResetPassword(e) {
    e.preventDefault();
    setIsLoading(true);
    setError({});
    if (
      !email ||
      !password ||
      !confirmPassword ||
      password !== confirmPassword
    ) {
      if (!email) {
        setError((p) => ({ ...p, email: "Email cannot be empty" }));
      }
      if (!password) {
        setError((p) => ({ ...p, password: "Password cannot be empty" }));
      }
      if (!confirmPassword) {
        setError((p) => ({
          ...p,
          confirmPassword: "Confirm Password cannot be empty",
        }));
      }

      if (password !== confirmPassword) {
        setError((p) => ({
          ...p,
          password: "Password and Confirm Password do not match.",
          confirmPassword: "Password and Confirm Password do not match.",
        }));
      }

      setIsLoading(false);
      return;
    }

    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      axios({
        url: `${getAPIUrl()}reset-password`,
        method: "post",
        data: {
          token: router.query.token,
          email,
          password,
          password_confirmation: confirmPassword,
        },
        headers: {
          Accept: "application/json",
        },
        withCredentials: true,
      })
        .then((res) => {
          setIsLoading(false);
          setSuccessMessage(res.data.data);
        })
        .catch((err) => {
          const a = err.response.data.message;
          Object.keys(a)?.forEach((item) =>
            setError((p) => ({
              ...p,
              [item]: a[item][0],
            })),
          );
          setIsLoading(false);
        });
    });
  }

  useEffect(() => {
    const { email } = router.query;
    if (email) setEmail(email);
  }, [router]);
  return (
    <div className="fixed inset-0 bg-[rgba(0,0,0,0.8)] z-50">
      <Head>
        <title>Reset Password | TravelGay</title>
      </Head>
      <div className="fixed inset-0 bg-[rgba(0,0,0,0.7)] max-w-[568px] h-max max-h-[90vh] m-auto overflow-auto">
        <div className="bg-white mx-4 md:mx-0 rounded">
          <div className="px-4 py-10 md:px-6 md:py-14 lg:px-[100px] relative">
            <h1 className="font-semibold text-xl lg:text-2xl xl:text-[32px]">
              Set a new password
            </h1>
            <p className="text-xs lg:text-sm mt-2">
              Use a minimum of 10 characters, including uppercase letters,
              lowercase letters and numbers.
            </p>
            <form onSubmit={handleResetPassword}>
              <label htmlFor="email" className="flex flex-col mt-4 lg:mt-6">
                <span>Email</span>
                <input
                  type="email"
                  name="email"
                  id="email"
                  className="border border-slate-200 border-solid mt-1 rounded-lg"
                  placeholder="johndoe@example.com"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {error?.email && (
                  <p className="text-red-500 text-xs my-1">{error.email}</p>
                )}
              </label>
              <label htmlFor="password" className="flex flex-col mt-2">
                <span>Password</span>
                <input
                  type="password"
                  name="password"
                  id="password"
                  className="border border-slate-200 border-solid mt-1 rounded-lg"
                  placeholder="********"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
                {error?.password && (
                  <p className="text-red-500 text-xs my-1">{error.password}</p>
                )}
              </label>
              <label htmlFor="confirmPassword" className="flex flex-col mt-2">
                <span>Confirm Password</span>
                <input
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  className="border border-slate-200 border-solid mt-1 rounded-lg"
                  placeholder="********"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
                {error?.confirmPassword && (
                  <p className="text-red-500 text-xs my-1">
                    {error.confirmPassword}
                  </p>
                )}
              </label>
              <button
                type="submit"
                disabled={isLoading}
                className={`w-full text-center py-3 bg-[#743D7D] rounded-lg text-white text-lg mt-2 ${
                  isLoading ? "opacity-70" : ""
                }`}
              >
                {isLoading ? "Loading..." : "Reset Password"}
              </button>
              {successMessage && (
                <div className="text-center mt-4">
                  <p className="text-green-500 text-sm">{successMessage}.</p>
                  <p className="text-sm">
                    Go to{" "}
                    <Link href="/" className="underline">
                      Homepage
                    </Link>
                  </p>
                </div>
              )}
              {error?.token && (
                <p className="text-red-500 text-xs my-1">{error.token}</p>
              )}
            </form>

            <p className="mt-4 lg:mt-8 pt-4 border-t text-xs md:text-sm text-center ">
              By signing in or creating an account, you agree with our{" "}
              <Link legacyBehavior href="#">
                <a className="text-blue-500"> Terms & conditions </a>
              </Link>
              and{" "}
              <Link legacyBehavior href="#">
                <a className="text-blue-500"> Privacy statement</a>
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
