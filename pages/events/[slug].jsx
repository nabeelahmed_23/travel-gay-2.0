import { useRef, useEffect, useState } from "react";
import Head from "next/head";
// import Link from "next/link";
import Rate from "rc-rate";
import { IconContext } from "react-icons";
import PropTypes from "prop-types";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import parse from "html-react-parser";
import { HiOutlineArrowSmLeft, HiOutlineArrowSmRight } from "react-icons/hi";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import InternalNavigation from "../../components/Shared/internalNavigation/InternalNavigation";
import HotelSlider from "../../components/Shared/hotelSlider/HotelSlider";
import FeaturedSlider from "../../components/Shared/Featured/FeaturedSlider";
import { getEventsDetails } from "../../utils/services/ApiCalls";
import calls from "../../utils/services/PromiseHandler/PromiseHandler";
import AsideComunity from "../../components/aside/AsideComunity";
import AsideSearchHotel from "../../components/aside/AsideSearchHotel";
import Map from "../../features/map/Map";
import ReviewsCommentsWrapper from "../../features/comments/ReviewsCommentsWrapper";
import { getDate } from "../../utils/Helper";
import HotelImagePopup from "../../components/Shared/HotelImagePopup";

const sliderButton = {
  className: "text-[#743D7D] h-6 w-6 group-hover:text-white ",
};
export default function Slug({ data }) {
  const [trendingHotels, setTrendingHotels] = useState([]);
  const [imagePopup, setImagePopup] = useState(false);
  const hotelRef = useRef(null);
  const id = { event_id: data?.id };
  const trips = [
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip1.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip2.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip3.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip4.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
  ];

  const eventVenues = [
    {
      image: { original_url: "/homepage/party1.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party2.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party1.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party2.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
  ];

  // const handleChangeTab = (count) => setActiveTab(count);

  function handleSlider(ref, dir) {
    ref.current.splide.go(dir);
  }

  // const getAxtiveTabText = () => {
  //   if (activeTab === 1) {
  //     return (
  //       <>
  //         <p className="text-xs leading-5">
  //           Las Vegas pretty much invented the concert residency. Artists can
  //           pull in big bucks without the added cost of touring and an endless
  //           flood of tourists to entertain.
  //         </p>
  //         <p className="text-xs leading-5 mt-4">
  //           Celine Dion, Elton John, Janet Jackson, Bette Midler, Bruno Mars,
  //           Mariah Carey and many more have performed recent Vegas residencies
  //           at the various hotels and casinos, such as Caesar’s Palace.
  //         </p>
  //         <p className="text-xs leading-5 mt-4">
  //           Tickets can be pricey and the biggest artists walk away with more
  //           money than some of America’s highest-paid CEOs.
  //         </p>
  //       </>
  //     );
  //   }
  //   if (activeTab === 2) {
  //     return (
  //       <p className="text-xs leading-5">
  //         Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias
  //         ratione, obcaecati repudiandae sed quae odio e ibus nemo deserunt!
  //         Atque molestiae parietur adipisicing elit. Molestias ratione,
  //         obcaecati repudiandae sed quae odio e ibus nemo deserunt! Atque
  //         molestiae pari
  //       </p>
  //     );
  //   }

  //   return (
  //     <p className="text-xs leading-5">
  //       sectetur adipisicing elit. Molestias ratione, obcaecati repudiandae sed
  //       quae odio e ibus nemo deserunt! Atque molestiae parietur adipisicing
  //       elitsectetur adipisicing elit. Molestias ratione, obcaecati repudiandae
  //       sed quae odio e ibus nemo deserunt! Atque molestiae parietur adipisicing
  //       elit
  //     </p>
  //   );
  // };

  const apiCall = (url) => calls(url, null, null);

  const fetchHotels = async () => {
    const {
      data: { data: _trendingHotels },
    } = await apiCall(`events/${data?.slug}/hotels`);
    setTrendingHotels(_trendingHotels ?? []);
  };
  // const fetchGroupTrips = async () => {
  //   const {
  //     data: { data: _groupTrips },
  //   } = await apiCall(`states/${data?.slug}/group-trips`);
  //   setGroupTrips(_groupTrips ?? []);
  // };

  const fetchData = async () => {
    fetchHotels();
  };

  useEffect(() => {
    if (data?.slug) {
      fetchData();
    }
  }, [data?.slug]);

  return (
    <div>
      <Head>
        <title>Events Detail | TravelGay</title>
      </Head>
      <InternalHeroSection
        heading={data?.name ?? ""}
        description={data?.sub_title ?? ""}
        heroImage={data?.image?.original_url}
      />
      <div className="theme-container px-4 md:px-0 xl:mt-5">
        <InternalNavigation id={data?.id} type="event" />
        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_8px)_calc(30%_-_8px)] gap-4 xl:gap-16 xl:mt-8">
          <div>
            <div className="-mx-4 md:mx-0 h-48 w-[calc(100%_+_2rem)] md:w-full font-semibold flex justify-center items-center text-primary mb-5 bg-primary ">
              ADS
            </div>

            <div className="mt-[34px] lg:mt-[70px]">
              <div className="flex flex-col xl:flex-row xl:items-start xl:justify-between gap-4">
                <div>
                  {data?.name && (
                    <div className="items-start md:inline-flex justify-between xl:gap-3 ">
                      <h2 className="text-primary text-[20px] md:text-[28px] font-bold">
                        {data?.name}
                      </h2>

                      <div className="w-[130px]">
                        <Rate
                          count={5}
                          value={4}
                          style={{ fontSize: 24 }}
                          isHalf
                          edit={false}
                          character={<i className="anticon anticon-star" />}
                          className="w-full"
                        />
                      </div>
                    </div>
                  )}
                  {data?.address && (
                    <div className="flex items-center gap-2 text-sm md:text-base mt-2 md:mt-0">
                      <img src="/icons/location.svg" alt="Location" />{" "}
                      {data?.address}
                    </div>
                  )}
                </div>
                <div className="hidden xl:block">
                  <p className="font-semibold text-[#743D7D] md:text-end w-max">
                    {new Date(data?.recuringDates?.[0].start_date).getDate()}-
                    {getDate(data?.recuringDates?.[0].end_date, "dd mmmm yyyy")}
                  </p>
                  {data?.updated_at && (
                    <p className="text-xs w-max ">
                      Updated at: {getDate(data?.updated_at, "dd mmm yyyy")}
                    </p>
                  )}
                </div>
              </div>
              <section className="mt-3 md:mt-6">
                <div className="hidden md:grid grid-cols-1 md:grid-cols-4 md:grid-row-2 gap-4 md:max-h-[340px] mt-6">
                  {data?.media?.slice(0, 5).map((item, i) => (
                    <div
                      key={item.uuid}
                      className="first:md:col-span-2 first:md:row-span-2 first:md:max-h-[330px] [&:not(:nth-child(1))]:hidden md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[150px]"
                    >
                      {data?.media?.length > 5 && i === 4 ? (
                        <div className="relative h-full">
                          <img
                            src={item?.original_url}
                            alt=""
                            className=" rounded-lg w-full h-full object-cover"
                          />
                          <button
                            type="button"
                            onClick={() => setImagePopup(true)}
                            className="absolute inset-0 bg-[rgba(0,0,0,0.4)] rounded-lg items-center justify-center font-semibold text-white text-4xl hidden md:flex"
                          >
                            {(data?.media?.length ?? 0) - 5}+
                          </button>
                        </div>
                      ) : (
                        <img
                          src={item?.original_url}
                          alt=""
                          className=" rounded-lg w-full h-full object-cover"
                        />
                      )}
                    </div>
                  ))}
                </div>
                <div className="md:hidden">
                  {data?.media && (
                    <Splide
                      options={{
                        type: "loop",
                        perPage: 1,
                        autoplay: true,
                        rewind: true,
                        arrows: false,
                        gap: "0.5rem",
                        pagination: false,
                      }}
                    >
                      {data?.media?.map((item) => (
                        <SplideSlide key={item.uuid} className="max-h-[216px]">
                          <img
                            src={item?.original_url}
                            alt={data?.name}
                            className="rounded-lg w-full h-full object-cover"
                          />
                        </SplideSlide>
                      ))}
                    </Splide>
                  )}
                </div>
              </section>

              <div className="eventContent mt-6 text-[13px]">
                {parse(data?.content ?? "")}
              </div>

              <div className="xl:hidden flex items-center justify-between">
                <p className="font-semibold text-[#743D7D] md:text-end w-max">
                  {new Date(data?.recuringDates?.[0].start_date).getDate()}-
                  {getDate(data?.recuringDates?.[0].end_date, "dd mmmm yyyy")}
                </p>
                {data?.updated_at && (
                  <p className="text-xs w-max ">
                    Updated at: {getDate(data?.updated_at, "dd mmm yyyy")}
                  </p>
                )}
              </div>

              <button
                type="button"
                className="bg-[#743D7D] text-white font-semibold rounded flex items-center justify-center gap-4 w-full py-3 mt-3 xl:mt-5"
              >
                <img src="/icons/tg-logo-white.svg" alt="" />
                <span>Book Hotel For This Event</span>
              </button>
            </div>
            <div className="flex mt-8 ">
              <div className="rounded-md flex flex-col bg-primary  p-4 items-center add-hotel-rating w-full md:w-auto">
                <span className="text-xl md:text-base font-semibold text-[#743D7D]">
                  Rate the Tg Gay hamburg
                </span>
                <Rate allowHalf />
              </div>
            </div>
            <section
              className="theme-container px-4 md:px-0 mt-8"
              id="location"
            >
              <div className="relative">
                {data?.latitude && data?.longitude && (
                  <Map
                    lat={data?.latitude}
                    lng={data?.longitude}
                    name={data?.name}
                    address={data?.address}
                    className="w-full h-[400px]"
                  />
                )}
                {data?.latitude && data?.longitude && (
                  <h3 className="mt-3 font-bold">
                    <span className="opacity-60"> Book your hotel early –</span>
                    <span className="ml-1 text-primary underline">
                      check our top San Francisco hotels for gay travelers.
                    </span>
                  </h3>
                )}
              </div>
            </section>

            <div className="md:mt-[72px] w-full font-[600] flex justify-center items-center text-primary my-5 md:h-[200px] bg-primary h-[304px]">
              ADS
            </div>

            <div className="mt-6 xl:mt-[3rem]">
              <div className="flex justify-between items-center mb-3">
                <h2 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4">
                  Featured Hotels
                </h2>

                <div className="block">
                  <button
                    onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>

                  <button
                    onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>

              <HotelSlider
                noDataMessage="No Data Found"
                reff={hotelRef}
                data={trendingHotels}
                loc="notHome"
              />
            </div>
            <div className="mt-8 lg:mt-16">
              <ReviewsCommentsWrapper
                id={id}
                reviews={data?.reviews ?? []}
                comments={data?.comments ?? []}
                reportItem={{ id: data?.id, name: data?.name }}
              />
            </div>
            <div className="flex md:my-[72px] w-full font-[600] justify-center items-center text-primary my-5 md:h-[454px] bg-primary">
              ADS
            </div>
          </div>
          <aside className="xl:w-[calc(100%_-_64px)]">
            <div className="mb-5">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                Community Photos
              </h2>
              <div className="gap-2 flex mt-3">
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px]"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px]"
                    alt="Community"
                  />
                </div>
              </div>
            </div>
            <AsideSearchHotel heading=" Search Hotels by Area" data={trips} />
            <AsideComunity />
            <div className="mt-8 mb-12">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                San Francisco Events
              </h2>
              <FeaturedSlider data={eventVenues} sidebar />
            </div>

            <div className="mb-8 h-36 bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
            <div className="my-10">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                More Gay San Francisco
              </h2>
              <FeaturedSlider data={eventVenues} sidebar />
            </div>
          </aside>
        </div>
      </div>
      {imagePopup && (
        <HotelImagePopup media={data?.media} setImagePopup={setImagePopup} />
      )}
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await getEventsDetails(`events/${context.query.slug}`);
    return {
      props: {
        data: res.data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        error: error.message,
      },
    };
  }
}

Slug.propTypes = {
  data: PropTypes.shape().isRequired,
};
