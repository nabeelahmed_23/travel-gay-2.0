import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { signIn, useSession } from "next-auth/react";
import Script from "next/script";
import HeaderWrapper from "../components/header/HeaderWrapper";
import MainFooter from "../components/footer/MainFooter";

export default function Layout({ children }) {
  const [mounted, setMounted] = useState(false);
  const session = useSession();
  const onOneTapSignedIn = async (response) => {
    const { credential } = response;
    signIn("credentials", {
      token_id: credential,
      redirect: false,
      origin: "oneTap",
    });
  };

  const initializeGsi = async () => {
    window?.google.accounts.id.initialize({
      client_id: process.env.GOOGLE_CLIENT_ID,
      callback: onOneTapSignedIn,
    });
    window?.google.accounts.id.prompt(() => {
      // console.log(notification);
    });
  };

  useEffect(() => {
    if (session.status === "unauthenticated") {
      setTimeout(() => {
        if (window?.google) {
          initializeGsi();
        }
      }, 2000);
    }
  });
  useEffect(() => {
    setMounted(true);
  }, []);
  return (
    <>
      <HeaderWrapper />
      <Script src="https://accounts.google.com/gsi/client" />
      <main>{children}</main>
      {mounted && <MainFooter />}
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
