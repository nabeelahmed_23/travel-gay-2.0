import { useEffect, useRef, useState } from "react";
import Head from "next/head";
import Link from "next/link";
import PropTypes from "prop-types";
import HeroSection from "../components/home/HeroSection";
import HotelFormWrapper from "../features/hotelBookingForm/HotelFormWrapper";
import CarouselWrapper from "../features/carousel/CarouselWrapper";
import PartiesEvents from "../components/home/PartiesEvents";
import FeaturedNews from "../components/home/FeaturedNews";
import TravelFaqs from "../components/home/TravelFaqs";
import PopularPlace from "../components/home/PopularPlace";
import { homepage } from "../utils/services/ApiCalls";

export default function Home({ data, error }) {
  const [margin, setMargin] = useState(25);
  const ref = useRef();

  function getHeightBanner() {
    if (window.innerWidth <= 1023) {
      setMargin(ref.current?.scrollHeight);
    } else setMargin(25);
  }

  useEffect(() => {
    getHeightBanner();
    window.addEventListener("resize", getHeightBanner);
    return () => {
      window.addEventListener("resize", getHeightBanner);
    };
  }, []);

  return (
    <>
      <Head>
        <title>Home | TravelGay</title>
        <meta name="title" content={data?.meta_title} />
        <meta name="description" content={data?.meta_description} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <>
        <HeroSection data={data} error={error} />
        <div className="w-full relative theme-container -bottom-[46px] lg:bottom-[92px] transition-all duration-700">
          <div className="absolute bottom-0 left-0 lg:left-auto right-0 h-[111px] px-4 lg:px-0">
            <div ref={ref}>
              <PopularPlace />
              <div className="bg-secondary  p-3 lg:hidden rounded-br-md rounded-bl-md">
                {error ? (
                  <div className="mt-6">
                    <p className="text-red-500 text-sm text-center">{error}</p>
                  </div>
                ) : (
                  <h1 className="capitalize text-primary text-[28px] font-bold max-w-[260px] mx-auto text-center">
                    {data?.banner_title}
                  </h1>
                )}
                <p className="text-sm max-w-[310px] mx-auto mt-2 text-center capitalize">
                  {data?.banner_content}
                </p>
                <div className="flex items-center flex-wrap justify-center gap-x-4 max-w-[310px] mx-auto mt-6">
                  {data?.cities.slice(0, 3).map((item) => (
                    <Link
                      legacyBehavior
                      href={`/city/${item?.slug}`}
                      key={item.id}
                    >
                      <a className="text-black font-medium underline text-xs">
                        {item.label}
                      </a>
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="lg:mt-0" style={{ marginTop: margin - 25 }}>
          <HotelFormWrapper />
        </div>
        <div className="block pl-4">
          <CarouselWrapper />
        </div>
        <PartiesEvents />
        <FeaturedNews data={data} />
        <TravelFaqs data={data} />
      </>
    </>
  );
}

Home.propTypes = {
  data: PropTypes.objectOf(
    PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.arrayOf(
        PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.number,
          PropTypes.shape([]),
        ]),
      ),
    ]),
  ).isRequired,
  error: PropTypes.string.isRequired,
};

export async function getServerSideProps() {
  try {
    const res = await homepage("homepage");
    return {
      props: { data: res.data.data, error: "" },
    };
  } catch (error) {
    return {
      props: { error: "Something went wrong. Please try again." },
    };
  }
}
