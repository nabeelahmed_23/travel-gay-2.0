import Head from "next/head";
// import axios from "axios";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import InternalHeroSection from "../components/Shared/internalHero/InternalHeroSection";
import { getGlobalSearch } from "../utils/services/ApiCalls";
import EditorialTab from "../components/SearchTab/EditorialTab";
import EventsTab from "../components/SearchTab/EventsTab";
import VenueBookingTab from "../components/SearchTab/venueTab";
import HotelTab from "../components/SearchTab/HotelsTab";
import BarTab from "../components/SearchTab/BarTab";
import ProfileTab from "../components/SearchTab/ProfileTab";

export default function GlobalSearch() {
  const [searchResult, setSearchResult] = useState([]);
  const [activeTab, setActiveTab] = useState(1);
  const router = useRouter();

  const handleChangeTab = (count) => setActiveTab(count);
  const getAxtiveTabText = () => {
    if (activeTab === 1) {
      return (
        <EditorialTab
          // editorial={editorialsData}
          editorial={searchResult?.filter((item) => item.type === "Editorial")}
        />
      );
    }
    if (activeTab === 2) {
      return (
        <EventsTab
          // data={thisWeekEvents}
          data={searchResult?.filter((item) => item.type === "Event")}
        />
      );
    }
    if (activeTab === 3) {
      return (
        <VenueBookingTab
          // data={venues}
          data={searchResult?.filter((item) => item.type === "Venue")}
        />
      );
    }
    if (activeTab === 4) {
      return (
        <ProfileTab
          // data={profiles}
          data={searchResult?.filter((item) => item.type === "Profile")}
        />
      );
    }
    if (activeTab === 5) {
      return (
        <BarTab
          // data={editorialsData}
          data={searchResult?.filter((item) => item.type === "Bar")}
        />
      );
    }
    if (activeTab === 6) {
      return (
        <HotelTab
          // data={venues}
          data={searchResult?.filter((item) => item.type === "Hotel")}
        />
      );
    }
    return (
      <EditorialTab
        // editorial={editorialsData}
        editorial={searchResult?.filter((item) => item.type === "Editorial")}
      />
    );
  };

  async function getGlobalSearchData() {
    setSearchResult([]);

    try {
      const res = await getGlobalSearch(
        `global/search/results?q=${router.query.q}`,
      );

      setSearchResult(res.data.data);
    } catch (error) {
      // if (axios.isCancel(error)) {
      //   return;
      // }
      console.log(error);
    }
  }
  useEffect(() => {
    if (router.query?.q) {
      getGlobalSearchData();
    }
  }, [router.query.q]);
  return (
    <div>
      <Head>
        <title>Search | TravelGay</title>
      </Head>
      <InternalHeroSection
        heading="Search results "
        description={`You searched for ${router.query.q}`}
      />
      <div className="theme-container px-2 md:px-4 xl:mt-5">
        <div className="flex justify-center">
          <div className="px-0 py-4 md:px-4 flex flex-wrap gap-2 items-center ">
            <button
              type="button"
              onClick={() => handleChangeTab(1)}
              className={`text-xs lg:text-sm px-4 py-[10px] ${
                activeTab === 1
                  ? "bg-[#D74874] text-white"
                  : "text-primary bg-primary"
              }  rounded-md inline-block`}
            >
              Editorials
            </button>
            <button
              type="button"
              onClick={() => handleChangeTab(2)}
              className={`text-xs lg:text-sm px-4 py-[10px] ${
                activeTab === 2
                  ? "bg-[#D74874] text-white"
                  : "text-primary bg-primary"
              }  rounded-md inline-block`}
            >
              Events
            </button>
            <button
              type="button"
              onClick={() => handleChangeTab(3)}
              className={`text-xs lg:text-sm px-4 py-[10px] ${
                activeTab === 3
                  ? "bg-[#D74874] text-white"
                  : "text-primary bg-primary"
              }  rounded-md inline-block`}
            >
              Venues
            </button>
            <button
              type="button"
              onClick={() => handleChangeTab(4)}
              className={`text-xs lg:text-sm px-4 py-[10px] ${
                activeTab === 4
                  ? "bg-[#D74874] text-white"
                  : "text-primary bg-primary"
              }  rounded-md inline-block`}
            >
              Profiles
            </button>
            <button
              type="button"
              onClick={() => handleChangeTab(5)}
              className={`text-xs lg:text-sm px-4 py-[10px] ${
                activeTab === 5
                  ? "bg-[#D74874] text-white"
                  : "text-primary bg-primary"
              }  rounded-md inline-block`}
            >
              Bars
            </button>
            <button
              type="button"
              onClick={() => handleChangeTab(6)}
              className={`text-xs lg:text-sm px-4 py-[10px] ${
                activeTab === 6
                  ? "bg-[#D74874] text-white"
                  : "text-primary bg-primary"
              }  rounded-md inline-block`}
            >
              Hotels
            </button>
          </div>
        </div>
        <div className="xl:mt-8">
          <div className="px-0 md:px-4 mb-[34px] lg:mb-[70px]">
            <div className="mt-3">{getAxtiveTabText()}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

// const thisWeekEvents = [
//   {
//     image: "/images/event-pic.png",
//     title: "440 Castro",
//     description: [
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//     ],
//   },
//   {
//     image: "/images/event-pic.png",
//     title: "440 Castro",
//     description: [
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//     ],
//   },
//   {
//     image: "/images/event-pic.png",
//     title: "440 Castro",
//     description: [
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//       "Wednesday : Musical Wednesdays - VJ &",
//     ],
//   },
//   {
//     image: "/images/event-pic.png",
//     title: "440 Castro",
//     description: ["Wednesday : Musical Wednesdays - VJ & "],
//   },
//   {
//     image: "/images/event-pic.png",
//     title: "440 Castro",
//     description: ["Wednesday : Musical Wednesdays - VJ & ", "Rashid 1234"],
//   },
//   {
//     image: "/images/event-pic.png",
//     title: "440 Castro",
//     description: ["Wednesday : Musical Wednesdays - VJ & "],
//   },
// ];
// const editorialsData = [
//   {
//     image: {
//       original_url: "/homepage/Rectangle440.jpg",
//     },
//     name: "Axel Hotel Barcelona",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle441.jpg",
//     },
//     name: "Elysium Hotel",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle443.jpg",
//     },
//     name: "Foxberry Inn",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle440.jpg",
//     },
//     name: "Axel Hotel Barcelona",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle441.jpg",
//     },
//     name: "Elysium Hotel",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle443.jpg",
//     },
//     name: "Foxberry Inn",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle440.jpg",
//     },
//     name: "Axel Hotel Barcelona",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle441.jpg",
//     },
//     name: "Elysium Hotel",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
//   {
//     image: {
//       original_url: "/homepage/Rectangle443.jpg",
//     },
//     name: "Foxberry Inn",
//     sub_title:
//       "Experience London’s amazing gay bar scene with something to suit everyone.. Read More",
//   },
// ];

// const venues = [
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 3.2,
//     reviews: 22,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 4.5,
//     reviews: 342,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 3.5,
//     reviews: 342,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 4.1,
//     reviews: 34,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 3.2,
//     reviews: 45,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 4.5,
//     reviews: 234,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 3.5,
//     reviews: 456,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 4.1,
//     reviews: 456,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
//   {
//     first_media: "/homepage/hotelimage.jpg",
//     name: "Gay Las Vegas Hotel",
//     content: `Situated on the Las Vegas strip, a popular location
//     for nightlife, New York-New York so you’ll have venues like
//      Piranha ...`,
//     star_rating: 4.1,
//     reviews: 23,
//     address: "5012 S Arville St #4, Las Vegas, USA",
//   },
// ];
// const profiles = [
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "John",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "John",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "John",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
//   {
//     name: "Gay Las Vegas",
//     user_name: "doe",
//   },
// ];
