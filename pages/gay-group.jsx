import Head from "next/head";
import { useState, useEffect } from "react";
import axios from "axios";
import {
  getContinents,
  getCountries,
  getGroupTrips,
} from "../utils/services/ApiCalls";
import ContinentGrid from "../components/groupTrips/ContinentGrid";
import GroupTripHeader from "../components/groupTrips/GroupTripHeader";
import CountryFilter from "../components/groupTrips/CountryFilter";
import MonthFilter from "../components/groupTrips/MonthFilter";
import GroupTripFilterResultWrapper from "../components/groupTrips/GroupTripFilterResultWrapper";
import Loader from "../components/Shared/Loader";

export default function GayGroup() {
  const [isActive, setIsActive] = useState({
    country: false,
    month: false,
  });
  const [countries, setCountries] = useState([]);
  const [filterData, setFilterData] = useState({
    country: [],
    month: [],
  });
  const [isLoading, setIsLoading] = useState(true);
  const [continents, setContinents] = useState([]);
  const [groupTrips, setGroupTrips] = useState([]);
  const [error, setError] = useState("");
  const [isGroupTripLoading, setIsGroupTripLoading] = useState(false);

  async function getAllGroupTrips() {
    setIsGroupTripLoading(true);
    try {
      const res = await getGroupTrips("grouptrips", null, {
        country_id: filterData.country,
        month: filterData.month,
      });
      setGroupTrips(res.data.data);
      setIsGroupTripLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setIsGroupTripLoading(false);
      setError(error?.message);
    }
  }

  async function getAllCountries() {
    try {
      const res = await getCountries("countries");
      setCountries(res.data.data);
    } catch (error) {
      console.log(error.message);
    }
  }

  function handleCheckBoxSelect(e, filterName) {
    const { id, checked } = e.target;
    const { country, month } = filterData;
    if (filterName === "country") {
      const data = e.target.attributes.data.value;
      if (checked) {
        setFilterData((p) => ({
          ...p,
          country: [...country, data],
        }));
      } else {
        setFilterData((p) => ({
          ...p,
          country: country.filter((i) => i !== data),
        }));
      }
      return;
    }
    if (checked) {
      setFilterData((p) => ({
        ...p,
        month: [...month, id],
      }));
    } else {
      setFilterData((p) => ({
        ...p,
        month: month.filter((i) => i !== id),
      }));
    }
  }

  async function getAllContinents() {
    setIsLoading(true);
    try {
      const res = await getContinents("continents");
      setContinents(res.data.data);
      setIsLoading(false);
    } catch (error) {
      console.log(error.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getAllCountries();
    getAllContinents();
  }, []);

  useEffect(() => {
    getAllGroupTrips();
  }, [filterData]);

  if (isLoading) {
    return <Loader text="Please wait while we prepare the page." />;
  }
  return (
    <>
      <Head>
        <title>Group Trips | TravelGay</title>
      </Head>
      <GroupTripHeader text="Gay Group Trips: Around The World" />
      <div className="theme-container px-4 md:px-0 mt-28">
        <ContinentGrid continents={continents} />
        <div className="grid grid-cols-1 xl:grid-cols-[350px_1fr] gap-x-14 mt-16">
          <div>
            <h2 className="font-semibold text-2xl">Filter Events</h2>
            <CountryFilter
              isActive={isActive}
              setIsActive={setIsActive}
              countries={countries}
              handleCheckBoxSelect={(e) => handleCheckBoxSelect(e, "country")}
              filterData={filterData.country}
            />
            <MonthFilter
              isActive={isActive}
              setIsActive={setIsActive}
              handleCheckBoxSelect={(e) => handleCheckBoxSelect(e, "month")}
              filterData={filterData.month}
            />
            <button
              type="button"
              className="w-full text-center py-3 bg-[#743D7D] font-semibold text-white rounded"
              onClick={() => setFilterData({ country: [], month: [] })}
            >
              Reset Filter
            </button>
          </div>
          <div className="mt-6 xl:mt-0">
            <GroupTripFilterResultWrapper
              groupTrips={groupTrips}
              error={error}
              isLoading={isGroupTripLoading}
            />
          </div>
        </div>
      </div>
    </>
  );
}
