import Head from "next/head";
import { useState, useEffect } from "react";
import axios from "axios";
import InternalHeroSection from "../components/Shared/internalHero/InternalHeroSection";
import MonthFilter from "../components/groupTrips/MonthFilter";
import { getCities, getPrides } from "../utils/services/ApiCalls";
import PrideListingWrapper from "../components/EventCard/PrideListingWrapper";
import Pagination from "../components/Shared/Pagination";
import CityFilter from "../components/groupTrips/CityFilter";

export default function GayPrideCalendar() {
  const [isActive, setIsActive] = useState({
    cities: false,
    month: false,
  });
  const [cities, setCities] = useState([]);
  const [filterData, setFilterData] = useState({
    cities: [],
    month: [],
  });
  const [prides, setPrides] = useState([]);
  const [isPrideLoading, setIsPrideLoading] = useState(true);
  const [error, setError] = useState("");
  const [meta, setMeta] = useState({});
  const [paginationId, setPaginationId] = useState(1);

  async function getAllCities() {
    try {
      const res = await getCities("cities");
      setCities(res.data.data);
    } catch (error) {
      console.log(error.message);
    }
  }

  async function getAllPrides() {
    setIsPrideLoading(true);
    try {
      const res = await getPrides("events/type/pride", {
        page: paginationId,
        city_id: filterData.cities,
        month: filterData.month,
      });
      setPrides(res.data.data);
      setMeta(res.data.meta);
      setIsPrideLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setError(error?.message);
      setIsPrideLoading(false);
    }
  }

  function handleCheckBoxSelect(e, filterName) {
    const { id, checked } = e.target;
    const { cities, month } = filterData;
    setPaginationId(1);
    if (filterName === "cities") {
      const data = e.target.attributes.data.value;
      if (checked) {
        setFilterData((p) => ({
          ...p,
          cities: [...cities, data],
        }));
      } else {
        setFilterData((p) => ({
          ...p,
          cities: cities.filter((i) => i !== data),
        }));
      }
      return;
    }
    if (checked) {
      setFilterData((p) => ({
        ...p,
        month: [...month, id],
      }));
    } else {
      setFilterData((p) => ({
        ...p,
        month: month.filter((i) => i !== id),
      }));
    }
  }

  useEffect(() => {
    getAllCities();
  }, []);

  useEffect(() => {
    getAllPrides();
  }, [paginationId, filterData]);
  return (
    <>
      <Head>
        <title>Gay Pride Calendar 2023 / 2024 | TravelGay</title>
      </Head>
      <InternalHeroSection
        heading="Gay Pride Calendar"
        description="Explore the United Kingdom’s popular gay scene. Find the best gay bars, saunas, cruise clubs, hotels and more."
      />

      <div className="theme-container px-4 md:px-0 mt-8 md:mt-28">
        <div className="grid grid-cols-1 xl:grid-cols-[350px_1fr] gap-x-14">
          <div>
            <h2 className="font-semibold text-2xl">Filter Events</h2>
            <CityFilter
              isActive={isActive}
              setIsActive={setIsActive}
              cities={cities}
              handleCheckBoxSelect={(e) => handleCheckBoxSelect(e, "cities")}
              filterData={filterData?.cities}
            />
            <MonthFilter
              isActive={isActive}
              setIsActive={setIsActive}
              handleCheckBoxSelect={(e) => handleCheckBoxSelect(e, "month")}
              filterData={filterData?.month}
            />
            <button
              type="button"
              className="w-full text-center py-3 bg-[#743D7D] font-semibold text-white rounded"
              onClick={() => setFilterData({ cities: [], month: [] })}
            >
              Reset Filter
            </button>
          </div>
          <div className="mt-6 xl:mt-0">
            <div className="mb-6">
              <p className="text-[13px]">
                Our gay pride calendar gives you the full round-up of some of
                the biggest and best upcoming LGBTQi+ Pride celebrations across
                Europe, USA & beyond in 2022 / 2023. We cover the biggest Gay
                Pride events in cities like London, New York and Sao Paulo, as
                well as emerging Pride events in smaller destinations. We’ll
                also let you know which prides are cancelled because of
                coronavirus.
              </p>

              <p className="text-primary mt-4 text-[13px]">
                Tell us about your local Pride event.
              </p>
            </div>
            <div className="mt-6 xl:mt-0">
              <PrideListingWrapper
                prides={prides}
                error={error}
                isLoading={isPrideLoading}
              />
            </div>
            <div className="mt-8">
              <Pagination
                handlePagination={(e) => {
                  setPaginationId(e.target.id);
                  window.scrollTo(0, 0);
                }}
                meta={meta}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
