import axios from "axios";
import Head from "next/head";
import { useState, useRef, useEffect } from "react";
import { AiOutlineClose } from "react-icons/ai";
import { useRouter } from "next/router";
import ReCAPTCHA from "react-google-recaptcha";
import HaveWeGotSomething from "../components/haveWeGotSomething/HaveWeGotSomething";
import InternalHeroSection from "../components/Shared/internalHero/InternalHeroSection";
import {
  captchaTokenVerification,
  getAPIUrl,
  getBaseURL,
} from "../utils/Helper";

export default function ContactUs() {
  const [formData, setFormData] = useState({});
  const [error, setError] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);
  const [verifyToken, setVerifyToken] = useState(null);
  const ref = useRef();
  const router = useRouter();

  function handleChange(e) {
    const { name, value } = e.target;
    setFormData((p) => ({ ...p, [name]: value }));
  }

  async function handleContactSubmit(e) {
    e.preventDefault();
    setIsLoading(true);
    setError({});
    if (
      !formData?.first_name ||
      !formData?.last_name ||
      !formData?.subject ||
      !formData?.message
    ) {
      if (!formData?.first_name)
        setError((p) => ({ ...p, first_name: "First Name is required" }));
      if (!formData?.last_name)
        setError((p) => ({ ...p, last_name: "Last Name is required" }));
      if (!formData?.subject)
        setError((p) => ({ ...p, subject: "Subject is required" }));
      if (!formData?.message)
        setError((p) => ({ ...p, message: "Message is required" }));

      setIsLoading(false);
      return;
    }

    const { _widgetId } = ref.current;
    const verified = await captchaTokenVerification(verifyToken);

    if (!verified) {
      if (verifyToken) ref.current.reset(_widgetId);
      setError((p) => ({ ...p, captcha: "Captcha has failed. Try again." }));
      setVerifyToken(null);
      setIsLoading(false);
      return;
    }

    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      axios({
        url: `${getAPIUrl()}email/send-contact-message`,
        data: formData,
        method: "POST",
        headers: {
          Accept: "application/json",
        },
        withCredentials: true,
      })
        .then((res) => {
          setSuccessMessage(res.data.message);
          setTimeout(() => {
            router.push("/");
          }, 1000);
          setFormData({});
          setIsLoading(false);
        })
        .catch((err) => {
          console.log(err.message);
          setIsLoading(false);
        });
    });
  }

  useEffect(() => {
    const el = document.querySelector("#grecaptcha-wrapper div");
    if (el.children.length <= 0) {
      ref.current.props?.grecaptcha?.render(el, {
        sitekey: ref.current.props.sitekey,
        callback: ref.current.props.onChange,
      });
    }
  });
  return (
    <>
      <Head>
        <title>Contact Us | TravelGay</title>
      </Head>
      <InternalHeroSection
        heading="Contact Us "
        description="A popular destination for gay travelers. Las Vegas has it all – fine restaurants, casinos, nightlife and world-class entertainment by iconic performers."
      />
      <div className="theme-container xl:mt-5">
        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_8px)_calc(30%_-_8px)] gap-4 mt-0 md:mt-5 xl:mt-[70px]">
          <div>
            <div className="flex w-full my-5 md:my-0 font-[600] justify-center items-center text-primary h-[241px] bg-primary">
              ADS
            </div>
            <div className="contact-form p-3 md:p-0 my-5 mt-0 md:mt-20">
              <h3 className="text-primary text-xl mb-4 lg:text-2xl font-bold">
                Welcome To Gay Travel
              </h3>
              <p className="text-xs leading-5 mb-3">
                Use this form or send an email to{" "}
                <span className="font-semibold text-primary">
                  info@travelgay.com
                </span>
                . If your business operates within or targets the LGBTQ+ market,
                we invite you to get in touch. We also offer the{" "}
                <span className="font-semibold text-primary">
                  TravelGay Approved{" "}
                </span>
                program for venues that wish to showcase their LGBTQ+
                credentials.
              </p>
              <p className="text-xs leading-5 mb-3">
                We also work with tourist boards and venues on global
                advertising campaigns to an LGBTQ+ audience. Please contact one
                of the Travel Gay team on{" "}
                <span className="font-semibold text-primary">
                  44 203 933 8000
                </span>{" "}
                or{" "}
                <span className="font-semibold text-primary">
                  {" "}
                  +1 646 627 7588.
                </span>
              </p>
              <p className="text-xs leading-5 mb-3">
                You can also reach us at{" "}
                <span className="text-primary">Whatsapp ID : 442039338000</span>
              </p>
              <form onSubmit={handleContactSubmit}>
                <div className="my-3 px-1 py-3">
                  <div className="grid grid-col-1 lg:grid-cols-2 gap-0 lg:gap-4">
                    <label
                      htmlFor="first_name"
                      className="flex flex-col mt-2 lg:mt-4 text-sm"
                    >
                      <span>First Name* </span>
                      <input
                        type="text"
                        name="first_name"
                        id="first_name"
                        className="w-full border border-slate-200 border-solid mt-1 rounded-lg bg-transparent text-sm"
                        placeholder="John"
                        value={formData?.first_name ?? ""}
                        onChange={handleChange}
                      />
                      {error?.first_name && (
                        <p className="text-xs text-red-500 my-1">
                          {error?.first_name}
                        </p>
                      )}
                    </label>
                    <label
                      htmlFor="last_name"
                      className="flex flex-col mt-2 lg:mt-4 text-sm"
                    >
                      <span>Last Name*</span>
                      <input
                        type="text"
                        name="last_name"
                        id="last_name"
                        className="border border-slate-200 border-solid mt-1 rounded-lg text-sm bg-transparent"
                        placeholder="Doe "
                        value={formData?.last_name ?? ""}
                        onChange={handleChange}
                      />
                      {error?.last_name && (
                        <p className="text-xs text-red-500 my-1">
                          {error?.last_name}
                        </p>
                      )}
                    </label>
                  </div>
                  <div>
                    <label
                      htmlFor="subject"
                      className="flex flex-col mt-2 lg:mt-4 text-sm"
                    >
                      <span>Subject*</span>
                      <input
                        type="text"
                        name="subject"
                        id="subject"
                        className="border text-sm border-slate-200 border-solid mt-1 rounded-lg bg-transparent"
                        placeholder="Info Trips "
                        value={formData?.subject ?? ""}
                        onChange={handleChange}
                      />
                      {error?.subject && (
                        <p className="text-xs text-red-500 my-1">
                          {error?.subject}
                        </p>
                      )}
                    </label>
                  </div>
                  <div>
                    <label
                      htmlFor="message"
                      className="flex flex-col mt-2 lg:mt-4 text-sm"
                    >
                      <span>Your Message*</span>
                      <textarea
                        name="message"
                        id="message"
                        rows="4"
                        className="border text-sm border-slate-200 border-solid mt-1 rounded-lg bg-transparent resize-none"
                        placeholder="Write your Message"
                        value={formData?.message ?? ""}
                        onChange={handleChange}
                      />
                      {error?.message && (
                        <p className="text-xs text-red-500 my-1">
                          {error?.message}
                        </p>
                      )}
                    </label>
                  </div>

                  <div className="mt-4" id="grecaptcha-wrapper">
                    <ReCAPTCHA
                      sitekey={process.env.NEXT_PUBLIC_CAPTCHA_CLIENT_KEY}
                      onChange={(token) => setVerifyToken(token)}
                      ref={ref}
                    />
                    {error?.captcha && (
                      <p className="text-sm text-red-500">{error.captcha}</p>
                    )}
                  </div>

                  <div className="flex justify-center">
                    <button
                      type="submit"
                      className={`w-full bg-[#743D7D] y 2xl:px-4 py-3 rounded text-white 2xl:gap-1 my-4 ${
                        isLoading ? "opacity-70" : ""
                      }`}
                    >
                      <span className="text-sm 2xl:text-base">
                        {isLoading ? "Loading..." : "SEND"}
                      </span>
                    </button>
                  </div>
                  {successMessage && (
                    <div className="bg-green-500 py-3 px-4 text-white font-semibold rounded-md flex items-center justify-between gap-8 text-sm md:text-base">
                      {successMessage}
                      <button
                        type="button"
                        onClick={() => setSuccessMessage(null)}
                        className="text-white"
                      >
                        <AiOutlineClose />
                      </button>
                    </div>
                  )}
                </div>
              </form>
            </div>

            <div className="flex w-full font-[600] justify-center items-center text-primary my-5 h-[241px] bg-primary">
              ADS
            </div>
          </div>
          <aside className="md:p-0 px-4 md:w-full">
            <div className="mb-5 justify-center block">
              <h2 className="mb-[10px] md:text-[24px] font-semibold text-md text-primary">
                Instagram
              </h2>
              <div className="flex items-center">
                <img
                  src="/images/Ellipse1.png"
                  className="max-h-12 max-w-12"
                  alt="Community"
                />
                <span className="text-md inline-block ml-3 font-semibold text-[#666666]">
                  Kevinbridegreen
                </span>
              </div>
              <div className="justify-center gap-2 my-3 grid grid-cols-3 md:mt-5 xl:mt-8">
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
                <div className="">
                  <img
                    src="/images/asideHotelimages.png"
                    className="rounded-[5px] max-h-[168px] mx-auto"
                    alt="Community"
                  />
                </div>
              </div>
              <div className="flex justify-center my-3">
                <button
                  type="button"
                  className="w-8/12 bg-[#743D7D] px-2 py-3 pl-6 flex items-center gap-3 rounded text-white 2xl:gap-1 "
                >
                  <img src="/icons/ig-white.svg" alt="" />
                  <span className="ml-3 text-sm 2xl:text-base">
                    Follow on Instagram
                  </span>
                </button>
              </div>
            </div>
            <div className=" my-4 hidden max-w-[378px] h-0  md:h-[304px] bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
          </aside>
        </div>
      </div>
      <div className="my-5 bg-[#F8F8F9]">
        <HaveWeGotSomething />
      </div>
    </>
  );
}
