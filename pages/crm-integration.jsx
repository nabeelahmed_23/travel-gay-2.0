/* eslint-disable no-alert */
/* eslint-disable no-plusplus */
/* eslint-disable eqeqeq */
/* eslint-disable jsx-a11y/label-has-associated-control */
import axios from "axios";
import dynamic from "next/dynamic";
import Head from "next/head";
import Script from "next/script";
import { useEffect } from "react";

function CRMIntegration() {
  function reCaptchaAlert122266000041074142() {
    const recap = document.getElementById("recap122266000041074142");
    if (
      recap != undefined &&
      recap.getAttribute("captcha-verified") == "false"
    ) {
      document.getElementById("recapErr122266000041074142").style.visibility =
        "visible";
      return false;
    }
    return true;
  }

  function validateEmail122266000041074142() {
    const form = document.getElementById("WebToLeads122266000041074142");
    const emailFld = form.querySelectorAll("[ftype=email]");
    let i;
    for (i = 0; i < emailFld.length; i++) {
      const emailVal = emailFld[i].value;
      if (emailVal.replace(/^\s+|\s+$/g, "").length != 0) {
        const atpos = emailVal.indexOf("@");
        const dotpos = emailVal.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= emailVal.length) {
          alert("Please enter a valid email address. ");
          emailFld[i].focus();
          return false;
        }
      }
    }
    return true;
  }

  function checkMandatory122266000041074142(e) {
    const mndFileds = ["First Name", "Last Name", "Email", "Mobile"];
    const fldLangVal = [
      "First\x20Name",
      "Last\x20Name",
      "Email",
      "Contact\x20Number",
    ];

    const form = document.getElementById("WebToLeads122266000041074142");
    for (let i = 0; i < mndFileds.length; i++) {
      const fieldObj = form[mndFileds[i]];
      if (fieldObj) {
        if (fieldObj.value.replace(/^\s+|\s+$/g, "").length == 0) {
          if (fieldObj.type == "file") {
            alert("Please select a file to upload.");
            fieldObj.focus();
            return false;
          }
          alert(`${fldLangVal[i]} cannot be empty`);
          fieldObj.focus();
          e.preventDefault();
          return false;
        }
        if (fieldObj.nodeName == "SELECT") {
          if (fieldObj.options[fieldObj.selectedIndex].value == "-None-") {
            alert(`${fldLangVal[i]} cannot be none`);
            fieldObj.focus();
            e.preventDefault();
            return false;
          }
        } else if (fieldObj.type == "checkbox") {
          if (fieldObj.checked == false) {
            alert(`Please accept  ${fldLangVal[i]}`);
            fieldObj.focus();
            e.preventDefault();
            return false;
          }
        }
        try {
          if (fieldObj.name == "Last Name") {
            // eslint-disable-next-line no-restricted-globals, no-global-assign
            name = fieldObj.value;
          }
          // eslint-disable-next-line no-empty
        } catch (e) {}
      }
    }

    // eslint-disable-next-line no-undef
    trackVisitor122266000041074142();
    if (!validateEmail122266000041074142()) {
      return false;
    }

    if (!reCaptchaAlert122266000041074142()) {
      return false;
    }
    document
      .querySelector(".crmWebToEntityForm .formsubmit")
      .setAttribute("disabled", true);

    return false;
  }

  function tooltipShow122266000041074142(event) {
    const el = event.target;
    const tooltip = el.nextElementSibling;
    const tooltipDisplay = tooltip.style.display;
    if (tooltipDisplay == "none") {
      const allTooltip = document.getElementsByClassName("zcwf_tooltip_over");
      let i;
      for (i = 0; i < allTooltip.length; i++) {
        allTooltip[i].style.display = "none";
      }
      tooltip.style.display = "block";
    } else {
      tooltip.style.display = "none";
    }
  }

  useEffect(() => {
    axios
      .get(
        "https://accounts.zoho.eu/oauth/v2/auth?response_type=code&scope=ZohoCRM.modules.all&redirect_uri=http://localhost:3000&access_type=offline&client_id=1000.5FS8LSP4LYG91LANPTRM1S0VE7L5JP",
      )
      .then((res) => {
        console.log(res.data);
      });
  }, []);
  return (
    <>
      <Head>
        <title>CRM</title>
        <meta httpEquiv="Content-Type" content="text/html;charset=UTF-8" />
        <style
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html:
              "\nhtml,body{\n\tmargin: 0px;\n}\n#crmWebToEntityForm.zcwf_lblLeft {\n\twidth:100%;\n\tpadding: 25px;\n\tmargin: 0 auto;\n\tbox-sizing: border-box;\n}\n#crmWebToEntityForm.zcwf_lblLeft * {\n\tbox-sizing: border-box;\n}\n#crmWebToEntityForm{text-align: left;}\n#crmWebToEntityForm * {\n\tdirection: ltr;\n}\n.zcwf_lblLeft .zcwf_title {\n\tword-wrap: break-word;\n\tpadding: 0px 6px 10px;\n\tfont-weight: bold;\n}\n.zcwf_lblLeft .zcwf_col_fld input[type=text], .zcwf_lblLeft .zcwf_col_fld textarea {\n\twidth: 60%;\n\tborder: 1px solid #ccc !important;\n\tresize: vertical;\n\tborder-radius: 2px;\n\tfloat: left;\n}\n.zcwf_lblLeft .zcwf_col_lab {\n\twidth: 30%;\n\tword-break: break-word;\n\tpadding: 0px 6px 0px;\n\tmargin-right: 10px;\n\tmargin-top: 5px;\n\tfloat: left;\n\tmin-height: 1px;\n}\n.zcwf_lblLeft .zcwf_col_fld {\n\tfloat: left;\n\twidth: 68%;\n\tpadding: 0px 6px 0px;\n\tposition: relative;\n\tmargin-top: 5px;\n}\n.zcwf_lblLeft .zcwf_privacy{padding: 6px;}\n.zcwf_lblLeft .wfrm_fld_dpNn{display: none;}\n.dIB{display: inline-block;}\n.zcwf_lblLeft .zcwf_col_fld_slt {\n\twidth: 60%;\n\tborder: 1px solid #ccc;\n\tbackground: #fff;\n\tborder-radius: 4px;\n\tfont-size: 12px;\n\tfloat: left;\n\tresize: vertical;\n\tpadding: 2px 5px;\n}\n.zcwf_lblLeft .zcwf_row:after, .zcwf_lblLeft .zcwf_col_fld:after {\n\tcontent: '';\n\tdisplay: table;\n\tclear: both;\n}\n.zcwf_lblLeft .zcwf_col_help {\n\tfloat: left;\n\tmargin-left: 7px;\n\tfont-size: 12px;\n\tmax-width: 35%;\n\tword-break: break-word;\n}\n.zcwf_lblLeft .zcwf_help_icon {\n\tcursor: pointer;\n\twidth: 16px;\n\theight: 16px;\n\tdisplay: inline-block;\n\tbackground: #fff;\n\tborder: 1px solid #ccc;\n\tcolor: #ccc;\n\ttext-align: center;\n\tfont-size: 11px;\n\tline-height: 16px;\n\tfont-weight: bold;\n\tborder-radius: 50%;\n}\n.zcwf_lblLeft .zcwf_row {margin: 15px 0px;}\n.zcwf_lblLeft .formsubmit {\n\tmargin-right: 5px;\n\tcursor: pointer;\n\tcolor: #333;\n\tfont-size: 12px;\n}\n.zcwf_lblLeft .zcwf_privacy_txt {\n\twidth: 90%;\n\tcolor: rgb(0, 0, 0);\n\tfont-size: 12px;\n\tfont-family: Arial;\n\tdisplay: inline-block;\n\tvertical-align: top;\n\tcolor: #333;\n\tpadding-top: 2px;\n\tmargin-left: 6px;\n}\n.zcwf_lblLeft .zcwf_button {\n\tfont-size: 12px;\n\tcolor: #333;\n\tborder: 1px solid #ccc;\n\tpadding: 3px 9px;\n\tborder-radius: 4px;\n\tcursor: pointer;\n\tmax-width: 120px;\n\toverflow: hidden;\n\ttext-overflow: ellipsis;\n\twhite-space: nowrap;\n}\n.zcwf_lblLeft .zcwf_tooltip_over{\n\tposition: relative;\n}\n.zcwf_lblLeft .zcwf_tooltip_ctn{\n\tposition: absolute;\n\tbackground: #dedede;\n\tpadding: 3px 6px;\n\ttop: 3px;\n\tborder-radius: 4px;word-break: break-word;\n\tmin-width: 100px;\n\tmax-width: 150px;\n\tcolor: #333;\n\tz-index: 100;\n}\n.zcwf_lblLeft .zcwf_ckbox{\n\tfloat: left;\n}\n.zcwf_lblLeft .zcwf_file{\n\twidth: 55%;\n\tbox-sizing: border-box;\n\tfloat: left;\n}\n.clearB:after{\n\tcontent:'';\n\tdisplay: block;\n\tclear: both;\n}\n@media all and (max-width: 600px) {\n\t.zcwf_lblLeft .zcwf_col_lab, .zcwf_lblLeft .zcwf_col_fld {\n\t\twidth: auto;\n\t\tfloat: none !important;\n\t}\n\t.zcwf_lblLeft .zcwf_col_help {width: 40%;}\n}\n",
          }}
        />
      </Head>
      <div id="form123" />
      <div
        id="crmWebToEntityForm"
        className="zcwf_lblLeft crmWebToEntityForm"
        style={{ backgroundColor: "white", color: "black", maxWidth: "600px" }}
      >
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta httpEquiv="content-type" content="text/html;charset=UTF-8" />
        <form
          action="https://crm.zoho.eu/crm/WebToLeadForm"
          name="WebToLeads122266000041074142"
          id="WebToLeads122266000041074142"
          method="POST"
          onSubmit={checkMandatory122266000041074142}
          acceptCharset="UTF-8"
        >
          <input
            type="text"
            style={{ display: "none" }}
            name="xnQsjsdp"
            defaultValue="1afce4d3f3f1cb45fd0d817a0e76abda68af738ea49d31e76c1a6202167fac28"
          />
          <input type="hidden" name="zc_gad" id="zc_gad" defaultValue />
          <input
            type="text"
            style={{ display: "none" }}
            name="xmIwtLD"
            defaultValue="796e4a2f98b221c0c3dc91dda01514bdbc40aa1e9e0380075f7087121b2c8deb"
          />
          <input
            type="text"
            style={{ display: "none" }}
            name="actionType"
            defaultValue="TGVhZHM="
          />
          <input
            type="text"
            style={{ display: "none" }}
            name="returnURL"
            defaultValue="https://www.travelgay.com/thanks"
          />
          {/* Do not remove this code. */}
          <input
            type="text"
            style={{ display: "none" }}
            id="ldeskuid"
            name="ldeskuid"
          />
          <input
            type="text"
            style={{ display: "none" }}
            id="LDTuvid"
            name="LDTuvid"
          />
          {/* Do not remove this code. */}

          <div
            className="zcwf_title"
            style={{ maxWidth: "600px", color: "black" }}
          >
            TG Enquiry Form
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="First_Name">
                First Name<span style={{ color: "red" }}>*</span>
              </label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="First_Name"
                name="First Name"
                maxLength={40}
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="Last_Name">
                Last Name<span style={{ color: "red" }}>*</span>
              </label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="Last_Name"
                name="Last Name"
                maxLength={80}
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="Mobile">
                Contact Number<span style={{ color: "red" }}>*</span>
              </label>
            </div>
            <div className="zcwf_col_fld">
              <input type="text" id="Mobile" name="Mobile" maxLength={30} />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="Email">
                Email<span style={{ color: "red" }}>*</span>
              </label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                ftype="email"
                id="Email"
                name="Email"
                maxLength={100}
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="Description">Comments</label>
            </div>
            <div className="zcwf_col_fld">
              <textarea id="Description" name="Description" defaultValue="" />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF12">Destination</label>
            </div>
            <div className="zcwf_col_fld">
              <select
                className="zcwf_col_fld_slt"
                id="LEADCF12"
                name="LEADCF12"
                multiple
              >
                <option value="Afghanistan">Afghanistan</option>
                <option value="Aland Islands">Aland Islands</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="American Samoa">American Samoa</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Anguilla">Anguilla</option>
                <option value="Antarctica">Antarctica</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Aruba">Aruba</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bermuda">Bermuda</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegovina">
                  Bosnia and Herzegovina
                </option>
                <option value="Botswana">Botswana</option>
                <option value="Bouvet Island">Bouvet Island</option>
                <option value="Brazil">Brazil</option>
                <option value="British Indian Ocean Territory">
                  British Indian Ocean Territory
                </option>
                <option value="British Virgin Islands">
                  British Virgin Islands
                </option>
                <option value="Brunei">Brunei</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Cape Verde">Cape Verde</option>
                <option value="Cayman Islands">Cayman Islands</option>
                <option value="Central African Republic">
                  Central African Republic
                </option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Christmas Island">Christmas Island</option>
                <option value="Cocos Islands">Cocos Islands</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo">Congo</option>
                <option value="Cook Islands">Cook Islands</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cote dIvoire">Cote dIvoire</option>
                <option value="Croatia">Croatia</option>
                <option value="Cuba">Cuba</option>
                <option value="Curacao">Curacao</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Falkland Islands">Falkland Islands</option>
                <option value="Faroe Islands">Faroe Islands</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="French Guiana">French Guiana</option>
                <option value="French Polynesia">French Polynesia</option>
                <option value="French Southern Territories">
                  French Southern Territories
                </option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Gibraltar">Gibraltar</option>
                <option value="Greece">Greece</option>
                <option value="Greenland">Greenland</option>
                <option value="Grenada">Grenada</option>
                <option value="Guadeloupe">Guadeloupe</option>
                <option value="Guam">Guam</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guernsey">Guernsey</option>
                <option value="Guinea">Guinea</option>
                <option value="GuineaBissau">GuineaBissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Heard Island And McDonald Islands">
                  Heard Island And McDonald Islands
                </option>
                <option value="Honduras">Honduras</option>
                <option value="Hong Kong">Hong Kong</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran">Iran</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jersey">Jersey</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Laos">Laos</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libya">Libya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg(French)">Luxembourg(French)</option>
                <option value="Luxembourg(German)">Luxembourg(German)</option>
                <option value="Macao">Macao</option>
                <option value="Macedonia">Macedonia</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Martinique">Martinique</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mayotte">Mayotte</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia">Micronesia</option>
                <option value="Moldova">Moldova</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montenegro">Montenegro</option>
                <option value="Montserrat">Montserrat</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Myanmar">Myanmar</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="Netherlands Antilles">
                  Netherlands Antilles
                </option>
                <option value="New Caledonia">New Caledonia</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="Niue">Niue</option>
                <option value="Norfolk Island">Norfolk Island</option>
                <option value="North Korea">North Korea</option>
                <option value="Northern Ireland">Northern Ireland</option>
                <option value="Northern Mariana Islands">
                  Northern Mariana Islands
                </option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Palestine">Palestine</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Pitcairn">Pitcairn</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Puerto Rico">Puerto Rico</option>
                <option value="Qatar">Qatar</option>
                <option value="Reunion">Reunion</option>
                <option value="Romania">Romania</option>
                <option value="Russia">Russia</option>
                <option value="Rwanda">Rwanda</option>
                <option value="Saint Helena">Saint Helena</option>
                <option value="Saint Kitts And Nevis">
                  Saint Kitts And Nevis
                </option>
                <option value="Saint Lucia">Saint Lucia</option>
                <option value="Saint Pierre And Miquelon">
                  Saint Pierre And Miquelon
                </option>
                <option value="Saint Vincent And The Grenadines">
                  Saint Vincent And The Grenadines
                </option>
                <option value="Samoa">Samoa</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome And Principe">
                  Sao Tome And Principe
                </option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Serbia">Serbia</option>
                <option value="Serbia and Montenegro">
                  Serbia and Montenegro
                </option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia">Slovakia</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Georgia And The South Sandwich Islands">
                  South Georgia And The South Sandwich Islands
                </option>
                <option value="South Korea">South Korea</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Svalbard And Jan Mayen">
                  Svalbard And Jan Mayen
                </option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syria">Syria</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania">Tanzania</option>
                <option value="Thailand">Thailand</option>
                <option value="The Democratic Republic Of Congo">
                  The Democratic Republic Of Congo
                </option>
                <option value="Timor-Leste">Timor-Leste</option>
                <option value="Togo">Togo</option>
                <option value="Tokelau">Tokelau</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Turks And Caicos Islands">
                  Turks And Caicos Islands
                </option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">
                  United Arab Emirates
                </option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="United States">United States</option>
                <option value="United States Minor Outlying Islands">
                  United States Minor Outlying Islands
                </option>
                <option value="Uruguay">Uruguay</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Vatican">Vatican</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Vietnam">Vietnam</option>
                <option value="Virgin Islands">Virgin Islands</option>
                <option value="Wallis And Futuna">Wallis And Futuna</option>
                <option value="Western Sahara">Western Sahara</option>
                <option value="Yemen">Yemen</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
              </select>
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF122">Departure Month</label>
            </div>
            <div className="zcwf_col_fld">
              <select
                className="zcwf_col_fld_slt"
                id="LEADCF122"
                name="LEADCF122"
              >
                <option value="-None-">-None-</option>
                <option value="January">January</option>
                <option value="February">February</option>
                <option value="March">March</option>
                <option value="April">April</option>
                <option value="May">May</option>
                <option value="June">June</option>
                <option value="July">July</option>
                <option value="August">August</option>
                <option value="September">September</option>
                <option value="October">October</option>
                <option value="November">November</option>
                <option value="December">December</option>
              </select>
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF14">Sexual Orientation</label>
            </div>
            <div className="zcwf_col_fld">
              <select
                className="zcwf_col_fld_slt"
                id="LEADCF14"
                name="LEADCF14"
              >
                <option value="-None-">-None-</option>
                <option value="Prefer not to Say">Prefer not to Say</option>
                <option value="Gay Man">Gay Man</option>
                <option value="Gay woman / Lesbian">Gay woman / Lesbian</option>
                <option value="Straight / Heterosexual">
                  Straight / Heterosexual
                </option>
                <option value="Bisexual">Bisexual</option>
                <option value="Queer">Queer</option>
                <option value="Other">Other</option>
              </select>
              <div className="zcwf_col_help">
                {" "}
                <span
                  role="button"
                  tabIndex="0"
                  title="This is optional but will help us tailor our services accordingly."
                  style={{
                    cursor: "pointer",
                    width: "16px",
                    height: "16px",
                    display: "inline-block",
                    background: "#fff",
                    border: "1px solid #ccc",
                    color: "#ccc",
                    textAlign: "center",
                    fontSize: "11px",
                    lineHeight: "16px",
                    fontWeight: "bold",
                    borderRadius: "50%",
                  }}
                  onClick={tooltipShow122266000041074142}
                  onKeyDown={tooltipShow122266000041074142}
                >
                  ?
                </span>
                <div className="zcwf_tooltip_over" style={{ display: "none" }}>
                  <span className="zcwf_tooltip_ctn">
                    This is optional but will help us tailor our services
                    accordingly.
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div className="zcwf_row">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF3">Budget Per Person</label>
            </div>
            <div className="zcwf_col_fld">
              <select className="zcwf_col_fld_slt" id="LEADCF3" name="LEADCF3">
                <option value="-None-">-None-</option>
                <option value="1,500-2,000">1,500-2,000</option>
                <option value="2,000-3,000">2,000-3,000</option>
                <option value="3,000-5,000">3,000-5,000</option>
                <option value="5,000-7,500">5,000-7,500</option>
                <option value="7,500-10,000">7,500-10,000</option>
                <option value="10,000+">10,000+</option>
              </select>
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="Lead_Source">Lead Source</label>
            </div>
            <div className="zcwf_col_fld">
              <select
                className="zcwf_col_fld_slt"
                id="Lead_Source"
                name="Lead Source"
              >
                <option value="-None-">-None-</option>
                <option value="OOO Website">OOO Website</option>
                <option value="Website Visit">Website Visit</option>
                <option value="Chat">Chat</option>
                <option value="SMS Text">SMS Text</option>
                <option selected value="TravelGay.com">
                  TravelGay.com
                </option>
                <option value="Repeater">Repeater</option>
                <option value="Affiliate">Affiliate</option>
                <option value="Facebook Leads">Facebook Leads</option>
                <option value="Facebook Messaging">Facebook Messaging</option>
                <option value="Generic Email">Generic Email</option>
                <option value="OOO Newsletter">OOO Newsletter</option>
                <option value="ResponseIQ">ResponseIQ</option>
                <option value="Friend Of Darren">Friend Of Darren</option>
                <option value="LinkedIn">LinkedIn</option>
                <option value="Phone Call">Phone Call</option>
                <option value="Recommendation from friend/Client">
                  Recommendation from friend/Client
                </option>
                <option value="Staff">Staff</option>
                <option value="Facebook">Facebook</option>
                <option value="Twitter">Twitter</option>
                <option value="Google+">Google+</option>
                <option value="Google AdWords">Google AdWords</option>
                <option value="OOO Axel Arigato Event Feb2020">
                  OOO Axel Arigato Event Feb2020
                </option>
              </select>
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF5">Sales Channel</label>
            </div>
            <div className="zcwf_col_fld">
              <select className="zcwf_col_fld_slt" id="LEADCF5" name="LEADCF5">
                <option value="-None-">-None-</option>
                <option value="OOO Tailor-made">OOO Tailor-made</option>
                <option value="OOO Group Trips">OOO Group Trips</option>
                <option value="TravelGay Tailor-made">
                  TravelGay Tailor-made
                </option>
                <option selected value="TravelGay Group Trips">
                  TravelGay Group Trips
                </option>
              </select>
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF15">Lead Type</label>
            </div>
            <div className="zcwf_col_fld">
              <select
                className="zcwf_col_fld_slt"
                id="LEADCF15"
                name="LEADCF15"
              >
                <option value="-None-">-None-</option>
                <option selected value="Out Of Office">
                  Out Of Office
                </option>
                <option value="TravelGay.com">TravelGay.com</option>
              </select>
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF123">zc_gad</label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="LEADCF123"
                name="LEADCF123"
                maxLength={255}
                defaultValue
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF23">Campaign-source</label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="LEADCF23"
                name="LEADCF23"
                maxLength={255}
                defaultValue
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF22">Campaign Medium</label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="LEADCF22"
                name="LEADCF22"
                maxLength={255}
                defaultValue
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF24">Campaign Content</label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="LEADCF24"
                name="LEADCF24"
                maxLength={255}
                defaultValue
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF27">UTM Name</label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="LEADCF27"
                name="LEADCF27"
                maxLength={255}
                defaultValue
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF25">Campaign Term</label>
            </div>
            <div className="zcwf_col_fld">
              <input
                type="text"
                id="LEADCF25"
                name="LEADCF25"
                maxLength={255}
                defaultValue
              />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF44">Initial Referrer</label>
            </div>
            <div className="zcwf_col_fld">
              <textarea id="LEADCF44" name="LEADCF44" defaultValue="" />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF42">Enquiry URL</label>
            </div>
            <div className="zcwf_col_fld">
              <textarea id="LEADCF42" name="LEADCF42" defaultValue="" />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF45">Last Referrer</label>
            </div>
            <div className="zcwf_col_fld">
              <textarea id="LEADCF45" name="LEADCF45" defaultValue="" />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row wfrm_fld_dpNn">
            <div
              className="zcwf_col_lab"
              style={{ fontSize: "12px", fontFamily: "Arial" }}
            >
              <label htmlFor="LEADCF41">Landing Page URL</label>
            </div>
            <div className="zcwf_col_fld">
              <textarea id="LEADCF41" name="LEADCF41" defaultValue="" />
              <div className="zcwf_col_help" />
            </div>
          </div>
          <div className="zcwf_row">
            {" "}
            <div className="zcwf_col_lab" />
            <div className="zcwf_col_fld">
              <div
                className="g-recaptcha"
                data-sitekey="6LcJyz8aAAAAAIo5Q7_-Z_Q_QbTrQihSB5eZLfGU"
                data-theme="light"
                data-callback="rccallback122266000041074142"
                captcha-verified="false"
                id="recap122266000041074142"
              />
              <div
                id="recapErr122266000041074142"
                style={{ fontSize: "12px", color: "red", visibility: "hidden" }}
              >
                Captcha validation failed. If you are not a robot then please
                try again.
              </div>
            </div>
          </div>
          <div className="zcwf_row">
            <div className="zcwf_col_lab" />
            <div className="zcwf_col_fld">
              <input
                type="submit"
                id="formsubmit"
                className="formsubmit zcwf_button"
                defaultValue="SEND ENQUIRY"
                title="SEND ENQUIRY"
              />
              <input
                type="reset"
                className="zcwf_button"
                name="reset"
                defaultValue="Reset"
                title="Reset"
              />
            </div>
          </div>
        </form>
      </div>
      <Script
        src="https://www.google.com/recaptcha/api.js"
        id="google_captcha"
      />
      <Script type="text/javascript" id="VisitorTracking">
        {`var $zoho = $zoho || {};
              $zoho.salesiq = $zoho.salesiq || {
                widgetcode:
                  "93d4f956369ad499476550f06b606ea769733c0e656a7e377dec34db9c2cf0d72da405a2803a8d918c4bda0c432af8e4",
                values: {},
                ready: function () {},
              };
              var d = document;
              s = d.createElement("script");
              s.type = "text/javascript";
              s.id = "zsiqscript";
              s.defer = true;
              s.src = "https://salesiq.zoho.eu/widget";
              t = d.getElementsByTagName("script")[0];
              t.parentNode.insertBefore(s, t);
              function trackVisitor122266000041074142() {
                try {
                  if ($zoho) {
                    var LDTuvidObj =
                      document.forms["WebToLeads122266000041074142"]["LDTuvid"];
                    if (LDTuvidObj) {
                      LDTuvidObj.value = $zoho.salesiq.visitor.uniqueid();
                    }
                    var firstnameObj =
                      document.forms["WebToLeads122266000041074142"]["First Name"];
                    if (firstnameObj) {
                      name = firstnameObj.value + " " + name;
                    }
                    $zoho.salesiq.visitor.name(name);
                    var emailObj = document.forms["WebToLeads122266000041074142"]["Email"];
                    if (emailObj) {
                      email = emailObj.value;
                      $zoho.salesiq.visitor.email(email);
                    }
                  }
                } catch (e) {}
              }
              function rccallback122266000041074142() {
                if (document.getElementById("recap122266000041074142") != undefined) {
                  document
                    .getElementById("recap122266000041074142")
                    .setAttribute("captcha-verified", true);
                }
                if (
                  document.getElementById("recapErr122266000041074142") != undefined &&
                  document.getElementById("recapErr122266000041074142").style.visibility ==
                    "visible"
                ) {
                  document.getElementById("recapErr122266000041074142").style.visibility =
                    "hidden";
                }
              }
              `}
      </Script>
      <Script
        id="wf_anal"
        src="https://crm.zohopublic.eu/crm/WebFormAnalyticsServeServlet?rid=796e4a2f98b221c0c3dc91dda01514bdbc40aa1e9e0380075f7087121b2c8debgid1afce4d3f3f1cb45fd0d817a0e76abda68af738ea49d31e76c1a6202167fac28gidd5d4836131045889aacd8ee89fe126degidc4408f450319003e0c12e54df63a8632&tw=cbfb750ab7d38d47a14b773ee2feb5d02289cbd83eea2c768228fc69ce2034ab"
      />
    </>
  );
}

export default dynamic(() => Promise.resolve(CRMIntegration), { ssr: false });
