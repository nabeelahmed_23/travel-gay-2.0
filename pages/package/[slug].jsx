import Head from "next/head";
import { useState, useEffect } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import parse from "html-react-parser";
import PropTypes from "prop-types";
import GroupTripContactForm from "../../components/groupTrips/GroupTripContactForm";
import GroupTripDayDetail from "../../components/groupTrips/GroupTripDayDetail";
import GroupTripHeader from "../../components/groupTrips/GroupTripHeader";
import GroupTripTestimonial from "../../components/groupTrips/GroupTripTestimonial";
import { getSingleGroupTrip } from "../../utils/services/ApiCalls";
import { getDate, scrollIntoView } from "../../utils/Helper";

export default function Slug({ data }) {
  const [content, setContent] = useState("");
  useEffect(() => {
    if (data?.content) setContent(parse(data?.content));
  }, []);

  return (
    <div>
      <Head>
        <title>Group Trip</title>
      </Head>
      <GroupTripHeader
        text={data?.name}
        showPrice
        totalDays={data?.trip_itinerary?.length}
        price={data?.price}
      />
      <div className="theme-container px-4 md:px-0">
        <section className="grid lg:grid-cols-[1fr_317px] items-start gap-4 mt-16">
          <div>
            <h1 className="text-[28px] font-bold text-primary ">
              About this Trip
            </h1>
            <div className="mt-3 text-sm group-trip-content">{content}</div>
          </div>
          <div className="-mx-4 md:mx-0 bg-secondary px-4 py-6 rounded">
            <h2 className="text-primary text-[28px] font-semibold text-center">
              Departure Dates
            </h2>
            {data?.upcoming_trip && (
              <p className="text-center font-semibold sideRedBar relative pl-10 text-sm">
                {getDate(data?.upcoming_trip?.start_date, "dd mmmm yyyy eee")}
              </p>
            )}
            <button
              type="button"
              className="bg-[#743D7D] py-4 w-full text-center rounded text-white font-semibold my-2"
              onClick={() => scrollIntoView("groupTripContactForm", -200)}
            >
              MAKE EQUIRY
            </button>
            <p className="text-center font-semibold text-sm">
              Call Our Travel Expert:
            </p>
            <a
              href="tel:1-8881489-8383"
              className="text-center font-semibold text-primary text-sm inline-block w-full"
            >
              1-8881489-8383
            </a>
          </div>
        </section>

        <section className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-4 mt-8">
          {data?.media?.map((item) => (
            <img
              src={item?.original_url}
              alt=""
              className="rounded w-full aspect-video"
              key={`groupTripDetailImages${item.uuid}`}
            />
          ))}
        </section>

        <div className="mt-6 md:mt-16">
          <h3 className="text-primary font-bold text-[28px]">
            Group Trip Breakdown
          </h3>
          <div className="mt-4 md:mt-10">
            {data?.trip_itinerary?.map((item) => (
              <GroupTripDayDetail
                key={`groupTripItinerary${item.id}`}
                itinerary={item}
              />
            ))}
          </div>
        </div>
        <section className="bg-[#F8F8FA] py-10 my-8 md:my-16 -mx-4 md:mx-0">
          <h5 className="text-center text-primary font-bold text-2xl sm:text-[28px]">
            Call our Travel Experts
          </h5>
          <a
            href="tel:1-888-489-8383"
            className="text-center mt-4 w-full inline-block"
          >
            1-888-489-8383
          </a>
          <div className="text-center mt-4">
            <button
              type="button"
              className="py-4 px-20 text-white font-bold rounded bg-[#743D7D]"
              onClick={() => scrollIntoView("groupTripContactForm", -200)}
            >
              Make Enquiry
            </button>
          </div>
        </section>

        <section>
          <div className="mb-8">
            <h6 className="text-primary font-bold text-xl">
              Further Information
            </h6>
            <p className="text-[13px] mt-2">
              You’ll be sailing in style on the Lord of the Glens, a splendid
              Scottish cruise ship. It’s an elegant seafaring yacht that can
              sail through the Caledonian Canal but also sail the Atlantic. This
              will be an all LGBT trip of no more than 54 guests. Expect a
              Scottish breakfast every morning and nightly entertainment.
            </p>
          </div>
          <div className="mb-8">
            <h6 className="text-primary font-bold text-xl">What next ?</h6>
            <p className="text-[13px] mt-2">
              To get going, please submit an enquiry and one of our expert team
              will get back to you within the next 24 hours to discuss your
              requirements. All the prices on our website do not include
              flights, but we can, of course, arrange them for you.
            </p>
          </div>
          <div>
            <h6 className="text-primary font-bold text-xl">
              What our client Say
            </h6>
            <div className="mt-2 -mx-4">
              <Splide
                options={{
                  autoplay: true,
                  arrows: false,
                  pagination: false,
                  type: "loop",
                  gap: "0rem",
                  perPage: 3,
                  breakpoints: {
                    1024: {
                      perPage: 2,
                    },
                    768: {
                      perPage: 1,
                    },
                  },
                }}
              >
                {[0, 1, 2].map((a) => (
                  <SplideSlide key={`__testimonialSlide${a}`}>
                    <GroupTripTestimonial />
                  </SplideSlide>
                ))}
              </Splide>
            </div>
          </div>
        </section>

        <section className="mt-6 md:mt-16">
          <h3 className="text-[28px] text-primary font-bold">
            Make an Enquiry
          </h3>
          <GroupTripContactForm />
        </section>
        <div className="bg-primary w-full h-[300px] flex items-center justify-center text-primary font-semibold text-3xl mt-16">
          Ads
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps(ctx) {
  const { slug } = ctx.query;
  try {
    const res = await getSingleGroupTrip(`grouptrips/${slug}`);
    return {
      props: {
        data: res.data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        error: error.message,
      },
    };
  }
}

Slug.propTypes = {
  data: PropTypes.shape().isRequired,
};
