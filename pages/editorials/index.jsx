/* eslint-disable no-nested-ternary */
import axios from "axios";
import { IconContext } from "react-icons";
import { useEffect, useState } from "react";
import { HiOutlineSearch } from "react-icons/hi";
import Skeleton from "react-loading-skeleton";
import Head from "next/head";
import { getFeaturedEditorials } from "../../utils/services/ApiCalls";
// import Searchbar from "../../features/searchbar/Searchbar";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import NewsSlide from "../../components/Shared/newsSlider/NewsSlide";
import Pagination from "../../components/Shared/Pagination";
import InputField from "../../components/Shared/InputField";

const iconClass = { className: "text-primary" };
export default function Index() {
  const [editorials, setEditorials] = useState([]);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [searchInput, setSearchInput] = useState("");
  const [paginationId, setPaginationId] = useState(1);
  const [meta, setMeta] = useState({});
  const categories = [
    "all",
    "articles",
    "blogs",
    "news",
    "entertainment",
    "interviews",
    "features",
  ];

  async function handleSearch(e) {
    setSearchInput(e.target.value);
    console.log(e.target.value);
    if (!isLoading) setIsLoading(true);
    try {
      const res = await getFeaturedEditorials(
        "editorials",
        {
          search: e.target.value,
        },
        true,
      );
      if (res.status === 200) {
        setEditorials(res.data.data);
        setError("");
        setMeta(res.data.meta);
        setIsLoading(false);
      }
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setEditorials(null);
      setError(error.message);
      setIsLoading(false);
    }
  }

  async function handleFilter(e) {
    if (!isLoading) setIsLoading(true);
    try {
      const res = await getFeaturedEditorials(
        "editorials",
        {
          sort: "created_at",
          order: e.target.value,
        },
        true,
      );
      if (res.status === 200) {
        setEditorials(res.data.data);
        setError("");
        setMeta(res.data.meta);
        setIsLoading(false);
      }
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setEditorials(null);
      setError(error.message);
      setIsLoading(false);
    }
  }

  async function handlePagination() {
    if (!isLoading) setIsLoading(true);
    window.scrollTo(0, 0);
    try {
      const res = await getFeaturedEditorials(
        "editorials",
        {
          page: paginationId,
        },
        true,
      );
      if (res.status === 200) {
        setEditorials(res.data.data);
        setMeta(res.data.meta);
        setError("");
        setIsLoading(false);
      }
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setEditorials(null);
      setError(error.message);
      setIsLoading(false);
    }
  }
  useEffect(() => {
    handlePagination();
  }, [paginationId]);

  return (
    <>
      <Head>
        <title>Editorials | TravelGay</title>
      </Head>
      <InternalHeroSection
        hotelForm={false}
        heading="Editorials"
        description="A popular destination for gay travelers. Las Vegas has it all – fine restaurants, casinos, nightlife and world-class entertainment by iconic performers."
      />
      <div className="theme-container px-4 md:px-0 mt-8">
        <div className="flex items-start flex-col md:flex-row justify-between gap-y-4">
          <div className="flex items-center gap-2 flex-wrap">
            {categories?.map((c) => (
              <button
                type="button"
                key={c}
                className="px-4 py-3 rounded-md bg-primary text-xs lg:text-sm text-primary capitalize"
              >
                {c}
              </button>
            ))}
          </div>
          <div className="flex items-center">
            <div className="w-full md:w-auto md:min-w-[180px]">
              {/* <Searchbar
                className="flex items-center bg-white rounded mt-[2px] xl:shadow-none border"
                handleSearch={(e) => {
                  handleSearch(e);
                }}
              >
                <div className="px-4 xl:px-3 2xl:px-4 border-l">
                  <IconContext.Provider value={iconClass}>
                    <HiOutlineSearch />
                  </IconContext.Provider>
                </div>
              </Searchbar> */}
              <div className="flex items-center">
                <div className="w-full md:w-auto md:min-w-[180px]">
                  <div className="flex items-center bg-white rounded mt-[2px] xl:shadow-none border">
                    <InputField
                      value={searchInput}
                      type="text"
                      name="search"
                      id="search"
                      autoComplete="off"
                      onChange={(e) => {
                        handleSearch(e);
                      }}
                      className="px-4 py-2 2xl:px-6 text 2xl:text-base 2xl:py-[12px] rounded w-full focus:outline-0 border-0"
                      placeholder="What are you looking for?"
                    />
                    <button
                      type="button"
                      className="px-4 xl:px-3 2xl:px-4 border-l"
                    >
                      <IconContext.Provider value={iconClass}>
                        <HiOutlineSearch />
                      </IconContext.Provider>
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div>
              <select
                name="sort"
                id="sort"
                className="bg-secondary min-w-[120px] h-[42px] 2xl:h-[50px] px-2 rounded-md ml-2 text-sm"
                onChange={handleFilter}
              >
                <option hidden disabled>
                  Sort By
                </option>
                <option value="desc">Most Recent</option>
                <option value="asc">Most Old</option>
              </select>
            </div>
          </div>
        </div>
        {/* <div className="grid grid-cols-1 lg:grid-cols-2 grid-rows-1 md:grid-rows-2 ">
          <div className="row-span-2 overflow-hidden">
            <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden h-[calc(100%_-_32px)] flex flex-col">
              <div className="h-full relative">
                <img
                  src="/homepage/featured3.jpg"
                  alt=""
                  className="w-full h-full lg:object-cover"
                />
                <div className="featured-editorial-gradient absolute bottom-0 inset-x-0 top-[20%] flex items-end">
                  <p className="text-white font-bold text-xl p-4">
                    Featured Article
                  </p>
                </div>
              </div>
              <div className="p-4">
                <h2 className="mt-1 font-semibold text-primary">
                  Tom Prior On Gay Travel, Mindfulness
                </h2>
                <p className="text-[13px] mt-1">
                  Welcome to Washington, DC – the capital of the United States
                  of America. There’s plenty of culture, nightlife and LGBTQ+ of
                  culture, nightlife check out....
                  <Link legacyBehavior href="#">
                    <a className="text-[13px] font-semibold text-primary">
                      Read More
                    </a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
          <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden h-[calc(100%_-_32px)] flex flex-col">
            <div className="h-full lg:max-h-[283px] relative">
              <img
                src="/homepage/featured3.jpg"
                alt=""
                className="w-full h-full lg:object-cover"
              />
              <div className="featured-editorial-gradient absolute bottom-0 inset-x-0 top-[20%] flex items-end">
                <p className="text-white font-bold text-xl p-4">
                  Featured Article
                </p>
              </div>
            </div>
            <div className="p-4">
              <h2 className="mt-1 font-semibold text-primary">
                Tom Prior On Gay Travel, Mindfulness
              </h2>
              <p className="text-[13px] mt-1">
                Welcome to Washington, DC – the capital of the United States of
                America. There’s plenty of culture, nightlife and LGBTQ+ of
                culture, nightlife check out....
                <Link legacyBehavior href="#">
                  <a className="text-[13px] font-semibold text-primary">
                    Read More
                  </a>
                </Link>
              </p>
            </div>
          </div>
          <div className="relative shadow-[0_0_10px_rgba(0,0,0,0.2)] m-4 rounded-md overflow-hidden h-[calc(100%_-_32px)] flex flex-col">
            <div className="h-full lg:max-h-[283px] relative">
              <img
                src="/homepage/featured3.jpg"
                alt=""
                className="w-full h-full lg:object-cover"
              />
              <div className="featured-editorial-gradient absolute bottom-0 inset-x-0 top-[20%] flex items-end">
                <p className="text-white font-bold text-xl p-4">
                  Featured Article
                </p>
              </div>
            </div>
            <div className="p-4">
              <h2 className="mt-1 font-semibold text-primary">
                Tom Prior On Gay Travel, Mindfulness
              </h2>
              <p className="text-[13px] mt-1">
                Welcome to Washington, DC – the capital of the United States of
                America. There’s plenty of culture, nightlife and LGBTQ+ of
                culture, nightlife check out....
                <Link legacyBehavior href="#">
                  <a className="text-[13px] font-semibold text-primary">
                    Read More
                  </a>
                </Link>
              </p>
            </div>
          </div>
        </div> */}
        {isLoading ? (
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-6">
            <div className="p-4 h-[400px] md:h-[438px] lg:h-[388px] xl:h-[455px]">
              <Skeleton className="w-full h-full" />
            </div>
            <div className="p-4 h-[400px] md:h-[438px] lg:h-[388px] xl:h-[455px]">
              <Skeleton className="w-full h-full" />
            </div>
            <div className="p-4 h-[400px] md:h-[438px] lg:h-[388px] xl:h-[455px]">
              <Skeleton className="w-full h-full" />
            </div>
          </div>
        ) : error ? (
          <div className="mt-6">
            <p className="text-sm text-red-500">{error}</p>
          </div>
        ) : editorials.length > 0 ? (
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 mt-6">
            {editorials?.map((e) => (
              <NewsSlide editorial={e} loc="editorial" key={e.id} />
            ))}
          </div>
        ) : (
          <p className="text-red-500 text-sm mt-6">No data found...</p>
        )}

        <div className="mt-8">
          <Pagination
            handlePagination={(e) => setPaginationId(e.target.id)}
            meta={meta}
          />
        </div>
      </div>
    </>
  );
}
