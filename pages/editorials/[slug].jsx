import parse from "html-react-parser";
import Link from "next/link";
import { useRef, useState } from "react";
import { IconContext } from "react-icons";
import Head from "next/head";
import PropTypes from "prop-types";
import { HiOutlineArrowSmLeft, HiOutlineArrowSmRight } from "react-icons/hi";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import calls from "../../utils/services/PromiseHandler/PromiseHandler";
import AsideComunity from "../../components/aside/AsideComunity";
import AsideSearchHotel from "../../components/aside/AsideSearchHotel";
import FeaturedSlider from "../../components/Shared/Featured/FeaturedSlider";
import NewsSlider from "../../components/Shared/newsSlider/NewsSlider";
import HotelSlider from "../../components/Shared/hotelSlider/HotelSlider";
import SliderWithText from "../../components/Shared/internalSliderText/SliderWithText";
import InternalNavigation from "../../components/Shared/internalNavigation/InternalNavigation";

const slides = [
  {
    image: { original_url: "/homepage/party1.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
  {
    image: { original_url: "/homepage/party2.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
  {
    image: { original_url: "/homepage/party1.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
  {
    image: { original_url: "/homepage/party2.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
];

const hotelsByArea = [
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip1.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip2.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip3.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip4.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
];

const sliderButton = {
  className: "text-[#743D7D] h-6 w-6 group-hover:text-white ",
};

export default function Slug({ data }) {
  const [isLoading] = useState(false);
  const hotelRef = useRef(null);

  function handleSlider(ref, dir) {
    ref.current.splide.go(dir);
  }

  return (
    <div>
      <Head>
        <title>
          {data?.name ? `${data?.name} | TravelGay` : "Editorial | TravelGay"}
        </title>
      </Head>
      <InternalHeroSection
        heroImage={data?.image?.original_url}
        heading={data?.name ?? ""}
        description={data?.sub_title ?? ""}
      />
      <div className="theme-container px-4 md:px-0 xl:mt-5 editorial-details-container">
        <InternalNavigation type="editorial" id={data?.id} />
        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_8px)_calc(30%_-_8px)] gap-4 xl:mt-8">
          <div>
            {" "}
            <div className="mb:my-[72px] my-7 w-full font-[600] flex justify-center items-center text-primary md:h-[201px] bg-primary h-[304px]">
              ADS
            </div>
            <div className="editorial-content">
              {data?.content && parse(data.content)}
            </div>
            <section className="mt-7">
              <h3 className="text-primary font-bold md:text-[28px] text-[20px]">
                More Gay Travel News, Interviews and Features
              </h3>

              <div className="mt-5">
                <NewsSlider
                  slidesPerPage={3}
                  editorials={slides}
                  sidebar
                  isLoading={isLoading}
                />
              </div>
            </section>
            <section className="mt-6">
              <div className="px-4 md:px-0">
                <div className="flex items-center justify-between gap-4">
                  <h2 className="text-xl text-primary md:text-[28px] font-bold ">
                    {data?.name} Tours
                  </h2>
                  <Link legacyBehavior href="#">
                    <a className="underline text-xs lg:text-sm text-[#666] w-[50px] lg:w-auto inline-block">
                      View All
                    </a>
                  </Link>
                </div>
                <p className="text-xs mt-3">
                  Browse a selection of tours in Las Vegas from our partners
                  with free cancellation 24 hours before your tour starts.
                </p>
                <div className="flex items-center justify-between gap-2 bg-[#e1f0ff] px-3 py-4 rounded-md mt-4">
                  <span className="text-xs">
                    The <strong>best experiences</strong> in
                    <strong> Las Vegas</strong> for your trip
                  </span>
                  <Link legacyBehavior href="#">
                    <a className="">
                      <img src="/images/getyourguide.jpg" alt="" />
                    </a>
                  </Link>
                </div>
              </div>
              <SliderWithText data={slides} withRating isLoading={isLoading} />
            </section>
            <section className="md:mt-[80px] mt-5">
              <div className="flex justify-between items-center mb-4">
                <h2 className="text-primary text-xl md:text-[28px] font-bold  ">
                  Featured San Francisco Hotels
                </h2>
                <div>
                  <button
                    onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>
                  <button
                    onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>
              <HotelSlider
                noDataMessage="No Data Found"
                reff={hotelRef}
                data={slides}
                loc="notHome"
                isLoading={isLoading}
              />
              <div className="mb:my-[72px] my-7  w-full font-[600] flex justify-center items-center text-primary  md:h-[201px] bg-primary h-[304px]">
                ADS
              </div>
            </section>
          </div>

          <div className="md:p-0 px-4 md:w-full">
            <div className="mb-5">
              <h2 className="mb-[10px] md:text-[24px] font-[700] text-[18px] text-primary">
                Community Photos
              </h2>
              <div className="gap-2 flex mt-3">
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px]"
                    alt="Community"
                  />
                </div>
                {/* <div className="flex-1 max-h-[168px] max-w-[172px] bg-[#FCF4F6] rounded-[5px]" /> */}
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px]"
                    alt="Community"
                  />
                </div>
              </div>
            </div>
            <AsideComunity />
            <div className=" my-4 hidden max-w-[378px] h-0  md:h-[304px] bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
            <div className="slidebar-on-side">
              <AsideSearchHotel
                heading=" Search Hotels by Area"
                data={hotelsByArea}
              />
            </div>
            <div className="hidden md:block">
              <div className="text-center pb-8">
                <h2 className="text-primary font-[700] text-[24px] text-left my-5">
                  More Gay San Francisco
                </h2>
                {slides?.length > 0 && <FeaturedSlider data={slides} sidebar />}
              </div>
            </div>
            <div className=" my-4 hidden max-w-[378px] h-0  md:h-[509px] bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Slug.propTypes = {
  data: PropTypes.shape().isRequired,
};

export async function getServerSideProps(context) {
  try {
    const res = await calls(`editorials/${context.query.slug}`, "get");
    return {
      props: {
        data: res?.data,
      },
    };
  } catch (error) {
    return {
      props: {
        error: error.message,
      },
    };
  }
}
