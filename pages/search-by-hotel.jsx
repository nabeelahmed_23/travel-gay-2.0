/* eslint-disable camelcase */
import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import Head from "next/head";
import { GrClose } from "react-icons/gr";
import PropTypes from "prop-types";
import axios from "axios";
import { startCase } from "lodash";
import Pagination from "../components/Shared/Pagination";
import {
  getFilters,
  getHotelListingBySearch,
} from "../utils/services/ApiCalls";
import HotelListing from "../components/scb/HotelListing";
import Filters from "../components/scb/Filters";
// eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxgl from "!mapbox-gl";

export default function SearchByHotel() {
  const [sliderValue, setSliderValue] = useState("0");
  const [sortBy, setSortBy] = useState(false);
  const [filters, setFilters] = useState(false);
  const [filterData, setFilterData] = useState({
    category: [],
    amenities: [],
  });
  const [hotelListing, setHotelListing] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");
  const [featured, setFeatured] = useState(0);
  const [rating, setRating] = useState([]);
  const [mapTheme, setMapTheme] = useState(false);
  const [showOnMap, setShowOnMap] = useState(false);
  const [coords, setCoords] = useState([]);
  const [page, setPage] = useState(1);
  const [category, setCategory] = useState([]);
  const [amenities, setAmenities] = useState([]);
  const [meta, setMeta] = useState({});
  const router = useRouter();

  const { name } = router.query;

  function handleInputChange(e) {
    const { name, id, checked } = e.target;
    if (checked) {
      if (filterData[name]?.length <= 0) {
        setFilterData((p) => ({ ...p, [name]: [id] }));
      } else {
        setFilterData((p) => ({ ...p, [name]: [...filterData[name], id] }));
      }
    } else {
      const a = filterData?.[name].filter((a) => a !== id);
      setFilterData((p) => ({ ...p, [name]: a }));
    }
  }
  function responsive() {
    if (window.innerWidth >= 1280) {
      setSortBy(true);
      setFilters(true);
    } else {
      setSortBy(false);
      setFilters(false);
    }
  }
  async function getHotelsListing() {
    setIsLoading(true);
    setError("");
    setHotelListing([]);

    try {
      const res = await getHotelListingBySearch("search", {
        ...router.query,
        tg_approved: featured,
        rating,
        page,
        category: filterData?.category,
        amenities: filterData?.amenities,
      });
      setHotelListing(res.data.data);
      setMeta(res.data.meta ?? {});
      setIsLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        return;
      }
      setError(
        error?.response?.data?.message?.[0]?.description ??
          error?.response?.data?.message,
      );
      setIsLoading(false);
    }
  }

  const fetchFilters = async () => {
    try {
      const res = await getFilters("hotels/filters");
      setCategory(res.data.hotel_categories);
      setAmenities(res.data.amenities);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    getHotelsListing();
  }, [router.query, featured, rating, page, filterData]);

  useEffect(() => {
    fetchFilters();
    responsive();
    // window.addEventListener("resize", responsive);
    // return () => {
    //   window.addEventListener("resize", responsive);
    // };
  }, []);

  function handleDelete(item, value) {
    const a = { ...filterData };
    a[item].splice(value, 1);
    setFilterData(a);
  }

  function handleRating(e) {
    const { checked, value } = e.target;
    if (checked) {
      setRating((p) => [...p, value]);
    } else setRating(rating.filter((i) => i !== value));
  }

  const handleShowOnMap = (coords) => {
    window.scrollTo(0, 0);
    setMapTheme(true);
    setCoords(coords);
    setShowOnMap(true);
  };

  if (mapTheme) {
    return (
      <div className="grid md:grid-cols-[295px_1fr] min-h-[50vh]">
        <Head>
          <title>Hotel Listing Map</title>
          <style>
            {`
          #menu-hotel-form{
            display:block !important;
          }
          footer{
            margin-top: 0px !important;
          }
          `}
          </style>
        </Head>
        <div className="-mt-6 hidden md:block md:max-h-[calc(100vh_-_316px)] lg:max-h-[calc(100vh_-_204px)] xl:max-h-[calc(100vh_-_152px)] 2xl:max-h-[calc(100vh_-_183px)] overflow-auto dropdownContent">
          <Filters
            featured={featured}
            setFeatured={setFeatured}
            sliderValue={sliderValue}
            setSliderValue={setSliderValue}
            handleInputChange={(e) => handleInputChange(e)}
            handleRating={(e) => handleRating(e)}
            rating={rating}
            filterData={filterData}
            amenities={amenities}
            category={category}
          />
        </div>
        <div>
          {isLoading && <p className="m-4">Loading...</p>}
          {!isLoading && hotelListing?.length > 0 && error.length <= 0 ? (
            <div className="relative h-screen  md:max-h-[calc(100vh_-_316px)] lg:max-h-[calc(100vh_-_204px)] xl:max-h-[calc(100vh_-_152px)] 2xl:max-h-[calc(100vh_-_183px)] ">
              <MapForPage
                hotelListing={hotelListing}
                coords={coords}
                setShowOnMap={setShowOnMap}
                showOnMap={showOnMap}
              />
              <button
                onClick={() => setMapTheme(false)}
                type="button"
                className="hidden md:flex py-2 px-3 rounded-md absolute top-6 right-6 md:top-12 md:right-10 bg-white items-center gap-4"
              >
                Back to hotel listing <GrClose />
              </button>
              <button
                onClick={() => setMapTheme(false)}
                type="button"
                className="py-2 px-3 rounded-md absolute top-6 right-6 md:top-12 md:right-10 flex md:hidden items-center gap-4"
              >
                <GrClose />
              </button>
            </div>
          ) : (
            !isLoading && (
              <>
                <p className="text-red-500 text-sm m-4">No data found...</p>
                <button
                  type="button"
                  className="mx-4 shadow-[0_0_15px_rgba(0,0,0,0.15)] px-4 py-2 rounded"
                  onClick={() => setMapTheme(false)}
                >
                  Go back to listing
                </button>
              </>
            )
          )}
        </div>
      </div>
    );
  }

  return (
    <div className={mapTheme ? "" : "px-4 md:px-0 theme-container"}>
      <Head>
        <title>Hotel Listing</title>
        <style>
          {`
          #menu-hotel-form{
            display:block !important;
          }
          `}
        </style>
      </Head>
      <div className={mapTheme ? "" : "mt-6 lg:mt-16"}>
        <div className="flex xl:hidden items-center justify-between">
          <button
            type="button"
            className="px-3 sm:px-5 py-1 border border-[#D74874] rounded-lg text-primary text-sm flex items-center gap-3"
            onClick={() => setSortBy(!sortBy)}
          >
            {" "}
            <span>
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="17"
                height="16"
                viewBox="0 0 17 16"
                fill="none"
              >
                <path
                  d="M16.2836 0.715095H10.0603C9.91222 0.714351 9.77013 0.772624 9.66525 0.877021C9.56036 0.981294 9.50146 1.12313 9.50146 1.27117C9.50146 1.41909 9.56036 1.56092 9.66525 1.66532C9.77014 1.76959 9.91223 1.82787 10.0603 1.82712H16.2836C16.4315 1.82787 16.5737 1.76959 16.6786 1.66532C16.7833 1.56092 16.8424 1.41909 16.8424 1.27117C16.8424 1.12313 16.7833 0.981294 16.6786 0.877021C16.5737 0.772624 16.4315 0.714352 16.2836 0.715095Z"
                  fill="#D74874"
                />
                <path
                  d="M13.4917 15.0107C13.6897 15.0096 13.8722 14.9035 13.9709 14.7319C14.0696 14.5603 14.0696 14.3491 13.9709 14.1775C13.8722 14.0059 13.6897 13.8997 13.4917 13.8987H10.0603C9.91222 13.8979 9.77013 13.9562 9.66525 14.0605C9.56036 14.1649 9.50146 14.3067 9.50146 14.4546C9.50146 14.6027 9.56036 14.7445 9.66525 14.8488C9.77014 14.9532 9.91223 15.0115 10.0603 15.0107L13.4917 15.0107Z"
                  fill="#D74874"
                />
                <path
                  d="M14.7418 11.2336C14.7764 11.0697 14.7353 10.8987 14.6297 10.7686C14.5241 10.6384 14.3654 10.5627 14.1978 10.5627H10.0595C9.91196 10.5627 9.77061 10.6213 9.66633 10.7255C9.56206 10.8298 9.50342 10.9712 9.50342 11.1187C9.50342 11.2661 9.56206 11.4076 9.66633 11.5119C9.77061 11.6161 9.91195 11.6747 10.0595 11.6747H14.1978C14.3254 11.6747 14.449 11.6309 14.5479 11.5506C14.647 11.4703 14.7154 11.3584 14.7418 11.2336L14.7418 11.2336Z"
                  fill="#D74874"
                />
                <path
                  d="M14.9034 7.22681H10.0595C9.91196 7.22681 9.77061 7.28545 9.66633 7.38972C9.56206 7.49387 9.50342 7.63534 9.50342 7.78276C9.50342 7.9303 9.56206 8.07165 9.66633 8.17593C9.77061 8.2802 9.91195 8.33884 10.0595 8.33884H14.9034C15.1014 8.33785 15.2839 8.2316 15.3826 8.06C15.4813 7.8884 15.4813 7.67724 15.3826 7.50566C15.2839 7.33407 15.1014 7.22781 14.9034 7.22681V7.22681Z"
                  fill="#D74874"
                />
                <path
                  d="M15.611 3.89088H10.0603C9.91222 3.89013 9.77013 3.94828 9.66525 4.05268C9.56036 4.15708 9.50146 4.29891 9.50146 4.44683C9.50146 4.59474 9.56036 4.73658 9.66525 4.84098C9.77014 4.94525 9.91223 5.00352 10.0603 5.00278H15.611C15.7384 5.00278 15.8621 4.95901 15.961 4.87867C16.0601 4.79845 16.1285 4.68649 16.1549 4.56189V4.56176C16.1895 4.39785 16.1483 4.22687 16.0428 4.0967C15.9372 3.96652 15.7786 3.89089 15.611 3.89089L15.611 3.89088Z"
                  fill="#D74874"
                />
                <path
                  d="M4.26617 15.0101L7.69253 11.585L6.90633 10.7988L4.82299 12.882V0.713135H3.71097V12.8836L1.62602 10.7988L0.841309 11.585L4.26617 15.0101Z"
                  fill="#D74874"
                />
              </svg>
            </span>
            <span>Sort By</span>
          </button>
          <button
            type="button"
            className="px-3 sm:px-5 py-1 border border-[#D74874] rounded-lg text-primary text-sm flex items-center gap-3"
            onClick={() => setFilters(!filters)}
          >
            {" "}
            <span>
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
              >
                <path
                  d="M6.8213 1.39239C6.30992 0.232581 5.01392 -0.265419 3.92662 0.28005C3.46818 0.51005 3.09939 0.903425 2.8838 1.39239H1.07129C0.553525 1.39239 0.133789 1.84011 0.133789 2.39239C0.133789 2.94467 0.553525 3.39239 1.07129 3.39239H2.88066C3.39204 4.55221 4.68805 5.05021 5.77534 4.50474C6.23375 4.27477 6.60254 3.88136 6.81816 3.39239H14.1963C14.7141 3.39239 15.1338 2.94467 15.1338 2.39239C15.1338 1.84011 14.7141 1.39239 14.1963 1.39239H6.8213Z"
                  fill="#D74874"
                />
                <path
                  d="M10.4182 5.30908C9.57544 5.31068 8.80886 5.82968 8.4488 6.64243H1.07129C0.553525 6.64243 0.133789 7.09014 0.133789 7.64243C0.133789 8.19471 0.553525 8.64243 1.07129 8.64243H8.44628C8.95766 9.80224 10.2537 10.3002 11.341 9.75477C11.7994 9.5248 12.1682 9.13139 12.3838 8.64243H14.1963C14.7141 8.64243 15.1338 8.19471 15.1338 7.64243C15.1338 7.09014 14.7141 6.64243 14.1963 6.64243H12.3869C12.0269 5.82989 11.2607 5.31096 10.4182 5.30908Z"
                  fill="#D74874"
                />
                <path
                  d="M4.84941 10.5569C4.00689 10.5587 3.24063 11.0777 2.88066 11.8902H1.07129C0.553525 11.8902 0.133789 12.3379 0.133789 12.8902C0.133789 13.4425 0.553525 13.8902 1.07129 13.8902H2.88066C3.39204 15.05 4.68805 15.548 5.77534 15.0026C6.23375 14.7726 6.60254 14.3792 6.81816 13.8902H14.1963C14.7141 13.8902 15.1338 13.4425 15.1338 12.8902C15.1338 12.3379 14.7141 11.8902 14.1963 11.8902H6.8213C6.46083 11.0766 5.69305 10.5575 4.84941 10.5569Z"
                  fill="#D74874"
                />
              </svg>
            </span>
            <span>Filters</span>
          </button>
          <button
            type="button"
            className="px-3 sm:px-5 py-1 border border-[#D74874] rounded-lg text-primary text-sm flex items-center gap-3"
            onClick={() => setMapTheme(true)}
          >
            {" "}
            <span>
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="8"
                height="11"
                viewBox="0 0 8 11"
                fill="none"
              >
                <path
                  d="M4.0983 0.267822C3.1325 0.267822 2.2061 0.638217 1.52314 1.29729C0.840314 1.95652 0.456543 2.85062 0.456543 3.7828C0.456543 7.29777 4.0983 10.8127 4.0983 10.8127C4.0983 10.8127 7.74005 7.29777 7.74005 3.7828C7.74005 2.85062 7.3563 1.95647 6.67345 1.29729C5.99045 0.638234 5.0641 0.267822 4.0983 0.267822ZM4.0983 5.16996C3.71641 5.16996 3.34992 5.0235 3.07999 4.76262C2.81004 4.5019 2.65831 4.14817 2.65864 3.7796C2.65881 3.41084 2.81089 3.05744 3.08118 2.79691C3.35147 2.53652 3.71797 2.3904 4.1 2.39072C4.48189 2.39121 4.84804 2.53816 5.11783 2.7992C5.38744 3.06024 5.53867 3.41411 5.53799 3.78273C5.53732 4.15083 5.38541 4.50371 5.11546 4.76378C4.84551 5.02384 4.47971 5.16997 4.09833 5.16997L4.0983 5.16996Z"
                  fill="#D74874"
                />
              </svg>
            </span>
            <span>Map</span>
          </button>
        </div>
        <div className="grid xl:grid-cols-[300px_1fr] xl:gap-8">
          <div id="filters">
            <button type="button" onClick={() => setMapTheme(true)}>
              <img
                src="/scb/map.jpg"
                alt=""
                className="w-full max-w-md hidden xl:block"
              />
            </button>

            {filters && (
              <Filters
                featured={featured}
                setFeatured={setFeatured}
                sliderValue={sliderValue}
                setSliderValue={setSliderValue}
                handleInputChange={(e) => handleInputChange(e)}
                handleRating={(e) => handleRating(e)}
                rating={rating}
                filterData={filterData}
                amenities={amenities}
                category={category}
              />
            )}
          </div>

          <div>
            {sortBy && (
              <>
                <div className="flex flex-col md:flex-row md:items-center">
                  <h1 className="font-semibold lg:text-lg xl:text-xl 2xl:text-2xl">
                    {name}:{" "}
                    <span className=" font-normal">
                      &nbsp;{hotelListing?.length} Results Found
                    </span>
                  </h1>
                </div>
                <div className="flex flex-col md:flex-row md:items-start md:justify-between mt-6 gap-y-4">
                  <div className="flex items-center flex-wrap gap-2">
                    {Object.keys(filterData)?.length > 0 &&
                      Object.keys(filterData).map((item) =>
                        Object.keys(filterData[item]).map((a, idx) => (
                          <div
                            className="bg-secondary flex items-center gap-2 py-1 px-3 rounded-full"
                            key={`filtersList${idx}`}
                          >
                            <p className="text-xs">
                              {startCase(filterData?.[item][idx])}
                            </p>
                            <button
                              type="button"
                              onClick={() => handleDelete(item, a)}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="10"
                                height="10"
                                viewBox="0 0 12 12"
                                fill="none"
                              >
                                <path
                                  d="M10.6894 1.21741L0.689392 11.2174"
                                  stroke="#333333"
                                  strokeWidth="1.5"
                                />
                                <path
                                  d="M10.6894 11.2174L0.689392 1.21741"
                                  stroke="#333333"
                                  strokeWidth="1.5"
                                />
                              </svg>
                            </button>
                          </div>
                        )),
                      )}
                    {filterData.length > 0 && (
                      <button
                        type="button"
                        className="px-4 py-1 text-xs"
                        onClick={() => setFilterData([])}
                      >
                        Clear all filters
                      </button>
                    )}
                  </div>
                  <div>
                    <select
                      name="sort"
                      id="sort"
                      className="bg-secondary h-[42px] px-2 rounded-md text-sm border-0 w-32"
                    >
                      <option hidden disabled>
                        Sort By
                      </option>
                      <option value="desc">Most Recent</option>
                      <option value="asc">Most Old</option>
                    </select>
                  </div>
                </div>
              </>
            )}

            <HotelListing
              error={error}
              isLoading={isLoading}
              hotelListing={hotelListing}
              handleShowOnMap={handleShowOnMap}
            />
            <Pagination
              meta={meta}
              handlePagination={(e) => {
                setPage(e.target.id);
                window.scrollTo(0, 0);
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export function MapForPage({ hotelListing, coords, showOnMap, setShowOnMap }) {
  const map = useRef();
  const mapContainer = useRef();

  useEffect(() => {
    if (map.current) return; // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [
        hotelListing[0]?.hotel?.longitude,
        hotelListing[0]?.hotel?.latitude,
      ],
      zoom: 10,
    });

    const geojson = {
      type: "FeatureCollection",
      features: hotelListing?.map((item) => ({
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: [item?.hotel?.longitude, item?.hotel?.latitude],
        },
        properties: {
          image: item?.hotel.image?.original_url,
          title: item?.hotel?.name,
          description: item?.hotel?.address,
        },
      })),
    };

    // eslint-disable-next-line no-restricted-syntax
    for (const feature of geojson.features) {
      // create a HTML element for each feature
      const el = document.createElement("div");
      el.className = "marker";

      // make a marker for each feature and add to the map
      new mapboxgl.Marker(el)
        .setLngLat(feature.geometry.coordinates)
        .setPopup(
          new mapboxgl.Popup({ offset: 25 }) // add popups
            .setHTML(
              `<img src=${feature.properties.image} class="mb-2"> </img><h3>${feature.properties.title}</h3><p>${feature.properties.description}</p>`,
            ),
        )
        .addTo(map.current);
    }
  });

  useEffect(() => {
    if (!showOnMap) return;

    if (coords.length) {
      map.current.flyTo({
        center: coords,
        zoom: 16,
      });
      setShowOnMap(false);
    }
  }, [showOnMap]);

  return <div ref={mapContainer} />;
}

MapForPage.propTypes = {
  hotelListing: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  coords: PropTypes.arrayOf(PropTypes.string).isRequired,
  showOnMap: PropTypes.bool.isRequired,
  setShowOnMap: PropTypes.func.isRequired,
};

export async function getServerSideProps(ctx) {
  const {
    "check-in": checkIn,
    "check-out": checkOut,
    name,
    q,
    type,
  } = ctx.query;
  if (!checkIn || !checkOut || !name || !q || !type) {
    return {
      redirect: {
        permanent: false,
        destination: "/404",
      },
    };
  }
  return {
    props: {},
  };
}
