import React, { useState, useEffect } from "react";
import "../styles/style.css";
import Script from "next/script";
import "react-loading-skeleton/dist/skeleton.css";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { SessionProvider } from "next-auth/react";
import LoadingBar from "react-top-loading-bar";
import { Provider } from "react-redux";
import Layout from "./Layout";
import store from "../utils/store";
import "rc-rate/assets/index.css";
import "mapbox-gl/dist/mapbox-gl.css";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
// Default theme
import "@splidejs/splide/dist/css/splide.min.css";
// eslint-disable-next-line import/no-webpack-loader-syntax
import mapboxgl from "!mapbox-gl";

if (mapboxgl) {
  mapboxgl.accessToken =
    "pk.eyJ1IjoibmFiZWVsYWhtZWQxOTk0IiwiYSI6ImNsOXdsanhuZjAyM2EzeG5vdTE2eDQ0a2EifQ.9tSGCDVZST6KbToULKrk7w";
}

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  const [progress, setProgress] = useState(0);
  const router = useRouter();
  useEffect(() => {
    router.events.on("routeChangeStart", () => setProgress(50));
    router.events.on("routeChangeComplete", () => setProgress(100));
    return () => {
      router.events.off("routeChangeStart", () => setProgress(50));
      router.events.off("routeChangeComplete", () => setProgress(100));
    };
  });
  return (
    <React.StrictMode>
      <SessionProvider session={session}>
        <Provider store={store}>
          <Script
            src="https://accounts.google.com/gsi/client"
            strategy="lazyLoad"
          />
          {
            // eslint-disable-next-line @next/next/no-before-interactive-script-outside-document
            <Script
              src={`https://secure${
                process.env.Pay360_Sandbox ? ".mite" : ""
              }.pay360.com/cardlock/scripts/cardLock.js`}
              strategy="beforeInteractive"
            />
          }
          <Layout>
            <LoadingBar progress={progress} color="#D84D78" />
            <Component {...pageProps} />
          </Layout>
        </Provider>
      </SessionProvider>
    </React.StrictMode>
  );
}

export default MyApp;

MyApp.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.shape().isRequired,
};
