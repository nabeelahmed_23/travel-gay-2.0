import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import Skeleton from "react-loading-skeleton";
import Head from "next/head";
import PropTypes from "prop-types";
import { startCase } from "lodash";
import Pagination from "../../components/Shared/Pagination";
import Loader from "../../components/Shared/Loader";
import GroupTripHeader from "../../components/groupTrips/GroupTripHeader";
import GroupTripResultTile from "../../components/groupTrips/GroupTripResultTile";
import { getGroupTrips } from "../../utils/services/ApiCalls";

export default function GroupTripSlug() {
  const [groupTrips, setGroupTrips] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");
  const [meta, setMeta] = useState({});
  const [paginationId, setPaginationId] = useState(1);
  const router = useRouter();

  async function getAllGroupTrips() {
    setIsLoading(true);
    setError("");
    setGroupTrips([]);
    try {
      const res = await getGroupTrips(
        `continents/${router.query.slug}/group-trips`,
        {
          page: paginationId,
        },
      );
      setGroupTrips(res.data.data);
      setMeta(res.data.meta);
      setIsLoading(false);
    } catch (error) {
      if (axios.isCancel(error)) {
        setError("");
        return;
      }
      setError(error?.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    if (router.query.slug) getAllGroupTrips();
  }, [router.query.slug]);

  if (isLoading) {
    return <Loader text="Please wait while we fetch the results." />;
  }

  return (
    <div>
      <Head>
        <title>{startCase(router.query.slug)} Group Trips | TravelGay</title>
      </Head>
      <GroupTripHeader
        text={`Gay Group Trips: ${startCase(router.query.slug)}`}
      />
      <div className="theme-container px-4 md:px-0 mt-28 flex flex-col gap-16">
        <GroupTripResultWrapper
          isLoading={isLoading}
          groupTrips={groupTrips}
          error={error}
        />
        <div className="mt-8">
          <Pagination
            handlePagination={(e) => setPaginationId(e.target.id)}
            meta={meta}
          />
        </div>
      </div>
    </div>
  );
}

export function GroupTripResultWrapper({ isLoading, groupTrips, error }) {
  if (isLoading) {
    return (
      <div className="flex flex-col gap-4">
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
        <Skeleton className="w-full h-52" />
      </div>
    );
  }
  if (error) {
    return (
      <div className="my-6">
        <p className="text-sm text-red-500">{error}</p>
      </div>
    );
  }
  return (
    <div className="flex flex-col gap-y-8">
      {groupTrips?.length && !isLoading > 0 ? (
        groupTrips?.map((a, i) => (
          <GroupTripResultTile key={`groupTripsList${i}`} groupTrip={a} />
        ))
      ) : (
        <p className="text-red-500 text-sm m-4">No data found</p>
      )}
    </div>
  );
}

GroupTripResultWrapper.defaultProps = {
  error: null,
};

GroupTripResultWrapper.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  groupTrips: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  error: PropTypes.string,
};
