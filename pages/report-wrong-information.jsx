import axios from "axios";
import Head from "next/head";
import { useRouter } from "next/router";
import { useState, useRef, useEffect } from "react";
import { AiOutlineClose } from "react-icons/ai";
import ReCAPTCHA from "react-google-recaptcha";
import InternalHero from "../components/Shared/internalHero/InternalHeroSection";
import {
  captchaTokenVerification,
  getAPIUrl,
  getBaseURL,
} from "../utils/Helper";

export default function ReportWrongInformation() {
  const [data, setData] = useState({});
  const [selectedImage, setSelectedImage] = useState(null);
  const [error, setError] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState(null);
  const [verifyToken, setVerifyToken] = useState(null);
  const captchaRef = useRef();
  const ref = useRef(null);
  const router = useRouter();

  function handleChange(e) {
    const { name, value } = e.target;
    setData((p) => ({ ...p, [name]: value }));
  }

  async function submitFormHandler(e) {
    e.preventDefault();
    setIsLoading(true);
    setError({});
    setSuccessMessage(null);
    if (
      !data?.first_name ||
      !data?.last_name ||
      !data?.email ||
      !data?.venue ||
      !data?.comments ||
      !selectedImage
    ) {
      if (!data?.first_name)
        setError((p) => ({ ...p, first_name: "First Name is required" }));
      if (!data?.last_name)
        setError((p) => ({ ...p, last_name: "Last Name is required" }));
      if (!data?.email) setError((p) => ({ ...p, email: "Email is required" }));
      if (!data?.venue) setError((p) => ({ ...p, venue: "Venue is required" }));
      if (!data?.comments)
        setError((p) => ({ ...p, comments: "Comment is required" }));
      if (!selectedImage)
        setError((p) => ({ ...p, selectedImage: "Image is required" }));

      setIsLoading(false);
      return;
    }

    const { _widgetId } = captchaRef.current;
    const verified = await captchaTokenVerification(verifyToken);

    if (!verified) {
      if (verifyToken) captchaRef.current.reset(_widgetId);
      setError((p) => ({ ...p, captcha: "Captcha has failed. Try again." }));
      setVerifyToken(null);
      setIsLoading(false);
      return;
    }

    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      const fd = new FormData();
      Object.keys(data)?.forEach((item) => {
        fd.append([item], data[item]);
      });
      fd.append("file", selectedImage);
      axios({
        url: `${getAPIUrl()}report/wrong`,
        data: fd,
        method: "POST",
        headers: {
          "Content-Type": "multipart/form-data",
          Accept: "application/json",
        },
        withCredentials: true,
      })
        .then((res) => {
          setData({});
          setSuccessMessage(res.data.data);
          setSelectedImage(null);
          ref.current.value = null;
          setIsLoading(false);
        })
        .catch((err) => {
          console.log(err.message);
          setIsLoading(false);
        });
    });
  }

  useEffect(() => {
    if (typeof window !== "undefined") {
      if (
        typeof JSON.parse(localStorage.getItem("reportItem"))?.name !==
        "undefined"
      )
        setData((p) => ({
          ...p,
          venue: JSON.parse(localStorage.getItem("reportItem")).name,
        }));
    }

    const handleBrowseAway = () => {
      if (typeof window !== "undefined") {
        setTimeout(() => {
          if (typeof window !== "undefined") {
            localStorage.removeItem("reportItem");
          }
        }, 2000);
        localStorage.removeItem("abandonCartId");
      }
    };

    router.events.on("routeChangeStart", handleBrowseAway);
    return () => {
      router.events.off("routeChangeStart", handleBrowseAway);
    };
  }, []);

  useEffect(() => {
    const el = document.querySelector("#grecaptcha-wrapper div");
    if (el.children.length <= 0) {
      ref.current.props?.grecaptcha?.render(el, {
        sitekey: ref.current.props.sitekey,
        callback: ref.current.props.onChange,
      });
    }
  });

  return (
    <>
      <Head>
        <title>Report Wrong Information | TravelGay</title>
      </Head>
      <InternalHero
        heading="Corrections & Updates"
        description="Your Use of TravelGay.com"
      />
      <div className="theme-container px-4 md:px-0">
        <div className="bg-primary w-full h-[225px] flex items-center justify-center text-primary font-semibold text-3xl mt-8 md:mt-16">
          Ads
        </div>
        <h2 className="text-primary text-xl md:text-[28px] font-bold mt-8 md:mt-16">
          Do we need to correct or update this listing? Please tell us using
          this form.
        </h2>
        <form
          className="mt-10 flex flex-wrap md:gap-x-7 lg:gap-x-16 gap-y-3 md:gap-y-7"
          onSubmit={submitFormHandler}
        >
          <label
            htmlFor="first_name"
            className="w-full md:w-[calc(50%_-_2rem)]"
          >
            <div className="text-sm md:text-base">First Name*</div>
            <input
              onChange={handleChange}
              value={data?.first_name ?? ""}
              type="text"
              name="first_name"
              id="first_name"
              className="form-input"
              placeholder="John"
            />
            {error?.first_name && (
              <p className="text-xs text-red-500 mt-1">{error?.first_name}</p>
            )}
          </label>
          <label htmlFor="last_name" className="w-full md:w-[calc(50%_-_2rem)]">
            <div className="text-sm md:text-base">Last Name*</div>
            <input
              onChange={handleChange}
              value={data?.last_name ?? ""}
              type="text"
              name="last_name"
              id="last_name"
              className="form-input"
              placeholder="Doe"
            />
            {error?.last_name && (
              <p className="text-xs text-red-500 mt-1">{error?.last_name}</p>
            )}
          </label>
          <label htmlFor="email" className="w-full md:w-[calc(50%_-_2rem)]">
            <div className="text-sm md:text-base">Email*</div>
            <input
              onChange={handleChange}
              value={data?.email ?? ""}
              type="email"
              name="email"
              id="email"
              className="form-input"
              placeholder="johndoe@example.com"
            />
            {error?.email && (
              <p className="text-xs text-red-500 mt-1">{error?.email}</p>
            )}
          </label>
          <label htmlFor="venue" className="w-full md:w-[calc(50%_-_2rem)]">
            <div className="text-sm md:text-base">Venue*</div>
            <input
              onChange={handleChange}
              value={data?.venue ?? ""}
              type="text"
              name="venue"
              id="venue"
              className="form-input"
              placeholder="Enter Venue"
            />
            {error?.venue && (
              <p className="text-xs text-red-500 mt-1">{error?.venue}</p>
            )}
          </label>
          <label htmlFor="comments" className="w-full">
            <div className="text-sm md:text-base">
              Correction or update to report*
            </div>
            <textarea
              onChange={handleChange}
              value={data?.comments ?? ""}
              name="comments"
              id="comments"
              rows={7}
              className="form-input"
            />
            {error?.comments && (
              <p className="text-xs text-red-500 mt-1">{error?.comments}</p>
            )}
          </label>
          <label htmlFor="image" className="cursor-pointer">
            <div className="text-sm md:text-base flex items-center gap-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="40"
                height="35"
                viewBox="0 0 40 35"
                fill="none"
              >
                <path
                  d="M39.0233 19.1046C38.8352 19.4716 38.8599 19.8841 38.753 20.27C37.7877 23.7299 35.5481 26.0378 31.9828 27.1348C31.2581 27.3576 30.4997 27.4581 29.7304 27.4553C27.8918 27.4486 26.0533 27.4524 24.2147 27.4534C23.7771 27.4534 23.4395 27.2837 23.2316 26.911C23.0395 26.5659 23.0573 26.216 23.2761 25.8889C23.5038 25.5466 23.8464 25.4167 24.2662 25.4186C25.9543 25.4243 27.6433 25.4157 29.3314 25.4224C31.8571 25.4319 33.858 24.404 35.3382 22.5001C38.2005 18.8202 36.9639 13.6858 32.7363 11.5239C32.1611 11.23 31.5452 11.0394 30.9066 10.9114C30.1641 10.7635 29.8967 10.4781 29.7987 9.75367C29.2819 5.93629 26.4196 3.02632 22.494 2.32846C18.6475 1.64482 14.709 3.49093 12.9071 6.82284C12.4427 7.68095 12.0952 7.82886 11.1338 7.58139C8.62893 6.93663 6.24087 8.58836 6.10523 11.0707C6.08246 11.4879 6.15375 11.9013 6.27553 12.3081C6.51413 13.1055 6.38938 13.4241 5.66267 13.883C4.03597 14.9108 3.02412 16.3473 2.80135 18.1878C2.50037 20.6748 3.46272 22.7153 5.51515 24.2599C6.58938 25.0687 7.83291 25.4394 9.21011 25.4281C11.2734 25.411 13.3377 25.4205 15.4011 25.4262C15.9753 25.4281 16.406 25.7713 16.4971 26.271C16.5832 26.7441 16.2981 27.2163 15.8268 27.3937C15.6456 27.4619 15.4595 27.4581 15.2723 27.4581C13.109 27.46 10.9447 27.461 8.78141 27.4572C8.53191 27.4572 8.28043 27.4401 8.0339 27.4079C5.60425 27.0826 3.72608 25.8841 2.31423 24.0077C0.96971 22.2204 0.386556 20.2112 0.672688 18.0123C0.951889 15.8666 1.98256 14.0935 3.73697 12.7262C4.00528 12.5167 4.08647 12.3223 4.03399 12.0037C3.51717 8.88798 5.99236 5.79975 9.26852 5.44608C9.81901 5.38635 10.3695 5.36549 10.912 5.4622C11.1734 5.50866 11.2804 5.41479 11.4071 5.22231C13.0823 2.68024 15.4486 1.04747 18.5158 0.40934C24.5711 -0.849846 30.475 2.83384 31.7482 8.6443C31.8116 8.93444 31.9393 9.07003 32.2403 9.16864C35.7699 10.3235 37.8936 12.7006 38.7965 16.1311C38.8926 16.4962 38.8708 16.8812 39.0223 17.2339C39.0223 17.689 39.0223 18.1441 39.0223 18.5993C38.9748 18.6476 38.9748 18.695 39.0223 18.7434C39.0233 18.8648 39.0233 18.9842 39.0233 19.1046Z"
                  fill="#D64D78"
                />
                <path
                  d="M18.753 16.3461C18.5857 16.3897 18.5223 16.4997 18.4382 16.5803C17.2867 17.6792 16.1372 18.7791 14.9907 19.8818C14.6877 20.1729 14.3412 20.3284 13.9105 20.2175C13.1353 20.0174 12.8561 19.1527 13.3709 18.5591C13.4343 18.4861 13.5076 18.4188 13.5779 18.3515C15.3551 16.6495 17.1313 14.9475 18.9084 13.2465C19.5421 12.6406 20.0846 12.6368 20.7133 13.2379C22.5173 14.9646 24.3232 16.6893 26.1221 18.4207C26.7865 19.0598 26.5805 19.9824 25.731 20.2118C25.2825 20.3322 24.9311 20.1625 24.6202 19.8638C23.4826 18.7696 22.341 17.6783 21.2005 16.586C21.1153 16.5044 21.0242 16.4267 20.8767 16.2949C20.8767 16.4855 20.8767 16.6106 20.8767 16.7358C20.8767 22.1357 20.8767 27.5356 20.8767 32.9355C20.8767 33.6561 20.5807 34.0543 19.9837 34.1482C19.3639 34.2459 18.8055 33.8268 18.7599 33.2247C18.75 33.0938 18.754 32.9611 18.754 32.8293C18.754 27.4654 18.754 22.1015 18.754 16.7377C18.753 16.6125 18.753 16.4864 18.753 16.3461Z"
                  fill="#D64D78"
                />
              </svg>
              {selectedImage?.name
                ? selectedImage?.name
                : "Choose File - Add a picture"}
            </div>
            <input
              type="file"
              name="image"
              id="image"
              className="hidden"
              ref={ref}
              onChange={(e) => setSelectedImage(e.target.files[0])}
            />
            {error?.selectedImage && (
              <p className="text-xs text-red-500 mt-1">
                {error?.selectedImage}
              </p>
            )}
          </label>
          {selectedImage && (
            <button
              type="button"
              className="bg-[#743D7D] rounded-md text-white py-1 px-4 ml-4 md:ml-0"
              onClick={() => {
                setSelectedImage(null);
                ref.current.value = null;
              }}
            >
              Clear
            </button>
          )}
          <div className="w-full" id="grecaptcha-wrapper">
            <ReCAPTCHA
              sitekey={process.env.NEXT_PUBLIC_CAPTCHA_CLIENT_KEY}
              onChange={(token) => setVerifyToken(token)}
              ref={captchaRef}
            />
            {error?.captcha && (
              <p className="text-sm text-red-500">{error.captcha}</p>
            )}
          </div>
          <button
            type="submit"
            className={`w-full py-3 bg-[#743D7D] text-white rounded-md font-semibold text-base md:text-xl ${
              isLoading ? "opacity-70" : ""
            }`}
          >
            {isLoading ? "Loading..." : "Send"}
          </button>
        </form>
        {successMessage && (
          <div className="bg-green-500 py-3 px-4 text-white font-semibold rounded-md flex items-center justify-between gap-8 text-sm md:text-base mt-4">
            {successMessage}
            <button
              type="button"
              onClick={() => setSuccessMessage(null)}
              className="text-white"
            >
              <AiOutlineClose />
            </button>
          </div>
        )}
      </div>
    </>
  );
}
