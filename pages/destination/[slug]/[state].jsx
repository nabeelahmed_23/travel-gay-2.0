import { useMemo, useRef, useEffect, useState } from "react";
import Head from "next/head";
import Link from "next/link";
import { IconContext } from "react-icons";
import { BsArrowUpRight } from "react-icons/bs";
// import { AiOutlinePlus } from "react-icons/ai";
import { HiOutlineArrowSmLeft, HiOutlineArrowSmRight } from "react-icons/hi";
import InternalHeroSection from "../../../components/Shared/internalHero/InternalHeroSection";
import InternalNavigation from "../../../components/Shared/internalNavigation/InternalNavigation";
import InternalFadeSlider from "../../../components/Shared/internalFadeSlider/InternalFadeSlider";
import SliderWithText from "../../../components/Shared/internalSliderText/SliderWithText";
import NewsSlider from "../../../components/Shared/newsSlider/NewsSlider";
import HotelSlider from "../../../components/Shared/hotelSlider/HotelSlider";
import PartySlider from "../../../components/Shared/partySlider/PartySlider";
import FeaturedSlider from "../../../components/Shared/Featured/FeaturedSlider";
import { getSingleCity } from "../../../utils/services/ApiCalls";
import calls from "../../../utils/services/PromiseHandler/PromiseHandler";
import FrequentlyAsk from "../../../components/aside/FrequentlyAsk";
import AsideComunity from "../../../components/aside/AsideComunity";
import AsideSearchHotel from "../../../components/aside/AsideSearchHotel";
import TodayEventSlider from "../../../components/Shared/Events/TodayEventSlider";
import HotelFormWrapper from "../../../features/hotelBookingForm/HotelFormWrapper";

export default function Slug({ data }) {
  const [editorials, setEditorials] = useState([]);
  const [allEvents, setAllEvents] = useState([]);
  const [todayEvents, setTodayEvents] = useState([]);
  const [tomorrowEvents, setTomorrowEvents] = useState([]);
  const [trendingHotels, setTrendingHotels] = useState([]);
  const [venues, setVenues] = useState([]);

  // const [groupTrips, setGroupTrips] = useState([]);
  const hotelRef = useRef(null);
  const newsRef = useRef(null);
  const partyRef = useRef(null);
  const featuredRef = useRef(null);
  const [activeTab, setActiveTab] = useState(1);
  const featuredRefSideTwo = useRef(null);
  const iconClass = useMemo(() => ({ className: "text-primary" }), []);
  const sliderButton = useMemo(
    () => ({ className: "text-[#743D7D] h-6 w-6 group-hover:text-white " }),
    [],
  );
  const internalNavigation = [
    { id: 1, name: "All" },
    { id: 2, name: "Belfast" },
    { id: 3, name: "Birmingham" },
    { id: 4, name: "Dance Clubs" },
    { id: 5, name: "Blackpool" },
    { id: 6, name: "Hotels" },
    { id: 7, name: "Brighton" },
    { id: 8, name: "Cardiff" },
    { id: 9, name: "Execter" },
    { id: 10, name: "Oxford" },
    { id: 11, name: "Dance Clubs" },
    { id: 12, name: "Blackpool" },
    { id: 13, name: "Hotels" },
    { id: 14, name: "Brighton" },
    { id: 15, name: "Cardiff" },
    { id: 16, name: "Dance Clubs" },
    { id: 17, name: "Cruise Clubs" },
    { id: 18, name: "Tours" },
    { id: 19, name: "City Guide" },
    { id: 20, name: "Gay Map" },
    { id: 21, name: "Bars" },
    { id: 22, name: "Dance Bars" },
  ];
  const trips = [
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip1.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip2.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip3.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip4.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
  ];
  // const venues = [
  //   {
  //     image: { original_url: "/homepage/party1.jpg" },
  //     label: "Sweatbox Sauna",
  //     desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  //   },
  //   {
  //     image: { original_url: "/homepage/party2.jpg" },
  //     label: "Sweatbox Sauna",
  //     desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  //   },
  //   {
  //     image: { original_url: "/homepage/party1.jpg" },
  //     label: "Sweatbox Sauna",
  //     desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  //   },
  //   {
  //     image: { original_url: "/homepage/party2.jpg" },
  //     label: "Sweatbox Sauna",
  //     desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  //   },
  // ];

  const handleChangeTab = (count) => setActiveTab(count);

  function handleSlider(ref, dir) {
    ref.current.splide.go(dir);
  }

  const getAxtiveTabText = () => {
    if (activeTab === 1) {
      return (
        <>
          <p className="text-xs leading-5">
            Las Vegas pretty much invented the concert residency. Artists can
            pull in big bucks without the added cost of touring and an endless
            flood of tourists to entertain.
          </p>
          <p className="text-xs leading-5 mt-4">
            Celine Dion, Elton John, Janet Jackson, Bette Midler, Bruno Mars,
            Mariah Carey and many more have performed recent Vegas residencies
            at the various hotels and casinos, such as Caesar’s Palace.
          </p>
          <p className="text-xs leading-5 mt-4">
            Tickets can be pricey and the biggest artists walk away with more
            money than some of America’s highest-paid CEOs.
          </p>
        </>
      );
    }
    if (activeTab === 2) {
      return (
        <p className="text-xs leading-5">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias
          ratione, obcaecati repudiandae sed quae odio e ibus nemo deserunt!
          Atque molestiae parietur adipisicing elit. Molestias ratione,
          obcaecati repudiandae sed quae odio e ibus nemo deserunt! Atque
          molestiae pari
        </p>
      );
    }

    return (
      <p className="text-xs leading-5">
        sectetur adipisicing elit. Molestias ratione, obcaecati repudiandae sed
        quae odio e ibus nemo deserunt! Atque molestiae parietur adipisicing
        elitsectetur adipisicing elit. Molestias ratione, obcaecati repudiandae
        sed quae odio e ibus nemo deserunt! Atque molestiae parietur adipisicing
        elit
      </p>
    );
  };
  const apiCall = (url) => calls(url, null, null);
  const date = new Date();
  const fetchHotels = async () => {
    const {
      data: { data: _trendingHotels },
    } = await apiCall(`states/${data?.slug}/hotels`);
    setTrendingHotels(_trendingHotels ?? []);
  };
  // const fetchGroupTrips = async () => {
  //   const {
  //     data: { data: _groupTrips },
  //   } = await apiCall(`states/${data?.slug}/group-trips`);
  //   setGroupTrips(_groupTrips ?? []);
  // };
  const fetchEditorials = async () => {
    const {
      data: { data: _editorials },
    } = await apiCall(`states/${data?.slug}/editorials`);
    setEditorials(_editorials ?? []);
  };
  const fetchTodayEvents = async () => {
    const {
      data: { data: _todayEvents },
    } = await apiCall(
      `states/${data?.slug}/events?date_from=${date?.getFullYear()}-${
        date.getMonth() + 1
      }-${date?.getDate()}&date_to=${date?.getFullYear()}-${
        date.getMonth() + 1
      }-${date?.getDate()}`,
    );
    setTodayEvents(_todayEvents ?? []);
  };
  const fetchTomorrowEvents = async () => {
    const tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
    const getMonth = () => {
      if (tomorrow.getDate() === 1 && tomorrow.getMonth() + 1 === 1) {
        return 1;
      }

      if (tomorrow.getDate() === 1) {
        return date.getMonth() + 2;
      }
      return date.getMonth() + 1;
    };
    console.log(getMonth(), tomorrow.getDate());
    const {
      data: { data: _tomorrowEvents },
    } = await apiCall(
      `states/${
        data?.slug
      }/events?date_from=${date?.getFullYear()}-${getMonth()}-${tomorrow.getDate()}&date_to=${date?.getFullYear()}-${getMonth()}-${tomorrow.getDate()}`,
    );
    setTomorrowEvents(_tomorrowEvents ?? []);
  };
  const fetchAllEvents = async () => {
    const {
      data: { data: _allEvents },
    } = await apiCall(`states/${data?.slug}/events`);

    setAllEvents(_allEvents ?? []);
  };
  const fetchVenues = async () => {
    const {
      data: { data: _venues },
    } = await apiCall(`states/${data?.slug}/venues`);

    setVenues(_venues ?? []);
  };
  const fetchData = async () => {
    fetchHotels();
    // fetchGroupTrips();
    fetchEditorials();
    fetchTodayEvents();
    fetchTomorrowEvents();
    fetchAllEvents();
    fetchVenues();
  };
  useEffect(() => {
    fetchData();
    // const winPath = window.location.href.split("/")[4];
    // setCurrentPage(winPath);
    // console.log("winPath------>>", winPath);
  }, [data?.slug]);

  return (
    <div>
      <Head>
        <title>State | TravelGay</title>
      </Head>
      <InternalHeroSection
        heading={data?.name ?? " "}
        description={data?.sub_title ?? ""}
        heroImage={data?.image?.original_url}
      />
      <div className="theme-container xl:mt-5">
        <InternalNavigation internalNavigation={internalNavigation} />
        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_8px)_calc(30%_-_8px)] gap-4 xl:mt-8">
          <div>
            <InternalFadeSlider data={data?.media} city={data?.name} />
            <SliderWithText data={editorials} />
            <div className="px-4 md:px-0 mt-[34px] lg:mt-[70px] mb-[34px] lg:mb-[70px]">
              <div>
                {/* <div className="flex items-end mx-1 w-full pr-2 pb-1 h-[145px]">
                  <div className="flex-1 bg-secondary grid mx-4 md:mx-0 rounded shadow-[0_0_10px_rgba(0,0,0,0.15)]">
                    <div className="relative">
                      <img
                        src="/"
                        alt=""
                        className="h-[115px] lg:h-[123px] w-[146px] lg:w-[142px] shadow-md absolute bottom-0 rounded"
                      />
                      <img
                        src="/images/featuredslidelogo.jpg"
                        alt=""
                        className="absolute left-1/2 -translate-x-1/2 rounded-full"
                      />
                    </div>
                    <div>
                      <h4 className="text-sm md:text-base font-semibold">
                        Test
                      </h4>
                      <p className="text-xs mt-2">Test</p>
                    </div>
                  </div>
                </div> */}
                {todayEvents?.length > 0 && (
                  <>
                    <h3 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4 ">
                      What&apos;s On Today
                    </h3>
                    <TodayEventSlider
                      reff={featuredRefSideTwo}
                      data={todayEvents}
                    />
                  </>
                )}
                {/* {todayEvents?.length > 0 && (
                <TodayEventSlider
                    reff={featuredRefSideTwo}
                    data={todayEvents}
                  />
                )} */}

                {/* <FeaturedSlider
                  reff={featuredRefSideTwo}
                  pagination={false}
                  data={venues}
                  sidebar
                /> */}
              </div>
              {/* <div className="md:columns-2">
                <FeaturedSlider
                  reff={featuredRefSideTwo}
                  pagination={false}
                  data={venues}
                  sidebar
                />
                <FeaturedSlider
                  reff={featuredRefSideTwo}
                  data={venues}
                  pagination={false}
                  sidebar
                />
              </div> */}
            </div>
            <div className="px-4 md:px-0 mt-[34px] lg:mt-[70px] mb-[34px] lg:mb-[70px]">
              <div>
                {tomorrowEvents?.length > 0 && (
                  <>
                    <h3 className="text-primary text-[20px] md:text-[28px] font-[700] my-4">
                      What&apos;s On Tomorrow
                    </h3>
                    <TodayEventSlider
                      reff={featuredRefSideTwo}
                      data={tomorrowEvents}
                    />
                  </>
                )}
              </div>
            </div>

            <div className="my-6 sm:hidden">
              <HotelFormWrapper />
            </div>

            <div className="md:my-[72px] w-full font-[600] flex justify-center items-center text-primary my-5 md:h-[428px] bg-primary h-[304px]">
              ADS
            </div>
            <div className="bg-secondary rounded-md relative xl:bg-white p-4 xl:p-0 mt-32 xl:mt-[7.25rem]">
              <div className="relative -top-28 xl:-top-10 xl:absolute xl:w-[330px] 2xl:w-[340px] xl:h-[390px] 2xl:h-[376px] flex justify-center">
                <img
                  src="/images/cityimg.jpg"
                  alt=""
                  className="rounded-md md:mx-auto xl:mr-auto xl:ml-0 xl:w-full xl:h-full xl:object-cover shadow-[0_0_20px_rgba(0,0,0,0.25)]"
                />
              </div>
              {/* <div className="hidden xl:block w-[170px]" /> */}

              <div className="xl:bg-[#fcf4f6] -mt-24 xl:p-4 xl:mt-0 xl:rounded-md xl:pl-36 xl:pt-4 xl:pb-8 xl:w-[690px] xl:h-[376px] xl:ml-auto">
                <div className="xl:w-[405px] 2xl:w-[515px] xl:ml-auto">
                  <div className="flex justify-between items-center">
                    <h3 className="text-primary text-xl lg:text-2xl font-bold">
                      About {data?.name ?? ""}
                    </h3>
                    {/* <div className="hidden lg:block">
                      <button
                        onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                        type="button"
                        className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                      >
                        <IconContext.Provider value={sliderButton}>
                          <HiOutlineArrowSmLeft />
                        </IconContext.Provider>
                      </button>

                      <button
                        onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                        type="button"
                        className="border border-[#743d7d] rounded-md p-1 group transition bg-[#743d7d] ml-2"
                      >
                        <IconContext.Provider value={sliderButton}>
                          <HiOutlineArrowSmRight style={{ color: "#fff" }} />
                        </IconContext.Provider>
                      </button>
                    </div> */}
                  </div>
                  <div className="overflow-auto w-full flex justify-start items-center">
                    <button type="button" onClick={() => handleChangeTab(1)}>
                      {" "}
                      <p
                        className={`mt-4 xl:mt-3 pb-2 text-sm font-semibold w-max ${
                          activeTab === 1
                            ? "text-black border-b border-rose-500"
                            : ""
                        }`}
                      >
                        Say hello to the Las Vegas
                      </p>{" "}
                    </button>
                    <button
                      // className="hidden md:block xl:hidden 2xl:block"
                      type="button"
                      onClick={() => handleChangeTab(2)}
                    >
                      <p
                        className={`ml-5 mt-4 xl:mt-3 pb-2 text-sm font-semibold w-max ${
                          activeTab === 2
                            ? "text-black border-b border-rose-500"
                            : ""
                        }`}
                      >
                        Gay Nevada
                      </p>
                    </button>{" "}
                    <button type="button" onClick={() => handleChangeTab(3)}>
                      <p
                        className={`ml-5 mt-4 xl:mt-3 pb-2 text-sm font-semibold w-max ${
                          activeTab === 3
                            ? "text-black border-b border-rose-500"
                            : ""
                        }`}
                      >
                        LGBT rights
                      </p>
                    </button>
                  </div>
                  <div className="mt-3">{getAxtiveTabText()}</div>
                  <Link legacyBehavior href="#">
                    <a className="text-primary text-xs w-max lg:text-sm flex items-center gap-1 mt-6 lg:mt-10 font-medium underline">
                      Gay Las Vegas - Travel Gay Guide
                      <IconContext.Provider value={iconClass}>
                        <BsArrowUpRight />
                      </IconContext.Provider>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="pl-4 md:pl-0 mt-6 xl:mt-[4.35rem]">
              <div className="flex justify-between items-center mb-3">
                <h2 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4">
                  Trending Hotels in {data?.name ?? ""}
                </h2>
                {/* <div className="hidden lg:block">
                  <button
                    onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>

                  <button
                    onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>{" "} */}
                <div className="hidden lg:block">
                  <button
                    onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>

                  <button
                    onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>

              <HotelSlider
                noDataMessage="No Data Found"
                reff={hotelRef}
                data={trendingHotels}
                loc="notHome"
              />
            </div>
            <div className="pl-4 md:pl-0 mt-6 xl:mt-16">
              <div className="flex justify-between items-center mb-4">
                <h2 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4 ">
                  News & Features
                </h2>
                <div className="hidden lg:block">
                  <button
                    onClick={() => handleSlider(newsRef, `-${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>

                  <button
                    onClick={() => handleSlider(newsRef, `+${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>
              <NewsSlider
                slidesPerPage={4}
                editorials={editorials}
                reff={newsRef}
                sidebar
              />
            </div>
            <div className="hidden md:flex md:my-[72px] w-full font-[600] justify-center items-center text-primary my-5 md:h-[241px] bg-primary">
              ADS
            </div>
            <div className="pl-4 md:pl-0 mt-6 xl:mt-[5.5rem]">
              <div className="flex justify-between items-center pr-4 md:pr-0 lg:mb-4">
                <h2 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4 ">
                  {data?.name} Events
                </h2>
                <div className="flex items-center ">
                  <button type="button">
                    <span className="whitespace-nowrap flex items-center gap-3 font-semibold text-[#743D7D] mr-3">
                      Add Your Event{" "}
                      <svg
                        width="9"
                        height="14"
                        viewBox="0 0 9 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M1.48444 1.5L7.56636 7L1.48444 12.5"
                          stroke="#743D7D"
                          strokeWidth="1.5"
                        />
                      </svg>
                    </span>
                  </button>
                  <div className="border-l pl-4  hidden lg:block">
                    <button
                      onClick={() => handleSlider(partyRef, `-${"{i}"}`)}
                      type="button"
                      className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                    >
                      <IconContext.Provider value={sliderButton}>
                        <HiOutlineArrowSmLeft />
                      </IconContext.Provider>
                    </button>

                    <button
                      onClick={() => handleSlider(partyRef, `+${"{i}"}`)}
                      type="button"
                      className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                    >
                      <IconContext.Provider value={sliderButton}>
                        <HiOutlineArrowSmRight />
                      </IconContext.Provider>
                    </button>
                  </div>
                </div>
              </div>
              <PartySlider reff={partyRef} data={allEvents} loc="notHome" />
            </div>

            <div className="flex md:hidden md:my-[72px] w-full font-[600] justify-center items-center text-primary my-5 h-[304px] bg-primary">
              ADS
            </div>

            <div className="mt-6 xl:mt-[4.5rem]">
              <div className="flex justify-between items-center mb-4 px-4 md:px-0">
                <h2 className="text-primary text-[20px] md:text-[28px] font-[700] ">
                  Featured Venues
                </h2>
                <div className="flex items-center">
                  {/* <button
                    className="p-2 rounded-md bg-[#743D7D] lg:mr-4"
                    type="button"
                  >
                    <span className="invert">
                      <AiOutlinePlus />
                    </span>
                  </button> */}
                  <div className="border-l pl-4 hidden lg:block">
                    <button
                      onClick={() => handleSlider(featuredRef, `-${"{i}"}`)}
                      type="button"
                      className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                    >
                      <IconContext.Provider value={sliderButton}>
                        <HiOutlineArrowSmLeft />
                      </IconContext.Provider>
                    </button>

                    <button
                      onClick={() => handleSlider(featuredRef, `+${"{i}"}`)}
                      type="button"
                      className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                    >
                      <IconContext.Provider value={sliderButton}>
                        <HiOutlineArrowSmRight />
                      </IconContext.Provider>
                    </button>
                  </div>
                </div>
              </div>
              {venues?.length > 0 && (
                <FeaturedSlider
                  reff={featuredRef}
                  data={venues}
                  sliderRows={2}
                  sliderColumns={2}
                  sidebar={false}
                />
              )}
            </div>
            {/* <div className="mt-16">
              <div className="px-4 md:px-0">
                <div className="flex items-center justify-between gap-4">
                  <h2 className="text-xl text-primary xl:text-2xl font-bold xl:font-semibold">
                    Las Vegas Tours
                  </h2>
                  <Link legacyBehavior href="#">
                    <a className="underline text-xs lg:text-sm text-[#666] w-[50px] lg:w-auto inline-block">
                      View All
                    </a>
                  </Link>
                </div>
                <p className="text-xs mt-3">
                  Browse a selection of tours in Las Vegas from our partners
                  with free cancellation 24 hours before your tour starts.
                </p>
                <div className="flex items-center justify-between gap-2 bg-[#e1f0ff] px-3 py-4 rounded-md mt-4">
                  <span className="text-xs">
                    The <strong>best experiences</strong> in
                    <strong> Las Vegas</strong> for your trip
                  </span>
                  <Link legacyBehavior href="#">
                    <a className="">
                      <img src="/images/getyourguide.jpg" alt="" />
                    </a>
                  </Link>
                </div>
              </div>
              <SliderWithText club data={groupTrips} />
            </div> */}
          </div>
          <aside className="md:p-0 px-4 md:w-full">
            <FrequentlyAsk />
            <div className="mb-5">
              <h2 className="mb-[10px] md:text-[24px] font-[700] text-[18px] text-primary">
                Community Photos
              </h2>
              <div className="gap-2 flex mt-3">
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px]"
                    alt="Community"
                  />
                </div>
                {/* <div className="flex-1 max-h-[168px] max-w-[172px] bg-[#FCF4F6] rounded-[5px]" /> */}
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px]"
                    alt="Community"
                  />
                </div>
              </div>
            </div>
            <AsideComunity />
            <div className=" my-4 hidden max-w-[378px] h-0  md:h-[304px] bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
            <AsideSearchHotel heading=" Search Hotels by Area" data={trips} />
          </aside>
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  try {
    const res = await getSingleCity(`states/${context.query.state}`);
    return {
      props: {
        data: res?.data ?? null,
      },
    };
  } catch (error) {
    console.log(error);
    return {
      props: {
        error: error.message,
      },
    };
  }
}
