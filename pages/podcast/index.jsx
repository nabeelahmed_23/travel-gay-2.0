import { useState, useEffect } from "react";
import Head from "next/head";
import Skeleton from "react-loading-skeleton";
import parse from "html-react-parser";
import AsideComunity from "../../components/aside/AsideComunity";
import TGpodcast from "../../components/podcast/index";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import { getPodcasts } from "../../utils/services/ApiCalls";
import Pagination from "../../components/Shared/Pagination";

export default function Index() {
  const [podcasts, setPodcasts] = useState([]);
  const [meta, setMeta] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);

  async function getVideoPodcasts() {
    setIsLoading(true);
    try {
      const res = await getPodcasts("podcasts/type/video", { page });
      setPodcasts(res.data.data);
      setMeta(res.data.meta);
      setIsLoading(false);
    } catch (error) {
      console.log(error.response.data.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getVideoPodcasts();
  }, []);
  return (
    <div>
      <Head>
        <title>The Travel Gay Podcast</title>
      </Head>
      <InternalHeroSection
        heading="The Travel Gay Podcast"
        breadCrumbsText={["> ", "The Travel Gay Podcast"]}
        description="Travel Gay is the world’s largest and most viewed gay travel and lifestyle website, attracting millions of visitors per month.
         "
      />
      <div className="theme-container xl:mt-5 px-4">
        <div className="w-full grid grid-cols-1 lg:grid-cols-[65%_35%] xl:grid-cols-[70%_30%] gap-6 xl:gap-[60px] mt-5 xl:mt-[59px]">
          <div>
            <div className="hidden md:flex  w-full font-[600] justify-center items-center text-primary my-5 md:h-[241px] bg-primary">
              ADS
            </div>
            <TGpodcast />
            <div className="my-7 md:my-[50px]">
              <p className="my-3 text-[13px] text-[#666666]">
                TravelGay.com has launched its very own podcast. We’ll be
                hosting regular interviews with all sorts of interesting people,
                from internationally renowned celebrities to those who have
                paved the way for LGBT rights globally.
              </p>
              <p className="my-3 text-[13px] text-[#666666]">
                If you think you’d make a great guest on our podcast or are
                representing someone who might, then don’t hesitate to get in
                touch with us or call us on +44 203 933 8000 or +1 310 651 8008.
              </p>
              <p className="my-3 text-[13px] text-[#666666]">
                In Season 1 of the podcast, we’ll be speaking to guests
                including Stephen Fry, Dr Ranj Singh, James Longman, The Points
                Guy Brian Kelly and many more!
              </p>
              <p className="my-3 text-[13px] text-[#666666]">
                We’re available on all the major podcast platforms including
                Spotify, Google Podcasts and Apple Podcasts. You can also watch
                most of our interviews on the Travel Gay video channel too.
              </p>
            </div>

            <div className="mt-5">
              <h3 className="text-[28px] font-bold text-primary">
                More Travel Gay Celebrity Interviews
              </h3>
              <div className="mt-4 gap-4 md:grid grid-cols-2">
                {isLoading && (
                  <>
                    <div>
                      <Skeleton className="h-[315px] w-full" />
                      <Skeleton className="mt-4 h-12 w-full" />
                    </div>
                    <div>
                      <Skeleton className="h-[315px] w-full" />
                      <Skeleton className="mt-4 h-12 w-full" />
                    </div>
                  </>
                )}
                {podcasts?.length > 0 ? (
                  !isLoading &&
                  podcasts?.map((item) => (
                    <div key={item?.id}>
                      <div className="w-full relative my-2 video-iframe-wrapper">
                        {/* <img src="/images/vid-1.png" alt="" />
                      <span className="play-icon">
                        <img
                          width="50px"
                          className="absolute top-[50%] left-[50%]"
                          style={{
                            transform: "translate(-50%, -50%)",
                          }}
                          src="/images/playhover.webp"
                          alt=""
                        />
                      </span> */}
                        {parse(item?.iframe)}
                      </div>

                      <div className="mt-4 flex items-start justify-between gap-4">
                        <h3 className="text-primary text-[13px] font-semibold">
                          {item?.name}
                        </h3>
                        {/* <span className="text-[13px] text-[#666666]">
                        11kviews . 1 year ago
                      </span> */}
                      </div>
                    </div>
                  ))
                ) : (
                  <p>No podcasts found...</p>
                )}
              </div>
              <div className="mt-8">
                <Pagination
                  meta={meta}
                  handlePagination={(e) => {
                    setPage(e.target.id);
                    window.scrollTo(0, 0);
                  }}
                />
              </div>
            </div>
          </div>

          <div className="md:w-auto">
            <div className="hidden md:flex w-full font-[600] justify-center items-center text-primary mt-5 mb-8 md:h-[241px] bg-primary">
              ADS
            </div>
            <AsideComunity />
          </div>
        </div>
      </div>
    </div>
  );
}
