import Head from "next/head";
import { useEffect, useState } from "react";
import { useSession } from "next-auth/react";
import axios from "axios";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { getCountries } from "../../../utils/services/ApiCalls";
import {
  getDate,
  getArrayWithNumberOfElements,
  getBaseURL,
  getAPIUrl,
} from "../../../utils/Helper";
import BookingConfirmPopup from "../../../components/scb/BookingConfirmPopup";
import FailedBookingPopup from "../../../components/scb/FailedBookingPopup";
import { roomSelect } from "../../../utils/reducer/roomReducer";
import Loader from "../../../components/Shared/Loader";

const leadGuest = {
  guest_first_name: "",
  guest_last_name: "",
  type: "Adult",
  dob: "1970-01-01",
  title: "Mx",
};
export default function BookingConfirmation() {
  const [reservationData, setReservationData] = useState({
    email: "johndoe@gmail.com",
    first_name: "John",
    last_name: "Doe",
    mobile: "071156541891",
    country: "1",
    zip_code: "44000",
    guests: [{ ...leadGuest }],
    card_holder_name: "John Doe",
  });
  const [cardNo, setCardNo] = useState(null);
  const [date, setDate] = useState("12/23");
  const [cvv, setCvv] = useState(null);
  const [item, setItem] = useState([]);
  const [routerQuery, setRouterQuery] = useState({});
  const [prebookingRequest, setPrebookingRequest] = useState({});
  const [roomInfo, setRoomInfo] = useState({});
  const [hotel, setHotel] = useState(null);
  const [error, setError] = useState({});
  const [countries, setCountries] = useState([]);
  const [mounted, setMounted] = useState(false);
  const [bookingConfirmationPopup, setBookingConfirmationPopup] =
    useState(false);
  const [failedBooking, setFailedBooking] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [roomsArray, setRoomsArray] = useState([]);
  const [guests, setGuests] = useState([]);
  const [chargeDate, setChargeDate] = useState("");
  const [cancellationDate, setCancellationDate] = useState("");
  const [refundable, setRefundable] = useState(false);
  let card;
  const router = useRouter();
  const session = useSession();
  const dispatch = useDispatch();

  async function paymentTokenToBackend(reservationData, date, token) {
    const a = prebookingRequest?.room_bookings?.map((item) => {
      const x = { ...item };
      delete x.booking_token;
      return x;
    });

    const roomBookings = item?.map((i, idx) => ({
      meal_basis_code: i?.roomDetails?.meal_basis_code,
      booking_token: i?.roomDetails?.booking_token,
      prebooking_token: roomInfo?.rooms[idx]?.booking_token,
      // supplier: i?.roomDetails?.supplier,
      name: i?.roomDetails?.type_name,

      ...a[idx],
      // price: i?.roomDetails?.total,
      guests: getArrayWithNumberOfElements(
        guests[idx].adults + guests[idx].children.length,
      ).map((a, i) => {
        const g = reservationData?.guests[idx];
        if (i + 1 <= guests[idx].adults) {
          if (i === 0) {
            return {
              type: "Adult",
              title: "Mx",
              first_name: g.guest_first_name,
              last_name: g.guest_last_name,
              dob: "1970-01-01",
            };
          }
          return {
            type: "Adult",
            title: "Mx",
            first_name: g.guest_first_name + i,
            last_name: g.guest_last_name + i,
            dob: "1970-01-01",
          };
        }
        return {
          type: "Child",
          title: "Mx",
          first_name: g.guest_first_name + i,
          last_name: g.guest_last_name + i,
          dob: "1970-01-01",
        };
      }),
    }));

    axios.get(`${getBaseURL()}sanctum/csrf-cookie`).then(() => {
      axios({
        method: "POST",
        url: `${getAPIUrl()}bookings`,
        withCredentials: true,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        data: {
          hotel_id: hotel?.id,
          "check-in": routerQuery["check-in"],
          "check-out": routerQuery["check-out"],
          booking_token: roomInfo?.booking_token,
          currency: item?.[0]?.roomDetails?.currency,
          booking_reference: "UniqueReference35",
          errata: roomInfo?.errata,
          pending_booking_id:
            typeof window !== "undefined" &&
            localStorage.getItem("abandonCartId")
              ? Number(localStorage.getItem("abandonCartId"))
              : null,
          payment: {
            type: "card",
            cardlock_token: token,
            card_holder_name: reservationData.card_holder_name,
            card_expiry: date.replace("/", ""),
            card_cv2: cvv,
          },
          customer: {
            type: "Adult",
            title: "Mx",
            first_name: reservationData?.first_name,
            last_name: reservationData?.last_name,
            dob: "1970-01-31",
            address1: "foo address",
            city: "London",
            county: "London",
            email: reservationData.email,
            mobile: reservationData.mobile,
            phone: reservationData.mobile,
            booking_country_code: "GB",
            postcode: reservationData?.zip_code,
          },
          room_bookings: roomBookings,
        },
      })
        .then((res) => {
          if (typeof window !== "undefined") {
            localStorage.setItem(
              "bookingResult",
              JSON.stringify(res.data.data),
            );
          }
          router.push(`${window.location.pathname}/confirmation`);
          setCardNo("");
          setCvv("");
          setIsLoading(false);
        })
        .catch((err) => {
          setFailedBooking(true);
          setError(err?.response?.data?.message);
          setIsLoading(false);
        });
    });
  }

  const preSubmitHandler = () => {
    setError({});
    setIsLoading(true);
    if (
      !reservationData?.first_name ||
      !reservationData?.last_name ||
      !reservationData?.email ||
      !reservationData?.mobile ||
      !reservationData?.country ||
      !reservationData?.card_holder_name ||
      !cardNo ||
      !date ||
      !cvv
    ) {
      if (!reservationData?.first_name)
        setError((p) => ({
          ...p,
          first_name: ["First Name field can not be empty"],
        }));
      if (!reservationData?.last_name)
        setError((p) => ({
          ...p,
          last_name: ["Last Name field can not be empty"],
        }));
      if (!reservationData?.email)
        setError((p) => ({ ...p, email: ["Email field can not be empty"] }));
      if (!reservationData?.mobile)
        setError((p) => ({ ...p, mobile: ["Mobile field can not be empty"] }));
      if (!reservationData?.country)
        setError((p) => ({
          ...p,
          country: ["Country field can not be empty"],
        }));
      if (!reservationData?.card_holder_name)
        setError((p) => ({
          ...p,
          card_holder_name: ["Card Holder Name field can not be empty"],
        }));
      if (!cardNo)
        setError((p) => ({
          ...p,
          card: ["Card Number field can not be empty"],
        }));
      if (!date)
        setError((p) => ({
          ...p,
          card_expiry: ["Expiry date field can not be empty"],
        }));
      if (!cvv)
        setError((p) => ({
          ...p,
          cvv: ["CVV field can not be empty"],
        }));
      setIsLoading(false);
      return false;
    }
    const a = card.formElements.token.value;
    const myInterval = setInterval(() => {
      if (a !== card.formElements.token.value) {
        paymentTokenToBackend(
          reservationData,
          date,
          card.formElements.token.value,
        );
        clearInterval(myInterval);
      }
    }, 100);
    return true;
  };

  function errorHandler(response) {
    console.log(
      `Received a status ${response.status} response, with message ${response.message}`,
    );
  }

  function handleCardNo(e) {
    const { value } = e.target;
    const re = /^[0-9\b]+$/;
    if (
      (e.target.value === "" || re.test(e.target.value)) &&
      value.length <= 16
    ) {
      setCardNo(value);
    }
  }

  function handleDateChange(e) {
    const { value } = e.target;
    if (value.length <= 5) {
      setDate(
        value
          .replace(
            /^([1-9]\/|[2-9])$/g,
            "0$1/", // 3 > 03/
          )
          .replace(
            /^(0[1-9]|1[0-2])$/g,
            "$1/", // 11 > 11/
          )
          .replace(
            /^([0-1])([3-9])$/g,
            "0$1/$2", // 13 > 01/3
          )
          .replace(
            /^(0?[1-9]|1[0-2])([0-9]{2})$/g,
            "$1/$2", // 141 > 01/41
          )
          .replace(
            /^([0]+)\/|[0]+$/g,
            "0", // 0/ > 0 and 00 > 0
          )
          .replace(
            /[^\d/]|^[/]*$/g,
            "", // To allow only digits and `/`
          )
          .replace(
            /\/\//g,
            "/", // Prevent entering more than 1 `/`
          ),
      );
    }
  }

  function handleCVVChange(e) {
    const { value } = e.target;
    if (value.length <= 3) {
      setCvv(value);
    }
  }

  function transformToPay360() {
    // eslint-disable-next-line no-undef, no-new
    card = new CardLock({
      publishableId: process.env.CardLock_ID,
      formElementId: "form",
      panElementId: "card",
      cvvElementId: "cvv",
      tokenElementId: "token",
      // eslint-disable-next-line no-script-url
      merchantAction: "javascript:void(0)",
      preSubmitCallback: preSubmitHandler,
      errorHandler,
    });
  }

  async function getAllCountries() {
    try {
      const res = await getCountries("countries");
      setCountries(res.data.data);
    } catch (error) {
      console.log(error.message);
    }
  }

  useEffect(() => {
    transformToPay360();
  });

  useEffect(() => {
    if (typeof window !== "undefined") {
      setItem(JSON.parse(localStorage.getItem("room")));
      setRouterQuery(JSON.parse(localStorage.getItem("routerQuery")));
      setHotel(JSON.parse(localStorage.getItem("hotel")));
      setRoomInfo(JSON.parse(localStorage.getItem("roomInfo")));
      setPrebookingRequest(
        JSON.parse(localStorage.getItem("prebookingRequest")),
      );
      setGuests(JSON.parse(localStorage.getItem("guests")));
      setRoomsArray(JSON.parse(localStorage.getItem("roomsArray")));
    }
    getAllCountries();
    setMounted(true);
  }, []);

  useEffect(() => {
    const handleBrowseAway = (e) => {
      if (typeof window !== "undefined" && !e.includes("confirmation")) {
        console.log("Cleaning localstorage ... ");
        setTimeout(() => {
          if (typeof window !== "undefined") {
            localStorage.removeItem("hotel");
            localStorage.removeItem("room");
            localStorage.removeItem("routerQuery");
            localStorage.removeItem("roomInfo");
            localStorage.removeItem("prebookingRequest");
            localStorage.removeItem("guests");
            localStorage.removeItem("roomsArray");
            localStorage.removeItem("roomBooking");
            dispatch(roomSelect([]));
          }
        }, 2000);
        localStorage.removeItem("abandonCartId");
      }
    };

    router.events.on("routeChangeStart", handleBrowseAway);
    return () => {
      router.events.off("routeChangeStart", handleBrowseAway);
    };
  }, []);

  useEffect(() => {
    if (roomInfo) {
      const endsAt = roomInfo?.cancellations?.filter(
        (item) => item?.amount === 0,
      )?.[0]?.ends_at;
      if (endsAt) {
        const date = new Date(endsAt);
        date.setDate(date.getDate() - 2);
        setChargeDate(new Date(date));
        setCancellationDate(new Date(date.setDate(date.getDate() - 1)));
        setRefundable(true);
      } else {
        const checkIn = new Date(routerQuery["check-in"]);
        checkIn.setDate(checkIn.getDate() - 1);
        setChargeDate(checkIn);
        setRefundable(false);
      }
    }
  }, [roomInfo]);

  useEffect(() => {
    if (session.status === "authenticated") {
      setReservationData((p) => ({ ...p, email: session.data.user.email }));
    }
  }, [session]);

  function handleChange(e, i) {
    const { name, value } = e.target;
    const a = { ...reservationData };
    a.guests[i] = { ...a.guests[i], [name]: value };
    setReservationData(a);
  }

  function handleMainGuest(e) {
    const { name, value } = e.target;
    setReservationData((p) => ({ ...p, [name]: value }));
  }

  return (
    <>
      <div className="theme-container lg:p-0 px-4 mt-[40px] lg:flex">
        <Head>
          <title>Booking | TravelGay</title>
        </Head>
        <aside className="xl:max-w-[426px] lg:max-w-[40%]">
          <div className="px-4 md:px-[40px] rounded-2xl shadow-[0_0_24px_rgba(0,0,0,0.1)]">
            <div className="bg-[#743D7D] -mx-4 md:mx-[-40px] rounded-2xl rounded-b-none p-3 md:p-6 text-white shadow-[0_4px_15px_rgba(0,0,0,0.2)]">
              <h4 className="font-semibold">
                {mounted &&
                  typeof window !== "undefined" &&
                  JSON.parse(localStorage.getItem("hotel"))?.name}
              </h4>
              <small>
                {mounted &&
                  typeof window !== "undefined" &&
                  JSON.parse(localStorage.getItem("hotel"))?.address}
              </small>
            </div>
            <div className="-mx-4 md:mx-[-40px]">
              {item?.image && (
                <img
                  src="/scb/hotel1.jpg"
                  alt=""
                  className="max-h-[182px] w-full object-cover rounded-b-xl"
                />
              )}
            </div>
            <div className="flex gap-6 py-7 border-b">
              <div>
                <strong className="flex items-center gap-2">
                  <img src="/icons/calendar.svg" alt="" />
                  Check In
                </strong>
                <span className="inline-block mt-1 text-sm">
                  {getDate(new Date(routerQuery["check-in"]), "mmmm dd,yyyy")}
                </span>
              </div>
              <div>
                <strong className="flex items-center gap-2">
                  <img src="/icons/calendar.svg" alt="" />
                  Check Out
                </strong>
                <span className="inline-block mt-1 text-sm">
                  {getDate(new Date(routerQuery["check-out"]), "mmmm dd,yyyy")}
                </span>
              </div>
            </div>

            {/* <div className="flex items-start py-7  justify-between">
                <div className="pb-4">
                  <h4 className="font-semibold">
                    {item?.rooms?.[0]?.description}
                  </h4>
                  <h6 className="font-[500] text-[14px]">
                    {item?.length} Room x{" "}
                    {getDifferenceInDays(
                      new Date(routerQuery["check-in"]),
                      new Date(routerQuery["check-out"]),
                    )}{" "}
                    {getDifferenceInDays(
                      new Date(routerQuery["check-in"]),
                      new Date(routerQuery["check-out"]),
                    ) <= 1
                      ? "Night"
                      : "Nights"}
                  </h6>
                </div>
                <div>
                  <span className="text-lg text-primary font-semibold">
                    {item[0]?.roomDetails?.currency}
                    {roomInfo?.price}
                  </span>
                </div>
              </div> */}

            {roomsArray?.map((a, i) => (
              <div className="mt-4 border-b" key={`roomAmenities${i}`}>
                <p className="font-semibold text-black ">
                  {a?.noOfRooms} x {a?.roomDetails?.type_name}
                </p>
                <div className="flex items-center flex-wrap max-w-[440px]">
                  <div className="flex items-center gap-1 text-sm">
                    <img src="/scb/beds.svg" alt="" />2 King Beds
                  </div>
                  <div className="flex items-center gap-1 text-sm">
                    <img src="/scb/wifi.svg" alt="" />
                    Free Wifi
                  </div>
                  <div className="flex items-center gap-1 text-sm">
                    <img src="/scb/people.svg" alt="" />3 People
                  </div>
                  <div className="flex items-center gap-1 text-sm">
                    <img src="/scb/beds.svg" alt="" />2 King Beds
                  </div>
                  <div className="flex items-center gap-1 text-sm">
                    <img src="/scb/wifi.svg" alt="" />
                    Free Wifi
                  </div>
                </div>
                <div className="py-2 text-end font-semibold text-primary">
                  {a?.roomDetails?.currency}
                  {a?.roomDetails?.total}
                </div>
              </div>
            ))}

            {/* <div className="flex items-center justify-between my-5">
              <h4 className="font-semibold">Cancellation </h4>
              <button
                type="button"
                className="btn-primary text-white rounded-[50vw] h-[28px] min-w-[132px]"
              >
                Free
              </button>
            </div> */}

            {/* <div>
                <div className="flex items-center justify-between mt-5 mb-4">
                  <h4 className="font-semibold">Loyalty Points </h4>
                  <button
                    type="button"
                    className="bg-[#E39318] text-[13px] text-white rounded-[50vw] h-[28px] min-w-[132px]"
                  >
                    Get {item[0]?.roomDetails?.currency}75
                  </button>
                </div>
                <p className="text-[13px] text-black mb-[30px]">
                  <a className="text-[#D74874] underline">Login</a> in to Redeem
                  the points and get discount on the final price.{" "}
                </p>
              </div>
              <div className="border-t" /> */}

            <div className="mt-5">
              <div className="flex justify-between align-center">
                <span className="text-sm">Total Price</span>
                <span className="text-sm">
                  {item[0]?.roomDetails?.currency}
                  {roomInfo?.price}
                </span>
              </div>
              {/* <div className="flex justify-between align-center mt-2">
                <span className="text-sm">Taxes and Fees</span>
                <span className="text-sm">
                  {item[0]?.roomDetails?.currency}7.58
                </span>
              </div> */}
              {/* <div className="flex justify-between align-center mt-2">
                <span className="text-sm">Loyalty Points</span>
                <span className="text-sm">-€89.00</span>
              </div> */}
              <div className="flex justify-between align-center mt-2">
                <strong className="">Total Amount</strong>
                <span className=" font-semibold text-[#D74874]">
                  {item[0]?.roomDetails?.currency}
                  {roomInfo?.price?.toFixed(2)}
                </span>
              </div>
            </div>

            <p className="py-4 border-b font-semibold text-[#743D7D] text-sm">
              Your card will be charged on{" "}
              {getDate(new Date(chargeDate), "mmm, dd yyyy")}
            </p>
            {refundable && (
              <div className="py-5 border-b">
                <p className="py-1 w-full btn-primary text-white font-semibold text-center rounded-full">
                  Free Cancellation untill{" "}
                  {getDate(new Date(cancellationDate), "mmm, dd yyyy")}
                </p>
              </div>
            )}

            <div className="mt-6">
              <span className="text-[#D74874] font-semibold">
                Safe & Secured
              </span>
              <p className="pt-4 pb-8 text-[13px]">
                Your sensitive data is secure with us. We do not store any
                payment or credit card information. Keeping you safe is our top
                priority. We use SSL 256 Encryption methods for all site
                transactions
              </p>
            </div>
          </div>
        </aside>
        <form
          className="mt-6 lg:mt-0 lg:ml-4 w-full"
          id="form"
          method="post"
          action={`https://api${
            process.env.Pay360_Sandbox ? ".mite" : ""
          }.pay360.com/cardlock/fallback`}
        >
          <div className="py-[20px] px-4 lg:px-[40px] rounded-[8px] shadow-[0_0_12px_rgba(0,0,0,0.1)]">
            <h4 className="text-[#D74874] text-[24px] font-semibold mb-3">
              Your Information
            </h4>
            <div className="md:flex gap-6 justify-between">
              <div className="w-full md:mt-0 mt-3">
                <label htmlFor="first_name" className="block">
                  <span className="font-semibold mb-1 inline-block">
                    First Name
                  </span>
                  <input
                    type="text"
                    className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
                    name="first_name"
                    id="first_name"
                    placeholder="First Name"
                    onChange={handleMainGuest}
                    value={reservationData?.first_name ?? ""}
                  />
                  {error?.name?.length > 0 && (
                    <p className="text-xs text-red-500 m-1">
                      {error?.first_name[0]}
                    </p>
                  )}
                </label>
              </div>
              <div className="w-full md:mt-0 mt-3">
                <label htmlFor="last_name" className="block">
                  <span className="font-semibold mb-1 inline-block">
                    Last Name
                  </span>
                  <input
                    type="text"
                    className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
                    name="last_name"
                    id="last_name"
                    placeholder="Last Name"
                    onChange={handleMainGuest}
                    value={reservationData?.last_name ?? ""}
                  />
                  {error?.name?.length > 0 && (
                    <p className="text-xs text-red-500 m-1">
                      {error?.last_name[0]}
                    </p>
                  )}
                </label>
              </div>
            </div>
            <div className="md:flex gap-6 mt-6 justify-between">
              <div className="w-full md:mt-0 mt-3">
                <label htmlFor="email" className="block">
                  <span className="font-semibold mb-1 inline-block">
                    {" "}
                    Email
                  </span>
                  <input
                    type="email"
                    className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
                    name="email"
                    id="email"
                    placeholder="example@gmail.com"
                    onChange={handleMainGuest}
                    value={reservationData?.email ?? ""}
                  />
                  {error?.email?.length > 0 && (
                    <p className="text-xs text-red-500 m-1">
                      {error?.email[0]}
                    </p>
                  )}
                </label>
              </div>
              <div className="w-full md:mt-0 mt-3">
                <label htmlFor="mobile" className="block">
                  <span className="font-semibold mb-1 inline-block">
                    Mobile
                  </span>
                  <input
                    type="text"
                    className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
                    name="mobile"
                    id="mobile"
                    placeholder="00444400002222"
                    onChange={(e) => {
                      const re = /^[0-9\b]+$/;
                      if (e.target.value === "" || re.test(e.target.value)) {
                        handleMainGuest(e);
                      }
                    }}
                    value={reservationData?.mobile ?? ""}
                  />
                  {error?.mobile?.length > 0 && (
                    <p className="text-xs text-red-500 m-1">
                      {error?.mobile[0]}
                    </p>
                  )}
                </label>
              </div>
            </div>
            <div className="mt-6 md:flex gap-6 justify-between ">
              <div className="w-full md:w-6/12 md:mt-0 mt-3">
                <label htmlFor="country" className="block">
                  <span className="font-semibold mb-1 inline-block">
                    {" "}
                    Country
                  </span>
                  <select
                    className="block cursor-pointer border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
                    name="country"
                    id="country"
                    onChange={handleMainGuest}
                    value={reservationData?.country ?? "0"}
                  >
                    <option value="0" hidden>
                      {" "}
                      Select{" "}
                    </option>
                    {countries?.map((country, i) => (
                      <option value={country.id} key={`countries${i}`}>
                        {country.name}
                      </option>
                    ))}
                  </select>
                </label>
                {error?.country?.length > 0 && (
                  <p className="text-xs text-red-500 m-1">
                    {error?.country[0]}
                  </p>
                )}
              </div>
              <label
                htmlFor="zip_code"
                className="block md:w-[calc(50%_-_12px)] md:mt-0 mt-3"
              >
                <span className="font-semibold mb-1 inline-block">
                  Zip Code
                </span>
                <input
                  type="text"
                  className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
                  name="zip_code"
                  id="zip_code"
                  placeholder="AB12 CD3"
                  onChange={handleMainGuest}
                  value={reservationData?.zip_code ?? ""}
                />
              </label>
              {error?.zip_code?.length > 0 && (
                <p className="text-xs text-red-500 m-1">{error?.zip_code[0]}</p>
              )}
            </div>
            <div className="mt-4">
              <label htmlFor="guest" className="ml-3 cursor-pointer">
                <input
                  style={{
                    color: "#743D7D",
                    border: "1px solid #ddd",
                  }}
                  type="checkbox"
                  name="guest"
                  id="guest"
                  className="h-[22px] w-[22px]"
                />
                <span className="ml-2 text-sm"> I am also a guest</span>
              </label>
            </div>
          </div>
          <div className="mt-7 py-[20px] px-4 lg:px-[40px] rounded-[8px] shadow-[0_0_12px_rgba(0,0,0,0.1)]">
            <h4 className="text-[#D74874] text-[24px] font-semibold mb-3">
              Guests Information
            </h4>

            {getArrayWithNumberOfElements(item.length)?.map((item, i) => (
              <div className="mb-4" key={`guestsInformation${i}`}>
                <LeadGuestInfo
                  error={error}
                  handleChange={(e) => handleChange(e, i)}
                  reservationData={reservationData.guests[i]}
                  index={i + 1}
                />
              </div>
            ))}
          </div>
          <div className="mt-7 py-[20px] px-4 lg:px-[40px] rounded-[8px] shadow-[0_0_12px_rgba(0,0,0,0.1)]">
            <h4 className="text-[#D74874] text-[24px] font-semibold mb-3">
              Additional Information
            </h4>

            <ul className="ml-[19px]">
              <li className="text-[14px] list-disc">No Account Needed</li>
              <li className="text-[14px] list-disc">No Credit Card fees</li>
              <li className="text-[14px] list-disc">No Booking Fees</li>
              <li className="text-[14px] list-disc">Secure Payment</li>
              {roomInfo?.errata?.length > 0 &&
                roomInfo?.errata?.map((item, i) => (
                  <li className="text-[14px] list-disc" key={`errata${i}`}>
                    {item}
                  </li>
                ))}
              {/* <li className="text-[14px] list-disc">
                Keeping you safe is our top priority. We use SSL 256 Encryption
                methods for all site transactions
              </li>
              <li className="text-[14px] list-disc">
                Your sensitive data is secure with us. We do not store any
                payment or credit card information.
              </li>
              <li className="text-[14px] list-disc">
                Estimated total amount of taxes & fees for this booking:7.58
                Euro payable on arrival.
              </li>
              <li className="text-[14px] list-disc">LGTBIQ friendly.</li>
              <li className="text-[14px] list-disc">Only Adults 18.</li>
              <li className="text-[14px] list-disc">
                Check-in hour 15:00-05:30.
              </li>
              <li className="text-[14px] list-disc">
                Car park YES (With additional debit notes) 24.00 EUR Per
                unit/night.
              </li>
              <li className="text-[14px] list-disc">
                Due to the pandemic, accommodation and service providers may
                implement processes and policies to help protect the safety of
                all of us.
              </li>
              <li className="text-[14px] list-disc">
                This may result in the unavailability or changes in certain
                services and amenities that are normally available from them.
              </li>
              <li className="text-[14px] list-disc">
                More info click here{" "}
                <a className="text-[#3662D8] cursor-pointer underline" href="#">
                  https://cutt.ly/MT8BJcv (15/05/2020-31/12/2022)
                </a>
              </li> */}
            </ul>
            <div className="mt-4">
              <input
                style={{
                  color: "#743D7D",
                  border: "1px solid #ddd",
                }}
                type="checkbox"
                name="termsConditions"
                id="termsConditions"
                className="h-[22px] w-[22px]"
              />
              <label
                htmlFor="termsConditions"
                className="ml-3 cursor-pointer text-sm"
              >
                I have read the important information and agree to the{" "}
                <a className="text-[#3662D8] cursor-pointer underline" href="#">
                  Terms and Conditions{" "}
                </a>
              </label>
            </div>
          </div>
          <div className="mt-7 py-[20px] px-4 lg:px-[40px] rounded-[8px] shadow-[0_0_12px_rgba(0,0,0,0.1)]">
            <h4 className="text-[#D74874] text-[24px] font-semibold mb-3">
              Payment Details
            </h4>
            <div className="flex items-center">
              <label
                className="flex items-center cursor-pointer"
                htmlFor="allmethods"
              >
                <input
                  style={{
                    color: "#743D7D",
                    border: "1px solid #ddd",
                  }}
                  type="radio"
                  name="payDetails"
                  id="allmethods"
                  className="h-[27px] w-[27px] mr-3"
                />
                <img
                  src="/icons/pay-all-methods.svg"
                  alt=""
                  className="w-[calc(100%_-_40px)]"
                />
              </label>
            </div>
            <div className="mt-3">
              <label
                className="flex items-center cursor-pointer"
                htmlFor="paypal"
              >
                <input
                  style={{
                    color: "#743D7D",
                    border: "1px solid #ddd",
                  }}
                  type="radio"
                  name="payDetails"
                  id="paypal"
                  className="h-[27px] w-[27px] mr-3"
                />
                <img src="/icons/paypal.svg" alt="" />
              </label>
            </div>
            <div className="mt-3">
              <label
                className="flex items-center cursor-pointer"
                htmlFor="gPay"
              >
                <input
                  style={{
                    color: "#743D7D",
                    border: "1px solid #ddd",
                  }}
                  type="radio"
                  name="payDetails"
                  id="gPay"
                  className="h-[27px] w-[27px] mr-3"
                />
                <img src="/icons/gPay.svg" alt="" />
              </label>
            </div>
            <div className="md:flex gap-6 mt-4 justify-between">
              <div className="w-full">
                <label htmlFor="card_holder_name" className="block">
                  <span className="font-semibold inline-block my-2">
                    Cardholder Name{" "}
                  </span>
                  <input
                    type="text"
                    className="border-solid border-[#AAA9A9] text-sm py-2 rounded h-[50px] w-full"
                    name="card_holder_name"
                    id="card_holder_name"
                    placeholder="Full Name"
                    onChange={handleChange}
                    value={reservationData?.card_holder_name ?? ""}
                  />
                  {error?.card_holder_name?.length > 0 && (
                    <p className="text-xs text-red-500 m-1">
                      {error?.card_holder_name[0]}
                    </p>
                  )}
                </label>
              </div>
              <input type="hidden" name="token" id="token" />

              <div className="w-full md:mt-0 mt-3">
                <label htmlFor="card" className="block">
                  <span className="font-semibold inline-block my-2">
                    Card Number
                  </span>
                  <input
                    type="text"
                    className="inputNumber border-solid border-[#AAA9A9] text-sm py-2 rounded h-[50px] w-full"
                    name="card"
                    id="card"
                    placeholder="1234 1234 1234 1234"
                    value={cardNo ?? ""}
                    onChange={handleCardNo}
                  />
                </label>
                {error?.card && (
                  <p className="text-xs text-red-500 m-1">{error.card[0]}</p>
                )}
              </div>
            </div>
            <div className="md:flex gap-6 mt-4 justify-between">
              <div className="w-full">
                <label htmlFor="card_expiry" className="block">
                  <span className="font-semibold inline-block my-2">
                    Expiry Date
                  </span>
                  <input
                    type="text"
                    className="border-solid border-[#AAA9A9] text-sm py-2 rounded h-[50px] w-full"
                    name="card_expiry"
                    id="card_expiry"
                    placeholder="MM/YY"
                    onChange={handleDateChange}
                    value={date?.replace("^(0[1-9]|1[0-2])/?([0-9]{2})$") ?? ""}
                  />
                </label>
                {error?.card_expiry?.length > 0 && (
                  <p className="text-xs text-red-500 m-1">
                    {error?.card_expiry[0]}
                  </p>
                )}
              </div>
              <div className="w-full md:mt-0 mt-3">
                <label htmlFor="cvv" className="block">
                  <span className="font-semibold inline-block my-2">CVC</span>
                  <input
                    type="number"
                    maxLength={3}
                    max="999"
                    className="inputNumber border-solid border-[#AAA9A9] text-sm py-2 rounded h-[50px] w-full"
                    name="cvv"
                    id="cvv"
                    placeholder="431"
                    value={cvv ?? ""}
                    onChange={handleCVVChange}
                  />
                </label>
                {error?.cvv && (
                  <p className="text-xs text-red-500 m-1">{error.cvv[0]}</p>
                )}
              </div>
            </div>
            {error?.reason && (
              <p className="text-sm text-red-500 mt-4">{error?.reason}</p>
            )}
            <button
              type="submit"
              disabled={isLoading}
              className={`text-white  block w-full mt-4 bg-[#743D7D] h-[50px] rounded ${
                isLoading ? "opacity-70" : ""
              }`}
            >
              {isLoading ? "Loading..." : "Confirm Booking"}
            </button>
            <p className="mt-2 text-center text-[13px] text-black">
              Your card will be charged on {getDate(chargeDate, "mmm, dd yyyy")}
            </p>
          </div>
        </form>
      </div>
      {isLoading && <Loader text="Please wait while we confirm the booking." />}
      {bookingConfirmationPopup && (
        <BookingConfirmPopup
          setBookingConfirmationPopup={setBookingConfirmationPopup}
        />
      )}
      {failedBooking && (
        <FailedBookingPopup setFailedBooking={setFailedBooking} />
      )}
    </>
  );
}

export async function getServerSideProps(ctx) {
  const {
    hotel_booking_token: hotelBookingToken,
    booking_token: bookingToken,
    "check-in": checkIn,
    "check-out": checkOut,
    supplier_reference1: supplierReference1,
    supplier_reference2: supplierReference2,
    hotel_name: hotelName,
  } = ctx.query;

  if (
    !hotelBookingToken ||
    !bookingToken ||
    !checkIn ||
    !checkOut ||
    typeof supplierReference1 === "undefined" ||
    typeof supplierReference2 === "undefined" ||
    !hotelName
  ) {
    return {
      redirect: {
        permanent: false,
        destination: "/404",
      },
    };
  }
  return {
    props: {},
  };
}

export function LeadGuestInfo({ error, handleChange, reservationData, index }) {
  return (
    <>
      <span className="mb-3 inline-block">Lead guest for Room# {index}</span>
      <div className="md:flex gap-6 justify-between mt-4">
        <div className="w-full">
          <input
            type="text"
            className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
            name="guest_first_name"
            id="guest_first_name"
            placeholder="First Name"
            onChange={handleChange}
            value={reservationData?.guest_first_name ?? ""}
          />
          {error?.guest_first_name?.length > 0 && (
            <p className="text-xs text-red-500 m-1">
              {error?.guest_first_name[0]}
            </p>
          )}
        </div>
        <div className="w-full md:mt-0 mt-3">
          <input
            type="text"
            className="block border py-2 border-solid rounded border-[#AAA9A9] h-[50px] w-full"
            name="guest_last_name"
            id="guest_last_name"
            onChange={handleChange}
            placeholder="Last Name"
            value={reservationData?.guest_last_name ?? ""}
          />
          {error?.guest_last_name?.length > 0 && (
            <p className="text-xs text-red-500 m-1">
              {error?.guest_last_name[0]}
            </p>
          )}
        </div>
      </div>
    </>
  );
}
LeadGuestInfo.defaultProps = {
  error: {},
  reservationData: null,
};
LeadGuestInfo.propTypes = {
  error: PropTypes.shape(),
  reservationData: PropTypes.shape(),
  handleChange: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};
