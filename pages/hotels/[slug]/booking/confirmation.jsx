/* eslint-disable no-param-reassign */
import { useState, useEffect, useRef } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { roomSelect } from "../../../../utils/reducer/roomReducer";
import { monthArray, getDate, getBaseURL } from "../../../../utils/Helper";

export default function Confirmation() {
  const [bookingResult, setBookingResult] = useState();
  const [mounted, setMounted] = useState(false);
  const [chargeDate, setChargeDate] = useState("");
  const [hotel, setHotel] = useState({});
  const [freeRefundable, setFreeRefundable] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  const ref = useRef(null);
  useEffect(() => {
    const handleBrowseAway = (e) => {
      if (typeof window !== "undefined" && !e.includes("booking")) {
        console.log("Cleaning localstorage ... ");
        setTimeout(() => {
          if (typeof window !== "undefined") {
            localStorage.removeItem("hotel");
            localStorage.removeItem("room");
            localStorage.removeItem("routerQuery");
            localStorage.removeItem("roomInfo");
            localStorage.removeItem("prebookingRequest");
            localStorage.removeItem("guests");
            localStorage.removeItem("roomsArray");
            localStorage.removeItem("roomBooking");
            localStorage.removeItem("bookingReference");
            localStorage.removeItem("abandonCartId");
            localStorage.removeItem("bookingResult");
            dispatch(roomSelect([]));
          }
        }, 2000);
      } else {
        setTimeout(() => {
          if (typeof window !== "undefined") {
            localStorage.removeItem("abandonCartId");
            localStorage.removeItem("bookingResult");
            dispatch(roomSelect([]));
          }
        }, 2000);
      }
    };
    if (typeof window !== "undefined") {
      setBookingResult(JSON.parse(localStorage.getItem("bookingResult")));
      setHotel(JSON.parse(localStorage.getItem("hotel")));
    }

    setMounted(true);
    router.events.on("routeChangeStart", handleBrowseAway);
    return () => {
      router.events.off("routeChangeStart", handleBrowseAway);
    };
  }, []);

  useEffect(() => {
    if (bookingResult) {
      const a = bookingResult?.cancellations?.filter(
        (item) => item?.amount === 0,
      )?.[0];
      if (a) {
        const date = new Date(a?.tg_ends_at);
        setFreeRefundable(true);
        setChargeDate(new Date(date));
      } else {
        const date = bookingResult?.cancellations[0]?.tg_ends_at;
        setChargeDate(new Date(date));
        setFreeRefundable(false);
      }
    }
  }, [bookingResult]);

  return (
    <div className="theme-container lg:p-0 px-4" ref={ref} id="GFC">
      <Head>
        <title>Booking Confirmation | TravelGay</title>
      </Head>
      <div className="flex justify-center w-full max-w-[700px] mx-auto">
        <div className="mt-12 xl:mt-10">
          <div>
            <h1 className="text-primary text-xl md:text-[34px] font-semibold text-center">
              Your Booking Is Confirmed
            </h1>
            <div className="mt-6">
              <div className="flex items-center justify-center gap-3">
                <img src="/icons/tick-red-bg.svg" alt="" />
                <span className="text-sm md:text-base">
                  Check your inbox for your booking confirmation
                </span>
              </div>
              <div className="flex items-center justify-center gap-3 mt-4 ">
                <img src="/icons/tick-red-bg.svg" alt="" />
                <span className="text-sm md:text-base">
                  Please make a note of your Booking Reference
                </span>
              </div>
            </div>
          </div>
          <div className="mt-12 grid md:grid-cols-[213px_1fr] gap-4 rounded-lg border w-full">
            <div className="flex flex-row md:flex-col shadow-[0_0_10px_rgba(0,0,0,0.1)] border rounded-lg pt-2">
              <div className="flex flex-1 justify-evenly">
                <div className="flex flex-col justify-center">
                  <strong>Check In</strong>
                  <span className="text-center font-medium">
                    {bookingResult?.check_in &&
                      monthArray[bookingResult.check_in.split("-")[1] - 1]}
                  </span>
                  <span className="text-primary font-medium text-[32px] text-center -mt-2">
                    {bookingResult?.check_in &&
                      bookingResult?.check_in?.split("-")[2].split("T")[0]}
                  </span>
                  <span className="text-center font-medium inline-block -mt-2">
                    {bookingResult?.check_in &&
                      bookingResult?.check_in?.split("-")[0]}
                  </span>
                </div>
                <div className="flex flex-col justify-center">
                  <strong>Check Out</strong>
                  <span className="text-center font-medium">
                    {bookingResult?.check_out &&
                      monthArray[bookingResult.check_out.split("-")[1] - 1]}
                  </span>
                  <span className="text-primary font-medium text-[32px] text-center -mt-2">
                    {bookingResult?.check_out &&
                      bookingResult?.check_out?.split("-")[2].split("T")[0]}
                  </span>
                  <span className="text-center font-medium inline-block -mt-2">
                    {bookingResult?.check_out &&
                      bookingResult?.check_out?.split("-")[0]}
                  </span>
                </div>
              </div>
              <div className="mt-1">
                <img
                  src="/images/image 94.png"
                  alt=""
                  className="rounded-b-lg max-h-[115px] w-full object-cover -mt-3 md:mt-0"
                />
              </div>
            </div>

            <div className="p-4">
              <h2 className="font-semibold text-lg md:text-2xl">
                {hotel.name}
              </h2>
              <p className="text-black inline-block mt-2 text-sm md:text-base">
                {hotel.address}
              </p>
              <h3 className="mt-3 font-semibold text-lg md:text-2xl">
                Booking Reference
              </h3>
              <span className="text-primary font-medium inline-block mt-2">
                #{bookingResult?.meta?.supplier_booking_reference}
              </span>
              <div className="flex flex-col md:flex-row md:items-center gap-2 mt-3">
                {mounted && (
                  <a
                    href={`${getBaseURL()}booking-confirmation/${
                      bookingResult?.id
                    }`}
                    className="bg-[#743D7D] font-semibold text-white px-5 py-3 rounded"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Download Confirmation
                  </a>
                )}
                <button
                  type="button"
                  className="bg-[#743D7D] font-semibold text-white px-5 py-3 rounded"
                  onClick={() => {
                    if (typeof window !== "undefined") window.print();
                  }}
                >
                  Print Full Version{" "}
                </button>
              </div>
            </div>
          </div>
          {bookingResult?.reservations &&
            bookingResult?.reservations?.map((item, i) => (
              <ConfirmationRoomCard
                item={item}
                bookingResult={bookingResult}
                key={`ConfirmationRoomCard${i}`}
              />
            ))}
          <div className="confirmation-table-room-card-wrapper">
            <h3 className="text-xl font-semibold text-primary mb-2">
              Total Amount
            </h3>
            <div className="flex items-center justify-between mt-2 text-sm">
              <span>Total Price</span>
              <span>
                {" "}
                {bookingResult?.currency}
                {bookingResult?.price && bookingResult.price / 100}
              </span>
            </div>
            {/* <div className="flex items-center justify-between mt-2 text-sm">
                <span>Taxes and Fees</span>
                <span>{bookingResult?.currency}7.58</span>
              </div> */}
            <div className="flex items-center justify-between mt-2 text-sm">
              <span className="font-semibold">Total Amount</span>
              <span className="text-primary font-semibold">
                {bookingResult?.currency}
                {bookingResult?.price && bookingResult.price / 100}
              </span>
            </div>
            {freeRefundable && (
              <div className="bg-[#D74874] text-xs  md:text-base font-semibold text-white text-center px-4 py-1 rounded-full mt-5">
                Free Cancellation until{" "}
                {getDate(new Date(chargeDate), "mmm, dd yyyy")}
              </div>
            )}
            <p className="text-primary font-medium text-xs md:text-sm mt-2">
              Your card will be charged on{" "}
              {getDate(new Date(chargeDate), "mmm, dd yyyy")}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export function ConfirmationRoomCard({ item, bookingResult }) {
  const adults = item?.guests?.reduce((acc, curr) => {
    if (curr?.type === "Adult") {
      acc += 1;
    }
    return acc;
  }, 0);

  const children = item?.guests?.reduce((acc, curr) => {
    if (curr?.type === "Child") {
      acc += 1;
    }
    return acc;
  }, 0);

  return (
    <div className="confirmation-table-room-card-wrapper">
      <strong className="font-semibold"> {item?.name}</strong>
      <div className="mt-2">
        <div className="confirmation-table-grid">
          <strong>Name</strong>
          <span>
            {`${item?.guests[0]?.first_name} ${item?.guests[0]?.last_name}`}
          </span>
          <strong>Guests</strong>
          <span>
            {adults} Adults{children > 0 ? `, ${children} Children` : ""}
          </span>
        </div>
        <hr className="my-3" />
        <div className="confirmation-table-grid">
          <strong>Amenities</strong>
          <div className="flex items-center flex-wrap max-w-[440px]">
            <div className="flex items-center gap-1 text-sm">
              <img src="/scb/beds.svg" alt="" />2 King Beds
            </div>
            <div className="flex items-center gap-1 text-sm">
              <img src="/scb/wifi.svg" alt="" />
              Free Wifi
            </div>
            <div className="flex items-center gap-1 text-sm">
              <img src="/scb/people.svg" alt="" />
              {adults + children} People
            </div>
            <div className="flex items-center gap-1 text-sm">
              <img src="/scb/beds.svg" alt="" />2 King Beds
            </div>
            <div className="flex items-center gap-1 text-sm">
              <img src="/scb/wifi.svg" alt="" />
              Free Wifi
            </div>
          </div>
        </div>
        <hr className="my-3" />
        <div className="confirmation-table-grid">
          <span>Price</span>
          <span>
            {bookingResult?.currency} {item?.price && item.price / 100}
          </span>
          <strong className="">Total Amount</strong>
          <strong className="text-primary">
            {" "}
            {bookingResult?.currency} {item?.price && item.price / 100}
          </strong>
        </div>
      </div>
    </div>
  );
}

ConfirmationRoomCard.propTypes = {
  item: PropTypes.shape().isRequired,
};
