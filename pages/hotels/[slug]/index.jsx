import { useState, useEffect, useRef } from "react";
import Link from "next/link";
import { IconContext } from "react-icons";
import { ImLocation } from "react-icons/im";
import Head from "next/head";
import { BsChevronRight } from "react-icons/bs";
import PropTypes from "prop-types";
import queryString from "query-string";
import Rate from "rc-rate";
import parse from "html-react-parser";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import {
  getDate,
  getQueryFromArrayObject,
  getArrayFromURLParams,
} from "../../../utils/Helper";
import { hotelSearch } from "../../../utils/reducer/hotelReducer";
import TGApprovedLogoWhite from "../../../components/Shared/TGApprovedLogoWhite";
import { getHotelDetails } from "../../../utils/services/ApiCalls";
import Map from "../../../features/map/Map";
import GuestRoomPopup from "../../../components/scb/GuestRoomPopup";
import HotelRooms from "../../../components/scb/HotelRooms";
import CartComponent from "../../../components/scb/CartComponent";
import ReviewsCommentsWrapper from "../../../features/comments/ReviewsCommentsWrapper";
import { roomSelect } from "../../../utils/reducer/roomReducer";
import HotelImagePopup from "../../../components/Shared/HotelImagePopup";
import InternalNavigation from "../../../components/Shared/internalNavigation/InternalNavigation";

const locationIcon = { className: "fill-[#D74874]" };
const facilitiesIcon = { className: "w-5 h-5 fill-[#666]" };
const temp = {
  adults: 1,
  children: [],
};
export default function Index({ data }) {
  const [imagePopup, setImagePopup] = useState(false);
  const [formFields, setFormFields] = useState({
    check_in: "",
    check_out: "",
  });
  const [adultRoomDropdown, setAdultRoomDropdown] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [rooms, setRooms] = useState(data?.rooms ?? []);
  const [error, setError] = useState(null);
  const [mounted, setMounted] = useState(false);
  const [members, setMembers] = useState([]);
  const hotelForm = useSelector((state) => state.hotelSearch.value);
  const router = useRouter();
  const ref = useRef();
  const checkInRoom = useRef();
  const checkOutRoom = useRef();
  const dispatch = useDispatch();

  const links = [
    {
      label: "Photo Gallery",
      link: "photo-gallery",
    },
    {
      label: "Hotel Description",
      link: "hotel-description",
    },
    {
      label: "All Facilities",
      link: "facilities",
    },
    {
      label: "Location",
      link: "location",
    },
    {
      label: "Prices And Rooms",
      link: "prices",
    },
  ];
  const { "check-in": checkIn, "check-out": checkOut } = router.query;
  async function handleSubmit(e) {
    e.preventDefault();
    setIsLoading(true);

    if (formFields.adults < 0 || formFields.rooms < 0) {
      setIsLoading(false);
      return;
    }

    const { check_in: checkIn, check_out: checkOut, q } = formFields;
    const a = getQueryFromArrayObject(hotelForm?.rooms);
    const stringifiedQuery = queryString.stringify({
      "check-in": checkIn,
      "check-out": checkOut,
    });

    const apiUrl = `hotels/${q}${encodeURI("?") + stringifiedQuery + a}`;

    const query = queryString.stringify({
      "check-in": checkIn,
      "check-out": checkOut,
    });

    router.push(
      `/hotels/${router.query.slug}${encodeURI("?")}${query + a}`,
      undefined,
      { scroll: false, shallow: true },
    );

    setError("");

    try {
      const res = await getHotelDetails(apiUrl);
      setRooms(res?.data?.data?.rooms);
      setIsLoading(false);
    } catch (error) {
      setError(error?.response?.data?.message);
      setIsLoading(false);
    }
  }

  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      setAdultRoomDropdown(false);
    } else {
      setAdultRoomDropdown(true);
    }
  }

  useEffect(() => {
    setFormFields((p) => ({
      ...p,
      check_in: checkIn || getDate(new Date(), "yyyy-mm-dd"),
      check_out:
        checkOut ||
        getDate(new Date().setDate(new Date().getDate() + 1), "yyyy-mm-dd"),
      q: router?.query?.slug,
      rooms: [{ adults: router.query["rooms[0][adults]"] }],
    }));
    setMounted(true);
    const arr = window.location.search
      .slice(1)
      .split("&")
      .filter((item) => item.includes("rooms["));
    if (arr?.length <= 0) {
      setMembers([temp]);
    } else {
      setMembers(getArrayFromURLParams(arr));
    }
    if (typeof window !== "undefined") {
      const a = JSON.parse(localStorage.getItem("roomsArray"));
      if (a && a[0]?.roomDetails?.hotel_booking_token !== data?.booking_token) {
        localStorage.setItem("roomsArray", JSON.stringify([]));
      }
    }

    function routeChangeStart(e) {
      if (!e.includes("booking") && typeof window !== "undefined") {
        localStorage.removeItem("roomsArray");
        dispatch(roomSelect([]));
      }
    }
    router.events.on("routeChangeStart", routeChangeStart);
    window.addEventListener("click", handleClickOutside);
    return () => {
      window.removeEventListener("click", handleClickOutside);
      router.events.on("routeChangeStart", routeChangeStart);
    };
  }, []);

  useEffect(() => {
    if (members?.length > 0) {
      dispatch(
        hotelSearch({
          name: "rooms",
          value: [...members],
        }),
      );
    }
  }, [members]);

  return (
    <>
      <Head>
        <title>{data?.hotel?.name}</title>
        <meta name="title" content={data?.hotel?.meta_title} />
        <meta name="description" content={data?.hotel?.meta_description} />
        <style>
          {`
          #menu-hotel-form{
            display:block !important;
          }
          `}
        </style>
      </Head>
      <nav className="hidden xl:block bg-secondary py-8">
        <ul className="theme-container flex items-center gap-6">
          {mounted &&
            links.map((item) => (
              <li key={item.link}>
                <Link legacyBehavior href={`${router.asPath}#${item.link}`}>
                  <a>{item.label}</a>
                </Link>
              </li>
            ))}
        </ul>
      </nav>

      <section className="theme-container px-4 md:px-0 mt-5 md:mt-10">
        <InternalNavigation type="hotel" id={data?.hotel?.id} />
        <div className="flex flex-col md:flex-row md:items-start md:justify-between gap-6">
          <div>
            <div className="flex items-center gap-3">
              <h1 className="text-xl md:text-2xl lg:text-3xl font-semibold text-primary">
                {data?.hotel?.name}
              </h1>
              <Rate
                value={Number(data?.hotel?.star_rating)}
                disabled
                allowHalf
              />
            </div>
            <p className="flex items-center gap-2 mt-1">
              <IconContext.Provider value={locationIcon}>
                <ImLocation />
              </IconContext.Provider>{" "}
              <span className="text-sm md:text-base">
                {data?.hotel?.address}
              </span>
            </p>
          </div>
          {data?.tg_approved === 1 && (
            <img
              src="/tg-approved.jpg"
              alt=""
              className="hidden md:block md:max-w-[190px] "
            />
          )}
        </div>
      </section>
      <section
        className="theme-container px-4 md:px-0 mt-3 md:mt-6"
        id="photo-gallery"
      >
        <div className="hidden md:grid grid-cols-1 md:grid-cols-4 md:grid-row-2 gap-4 md:max-h-[500px]">
          {data?.hotel?.media?.slice(0, 5).map((item, i) => (
            <div
              key={item.uuid}
              className="first:md:col-span-2 first:md:row-span-2 first:md:max-h-[356px] first:lg:max-h-[416px] first:xl:max-h-[500px] [&:not(:nth-child(1))]:hidden md:[&:not(:nth-child(1))]:block md:[&:not(:nth-child(1))]:h-[170px] lg:[&:not(:nth-child(1))]:h-[200px] xl:[&:not(:nth-child(1))]:h-[242px]"
            >
              {data?.hotel?.media?.length > 5 && i === 4 ? (
                <div className="relative h-full">
                  <img
                    src={item?.original_url}
                    alt=""
                    className=" rounded-lg w-full h-full object-cover"
                  />
                  <button
                    type="button"
                    onClick={() => setImagePopup(true)}
                    className="absolute inset-0 bg-[rgba(0,0,0,0.4)] rounded-lg items-center justify-center font-semibold text-white text-4xl hidden md:flex"
                  >
                    {(data?.hotel?.media?.length ?? 0) - 5}+
                  </button>
                </div>
              ) : (
                <img
                  src={item?.original_url}
                  alt=""
                  className=" rounded-lg w-full h-full object-cover"
                />
              )}
            </div>
          ))}
        </div>
        <div className="md:hidden">
          {data?.media && (
            <Splide
              options={{
                type: "loop",
                perPage: 1,
                autoplay: true,
                rewind: true,
                arrows: false,
                gap: "0.5rem",
                pagination: false,
              }}
            >
              {data?.media?.map((item) => (
                <SplideSlide key={item.uuid} className="max-h-[216px]">
                  <img
                    src={item?.original_url}
                    alt={data?.name}
                    className="rounded-lg w-full h-full object-cover"
                  />
                </SplideSlide>
              ))}
            </Splide>
          )}
        </div>
      </section>
      <section
        className="mt-2 md:mt-10 theme-container px-4 md:px-0"
        id="hotel-description"
      >
        <div className="flex flex-col-reverse lg:flex-row lg:items-start lg:justify-between gap-2 lg:gap-6">
          <h2 className="text-xl md:text-2xl font-semibold text-primary">
            Why Travelgay Loves this Hotel?
          </h2>
          <div className="flex items-center flex-wrap gap-2 md:gap-4">
            {data?.hotel?.tg_approved === 1 && (
              <img
                src="/tg-approved.jpg"
                alt=""
                className="block md:hidden max-w-[100px]"
              />
            )}
            {data?.hotel?.awards?.length > 0 && (
              <div className="flex items-center gap-1">
                <div className="w-7 h-7 flex items-center justify-center">
                  <img src="/scb/award-badge.png" alt="" />
                </div>
                <div>
                  <h6 className="text-[10px]  text-slate-700">
                    2018 Most Booked
                  </h6>
                  <p className="text-[8px] ">Top 100</p>
                </div>
              </div>
            )}

            <div className="flex items-center gap-1 md:gap-3 w-[140px] md:w-auto">
              <div className="bg-[#D74874] text-white font-semibold flex items-center justify-center h-8 w-8 rounded-lg">
                {data?.hotel?.averageRating?.averageRating}
              </div>
              <div>
                <p className="text-[10px] md:text-xs">Audience Rating</p>
                <p className="text-[10px] md:text-xs">
                  From {data?.hotel?.averageRating?.totalNumberOfRating} votes
                </p>
              </div>
            </div>

            <div className="rounded-md bg-primary flex flex-col px-4 py-1 items-center add-hotel-rating">
              <span className="text-xs font-semibold text-[#743D7D]">
                Rate {data?.name}
              </span>
              <Rate allowHalf />
            </div>
          </div>
        </div>
      </section>
      <section className="theme-container px-4 md:px-0">
        <div className="mt-2 text-sm">
          {data?.hotel?.content && parse(data?.hotel?.content)}
        </div>
      </section>
      {data?.hotel?.facilities?.length > 0 && (
        <section className="theme-container px-4 md:px-0 mt-8" id="facilities">
          <h2 className="text-xl md:text-2xl font-semibold text-primary">
            Services And Facilities
          </h2>
          <div className="mt-3 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-4">
            {data?.hotel?.facilities?.map((item) => (
              <div className="flex items-start gap-2" key={item.id + item.name}>
                <div>
                  <IconContext.Provider value={facilitiesIcon}>
                    <BsChevronRight />
                  </IconContext.Provider>
                </div>
                <p className="text-sm">{item?.name}</p>
              </div>
            ))}
          </div>
        </section>
      )}

      <section className="theme-container px-4 md:px-0 mt-8" id="location">
        <div className="relative">
          {data?.hotel?.latitude && data?.hotel?.longitude && (
            <Map
              lat={
                data?.hotel?.latitude.includes("°")
                  ? data?.hotel?.latitude.replace("°", ".")
                  : data?.hotel?.latitude
              }
              lng={
                data?.hotel?.longitude.includes("°")
                  ? data?.hotel?.longitude.replace("°", ".")
                  : data?.hotel?.longitude
              }
              name={data?.hotel?.name}
              address={data?.hotel?.address}
              className="w-full h-[400px]"
            />
          )}
        </div>
      </section>
      <section className="theme-container px-4 md:px-0 mt-8" id="prices">
        <div className=" shadow-[0_0_13px_rgba(0,0,0,0.1)] bg-white rounded-b-md">
          <div className="bg-[#743D7D] flex items-center justify-center gap-4 py-5 px-6 rounded-md">
            <div>
              <TGApprovedLogoWhite className="w-[40px]" />
            </div>
            <p className="font-semibold text-white">
              Check Availability of Rooms at {data?.hotel?.name}
            </p>
          </div>
          <form onSubmit={handleSubmit}>
            <div className="p-2 lg:py-6 lg:px-8 grid grid-cols-2 md:grid-cols-[1fr_1fr_1fr_206px] gap-2">
              <button
                type="button"
                className="text-left"
                onClick={() => {
                  checkInRoom.current.showPicker();
                }}
              >
                <label
                  htmlFor="check_in_room"
                  className="bg-primary rounded-md p-2 text-sm lg:text-base font-medium flex flex-col h-full justify-between"
                >
                  Check In
                  <input
                    type="date"
                    name="check_in_room"
                    ref={checkInRoom}
                    id="check_in_room"
                    min={getDate(new Date(), "yyyy-mm-dd")}
                    className="p-0 m-0 bg-transparent text-xs lg:text-sm font-normal w-full"
                    value={formFields?.check_in}
                    onChange={(e) =>
                      setFormFields((p) => ({ ...p, check_in: e.target.value }))
                    }
                  />
                </label>
              </button>
              <button
                type="button"
                className="text-left"
                onClick={() => checkOutRoom.current.showPicker()}
              >
                <label
                  htmlFor="check_out_room"
                  className="bg-primary rounded-md p-2 text-sm lg:text-base  font-medium flex flex-col h-full justify-between"
                >
                  Check Out
                  <input
                    type="date"
                    name="check_out_room"
                    id="check_out_room"
                    ref={checkOutRoom}
                    min={formFields?.check_in}
                    className="p-0 m-0 bg-transparent text-xs lg:text-sm font-normal w-full"
                    value={formFields?.check_out}
                    onChange={(e) =>
                      setFormFields((p) => ({
                        ...p,
                        check_out: e.target.value,
                      }))
                    }
                  />
                </label>
              </button>
              <div
                className="bg-primary rounded-md p-2 text-sm lg:text-base relative font-medium flex flex-col justify-between h-full"
                ref={ref}
              >
                Rooms and Guests
                <div className="font-normal text-xs lg:text-sm">
                  {hotelForm?.rooms?.length} Rooms{" "}
                  {hotelForm?.rooms?.reduce((acc, curr) => {
                    // eslint-disable-next-line no-param-reassign
                    acc += curr.adults;
                    return acc;
                  }, 0)}{" "}
                  Adults
                </div>
                {adultRoomDropdown && (
                  <div className="absolute w-[calc(100vw_-_3rem)] -left-2 md:w-auto md:inset-x-[-2rem] lg:inset-x-0 bg-white px-6 py-4 shadow-[0_0_8px_rgba(0,0,0,0.15)] rounded-md top-[4.5rem]">
                    <GuestRoomPopup members={members} setMembers={setMembers} />
                  </div>
                )}
              </div>

              <button
                type="submit"
                className="bg-[#743D7D] rounded-md text-white lg:text-lg xl:text-xl"
              >
                Search
              </button>
            </div>
          </form>
        </div>
      </section>
      <section className="theme-container px-4 md:px-0 mt-8">
        <HotelRooms
          isLoading={isLoading}
          error={error}
          rooms={rooms}
          data={data}
        />
      </section>
      <section className="theme-container px-4 md:px-0 mt-8">
        <ReviewsCommentsWrapper
          id={{ hotel_id: data?.hotel?.id }}
          reviews={data?.hotel?.reviews ?? []}
          comments={data?.hotel?.comments ?? []}
        />
      </section>
      {imagePopup && (
        <HotelImagePopup
          media={data?.hotel?.media}
          setImagePopup={setImagePopup}
        />
      )}

      <CartComponent data={data} />
    </>
  );
}

Index.propTypes = {
  data: PropTypes.shape().isRequired,
};

export async function getServerSideProps(context) {
  const {
    "check-in": checkIn,
    "check-out": checkOut,
    slug,
    ...rest
  } = context.query;

  const a = Object.keys(rest)?.reduce((acc, curr) => {
    // eslint-disable-next-line no-param-reassign
    acc += `&${curr}=${rest[curr]}`;
    return acc;
  }, "");
  const arr = a.split("&").filter((item) => item.includes("rooms["));
  const b = getArrayFromURLParams(arr);
  // console.log(JSON.stringify(b));
  // const dataToSend = {
  //   q: slug,
  //   "check-in": checkIn,
  //   "check-out": checkOut,
  //   rooms: b,
  // };
  // console.log(dataToSend);
  const apiUrl = `hotels/${slug}?${queryString.stringify({
    "check-in": checkIn,
    "check-out": checkOut,
  })}${getQueryFromArrayObject(b)}`;

  console.log(apiUrl);
  try {
    const res = await getHotelDetails(apiUrl);
    return {
      props: {
        data: res.data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        error: error.message,
      },
    };
  }
}
