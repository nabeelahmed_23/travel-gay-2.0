import { useState, useEffect } from "react";
import Head from "next/head";
import parse from "html-react-parser";
import Skeleton from "react-loading-skeleton";
import AsideComunity from "../../components/aside/AsideComunity";
import AsideSearchHotel from "../../components/aside/AsideSearchHotel";
import FeaturedSlider from "../../components/Shared/Featured/FeaturedSlider";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import { getPodcasts } from "../../utils/services/ApiCalls";
import Pagination from "../../components/Shared/Pagination";

const slides = [
  {
    image: { original_url: "/homepage/party1.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
  {
    image: { original_url: "/homepage/party2.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
  {
    image: { original_url: "/homepage/party1.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
  {
    image: { original_url: "/homepage/party2.jpg" },
    label: "Sweatbox Sauna",
    name: "Las Vegas Strip: The High Roller at The LINQ Ticket",

    sub_title:
      "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
  },
];

// const hotelGroupTrips = [
//   {
//     label: "Sydney to Dubai",
//     image: { original_url: "/homepage/trip2.jpg" },
//     date: "3-Jul-2022",
//     price: "4500",
//   },
//   {
//     label: "Sydney to Dubai",
//     image: { original_url: "/homepage/trip1.jpg" },
//     date: "3-Jul-2022",
//     price: "4500",
//   },
//   {
//     label: "Sydney to Dubai",
//     image: { original_url: "/homepage/trip4.jpg" },
//     date: "3-Jul-2022",
//     price: "4500",
//   },
//   {
//     label: "Sydney to Dubai",
//     image: { original_url: "/homepage/trip3.jpg" },
//     date: "3-Jul-2022",
//     price: "4500",
//   },
// ];
const hotelsByArea = [
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip1.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip2.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip3.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
  {
    label: "Sydney to Dubai",
    image: { original_url: "/homepage/trip4.jpg" },
    date: "3-Jul-2022",
    price: "4500",
  },
];

export default function Index() {
  const [videos, setVideos] = useState([]);
  const [meta, setMeta] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);

  async function getVideoPodcasts() {
    setIsLoading(true);
    try {
      const res = await getPodcasts("podcasts/type/video", { page });
      setVideos(res.data.data);
      setMeta(res.data.meta);
      setIsLoading(false);
    } catch (error) {
      console.log(error.response.data.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getVideoPodcasts();
  }, []);
  return (
    <div>
      <Head>
        <title>The Travel Gay Videos</title>
      </Head>
      <InternalHeroSection
        heroImage="/images/videos-banner.png"
        heading="Travel Gay Videos"
        breadCrumbsText={["> ", "Travel Gay Videos"]}
        description="Celebrity Interviews, Travel Guides and LGBT Travel Tips"
      />
      <div className="theme-container xl:mt-5 px-4">
        <div className="grid grid-cols-1 lg:grid-cols-[70%_30%] gap-8 mt-5 xl:mt-[59px]">
          <div>
            <div className="mt-4">
              {" "}
              <p className="font-400 text-[#666666] text-[13px]">
                TravelGay.com is the world’s most visited LGBTQ+ travel website.
                With our extensive global reach and partners in all four corners
                of the world, we regularly host interviews with influential
                people.
              </p>
              <p className="font-400 text-[#666666] text-[13px]">
                You can also listen to the Travel Gay Podcast if you’d prefer to
                listen on the go, as well as browsing our extensive editorial
                and gay travel guides.
              </p>
              <h3 className="text-primary text-[24px] mt-[30px] mb-4 font-[600]">
                Browse the Travel Gay YouTube channel
              </h3>
              <iframe
                id="_ytid_71120"
                data-origwidth="800"
                style={{ display: "block", borderRadius: 16 }}
                className="md:h-[504.07px] h-[300px]"
                width="100%"
                data-origheight="450"
                src="https://www.youtube.com/embed/e1YGNbVFC9o?enablejsapi=1&amp;autoplay=0&amp;cc_load_policy=0&amp;cc_lang_pref=&amp;iv_load_policy=1&amp;loop=0&amp;modestbranding=1&amp;rel=0&amp;fs=1&amp;playsinline=0&amp;autohide=2&amp;hl=en_US&amp;theme=dark&amp;color=red&amp;controls=1&amp;"
                title='Richard E Grant on playing a drag queen: "It gave me sleepless nights"'
                data-epytgalleryid="epyt_gallery_54014"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen=""
                data-no-lazy="1"
                data-skipgform_ajax_framebjll=""
                data-gtm-yt-inspected-5="true"
              />
              <div className="mt-7">
                <h4 className="text-primary text-[20px] font-[600]">
                  Stephen Fry speaks to Travel Gay | Gay Travel
                </h4>
                <p className="font-[400] text-[13px]">
                  Stephen Fry talks to Darren Burn from TravelGay.com about his
                  travels around the world, including his trips to uncover
                  homophobia in Russia, Brazil and Uganda. He also speaks about
                  his love of Greece and ancient mythology....
                </p>
              </div>
            </div>

            <div className="grid sm:grid-cols-2 gap-4 mt-7">
              {isLoading && (
                <>
                  <div>
                    <Skeleton className="h-[315px] w-full" />
                    <Skeleton className="mt-4 h-12 w-full" />
                  </div>
                  <div>
                    <Skeleton className="h-[315px] w-full" />
                    <Skeleton className="mt-4 h-12 w-full" />
                  </div>
                </>
              )}
              {videos?.length > 0 ? (
                !isLoading &&
                videos?.map((item) => (
                  <div key={item?.id}>
                    <div className="w-full relative my-2 video-iframe-wrapper">
                      {/* <img src="/images/vid-1.png" alt="" />
                      <span className="play-icon">
                        <img
                          width="50px"
                          className="absolute top-[50%] left-[50%]"
                          style={{
                            transform: "translate(-50%, -50%)",
                          }}
                          src="/images/playhover.webp"
                          alt=""
                        />
                      </span> */}
                      {parse(item?.iframe)}
                    </div>

                    <div className="mt-4 flex items-start justify-between gap-4">
                      <h3 className="text-primary text-[13px] font-semibold">
                        {item?.name}
                      </h3>
                      {/* <span className="text-[13px] text-[#666666]">
                        11kviews . 1 year ago
                      </span> */}
                    </div>
                  </div>
                ))
              ) : (
                <p>No videos found...</p>
              )}
            </div>
            <div className="mt-8">
              <Pagination
                meta={meta}
                handlePagination={(e) => {
                  setPage(e.target.id);
                  window.scrollTo(0, 0);
                }}
              />
            </div>
          </div>

          <div className="md:w-full">
            <div className="mb-5">
              <h2 className="mb-[10px] md:text-[24px] font-semibold text-[18px] text-primary">
                Community Photos
              </h2>
              <div className="gap-2 flex mt-3">
                <div className="w-full">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] w-full"
                    alt="Community"
                  />
                </div>
                {/* <div className="flex-1 max-h-[168px] max-w-[172px] bg-[#FCF4F6] rounded-[5px]" /> */}
                <div className="w-full">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] w-full"
                    alt="Community"
                  />
                </div>
              </div>
              <AsideSearchHotel
                heading=" Search Hotels by Area"
                data={hotelsByArea}
              />
            </div>
            <AsideComunity />

            <div className="hidden md:block sidebar-featured-slider">
              <div className="pt-6">
                <h2 className="text-primary text-[24px] mt-[30px] font-semibold">
                  San Francisco Events
                </h2>
                {slides?.length > 0 && <FeaturedSlider data={slides} sidebar />}
              </div>
              <div className="pt-6">
                <h2 className="text-primary text-[24px] mt-[30px] font-semibold">
                  More Gay San Francisco
                </h2>
                {slides?.length > 0 && <FeaturedSlider data={slides} sidebar />}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
