import Head from "next/head";
import { useState, useRef } from "react";
import PropTypes from "prop-types";
import Rate from "rc-rate";
import { IconContext } from "react-icons";
import HTMLReactParser from "html-react-parser";
import { HiOutlineArrowSmLeft, HiOutlineArrowSmRight } from "react-icons/hi";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
import { getVenueDetail } from "../../utils/services/ApiCalls";
import HotelImagePopup from "../../components/Shared/HotelImagePopup";
import Map from "../../features/map/Map";
import ReviewsCommentsWrapper from "../../features/comments/ReviewsCommentsWrapper";
import HotelSlider from "../../components/Shared/hotelSlider/HotelSlider";
import AsideSearchHotel from "../../components/aside/AsideSearchHotel";
import AsideComunity from "../../components/aside/AsideComunity";
import FeaturedSlider from "../../components/Shared/Featured/FeaturedSlider";
import ImageGrid from "../../components/Shared/imageGrid/ImageGrid";
import InternalNavigation from "../../components/Shared/internalNavigation/InternalNavigation";

const sliderButton = {
  className: "text-[#743D7D] h-6 w-6 group-hover:text-white ",
};

export default function Slug({ data, error }) {
  const [imagePopup, setImagePopup] = useState(false);
  const hotelRef = useRef(null);
  const id = { venue_id: data?.id };
  const trips = [
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip1.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip2.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip3.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip4.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
  ];

  const eventVenues = [
    {
      image: { original_url: "/homepage/party1.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party2.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party1.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party2.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
  ];

  function handleSlider(ref, dir) {
    ref.current.splide.go(dir);
  }

  if (error) {
    return <p className="theme-container px-4 md:px-0 mt-12">{error}</p>;
  }

  return (
    <>
      <Head>
        <title>{data?.name}</title>
        <meta title={data?.meta_title} content={data?.meta_description} />
      </Head>
      <InternalHeroSection heading={data?.name} description={data?.sub_title} />
      <div className="theme-container px-4 md:px-0 mt-6 md:mt-12">
        <InternalNavigation type="venue" id={data?.id} />
        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_8px)_calc(30%_-_8px)] gap-12">
          <div>
            <div>
              <div className="flex items-start flex-wrap md:flex-nowrap gap-2 ">
                <h1 className="text-primary text-xl md:text-2xl font-semibold">
                  {data?.name}
                </h1>
                <Rate />
              </div>
              <div className="flex items-center gap-2 mt-1">
                <img src="/icons/location.svg" alt="Location Icon" />
                <p className="text-[13px]">{data?.address}</p>
              </div>
            </div>

            <ImageGrid media={data?.media} setImagePopup={setImagePopup} />

            <div className="mt-4 grid md:grid-cols-[1fr_166px] gap-4">
              <div>
                <div className="bg-primary text-[13px] text-primary font-medium px-3 py-1 w-max rounded">
                  Today: Karaoke- 8pm- 1am
                </div>
                <div className="bg-primary text-[13px] text-primary font-medium px-3 py-1 rounded mt-2 md:mt-4">
                  Tomorrow: UK Drag every Weds, doors open 7pm- show 9pm. Happy
                  Hour 5-9pm, 50% off drinks
                </div>

                <p className="text-[13px] underline mt-2 text-black font-medium">
                  View More Events
                </p>
                <div className="mt-4 group-trip-content text-[13px]">
                  {HTMLReactParser(data?.content)}
                </div>

                <div className="flex items-center flex-wrap gap-2 mt-2">
                  <span className="text-[13px] font-semibold">Features:</span>
                  {data?.tags?.map((item) => (
                    <div className="flex items-center gap-2 shadow-[0_0_4px_rgba(0,0,0,0.25)] rounded py-[2px] px-2 text-[13px]">
                      <img src="" alt="" className="h-3 w-3" />
                      {item?.name}
                    </div>
                  ))}
                </div>
              </div>

              <div>
                <div className="rounded-md flex flex-col bg-primary py-2 px-1 items-center add-hotel-rating w-max md:w-[166px] shadow h-max">
                  <span className="text-[13px] font-semibold text-[#743D7D] text-center">
                    Rate {data?.name}
                  </span>
                  <Rate allowHalf />
                </div>

                <div className="flex md:flex-col flex-wrap items-center md:items-start gap-1 mt-8">
                  {data?.tg_approved && (
                    <img src="/icons/tgApproved.svg" alt="" className="w-36" />
                  )}

                  <div className="flex items-start gap-1 mt-2">
                    <div className="w-7 h-7 bg-[#B94873] rounded flex items-center justify-center text-xs text-white">
                      {data?.averageRating?.averageRating}
                    </div>
                    <div>
                      <h6 className="text-xs font-semibold text-primary">
                        Exceptional
                      </h6>
                      <p className="text-[10px]">
                        Based on {data?.averageRating?.totalNumberOfRating}{" "}
                        votes
                      </p>
                    </div>
                  </div>

                  {data?.audience_awards?.length > 0 &&
                    data?.audience_awards?.map((item) => (
                      <div className="flex items-center gap-1 mt-2">
                        <div className="w-7 h-7 flex items-center justify-center">
                          <img src="/scb/award-badge.png" alt="" />
                        </div>
                        <div>
                          <h6 className="text-[10px] font-semibold text-black">
                            {item?.name}
                          </h6>
                          <p className="text-[10px]">Top 100</p>
                        </div>
                      </div>
                    ))}
                </div>
                {data?.venue_scheduler?.map(
                  (item, i) =>
                    item?.open_time && (
                      <p
                        className={`${
                          i === 0 ? "mt-4" : "mt-2"
                        } font-medium text-sm`}
                      >
                        <span className="text-primary">
                          {item?.name?.substring(0, 3)}:
                        </span>{" "}
                        {item?.open_time}-{item?.close_time}
                      </p>
                    ),
                )}

                <p className="text-[13px] underline mt-2 text-black font-medium">
                  View More Events
                </p>
              </div>
            </div>

            <div className="flex items-center gap-3 mt-2 md:mt-8">
              <button
                type="button"
                className="btn-primary rounded text-white py-[6px] px-3 text-[13px] font-medium"
              >
                Post a review
              </button>
              <button
                type="button"
                className="btn-primary rounded text-white py-[6px] px-3 text-[13px] font-medium flex items-center gap-1"
              >
                Contact
                <img src="/icons/fb-white.svg" alt="" />
                <img src="/icons/instawhite.svg" alt="" />
                <img src="/icons/globe.svg" alt="" />
                <img src="/icons/phone-white.svg" alt="" />
              </button>
            </div>
            {data?.latitude && data?.longitude && (
              <div className="relative mt-4 md:mt-10">
                <Map
                  lat={
                    data?.latitude.includes("°")
                      ? data?.latitude.replace("°", ".")
                      : data?.latitude
                  }
                  lng={
                    data?.longitude.includes("°")
                      ? data?.longitude.replace("°", ".")
                      : data?.longitude
                  }
                  name={data?.name}
                  address={data?.address}
                  className="w-full h-[400px]"
                />
              </div>
            )}

            <div className="md:my-[72px] w-full font-[600] flex justify-center items-center text-primary my-5 md:h-[200px] bg-primary h-[304px]">
              ADS
            </div>

            <div>
              <ReviewsCommentsWrapper
                id={id}
                reviews={data?.reviews ?? []}
                comments={data?.comments ?? []}
                reportItem={{ id: data?.id, name: data?.name }}
              />
            </div>

            <div className="mt-0 md:mt-4 xl:mt-[4.35rem]">
              <div className="flex justify-between items-center mb-3">
                <h2 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4">
                  Featured Hotels
                </h2>

                <div className="block">
                  <button
                    onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>

                  <button
                    onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                    type="button"
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>

              <HotelSlider
                noDataMessage="No Data Found"
                reff={hotelRef}
                data={[]}
                loc="notHome"
              />
            </div>
          </div>
          <aside className="xl:w-[calc(100%_-_64px)]">
            <div className="mb-5">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                Community Photos
              </h2>
              <div className="gap-2 flex mt-3">
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px] shadow"
                    alt="Community"
                  />
                </div>
                <div className="relative rounded-[5px] overflow-hidden">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px] shadow"
                    alt="Community"
                  />
                  <div className="absolute inset-0 bg-[rgba(0,0,0,0.49)] flex items-center justify-center">
                    <span className="text-white font-semibold text-xl">
                      View More
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <AsideSearchHotel
              heading=" Search Hotels by Area"
              data={trips}
              hotels
            />
            <AsideComunity />
            <div className="my-5">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                San Francisco Events
              </h2>
              <FeaturedSlider data={eventVenues} sidebar />
            </div>

            <div className="my-8 h-36 bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
            <div className="my-10">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                More Gay San Francisco
              </h2>
              <FeaturedSlider data={eventVenues} sidebar />
            </div>
          </aside>
        </div>
        {imagePopup && (
          <HotelImagePopup media={data?.media} setImagePopup={setImagePopup} />
        )}
      </div>
    </>
  );
}

export async function getServerSideProps(ctx) {
  const { slug } = ctx.query;
  try {
    const res = await getVenueDetail(`venues/${slug}`);
    return {
      props: {
        data: res.data.data,
      },
    };
  } catch (error) {
    return {
      props: {
        error: error.message,
      },
    };
  }
}

Slug.propTypes = {
  data: PropTypes.shape().isRequired,
};
