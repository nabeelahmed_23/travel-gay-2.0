import { useRef, useState, useEffect } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import Head from "next/head";
import { IconContext } from "react-icons";
import { HiOutlineArrowSmLeft, HiOutlineArrowSmRight } from "react-icons/hi";
import InternalHeroSection from "../../components/Shared/internalHero/InternalHeroSection";
// import InternalNavigation from "../../components/Shared/internalNavigation/InternalNavigation";
import HotelSlider from "../../components/Shared/hotelSlider/HotelSlider";
import AsideSearchHotel from "../../components/aside/AsideSearchHotel";
import AsideComunity from "../../components/aside/AsideComunity";
import FeaturedSlider from "../../components/Shared/Featured/FeaturedSlider";
import { getVenues } from "../../utils/services/ApiCalls";
import VenueListingWrapper from "../../components/venue/VenueListingWrapper";

const sliderButton = {
  className: "text-[#743D7D] h-6 w-6 group-hover:text-white ",
};
export default function Index() {
  const [venues, setVenues] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const hotelRef = useRef();

  function handleSlider(ref, dir) {
    ref.current.splide.go(dir);
  }

  const trips = [
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip1.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip2.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip3.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
    {
      label: "Sydney to Dubai",
      image: { original_url: "/homepage/trip4.jpg" },
      date: "3-Jul-2022",
      price: "4500",
    },
  ];

  const eventVenues = [
    {
      image: { original_url: "/homepage/party1.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party2.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party1.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
    {
      image: { original_url: "/homepage/party2.jpg" },
      label: "Sweatbox Sauna",
      desc: "Zero for heros - key workers free entry 10am - 2pm (20% off all other times)",
    },
  ];

  async function getAllVenues() {
    setIsLoading(true);
    try {
      const res = await getVenues("venues");
      setVenues(res.data.data);
      setIsLoading(false);
    } catch (error) {
      setError(error.message);
      setIsLoading(false);
    }
  }

  useEffect(() => {
    getAllVenues();
  }, []);

  return (
    <>
      <Head>
        <title>Venue Listing | TravelGay</title>
      </Head>
      <InternalHeroSection
        heading="Amsterdam Gay Dance Clubs & Parties"
        description="Amsterdam has a well-established gay clubbing scene, with regular gay nightclubs and dance parties to suite all tastes."
      />

      <div className="theme-container px-4 md:px-0 xl:mt-5">
        {/* <InternalNavigation /> */}

        <div className="grid grid-cols-1 lg:grid-cols-[calc(70%_-_8px)_calc(30%_-_8px)] gap-4 xl:mt-8">
          <div>
            <h2 className="text-primary font-semibold text-xl md:text-2xl ">
              Welcome To Gay Amsterdam Venue Listing
            </h2>
            <div className="text-[13px] group-trip-content mt-1">
              <p>
                Amsterdam boasts one of Europes most alive, vibrant and
                inclusive gay scenes. The capital of the Netherlands has been a
                bastion of LGBT+ culture for centuries and has seen its
                community survive and flourish in the face of political and
                social adversity. Today, Amsterdam has some of the best gay
                clubs, bars and hotels in the world, offering gay travelers a
                truly unique and exciting visit.
              </p>
              <p>
                Known as “the Venice of the north” because of its distinctive
                canals and narrow bridges, Amsterdam is one of the most charming
                and also exciting city’s in the world, offering something for
                everyone’s taste. The city is also rich in culture and art, with
                an expanse of galleries, museums and exhibition spaces
                showcasing the finest art from the Netherlands and beyond. Key
                attractions include Anne Frank’s house, the Van Gogh Museum and
                the Museum of Sex.
              </p>
            </div>

            <div className="mt-6">
              <div className="flex items-start justify-between gap-4 md:gap-8">
                <h2 className="text-primary font-semibold text-xl md:text-2xl">
                  We’ve sorted our Amsterdam gay Hotels roundup by area:
                </h2>
                <div className="flex items-center">
                  <button
                    type="button"
                    onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmLeft />
                    </IconContext.Provider>
                  </button>

                  <button
                    type="button"
                    onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                    className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                  >
                    <IconContext.Provider value={sliderButton}>
                      <HiOutlineArrowSmRight />
                    </IconContext.Provider>
                  </button>
                </div>
              </div>

              <div className="mt-4">
                <Splide
                  ref={hotelRef}
                  options={{
                    type: "loop",
                    perPage: 3,
                    autoplay: true,
                    rewind: true,
                    arrows: false,
                    gap: "0.5rem",
                    pagination: false,
                    breakpoints: {
                      768: {
                        perPage: 1,
                        padding: {
                          right: "4rem",
                        },
                      },
                    },
                  }}
                >
                  <SplideSlide>
                    <div>
                      <img
                        src="/images/venue-image.jpg"
                        alt=""
                        className="rounded-lg w-full h-full object-cover"
                      />
                      <div className="text-sm font-semibold mt-2">
                        3790 S Las Vegas Blvd, Las Vegas
                      </div>
                    </div>
                  </SplideSlide>
                  <SplideSlide>
                    <div>
                      <img
                        src="/images/venue-image.jpg"
                        alt=""
                        className="rounded-lg w-full h-full object-cover"
                      />
                      <div className="text-sm font-semibold mt-2">
                        3790 S Las Vegas Blvd, Las Vegas
                      </div>
                    </div>
                  </SplideSlide>
                  <SplideSlide>
                    <div>
                      <img
                        src="/images/venue-image.jpg"
                        alt=""
                        className="rounded-lg w-full h-full object-cover"
                      />
                      <div className="text-sm font-semibold mt-2">
                        3790 S Las Vegas Blvd, Las Vegas
                      </div>
                    </div>
                  </SplideSlide>
                </Splide>
              </div>
            </div>

            <div className="mt-6 md:mt-[72px] w-full font-[600] flex justify-center items-center text-primary md:h-[200px] bg-primary h-[304px]">
              ADS
            </div>

            <div className="mt-4 md:mt-8">
              <div className="flex items-center justify-between">
                <h4 className="text-primary font-semibold text-2xl">
                  Gay Amsterdam Venues
                </h4>
                <select name="" id="" className="bg-primary rounded-lg">
                  <option value="">Sort By</option>
                  <option value="asc">Ascending</option>
                  <option value="desc">Descending</option>
                </select>
              </div>
              <div className="mt-4 flex flex-col gap-y-8">
                <VenueListingWrapper
                  isLoading={isLoading}
                  error={error}
                  venues={venues}
                />
              </div>

              <div className="md:mt-[72px] w-full font-[600] flex justify-center items-center text-primary md:h-[200px] bg-primary h-[304px]">
                ADS
              </div>

              <div className="mt-0 md:mt-4 xl:mt-[4.35rem]">
                <div className="flex justify-between items-center mb-3">
                  <h2 className="text-primary text-[20px] md:text-[28px] font-[700] mt-4 md:mt-7 mb-4">
                    Featured Hotels
                  </h2>

                  <div className="block">
                    <button
                      onClick={() => handleSlider(hotelRef, `-${"{i}"}`)}
                      type="button"
                      className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d]"
                    >
                      <IconContext.Provider value={sliderButton}>
                        <HiOutlineArrowSmLeft />
                      </IconContext.Provider>
                    </button>

                    <button
                      onClick={() => handleSlider(hotelRef, `+${"{i}"}`)}
                      type="button"
                      className="border border-[#743d7d] rounded-md p-1 group transition hover:bg-[#743d7d] ml-2"
                    >
                      <IconContext.Provider value={sliderButton}>
                        <HiOutlineArrowSmRight />
                      </IconContext.Provider>
                    </button>
                  </div>
                </div>

                <HotelSlider
                  noDataMessage="No Data Found"
                  reff={hotelRef}
                  data={[]}
                  loc="notHome"
                />
              </div>
            </div>
          </div>
          {/* First Grid child ends here */}

          <aside className="xl:w-[calc(100%_-_64px)]">
            <div className="mb-5">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                Community Photos
              </h2>
              <div className="gap-2 flex mt-3">
                <div className="">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px] shadow"
                    alt="Community"
                  />
                </div>
                <div className="relative rounded-[5px] overflow-hidden">
                  <img
                    src="/images/community-photo.png"
                    className="rounded-[5px] max-h-[168px] shadow"
                    alt="Community"
                  />
                  <div className="absolute inset-0 bg-[rgba(0,0,0,0.49)] flex items-center justify-center">
                    <span className="text-white font-semibold text-xl">
                      View More
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <AsideSearchHotel
              heading=" Search Hotels by Area"
              data={trips}
              hotels
            />
            <AsideComunity />
            <div className="my-5">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                San Francisco Events
              </h2>
              <FeaturedSlider data={eventVenues} sidebar />
            </div>

            <div className="my-8 h-36 bg-primary font-[600] lg:flex justify-center items-center text-primary mx-auto">
              ADS
            </div>
            <div className="my-10">
              <h2 className="mb-[10px] md:text-lg font-bold text-md text-primary">
                More Gay San Francisco
              </h2>
              <FeaturedSlider data={eventVenues} sidebar />
            </div>
          </aside>
        </div>
      </div>
    </>
  );
}
