/* eslint-disable no-param-reassign */
import GoogleProvider from "next-auth/providers/google";
import FacebookProvider from "next-auth/providers/facebook";
import CredentialsProvider from "next-auth/providers/credentials";
import NextAuth from "next-auth/next";
import {
  login,
  registration,
  socialLogin,
  tokenSignin,
} from "../../../utils/services/ApiCalls";

export const authOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        email: { name: "email", type: "email" },
        password: { name: "password", type: "password" },
        password_confirmation: { name: "password_confirmation", type: "text" },
        token_id: { name: "token_id", type: "text" },
        origin: { name: "origin", type: "text" },
      },
      async authorize(credentials) {
        if (credentials.origin === "registration") {
          const {
            email,
            password,
            password_confirmation: confirmPassword,
          } = credentials;

          try {
            const res = await registration("register", {
              email,
              password,
              password_confirmation: confirmPassword,
            });

            return res.data.data;
          } catch (error) {
            throw new Error(JSON.stringify(error.response.data.errors));
          }
        }

        if (credentials.origin === "oneTap") {
          const { token_id: tokenId } = credentials;
          const res = await tokenSignin("tokensignin", {
            token_id: tokenId,
          });
          return res.data;
        }

        const { email, password } = credentials;
        try {
          const res = await login("login", { email, password });
          return res.data;
        } catch (error) {
          throw new Error(JSON.stringify(error.response.data.errors));
        }
      },
    }),
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_SECRET_ID,
      authorization: {
        //   params: {
        //     prompt: "consent",
        //     access_type: "offline",
        //     response_type: "code",
        //     state: "S8we7MP7CasVj_ZitOcI34H5OmBbcEBDSXHsZKYCw5c",
        //     code_challenge_method: "S256",
        //     code_challenge: "ChaGd1ThqhZl2hvNKNiXjoj1FTCDLKEZrP5ylGJGpwE",
        //     client_id: process.env.GOOGLE_CLIENT_ID,
        //   },
      },
    }),
    FacebookProvider({
      clientId: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_SECRET_ID,
    }),
  ],
  pages: {
    error: "/",
  },
  callbacks: {
    signIn: async (user) => {
      const { account, user: profile } = user;
      const data = {
        provider: account.provider,
        token: account.access_token,
      };
      if (account.provider !== "credentials") {
        try {
          const res = await socialLogin(data);
          account.user_id = res.data.id;
          account.token = res.data.token;
          return true;
        } catch (error) {
          throw new Error("Unexpected Error occured!");
        }
      } else {
        account.token = profile.token;
        account.user_id = profile.value;
        account.image = profile.image;
        return true;
      }
    },
    jwt: async ({ token, account, user }) => {
      if (account) {
        token.token = account.token;
        token.user_id = account.user_id;
        token.image = account.image;
      }
      if (user) {
        token.user_id = user.value;
        token.image = user.image;
      }
      return token;
    },
    session: async ({ token, session }) => {
      session.user.token = token.token;
      session.user.user_id = token.user_id;
      session.user.image = token.image;
      return session;
    },
  },
  secret: "abcdefghijklmnop",
};

export default NextAuth(authOptions);
