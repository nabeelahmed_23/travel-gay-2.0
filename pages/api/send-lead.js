import axios from "axios";

export default async function handler(req, res) {
  try {
    const response = await axios.post(
      "https://www.zohoapis.eu/crm/v2/Leads",
      { ...req.body.data },
      {
        headers: { ...req.body.headers },
      },
    );
    console.log(response.data);
    res.status(200).json(response.data);
  } catch (error) {
    res.status(400).json(error.data);
  }
}
