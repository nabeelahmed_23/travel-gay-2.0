import axios from "axios";

export default async function handler(req, res) {
  try {
    const response = await axios.post(
      "https://accounts.zoho.eu/oauth/v2/token?refresh_token=1000.ec30daf5fdfa60bc401d9aebda5e542c.04962ecf1d7bc944aedd85d1aeaa9a28&client_id=1000.CQ22BMXZFG3V1A5LDTQXAKSS72ES2I&client_secret=1e7bac2cbba79f19069ee83f44ffa068f26b2332c0&grant_type=refresh_token",
    );
    res.status(200).json(response.data);
  } catch (error) {
    res.status(400).json({ error });
  }
}
