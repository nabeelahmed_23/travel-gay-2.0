import axios from "axios";

export default async function handler(req, res) {
  try {
    const response = await axios({
      url: "https://www.google.com/recaptcha/api/siteverify",
      params: {
        ...req.body,
        secret: process.env.NEXT_PUBLIC_CAPTCHA_CLIENT_SECRET,
      },
      headers: { Accept: "application/json" },
    });
    console.log(response.data);
    res.status(200).json(response.data);
  } catch (error) {
    res.status(200).json(error.response.data);
  }
}
