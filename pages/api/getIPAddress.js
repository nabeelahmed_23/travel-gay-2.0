import axios from "axios";

export default async function getIPAddress(req, res) {
  try {
    const response = await axios.get("https://api.ipify.org?format=json");
    res.status(200).json({ ip: response.data.ip });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
}
