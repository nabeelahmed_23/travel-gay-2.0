import Link from "next/link";

export default function Index() {
  return (
    <div className="w-screen h-[70vh] flex items-center justify-center">
      <div>
        <h1 className="text-primary text-4xl font-bold">404 Page Not Found</h1>
        <h2 className="mt-4">
          <Link legacyBehavior href="/">
            <a className="underline text-xl">Go to homepage</a>
          </Link>
        </h2>
      </div>
    </div>
  );
}
